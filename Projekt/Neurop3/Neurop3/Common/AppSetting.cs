﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Neurop3.Exceptions;
using System.Globalization;

namespace Neurop3.Common
{
    /// <summary>
    /// Staticka trida pro ziskavani dat z web.configu
    /// </summary>
    public static class AppSetting
    {
        public static T GetSetting<T>(String name)
        {
            string value = ConfigurationManager.AppSettings[name];

            if(value == null)
            {
                throw new FailedToGetAppParamException(String.Format("Could not find setting '{0}' in web.config", name));
            }

            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }
    }
}