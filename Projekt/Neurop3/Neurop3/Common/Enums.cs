﻿using Neurop3.Infrastructure;

namespace Neurop3.Common
{
    /// <summary>
    /// Ciselni pro navratove hodnoty
    /// </summary>
    public enum RequestResult
    {
        Success = 1,
        Failure = 2,
        Error = 3,
        Exception = 4,
        NotImplemented = 5,
    }
    
    /// <summary>
    /// Ciselnik pro hodnoceni narocnosti procviceneho baliku pacientem 
    /// </summary>
    public enum PatientFeedbackDifficulty
    {
        //[Display(Name = "lehke", ResourceType = typeof(Resources.Global))]
        [LocalizedDescription("lehke", typeof(Resources.Global))]
        Lehke = 1,
        //[Display(Name = "stredne_obtizne", ResourceType = typeof(Resources.Global))]
        [LocalizedDescription("stredne_obtizne", typeof(Resources.Global))]
        StredneObtizne = 2,
        //[Display(Name = "obtizne", ResourceType = typeof(Resources.Global))]
        [LocalizedDescription("obtizne", typeof(Resources.Global))]
        Obtizne = 3,
        //[Display(Name = "tezke", ResourceType = typeof(Resources.Global))]
        [LocalizedDescription("tezke", typeof(Resources.Global))]
        Tezke = 4,
        //[Display(Name = "velmi_tezke", ResourceType = typeof(Resources.Global))]
        [LocalizedDescription("velmi_tezke", typeof(Resources.Global))]
        VelmiTezke = 5,
    }

    /// <summary>
    /// Ciselnik pro hodnoceni poctu uloh v ramci baliku 
    /// </summary>
    public enum PatientFeedbackExerciseAmount
    {
        [LocalizedDescription("malo", typeof(Resources.Global))]
        Malo = 1,
        [LocalizedDescription("primerene", typeof(Resources.Global))]
        Primerene = 2,
        [LocalizedDescription("hodne", typeof(Resources.Global))]
        Hodne = 3,
    }

    /// <summary>
    /// Ciselni pro volbu pohlavi
    /// </summary>
    public enum Gender
    {
        [LocalizedDescription("muz", typeof(Resources.Global))]
        Muz = 1,
        [LocalizedDescription("zena", typeof(Resources.Global))]
        Zena = 2,
        [LocalizedDescription("neuvedeno", typeof(Resources.Global))]
        Neuvedeno = 3
    }

    /// <summary>
    /// Ciselnik pro volbu socialniho statu uzivatele
    /// </summary>
    public enum SocialStatus
    {
        [LocalizedDescription("svobodny_svobodna", typeof(Resources.Global))]
        Svobodny_svobodna = 1,
        [LocalizedDescription("zenaty_vdana", typeof(Resources.Global))]
        Zenaty_vdana = 2,
        [LocalizedDescription("vdovec_vdova", typeof(Resources.Global))]
        Vdovec_vdova = 3,
        [LocalizedDescription("rozvedeny_rozvedena", typeof(Resources.Global))]
        rozvedeny_rozvedena = 4,
        [LocalizedDescription("v_partnerskem_svazku", typeof(Resources.Global))]
        VPartnerskemSvazku = 5,
        [LocalizedDescription("neuvedeno", typeof(Resources.Global))]
        Neuvedeno = 6
    }

    /// <summary>
    /// Ciselnik uzivatelskych roli v aplikaci.
    /// </summary>
    public enum RoleEnum
    {
        [LocalizedDescription("superadmin", typeof(Resources.Global))]
        Superadmin = 0,
        [LocalizedDescription("administrator", typeof(Resources.Global))]
        Admin = 1,
        [LocalizedDescription("super_terapeut", typeof(Resources.Global))]
        Super_therapist = 2,
        [LocalizedDescription("terapeut",  typeof(Resources.Global))]
        Therapist = 3,
        [LocalizedDescription("pacient", typeof(Resources.Global))]
        Patient = 4,
        [LocalizedDescription("neprirazeno", typeof(Resources.Global))]
        Unassigned = 5,
    }

    /// <summary>
    /// Ciselnik pro aktualni stav ulohy
    /// </summary>
    public enum ExerciseStatusEnum
    {
        [LocalizedDescription("novy", typeof(Resources.Global))]
        Novy = 1,
        [LocalizedDescription("probihajici", typeof(Resources.Global))]
        Probihajici = 2, 
        [LocalizedDescription("dokoncena", typeof(Resources.Global))]
        Dokoncena = 3, 
        [LocalizedDescription("zkontrolovana", typeof(Resources.Global))]
        Zkontrolovana = 4, 
        [LocalizedDescription("uzavrena", typeof(Resources.Global))]
        Uzavrena = 5,
    }

    /// <summary>
    /// Ciselnik pro podporovane jazykove lokalizace.
    /// </summary>
    public enum LocaleEnum
    {
        [LocalizedDescription("cestina", typeof(Resources.Global))]
        Cz = 1,
        [LocalizedDescription("anglictina", typeof(Resources.Global))]
        En = 2,
        [LocalizedDescription("nemcina", typeof(Resources.Global))]
        De = 3,
        [LocalizedDescription("slovenstina", typeof(Resources.Global))]
        Sk = 4
    }

    /// <summary>
    /// Ciselnik moznych vstupu do programu
    /// </summary>
    public enum DataTypeRecipeEnum
    {
        [LocalizedDescription("text", typeof(Resources.Global))]
        Text = 1,
        [LocalizedDescription("cislo", typeof(Resources.Global))]
        Cislo = 2,
        [LocalizedDescription("logicka_hodnota", typeof(Resources.Global))]
        LogickaHodnota = 3,
        //[LocalizedDescription("single_select", typeof(Resources.Global))]
        //SingleSelect = 4,
        //[LocalizedDescription("multiple_select", typeof(Resources.Global))]
        //MultipleSelect = 5,
        [LocalizedDescription("obrazek", typeof(Resources.Global))]
        Obrazek = 6,
        [LocalizedDescription("zvuk", typeof(Resources.Global))]
        Zvuk = 7
    }

    /// <summary>
    /// Ciselnik moznych vystupu z programu.
    /// </summary>
    public enum DataTypeResultEnum
    {
        [LocalizedDescription("text", typeof(Resources.Global))]
        Text = 1,
        [LocalizedDescription("cislo", typeof(Resources.Global))]
        Cislo = 2,
        [LocalizedDescription("logicka_hodnota", typeof(Resources.Global))]
        LogickaHodnota = 3,
        //[LocalizedDescription("single_select", typeof(Resources.Global))]
        //SingleSelect = 4,
        //[LocalizedDescription("multiple_select", typeof(Resources.Global))]
        //MultipleSelect,
    }

    /// <summary>
    /// Ciselnik pro typ zpravy, kterou mohou zanechat uzivatele aplikace vyvojarum.
    /// </summary>
    public enum CommentType
    {
        [LocalizedDescription("chyba", typeof(Resources.Global))]
        Chyba = 1,
        [LocalizedDescription("vylepseni", typeof(Resources.Global))]
        Zlepseni = 2
    }

    /// <summary>
    /// Ciselnik urceny pro filtry entit v aplikaci. Na zaklade techto hodnot dochazi k filtraci.
    /// </summary>
    public enum ActiveEnum
    {
        [LocalizedDescription("ano", typeof(Resources.Global))]
        False = 0,
        [LocalizedDescription("ne", typeof(Resources.Global))]
        True = 1,
        [LocalizedDescription("neuvedeno", typeof(Resources.Global))]
        Both = 2,
    }
}