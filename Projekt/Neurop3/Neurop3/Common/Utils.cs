﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Web;
using Neurop3.Models;
using Neurop3.Models.Exercises;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;
using Neurop3.Helpers;
using Neurop3.Models.Administration;

namespace Neurop3.Common
{
    /// <summary>
    /// Trida pro definovani pomocnych statickych metod, ktere mohou byt pouzity kdekoli v aplikaci
    /// </summary>
    public class Utils
    {

        //TODO SMAZAT -- nejspis
        //public static string GetCurrentLocalization()
        //{
        //    //Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
        //    //var native = Thread.CurrentThread.CurrentCulture.NativeName;
        //    //var display = Thread.CurrentThread.CurrentCulture.DisplayName;
        //    //var name = Thread.CurrentThread.CurrentCulture.Name;
        //    var locale = Thread.CurrentThread.CurrentCulture.Name;
        //    switch (locale)
        //    {
        //        case "cs":
        //            return "Cz";
        //        default:
        //            return "Cz";
        //    }
        //}

        /// <summary>
        /// Metoda pro odesilani mailu
        /// </summary>
        /// <param name="userIds">Identifikatory uzivatelu, kterym ma prijit e-mail</param>
        /// <param name="subject">Predmet e-mailu</param>
        /// <param name="body">Telo e-mailu</param>
        /// <returns>Pocet odeslanych e-mailu</returns>
        public static int SendMail(List<string> userIds, string subject, string body)
        {
            var mailSended = 0;
            try
            {
                //Odesilani e-mailu vsem uzivatelum
                foreach (var userId in userIds)
                {
                    mailSended += SendMail(userId, subject, body);
                }

                if (mailSended != userIds.Count)
                {
                    //pokud e-mail neprisel vsem
                    throw new Exception(string.Format("Bylo odeslano pouze {0} mailu z {1}", mailSended, userIds.Count));
                }
                else
                {
                    return mailSended;
                }
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return mailSended;
            }
        }

        /// <summary>
        /// Odeslani e-mailu 
        /// </summary>
        /// <param name="message">Struktura urcena pro odesilani mailu konkretnimu uzivateli</param>
        /// <returns>Pocet odeslanych e-mailu</returns>
        public static int SendMail(MailMessage message)
        {
            return SendMail(message.UserId, message.Subject, message.Body);
        }

        /// <summary>
        /// Odeslani mailu konkretnimu uzivateli
        /// </summary>
        /// <param name="userId">Identifikator uzivatele</param>
        /// <param name="subject">Predmet e-mailu</param>
        /// <param name="body">Telo e-mailu</param>
        /// <returns>Pocet odeslanych e-mailu</returns>
        public static int SendMail(string userId, string subject, string body)
        {
            try
            {
                //Definovani zakladniho vzhledu emailu

                subject = string.Format("Neurop - {0}", subject);
                //pridani mista kolem zakladni zpravy
                body = string.Format("<div style=\"margin-top: 10px;\">{0}</div>", body);
                //pridani labelu o automaticke zprave
                body = string.Format("{0}{1}", "<div style=\"border-radius: 10px; background-color: rgb(200, 200,200); padding: 10px;\">Automatická zpráva z Neurop, prosím neodpovídat!</div>", body);
                //ohraniceni kolem celeho mailu
                body = string.Format("<div style=\"padding: 10px;\">{0}</div>", body);
                //body = string.Format("<div style=\"border-radius: 5px; background-color: rgb(250, 250, 250); padding: 15px; margin: 15px;\">{0}</div>", body);

                var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                userManager.SendEmail(userId, subject, body);
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Ziskani pripojeni do databaze.
        /// </summary>
        /// <returns>Pripojeni do databaze</returns>
        private static SqlConnection GetConnection()
        {
            var scsb = new SqlConnectionStringBuilder(
                ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
            {
                Pooling = true,
                AsynchronousProcessing = true
            };
            var conn = new SqlConnection(scsb.ConnectionString);
            conn.Open();
            return conn;
        }

        /// <summary>
        /// Metoda pro spusteni sql dotazu nad databazi
        /// </summary>
        /// <param name="query"></param>
        /// <returns>Vysledek oparace</returns>
        public static bool RunSqlQuery(string query)
        {
            using (SqlConnection conn = GetConnection())
            {
                var myCommand = new SqlCommand(query, conn);
                var result = myCommand.ExecuteNonQuery();
                return result > 0;
            }
        }

    }


}