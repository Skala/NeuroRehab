﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Neurop3.Startup))]
namespace Neurop3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
