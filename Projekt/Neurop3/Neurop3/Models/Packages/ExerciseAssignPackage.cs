﻿using Neurop3.Models.Exercises;
using Neurop3.Models.Packages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Packages
{
    [Table(name: "ExerciseAssignPackage")]
    public class ExerciseAssignPackage
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int ExerciseAssignId { get; set; }
        public virtual ExerciseAssign ExerciseAssign { get; set; }

        [Required]
        public int PackageId { get; set; }
        public virtual Package Package { get; set; }

        [Required]
        public int Order { get; set; }
    }
}