﻿using Neurop3.Models.Administration;
using Neurop3.Models.Exercises;
using Neurop3.Models.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Packages
{
    [Table(name: "Package")]
    public class Package
    {

        public Package()
        {
            this.ExerciseAssignPackages = new HashSet<ExerciseAssignPackage>();
            this.PatientFeedbacks = new HashSet<PatientFeedback>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "nazev_baliku", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "status", ResourceType = typeof(Resources.Global))]
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.Date)]
        public DateTime CreateTimeUtc { get; set; }

        [Required]
        [Display(Name = "datum_posledni_upravy", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.Date)]
        public DateTime LastAccessTimeUtc { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public string TherapistId { get; set; }

        [Required]
        public string PatientId { get; set; }


        public virtual ICollection<ExerciseAssignPackage> ExerciseAssignPackages { get; set; }
        public virtual ICollection<PatientFeedback> PatientFeedbacks { get; set; }
    }
}