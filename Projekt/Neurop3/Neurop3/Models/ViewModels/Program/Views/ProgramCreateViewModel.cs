﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Program.Views
{
    public class ProgramCreateViewModel
    {

        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "nazev_programu_musi_byt_mensi_nez_100_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global), Description = "nazev")]
        public string Name { get; set; }

        [Required]
        [StringLength(1024, MinimumLength = 16, ErrorMessageResourceName = "popis_programu_musi_mit_16_az_1024_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "popis_programu", ResourceType = typeof(Resources.Global))]
        public string Description { get; set; }
    }
}