﻿using Neurop3.Models.Programs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Program
{
    public class ListViewModel
    {
        public ListViewModel()
        {
            this.ProgramVMs = new List<ProgramViewModel>();
        }

        public ProgramListFilter Filter { get; set; }

        public List<ProgramViewModel> ProgramVMs { get; set; }
    }
}