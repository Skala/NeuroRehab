﻿using Neurop3.Models.Programs;
using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Neurop3.Models.ViewModels.Program
{
    public class ProgramViewModel
    {

        public ProgramViewModel()
        {
            this.ExerciseViewModels = new List<ExerciseViewModel>();
            this.ProgramRecipeViewModels = new List<ProgramRecipeViewModel>();
            this.ProgramResultViewModels = new List<ProgramResultViewModel>();
        }


        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "nazev_programu_musi_mit_od_3_do_100_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 3)]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [StringLength(1024, ErrorMessageResourceName = "popis_programu_musi_mit_od_10_do_1024_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 10 )]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "autor", ResourceType = typeof(Resources.Global))]
        public string AuthorId { get; set; }
        public virtual ApplicationUser Author { get; set; }

        [Required]
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "je_verejny", ResourceType = typeof(Resources.Global))]
        public bool IsPublic { get; set; }


        public bool IsUsed { get; set; }

        public HttpPostedFileBase File { get; set; }

        public bool ScriptExists { get; set; }

               
        public List<ExerciseViewModel> ExerciseViewModels { get; set; }
        public List<ProgramRecipeViewModel> ProgramRecipeViewModels { get; set; }
        public List<ProgramResultViewModel> ProgramResultViewModels { get; set; }
    }
}