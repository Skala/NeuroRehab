﻿using Neurop3.Models.ViewModels.Translation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Program
{
    public class ProgramResultViewModel
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "typ_programu", ResourceType = typeof(Resources.Global))]
        public int ProgramId { get; set; }
        

        [Required]
        [Display(Name = "cislo_radky", ResourceType = typeof(Resources.Global))]
        public int RowNumber { get; set; }

        //[Required]
        //[Display(Name = "nazev_radky", ResourceType = typeof(Resources.Global))]
        //public int RowNameId { get; set; }

        //[Display(Name = "popis_radky", ResourceType = typeof(Resources.Global))]
        //public int RowDescriptionId { get; set; }

        [Required]
        [Display(Name = "typ_dat", ResourceType = typeof(Resources.Global))]
        public int DataTypeId { get; set; }

        [Required]
        [Display(Name = "nazev_radky", ResourceType = typeof(Resources.Global))]
        public TranslationViewModel TranslationNameViewModel { get; set; }

        [Display(Name = "popis_radky", ResourceType = typeof(Resources.Global))]
        public TranslationViewModel TranslationDescriptionViewModel { get; set; }
    }
}