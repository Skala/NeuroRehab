﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class TherapistDataViewModel
    {
        [Required]
        public string UserId { get; set; }
        //public UserViewModel User { get; set; }

        [Required]
        [Display(Name = "datum_narozeni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "pohlavi", ResourceType = typeof(Resources.Global))]
        public int Gender { get; set; }

        [Required]
        [Display(Name = "vzdelani", ResourceType = typeof(Resources.Global))]
        public string Education { get; set; }
    }
}