﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class PatientDataViewModel
    {


        public PatientDataViewModel()
        {
            this.PatientDiagnoses = new List<PatientDiagnosisViewModel>();
            this.MedicalReports = new List<TherapistReportViewModel>();
        }

        public string UserId { get; set; }

        //public UserViewModel User { get; set; }

        [Required]
        [Display(Name = "datum_narozeni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "pohlavi", ResourceType = typeof(Resources.Global))]
        public int Gender { get; set; }

        [Required]
        [Display(Name = "zastupce", ResourceType = typeof(Resources.Global))]
        public string Representative { get; set; }

        [Required]
        [Display(Name = "socialni_status", ResourceType = typeof(Resources.Global))]
        public int SocialStatus { get; set; }

        [Required]
        [Display(Name = "profese", ResourceType = typeof(Resources.Global))]
        public string Profession { get; set; }

        [Display(Name = "diagnozy", ResourceType = typeof(Resources.Global))]
        public List<PatientDiagnosisViewModel> PatientDiagnoses { get; set; }

        [Display(Name = "lekarske_zpravy", ResourceType = typeof(Resources.Global))]
        public List<TherapistReportViewModel> MedicalReports { get; set; }


    }
}