﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class AdministratorDataViewModel
    {
        public string UserId { get; set; }
        public virtual UserViewModel User { get; set; }
    }
}