﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class PatientDiagnosisViewModel
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128, ErrorMessageResourceName = "atribut_musi_mit_od_0_do_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 5)]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [StringLength(1024, ErrorMessageResourceName = "atribut_musi_mit_od_0_do_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 5)]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "datum", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime TimeUtc { get; set; }

        [Required]
        public string PatientDataId { get; set; }
        //public PatientDataViewModel PatientData { get; set; }
    }
}