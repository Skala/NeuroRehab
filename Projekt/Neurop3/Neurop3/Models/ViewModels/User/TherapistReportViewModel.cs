﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class TherapistReportViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        [StringLength(128, ErrorMessageResourceName = "atribut_musi_mit_od_0_do_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 6)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]
        [StringLength(maximumLength: 2048, ErrorMessageResourceName = "atribut_musi_mit_od_0_do_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 10)]
        public string Report { get; set; }

        public string ReportShort
        {
            get
            {
                return this.Report.Length > 18 ? string.Concat(this.Report.Substring(0, 18), "...") : this.Report;
            }
        }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime TimeUtc { get; set; }

        [Required]
        [Display(Name = "zobrazeno", ResourceType = typeof(Resources.Global))]
        public bool Shown { get; set; }

        [Required]
        public string TherapistId { get; set; }

        public UserViewModel Therapist { get; set; }

        [Required]
        public string PatientDataId { get; set; }

        public UserViewModel Patient { get; set; }
    }
}