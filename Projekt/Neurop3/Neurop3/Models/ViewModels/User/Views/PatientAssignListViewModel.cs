﻿using Neurop3.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class PatientAssignListViewModel
    {
        public PatientAssignListViewModel()
        {
            this.PatientAssignViewModels = new List<PatientAssignViewModel>();
        }

        public PatientAssignListFilter Filter { get; set; }
        public List<PatientAssignViewModel> PatientAssignViewModels { get; set; }

    }
}