﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class ManageViewModel
    {
        public UserViewModel UserVM { get; set; }
        public bool IsEditable { get; set; }
    }
}