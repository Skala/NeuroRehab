﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class ListViewModel
    {
        public ListViewModel()
        {
            this.UserVMs = new List<UserViewModel>();
        }

        public List<UserViewModel> UserVMs { get; set; }
    }
}