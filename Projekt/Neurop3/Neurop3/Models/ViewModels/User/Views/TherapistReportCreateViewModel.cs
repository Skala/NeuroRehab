﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class TherapistReportCreateViewModel
    {
        [Required]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        [StringLength(128, ErrorMessageResourceName = "atribut_musi_mit_od_0_do_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 6)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]
        [StringLength(maximumLength: 2048, ErrorMessageResourceName = "atribut_musi_mit_od_0_do_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 10)]
        public string Report { get; set; }

        [Required]
        [Display(Name = "zobrazeno", ResourceType = typeof(Resources.Global))]
        public bool Shown { get; set; }

        [Required]
        public string TherapistId { get; set; }

        [Required]
        public string PatientDataId { get; set; }
    }
}