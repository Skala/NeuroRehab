﻿using Neurop3.Models.Exercises;
using Neurop3.Models.ViewModels.Exercise;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class PatientDetailViewModel
    {
        public PatientDetailViewModel()
        {
            this.ExerciseAssignVMs = new List<ExerciseAssignViewModel>();
        }

        public UserViewModel Patient { get; set; }

        public PatientDataViewModel PatientDataVM { get; set; }

        public List<ExerciseAssignViewModel> ExerciseAssignVMs { get; set; }

        public List<TherapistReportViewModel> TherapistReportVMs { get; set; }
    }
}