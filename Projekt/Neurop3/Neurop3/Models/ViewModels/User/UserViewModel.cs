﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace Neurop3.Models.ViewModels.User
{
    public class UserViewModel
    {

        public string Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "jmeno", ResourceType = typeof(Resources.Global))]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "prijmeni", ResourceType = typeof(Resources.Global))]
        public string LastName { get; set; }

        [StringLength(100)]
        [Display(Name = "uzivatelske_jmeno", ResourceType = typeof(Resources.Global))]
        public string UserName { get; set; }

        [EmailAddress]
        [StringLength(100)]
        [Display(Name = "email", ResourceType = typeof(Resources.Global))]
        public string Email { get; set; }

        [Required]
        [Display(Name = "potvrzeni_emailu", ResourceType = typeof(Resources.Global))]
        public bool EmailConfirmed { get; set; }

        [Phone]
        [StringLength(20)]
        [Display(Name = "telefonni_cislo", ResourceType = typeof(Resources.Global))]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "potvrzeni_telefonniho_cisla", ResourceType = typeof(Resources.Global))]
        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        [Display(Name = "dvoufaktorove_overovani_aktivni", ResourceType = typeof(Resources.Global))]
        public bool TwoFactorEnabled { get; set; }

        [Display(Name = "zamceno_do", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime? LockoutEndDate { get; set; }

        [Required]
        [Display(Name = "zamykani_aktivni", ResourceType = typeof(Resources.Global))]
        public bool LockoutEnabled { get; set; }

        [Required]
        [Display(Name = "pocet_neuspesnych_pristupu", ResourceType = typeof(Resources.Global))]
        public int AccessFailedCount { get; set; }

        [Display(Name = "role", ResourceType = typeof(Resources.Global))]
        public string RoleIds { get; set; }

        [Display(Name = "role", ResourceType = typeof(Resources.Global))]
        public List<IdentityUserRole> Roles { get; set; }

        public String RolesStringify
        {
            get {
                return this.Roles != null && this.Roles.Any() ? string.Join(",", this.Roles.Select(x => x.RoleId)) : "";
            }
        }
        
        [Display(Name = "cele_jmeno", ResourceType = typeof(Resources.Global))]
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }

        public bool IsEditable { get; set; }
    }
}