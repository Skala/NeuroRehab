﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class PatientAssignViewModel
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "pacient", ResourceType = typeof(Resources.Global))]
        public UserViewModel Patient { get; set; }

        [Required]
        [Display(Name = "terapeut", ResourceType = typeof(Resources.Global))]
        public UserViewModel Therapist { get; set; }

        public bool IsUsed { get; set; }

        [Required]
        public bool IsActive { get; set; }
    }
}