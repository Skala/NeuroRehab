﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.User
{
    public class PatientFeedbackViewModel
    {
        public PatientFeedbackViewModel()
        {
            this.Editable = true;
        }

        public Guid Id { get; set; }

        [Required]
        [Display(Name = "obtiznost", ResourceType = typeof(Resources.Global))]
        public int Difficulty { get; set; }

        [Required]
        [Display(Name = "mnozstvi_uloh", ResourceType = typeof(Resources.Global))]
        public int ExerciseAmount { get; set; }

        [Required]
        [StringLength(maximumLength: 1024, ErrorMessageResourceName = "_0_muze_mit_maximalne_2_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "zprava", ResourceType = typeof(Resources.Global))]
        public string Note { get; set; }

        public string NoteShort {
            get {
                return string.Concat(this.Note.Length > 15 ? this.Note.Substring(0, 15) : this.Note, "...");
            }
        }

        [DataType(DataType.DateTime)]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        public DateTime TimeUtc { get; set; }

        [Required]
        public int PackageId { get; set; }
        public Packages.Package Package { get; set; }

        public bool Editable { get; set; }
    }
}