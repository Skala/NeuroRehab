﻿using Neurop3.Models.Administration;
using Neurop3.Models.Exercises;
using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Package
{
    public class PackageViewModel
    {

        public PackageViewModel()
        {
            this.ExerciseAssignViewModels = new List<ExerciseAssignViewModel>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "nazev_baliku", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "status", ResourceType = typeof(Resources.Global))]
        public int StatusId { get; set; }
        public Status Status { get; set; }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime CreateTimeUtc { get; set; }

        [Required]
        [Display(Name = "datum_posledni_upravy", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime LastAccessTimeUtc { get; set; }

        [Required]
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        public virtual ICollection<ExerciseAssignViewModel> ExerciseAssignViewModels { get; set; }

        [Required]
        public string TherapistId { get; set; }

        [Required]
        public string PatientId { get; set; }
        public UserViewModel Patient { get; set; }

        public int AssignCount { get; set; }

        public string PackageTitle {
            get {
                return string.Format("{0}, {1}", this.Name, this.CreateTimeUtc);
            }
        }

    }
}