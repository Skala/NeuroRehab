﻿using Neurop3.Models.Packages;
using Neurop3.Models.ViewModels.Exercise;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Package
{
    public class ExerciseAssignPackageViewModel
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int ExerciseAssignId { get; set; }
        public ExerciseAssignViewModel ExerciseAssign { get; set; }

        [Required]
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int ExercisePackageId { get; set; }
        public PackageViewModel PackageViewModel { get; set; }

        [Required]
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Order { get; set; }
    }
}