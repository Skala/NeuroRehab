﻿using Neurop3.Models.Packages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Package
{
    public class ListViewModel
    {
        public ListViewModel() {
            this.PackageViewModels = new List<PackageViewModel>();
        }

        public PackageListFilter Filter { get; set; }
        public List<PackageViewModel> PackageViewModels { get; set; }

    }
}