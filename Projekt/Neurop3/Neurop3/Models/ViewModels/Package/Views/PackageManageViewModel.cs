﻿using Neurop3.Models.Administration;
using Neurop3.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Package
{
    public class PackageManageViewModel
    {
        public PackageManageViewModel()
        {
            this.ExerciseAssignPackageViewModels = new List<ExerciseAssignPackageViewModel>();
            this.patientFeedbackVMs = new List<PatientFeedbackViewModel>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(maximumLength: 1024, MinimumLength = 10, ErrorMessageResourceName = "nazev_baliku_musi_mit_alespon_10_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "nazev_baliku", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "status", ResourceType = typeof(Resources.Global))]
        public int StatusId { get; set; }
        public Status Status { get; set; }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime CreateTimeUtc { get; set; }

        [Required]
        [Display(Name = "datum_posledni_upravy", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime LastAccessTimeUtc { get; set; }

        [Required]
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        [Required]
        public string TherapistId { get; set; }

        [Required]
        [Display(Name = "jmeno_pacienta", ResourceType = typeof(Resources.Global))]
        public string PatientId { get; set; }
        public UserViewModel Patient { get; set; }

        public List<ExerciseAssignPackageViewModel> ExerciseAssignPackageViewModels { get; set; }
        public List<PatientFeedbackViewModel> patientFeedbackVMs { get; set; }


    }
}