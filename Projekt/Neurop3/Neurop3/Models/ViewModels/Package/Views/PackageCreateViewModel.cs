﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Package
{
    public class PackageCreateViewModel
    {
        [Required]
        [StringLength(maximumLength: 1024, MinimumLength = 10, ErrorMessageResourceName = "nazev_baliku_musi_mit_alespon_10_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "nazev_baliku", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "jmeno_pacienta", ResourceType = typeof(Resources.Global))]
        public string PatientId { get; set; }
    }
}