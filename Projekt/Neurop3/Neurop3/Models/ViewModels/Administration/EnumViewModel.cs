﻿using System;

namespace Neurop3.Models.ViewModels.Account
{
    [Serializable]
    public class EnumViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class PostedEnumItems
    {
        //this array will be used to POST values from the form to the controller
        public string[] EnumIds { get; set; }
    }
}