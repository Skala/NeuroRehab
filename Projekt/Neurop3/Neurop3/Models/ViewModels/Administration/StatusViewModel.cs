﻿using Neurop3.Models.ViewModels.Exercise;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Administration
{
    public class StatusViewModel
    {
        public StatusViewModel()
        {
            this.ExerciseAssignVMs = new List<ExerciseAssignViewModel>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        public List<ExerciseAssignViewModel> ExerciseAssignVMs { get; set; }
    }
}