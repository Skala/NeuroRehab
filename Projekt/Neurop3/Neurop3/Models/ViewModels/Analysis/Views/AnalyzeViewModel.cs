﻿using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Analysis
{
    public class AnalyzeViewModel
    {
        public AnalyzeViewModel()
        {
            this.ExerciseResultsViewModels = new List<ExerciseResultViewModel>();
            this.ExerciseResultsRowVMs = new List<ExerciseResultsRowViewModel>();
        }

        public UserViewModel patient { get; set; }
        public List<ExerciseResultViewModel> ExerciseResultsViewModels { get; set; }

        public List<ExerciseResultsRowViewModel> ExerciseResultsRowVMs { get; set; }

    }
}