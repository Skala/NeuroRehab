﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Neurop3.Models.ViewModels.Analysis
{
    [DataContract]
    public class DataPoint
    {
        [DataMember(Name = "x")]
        public int X { get; set; }
        [DataMember(Name = "y")]
        public int Y { get; set; }
        [DataMember(Name = "indexLabel")]
        public string Label { get; set; }
    }
}