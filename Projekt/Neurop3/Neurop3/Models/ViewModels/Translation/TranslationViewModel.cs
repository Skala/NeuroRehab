﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Translation
{
    public class TranslationViewModel
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(1000, ErrorMessageResourceName = "pole_muze_mit_maximalne_1000_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "cesky", ResourceType = typeof(Resources.Global))]
        public string Cz { get; set; }

        [StringLength(1000, ErrorMessageResourceName = "pole_muze_mit_maximalne_1000_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "anglicky", ResourceType = typeof(Resources.Global))]
        public string En { get; set; }

        [StringLength(1000, ErrorMessageResourceName = "pole_muze_mit_maximalne_1000_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "nemecky", ResourceType = typeof(Resources.Global))]
        public string De { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "nazev_sloupce_v_databazi", ResourceType = typeof(Resources.Global))]
        [RegularExpression(@"^\S*$", ErrorMessageResourceName = "text_nesmi_obsahovat_netisknutelne_znaky", ErrorMessageResourceType = typeof(Resources.Global))]
        public string DbColumnName { get; set; }

        public bool IsUsed { get; set; }

        public string LocalizedTranslation
        {
            get
            {
                HttpCookie cookie = HttpContext.Current.Request.Cookies["lang"];
                if (cookie != null)
                {
                    var lang = cookie.Value;
                    switch (lang)
                    {
                        case "en":
                            return !string.IsNullOrEmpty(this.En) ? this.En : this.Cz;
                        case "de":
                            return !string.IsNullOrEmpty(this.De) ? this.De : this.Cz;
                        default:
                            return this.Cz;
                    }
                }
                else
                {
                    return this.Cz;
                }
            }
        }
    }
}