﻿using Neurop3.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Translation
{
    public class ListViewModel
    {
        public ListViewModel() {
            this.TranslationViewModels = new List<TranslationViewModel>();
        }

        public TranslationListFilter Filter { get; set; }
        
        public List<TranslationViewModel> TranslationViewModels { get; set; }
    }
}