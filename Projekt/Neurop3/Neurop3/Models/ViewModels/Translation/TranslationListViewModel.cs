﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Translation
{
    public class TranslationListViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "cesky", ResourceType = typeof(Resources.Global))]
        public string Cz { get; set; }

        [Display(Name = "anglicky", ResourceType = typeof(Resources.Global))]
        public string En { get; set; }

        [Display(Name = "nemecky", ResourceType = typeof(Resources.Global))]
        public string De { get; set; }
    }
}