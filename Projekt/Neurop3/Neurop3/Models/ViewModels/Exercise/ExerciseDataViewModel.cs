﻿using Neurop3.Models.ViewModels.Translation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ExerciseDataViewModel
    {
        public int Id { get; set; }

        [Required]
        public int ExerciseId { get; set; }
        public ExerciseViewModel Exercise { get; set; }
        
        public int Row { get; set; }
        
        [Required(ErrorMessageResourceName = "polozka_musi_byt_vyplnena", ErrorMessageResourceType = typeof(Resources.Global))]
        public string Value { get; set; }

        public int DataType { get; set; }

        public TranslationViewModel TranslationName { get; set; }

        public TranslationViewModel TranslationDescription { get; set; }
    }
}