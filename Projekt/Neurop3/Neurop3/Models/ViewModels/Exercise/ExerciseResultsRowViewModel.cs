﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ExerciseResultsRowViewModel
    {
        public ExerciseResultsRowViewModel()
        {
            this.Values = new List<Values>();
        }

        public int Id { get; set; }

        public int ExerciseAssignId { get; set; }

        [Display(Name = "poradi", ResourceType = typeof(Resources.Global))]
        public int Sequence { get; set; }

        [Display(Name = "datum", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime TimeUtc { get; set; }

        public List<Values> Values { get; set; }
    }

    public class Values
    {
        public int RowIndex { get; set; }
        public string RowName { get; set; }
        public string Value { get; set; }
    }
}