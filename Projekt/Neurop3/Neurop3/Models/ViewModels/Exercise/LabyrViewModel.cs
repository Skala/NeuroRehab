﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class LabyrViewModel : ExerciseViewModelBase
    {
        [Display(Name = "zanechavani_stopy", ResourceType = typeof(Resources.Global))]
        public bool Trace { get; set; }

        [Display(Name = "cas_prezentace", ResourceType = typeof(Resources.Global))]
        public int PresentationTime { get; set; }

        [Display(Name = "startovni_bod", ResourceType = typeof(Resources.Global))]
        public bool StartPoint { get; set; }

        [Display(Name = "koncovy_bod", ResourceType = typeof(Resources.Global))]
        public bool EndPoint { get; set; }

        [Display(Name = "pocet_sloupcu", ResourceType = typeof(Resources.Global))]
        public int Columns { get; set; }

        [Display(Name = "pocet_radku", ResourceType = typeof(Resources.Global))]
        public int Rows { get; set; }

        [Display(Name = "horizontalni_mezera", ResourceType = typeof(Resources.Global))]
        public int GapHorizontal { get; set; }

        [Display(Name = "vertikalni_mezera", ResourceType = typeof(Resources.Global))]
        public int GapVertical { get; set; }

        [Range(15, 30, ErrorMessageResourceName = "velikost_bodu_chyba_rozsahu", ErrorMessageResourceType = typeof(Resources.Global))]
        [Display(Name = "velikost_bodu", ResourceType = typeof(Resources.Global))]
        public int Radius { get; set; }

        [Display(Name = "kriterium", ResourceType = typeof(Resources.Global))]
        public int Criterion { get; set; }

        [Display(Name = "pocet_pokusu", ResourceType = typeof(Resources.Global))]
        public int MaxTrials { get; set; }

        //Cas na splneni ulohy
        [Display(Name = "cas_na_ulohu", ResourceType = typeof(Resources.Global))]
        public int MaxTime { get; set; }

        //posloupnost bodu v bludisti od startu k cili
        [Display(Name = "posloupnost_bodu", ResourceType = typeof(Resources.Global))]
        public string Series { get; set; }

        //text instrukce
        [Display(Name = "instrukce", ResourceType = typeof(Resources.Global))]
        public string Instruction { get; set; }
    }
}