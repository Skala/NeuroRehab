﻿using Neurop3.Models.Administration;
using Neurop3.Models.ViewModels.Administration;
using Neurop3.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ExerciseAssignViewModel
    {
        public ExerciseAssignViewModel()
        {
            this.ExerciseResultsVMs = new List<ExerciseResultViewModel>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "jmeno_pacienta", ResourceType = typeof(Resources.Global))]
        public String PatientId { get; set; }
        public ApplicationUser Patient { get; set; }

        [Required]
        [Display(Name = "jmeno_terapeuta", ResourceType = typeof(Resources.Global))]
        public String TherapistId { get; set; }
        public ApplicationUser Therapist { get; set; }

        [Required]
        public int ExerciseId { get; set; }
        public Exercises.Exercise Exercise { get; set; }

        [Required]
        [Display(Name = "datum_prirazeni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime AssignDate { get; set; }

        [Required]
        [Display(Name = "datum_posledni_upravy", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime LastAccessDate { get; set; }

        [Required]
        [Display(Name = "status_ulohy", ResourceType = typeof(Resources.Global))]
        public int StatusId { get; set; }
        public Status Status { get; set; }

        public List<ExerciseResultViewModel> ExerciseResultsVMs { get; set; }

        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        public bool IsUsersAssignActive { get; set; }

        public int ResultsCount { get; set; }

        public string ExerciseAssignToPatientTitle
        {
            get
            {
                return string.Format("{0}, {1}, {2}, {3}", this.Therapist.FullName, this.Exercise.Name, this.LastAccessDate, this.Status.Name);
            }
        }
        public string ExerciseAssignByTherapistTitle
        {
            get
            {
                return string.Format("{0}, {1}, {2}, {3}", this.Patient.FullName, this.Exercise.Name, this.LastAccessDate, this.Status.Name);
            }
        }
    }
}