﻿using Neurop3.Models.Exercises;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ExerciseResultViewModel
    {
        public int Id { get; set; }

        [Required]
        public int ExerciseAssignId { get; set; }

        public ExerciseAssign ExerciseAssign { get; set; }

        [Required]
        public int Sequence { get; set; }

        [Required]
        public int Row { get; set; }

        [Required]
        public string Value { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime TimeUtc { get; set; }
    }
}