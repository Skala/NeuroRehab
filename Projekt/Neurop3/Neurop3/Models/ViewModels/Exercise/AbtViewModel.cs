﻿using Neurop3.Models.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class AbtViewModel : ExerciseViewModelBase
    {
        public Abt Abt { get; set; }
    }
}