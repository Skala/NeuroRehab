﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ExerciseManageViewModel
    {
        public ExerciseManageViewModel()
        {
            //this.ExerciseAssignViewModels = new List<ExerciseAssignViewModel>();
            this.ExerciseDataViewModels = new List<ExerciseDataViewModel>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "nazev_ulohy", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [StringLength(1000)]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "typ_programu", ResourceType = typeof(Resources.Global))]
        public int ProgramId { get; set; }

        public Programs.Program Program { get; set; }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }

        [Required]
        [Display(Name = "autor", ResourceType = typeof(Resources.Global))]
        public string AuthorId { get; set; }

        public ApplicationUser Author { get; set; }

        public List<ExerciseDataViewModel> ExerciseDataViewModels { get; set; }


        [Display(Name = "prirazeni", ResourceType = typeof(Resources.Global))]
        public List<ExerciseAssignViewModel> ExerciseAssignViewModels { get; set; }

        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        [Display(Name = "je_verejna", ResourceType = typeof(Resources.Global))]
        public bool IsPublic { get; set; }

        public bool HasAssign { get; set; }

        public bool HasActiveAssign { get; set; }

        public bool HasResult { get; set; }

        public bool IsAuthor { get; set; }

        public HttpFileCollectionBase Files { get; set; }
    }
}