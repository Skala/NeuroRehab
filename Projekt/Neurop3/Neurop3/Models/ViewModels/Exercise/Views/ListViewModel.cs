﻿using Neurop3.Models.Exercises;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ListViewModel
    {
        public ListViewModel()
        {
            this.ExerciseVMs = new List<ExerciseViewModel>();
        }


        public ExerciseListFilter Filter { get; set; }

        public List<ExerciseViewModel> ExerciseVMs { get; set; }
    }
}