﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ExerciseCreateViewModel
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "nazev_ulohy_musi_mit_od_3_do_100_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 3)]
        [Display(Name = "nazev_ulohy", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [StringLength(1000, ErrorMessageResourceName = "popis_ulohy_musi_mit_od_10_do_1000_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength =10)]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]

        public string Description { get; set; }

        [Required]
        [Display(Name = "typ_programu", ResourceType = typeof(Resources.Global))]
        public int ProgramId { get; set; }
    }
}