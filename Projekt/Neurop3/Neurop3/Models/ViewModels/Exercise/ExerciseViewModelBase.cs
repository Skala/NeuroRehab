﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Exercise
{
    public class ExerciseViewModelBase
    {
        [Display(Name = "nazev_ulohy", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        public int Id { get; set; }

        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }
    }
}