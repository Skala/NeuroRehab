﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Test
{
    public class RunPackageViewModel
    {
        public RunPackageViewModel()
        {
            this.RunTestViewModels = new List<RunTestViewModel>();
        }
        public List<RunTestViewModel> RunTestViewModels { get; set; }
    }
}