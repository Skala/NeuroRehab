﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Test
{
    public class RunTestViewModel
    {
        public int ExerciseAssignId { get; set; }
        public string Name { get; set; }
        public string InputParameters { get; set; }
        public string OutputParameters { get; set; }
        public string PathToScript { get; set; }

        public int PackageId { get; set; }
        public bool Finished { get; set; }
        public int Order { get; set; }
        public int TotalExercises { get; set; }
        public bool Demo { get; set; }
        public bool IsValid { get; set; }
        public string IsValidMessage { get; set; }

        public string MessageBefore { get; set; }
        public string MessageAfter { get; set; }
        public string Message { get; set; }
    }
}