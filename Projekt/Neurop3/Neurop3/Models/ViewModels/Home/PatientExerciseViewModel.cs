﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Home
{
    public class PatientExerciseViewModel
    {
        public string Name { get; set; }

        public int ExerciseAssignId { get; set; }
    }
}