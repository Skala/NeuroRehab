﻿using Neurop3.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Neurop3.Models.ViewModels.Account
{
    public class AccountManageViewModel
    {
        public UserViewModel UserVM { get; set; }

        public PatientDataViewModel PatientDataVM { get; set; }

        public TherapistDataViewModel TherapistDataVM { get; set; }

        public AdministratorDataViewModel AdministratorDataVM { get; set; }

        [Display(Name = "heslo", ResourceType = typeof(Resources.Global))]
        public bool HasPassword { get; set; }
    }
}