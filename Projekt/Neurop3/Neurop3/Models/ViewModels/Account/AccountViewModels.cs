﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Neurop3.Models.ViewModels.User;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Neurop3.Models.ViewModels.Account
{
    #region AccountViewModels

    //public class ExternalLoginConfirmationViewModel
    //{
    //    [Required]
    //    [Display(Name = "email", ResourceType = typeof(Resources.Global))]
    //    public string Email { get; set; }
    //}

    //public class ExternalLoginListViewModel
    //{
    //    public string ReturnUrl { get; set; }
    //}

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "kod", ResourceType = typeof(Resources.Global))]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "zapamatovat_prohlizec", ResourceType = typeof(Resources.Global))]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "email", ResourceType = typeof(Resources.Global))]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "email", ResourceType = typeof(Resources.Global))]
        [EmailAddress]
        public string Email { get; set; }

        //[Required]
        //[Display(Name = "user_name", ResourceType = typeof(Resources.Global))]
        //public string UserName { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "heslo", ResourceType = typeof(Resources.Global))]
        public string Password { get; set; }

        [Display(Name = "zapamatovat_si_me", ResourceType = typeof(Resources.Global))]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "email", ResourceType = typeof(Resources.Global))]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "telefon", ResourceType = typeof(Resources.Global))]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "jmeno", ResourceType = typeof(Resources.Global))]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "prijmeni", ResourceType = typeof(Resources.Global))]
        public string LastName { get; set; }

        //[Required]
        //[Display(Name = "user_name", ResourceType = typeof(Resources.Global))]
        //public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "_0_musi_mit_alespon_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "heslo", ResourceType = typeof(Resources.Global))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "heslo_znovu", ResourceType = typeof(Resources.Global))]
        [Compare("Password", ErrorMessageResourceName = "potvrzeni_hesla_selhalo", ErrorMessageResourceType = typeof(Resources.Global))]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "email", ResourceType = typeof(Resources.Global))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "_0_musi_mit_alespon_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "heslo", ResourceType = typeof(Resources.Global))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "heslo_znovu", ResourceType = typeof(Resources.Global))]
        [Compare("Password", ErrorMessageResourceName = "potvrzeni_hesla_selhalo", ErrorMessageResourceType = typeof(Resources.Global))]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "email", ResourceType = typeof(Resources.Global))]
        public string Email { get; set; }
    }
    #endregion
    
    #region ManageViewModels
    public class ManageIndexViewModel
    {
        public UserViewModel User { get; set; }

        [Display(Name = "nastaveni_hesla", ResourceType = typeof(Resources.Global))]
        public bool HasPassword { get; set; }

        [Display(Name = "externi_prihlaseni", ResourceType = typeof(Resources.Global))]
        public IList<UserLoginInfo> Logins { get; set; }

        [Display(Name = "telefonni_cislo", ResourceType = typeof(Resources.Global))]
        public string PhoneNumber { get; set; }

        [Display(Name = "dvoufaktorove_prihlaseni", ResourceType = typeof(Resources.Global))]
        public bool TwoFactor { get; set; }

        [Display(Name = "zapamatovat_prohlizec", ResourceType = typeof(Resources.Global))]
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessageResourceName = "_0_musi_mit_alespon_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "nove_heslo", ResourceType = typeof(Resources.Global))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "nove_heslo_znovu", ResourceType = typeof(Resources.Global))]
        [Compare("NewPassword", ErrorMessageResourceName = "potvrzeni_hesla_selhalo", ErrorMessageResourceType = typeof(Resources.Global))]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "soucasne_heslo", ResourceType = typeof(Resources.Global))]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessageResourceName = "_0_musi_mit_alespon_2_znaku", ErrorMessageResourceType = typeof(Resources.Global), MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "nove_heslo", ResourceType = typeof(Resources.Global))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "nove_heslo_znovu", ResourceType = typeof(Resources.Global))]
        [Compare("NewPassword", ErrorMessageResourceName = "potvrzeni_hesla_selhalo", ErrorMessageResourceType = typeof(Resources.Global))]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "telefonni_cislo", ResourceType = typeof(Resources.Global))]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "kod", ResourceType = typeof(Resources.Global))]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "telefonni_cislo", ResourceType = typeof(Resources.Global))]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
    #endregion
}
