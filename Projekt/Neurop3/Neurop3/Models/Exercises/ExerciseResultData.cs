﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Exercises
{
    [Table(name: "ExerciseResultData")]
    public class ExerciseResultData
    {
        public int Id { get; set; }

        [Required]
        public int ExerciseResultId { get; set; }
        public virtual ExerciseResult ExerciseResult { get; set; }

        [Required]
        public int Row { get; set; }

        [Required]
        public string Value { get; set; }

        [Required]
        public DateTime TimeUtc { get; set; }
    }
}