﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Neurop3.Common;

namespace Neurop3.Models.Exercises
{
    [Table(name: "ExerciseListFilter")]
    public class ExerciseListFilter
    {
        public ExerciseListFilter()
        {
            this.CurrentPage = 1;
            this.IsActive = ActiveEnum.Both.GetHashCode();
            this.IsPublic = ActiveEnum.Both.GetHashCode();
            this.RecordsPerPage = Int32.MaxValue;
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Display(Name = "seznam_id", ResourceType = typeof(Resources.Global))]
        public string ExerciseIds { get; set; }

        [Display(Name = "text", ResourceType = typeof(Resources.Global))]
        public string Text { get; set; }

        //[Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        //public DateTime CreatedDate { get; set; }

        [Required]
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public int IsActive { get; set; }

        [Required]
        [Display(Name = "verejne", ResourceType = typeof(Resources.Global))]
        public int IsPublic { get; set; }

        [Required]
        public string UserId { get; set; }
        //public virtual ApplicationUser User { get; set; }

        [Required]
        [Display(Name = "pocet_zaznamu_na_stranku", ResourceType = typeof(Resources.Global))]
        public int RecordsPerPage { get; set; }

        [NotMapped]
        public int CurrentPage { get; set; }

        [NotMapped]
        public int TotalRecords { get; set; }

        [NotMapped]
        [Display(Name = "program_id", ResourceType = typeof(Resources.Global))]
        public string ProgramIds { get; set; }

        public static ExerciseListFilter GetEmptyFilter()
        {
            return new ExerciseListFilter()
            {
                ExerciseIds = "",
                IsActive = 2,
                IsPublic = 2,
                ProgramIds = "",
                Text = "",
            };
        }
    }
}