﻿using Neurop3.Models.Administration;
using Neurop3.Models.Packages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Exercises
{
    [Table(name: "ExerciseAssign")]
    public class ExerciseAssign
    {
        public ExerciseAssign()
        {
            this.ExerciseResults = new HashSet<ExerciseResult>();
            this.ExerciseAssignPackages = new HashSet<ExerciseAssignPackage>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        //[Display(Name = "jmeno_pacienta", ResourceType = typeof(Resources.Global))]
        public String PatientId { get; set; }
        public virtual ApplicationUser Patient { get; set; }

        [Required]
        //[Display(Name = "jmeno_terapeuta", ResourceType = typeof(Resources.Global))]
        public String TherapistId { get; set; }
        public virtual ApplicationUser Therapist { get; set; }

        [Required]
        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        [Required]
        [Display(Name = "status_ulohy", ResourceType = typeof(Resources.Global))]
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }

        [Required]
        [Display(Name = "datum_prirazeni", ResourceType = typeof(Resources.Global))]
        public DateTime AssignDate { get; set; }

        [Required]
        [Display(Name = "datum_posledni_upravy", ResourceType = typeof(Resources.Global))]
        public DateTime LastAccessDate { get; set; }

        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        public virtual ICollection<ExerciseResult> ExerciseResults { get; set; }

        public virtual ICollection<ExerciseAssignPackage> ExerciseAssignPackages { get; set; }
    }
}