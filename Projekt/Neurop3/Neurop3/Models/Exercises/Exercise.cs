﻿
using Neurop3.Models.Programs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Exercises
{

    [Table(name: "Exercise")]
    public class Exercise
    {
        public Exercise()
        {
            this.ExerciseDatas = new HashSet<ExerciseData>();
            this.ExerciseAssigns = new HashSet<ExerciseAssign>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "nazev_ulohy", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [StringLength(1000)]
        public string Description { get; set; }
        
        [Required]
        [Display(Name = "typ_programu", ResourceType = typeof(Resources.Global))]
        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        public DateTime CreatedDate { get; set; }

        [Required]
        [Display(Name = "autor", ResourceType = typeof(Resources.Global))]
        public string AuthorId { get; set; }

        public virtual ApplicationUser Author { get; set; }
        
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        [Display(Name = "verejne", ResourceType = typeof(Resources.Global))]
        public bool IsPublic { get; set; }
                
        public virtual ICollection<ExerciseData> ExerciseDatas { get; set; }

        [Display(Name = "prirazeni", ResourceType = typeof(Resources.Global))]
        public virtual ICollection<ExerciseAssign> ExerciseAssigns { get; set; }
    }
}