﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Exercises
{
    [Table(name: "ExerciseData")]
    public class ExerciseData
    {
        public int Id { get; set; }

        [Required]
        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        [Required]
        public int Row { get; set; }

        [Required]
        public string Value { get; set; }
    }
}