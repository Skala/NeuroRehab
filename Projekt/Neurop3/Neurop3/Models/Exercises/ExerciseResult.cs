﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Neurop3.Models;
using System.ComponentModel.DataAnnotations;

namespace Neurop3.Models.Exercises
{
    [Table(name: "ExerciseResult")]
    public class ExerciseResult
    {
        //public ExerciseResult()
        //{
        //    this.ExerciseResultDatas = new HashSet<ExerciseResultData>();
        //}

        public int Id { get; set; }

        [Required]
        public int ExerciseAssignId { get; set; }

        public virtual ExerciseAssign ExerciseAssign { get; set; }

        [Required]
        public int Sequence { get; set; }

        [Required]
        public int Row { get; set; }

        [Required]
        public string Value { get; set; }

        [Required]
        public DateTime TimeUtc { get; set; }


        //[Required]
        //public DateTime LastResultTimeUtc{ get; set; }

        //[Required]
        //public string ResultRawData { get; set; }

        //public virtual ICollection<ExerciseResultData> ExerciseResultDatas { get; set; }


    }
}