﻿using Neurop3.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Exercises
{
    [NotMapped]
    public class ExerciseAssignListFilter
    {
        public ExerciseAssignListFilter() {
            IsActive = ActiveEnum.Both.GetHashCode();
        }

        public string Id { get; set; }

        public string PatientIds { get; set; }

        public string TherapistIds { get; set; }

        public string ExerciseIds { get; set; }

        public string StatusIds { get; set; }

        public DateTime AssignDate { get; set; }

        public DateTime LastAccessDate { get; set; }

        public int IsActive { get; set; }
    }
}