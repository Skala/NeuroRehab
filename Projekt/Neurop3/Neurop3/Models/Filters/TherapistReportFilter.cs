﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Filters
{
    public class TherapistReportFilter
    {
        public string PatientId { get; set; }
        public string TherapistId { get; set; }
        public string ReportIds { get; set; }
    }
}