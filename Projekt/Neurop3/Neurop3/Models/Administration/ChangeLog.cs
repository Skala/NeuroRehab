﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Administration
{
    [Table(name: "ChangeLog")]
    public class ChangeLog
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        public DateTime TimeUtc { get; set; }

        [Required]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "zprava", ResourceType = typeof(Resources.Global))]
        public string Message { get; set; }

        [Required]
        [Display(Name = "verze", ResourceType = typeof(Resources.Global))]
        public string Version { get; set; }
    }
}