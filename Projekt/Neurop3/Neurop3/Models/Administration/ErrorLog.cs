﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Administration
{
    [Table(name: ("ErrorLog"))]
    public class ErrorLog
    {
        [Key]
        public Guid ErrorId { get; set; }

        [Required]
        [StringLength(60)]
        public string Source { get; set; }

        [Required]
        [StringLength(100)]
        public string Type { get; set; }

        [Required]
        [StringLength(500)]
        public string Message { get; set; }

        [Required]
        [StringLength(50)]
        public string User { get; set; }

        [Required]
        public int HResult { get; set; }

        [Required]
        public DateTime TimeUtc { get; set; }

        [Required]
        public string StackTrace { get; set; }

        [Required]
        public string Stringify { get; set; }
    }
}