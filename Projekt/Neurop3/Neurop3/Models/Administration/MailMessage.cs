﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Administration
{
    /// <summary>
    /// Wrapper pro odeslani zpravy
    /// </summary>
    public class MailMessage
    {
        /// <summary>
        /// Identifikator prijmence
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Text zpravy v HTML
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Predmet zpravy
        /// </summary>
        public string Subject { get; set; }
    }
}