﻿using Neurop3.Models.Programs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Administration
{
    [Table(name: "Translation")]
    public class Translation
    {
        public Translation()
        {
            this.ProgramRecipeNames = new HashSet<ProgramRecipe>();
            this.ProgramRecipeDescriptions = new HashSet<ProgramRecipe>();
            this.ProgramResultNames = new HashSet<ProgramResult>();
            this.ProgramResultDescriptions = new HashSet<ProgramResult>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(1000)]
        [Display(Name = "cesky", ResourceType = typeof(Resources.Global))]
        public string Cz { get; set; }

        [StringLength(1000)]
        [Display(Name = "anglicky", ResourceType = typeof(Resources.Global))]
        public string En { get; set; }

        [StringLength(1000)]
        [Display(Name = "nemecky", ResourceType = typeof(Resources.Global))]
        public string De { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "nazev_sloupce_v_databazi", ResourceType = typeof(Resources.Global))]
        public string DbColumnName { get; set; }

        public string LocalizedTranslation
        {
            get {
                HttpCookie cookie = HttpContext.Current.Request.Cookies["lang"];
                if(cookie != null)
                {
                    var lang = cookie.Value;
                    switch (lang)
                    {
                        case "en":
                            return !string.IsNullOrEmpty(this.En) ? this.En : this.Cz;
                        case "de":
                            return !string.IsNullOrEmpty(this.De) ? this.De : this.Cz;
                        default:
                            return this.Cz;
                    }
                }
                else
                {
                    return this.Cz;
                }
            }
        }

        public virtual ICollection<ProgramRecipe> ProgramRecipeNames { get; set; }
        public virtual ICollection<ProgramRecipe> ProgramRecipeDescriptions { get; set; }
        public virtual ICollection<ProgramResult> ProgramResultNames { get; set; }
        public virtual ICollection<ProgramResult> ProgramResultDescriptions { get; set; }

    }
}