﻿using Neurop3.Models.Exercises;
using Neurop3.Models.Packages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Administration
{
    [Table(name:"Status")]
    public class Status
    {
        public Status()
        {
            this.ExerciseAssigns = new HashSet<ExerciseAssign>();
            this.ExercisesPackages = new HashSet<Packages.Package>();
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }
        
        [Required]
        [StringLength(100)]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        public virtual ICollection<ExerciseAssign> ExerciseAssigns { get; set; }
        public virtual ICollection<Packages.Package> ExercisesPackages { get; set; }
    }
}