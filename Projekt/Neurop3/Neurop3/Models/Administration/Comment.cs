﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Administration
{
    [Table(name: "Comment")]
    public class Comment
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "typ_pripominky", ResourceType = typeof(Resources.Global))]
        public int Type { get; set; }

        [Required]
        [Display(Name = "autor", ResourceType = typeof(Resources.Global))]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "datum_vytvoreni", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd.MM.yyyy HH:mm:ss}")]
        public DateTime TimeUtc { get; set; }

        [Required]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [Display(Name = "zprava", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        [Required]
        [Display(Name = "zobrazeno", ResourceType = typeof(Resources.Global))]
        public bool IsShown { get; set; }

        [Required]
        [Display(Name = "dokonceno", ResourceType = typeof(Resources.Global))]
        public bool IsDone { get; set; }

    }
}