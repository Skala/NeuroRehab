﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Administration
{
    [Table(name: "TranslationListFilter")]
    public class TranslationListFilter
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Display(Name = "text", ResourceType = typeof(Resources.Global))]
        public string Text { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "pocet_zaznamu_na_stranku", ResourceType = typeof(Resources.Global))]
        public int RecordsPerPage { get; set; }

        [NotMapped]
        public int CurrentPage { get; set; }

        [NotMapped]
        public int TotalRecords { get; set; }
    }
}