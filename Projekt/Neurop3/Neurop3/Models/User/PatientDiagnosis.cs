﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table("PatientDiagnosis")]
    public class PatientDiagnosis
    {
        [Key]
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [StringLength(1024)]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "datum", ResourceType = typeof(Resources.Global))]
        [DataType(DataType.DateTime)]
        public DateTime TimeUtc { get; set; }

        [Required]
        public string TherapistId { get; set; }
        public virtual ApplicationUser Therapist { get; set; }

        [Required]
        public string PatientDataId { get; set; }
        public virtual PatientData PatientData { get; set; }
    }
}