﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table(name: "AdministratorData")]
    public class AdministratorData
    {
        [Key]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}