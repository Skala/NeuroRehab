﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table("PatientData")]
    public class PatientData
    {
        public PatientData()
        {
            this.PatientDiagnoses = new HashSet<PatientDiagnosis>();
            this.MedicalReports = new HashSet<TherapistReport>();
        }

        //[ForeignKey(name: "User")]
        [Key]
        public string UserId { get; set; }

        public virtual ApplicationUser User { get; set; }
        
        [Required]
        [Display(Name = "datum_narozeni", ResourceType = typeof(Resources.Global))]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "pohlavi", ResourceType = typeof(Resources.Global))]
        public int Gender { get; set; }

        [Required]
        [Display(Name = "zastupce", ResourceType = typeof(Resources.Global))]
        public string Representative { get; set; }

        [Required]
        [Display(Name = "socialni_status", ResourceType = typeof(Resources.Global))]
        public int SocialStatus { get; set; }

        [Required]
        [Display(Name = "profese", ResourceType = typeof(Resources.Global))]
        public string Profession { get; set; }


        [Display(Name = "diagnozy", ResourceType = typeof(Resources.Global))]
        public virtual ICollection<PatientDiagnosis> PatientDiagnoses { get; set; }

        [Display(Name = "lekarske_zpravy", ResourceType = typeof(Resources.Global))]
        public virtual ICollection<TherapistReport> MedicalReports { get; set; }
    }
}