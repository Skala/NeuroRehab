﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table(name: "TherapistData")]
    public class TherapistData
    {

        //[ForeignKey("User")]
        [Key]
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [Required]
        [Display(Name = "datum_narozeni", ResourceType = typeof(Resources.Global))]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "pohlavi", ResourceType = typeof(Resources.Global))]
        public int Gender { get; set; }

        [Required]
        [Display(Name = "vzdelani", ResourceType = typeof(Resources.Global))]
        public string Education { get; set; }
    }
}