﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table(name: "TherapistReport")]
    public class TherapistReport
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(2048)]
        public string Report { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime TimeUtc { get; set; }

        [Required]
        public string TherapistId { get; set; }
        public virtual ApplicationUser Therapist { get; set; }

        [Required]
        public bool Shown { get; set; }

        [Required]
        public string PatientDataId { get; set; }
        public virtual PatientData PatientData { get; set; }
    }
}