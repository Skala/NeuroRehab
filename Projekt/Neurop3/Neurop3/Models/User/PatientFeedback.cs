﻿using Neurop3.Common;
using Neurop3.Models.Packages;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table(name: "PatientFeedback")]
    public class PatientFeedback
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public int Difficulty { get; set; }

        [Required]
        public int ExerciseAmount { get; set; }

        [Required]
        [StringLength(maximumLength: 1024, ErrorMessageResourceName = "_0_muze_mit_maximalne_2_znaku", ErrorMessageResourceType = typeof(Resources.Global))]
        public string Note { get; set; }
        
        [Required]
        public DateTime TimeUtc { get; set; }

        [Required]
        public int PackageId { get; set; }
        public virtual Package Package { get; set; }
    }
}