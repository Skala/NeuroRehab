﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table(name: "PatientAssignListFilter")]
    public class PatientAssignListFilter
    {
        [Key]
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Display(Name = "jmeno_pacienta", ResourceType = typeof(Resources.Global))]
        public string PatientName { get; set; }

        [Display(Name = "jmeno_terapeuta", ResourceType = typeof(Resources.Global))]
        public String TherapistName { get; set; }

        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public int IsActive { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "pocet_zaznamu_na_stranku", ResourceType = typeof(Resources.Global))]
        public int RecordsPerPage { get; set; }

        [NotMapped]
        public int CurrentPage { get; set; }

        [NotMapped]
        public int TotalRecords { get; set; }
    }
}