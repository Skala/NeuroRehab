﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.User
{
    [Table(name: "PatientAssign")]
    public class PatientAssign
    {
        public PatientAssign()
        {
            this.IsActive = true;
        }

        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "pacient", ResourceType = typeof(Resources.Global))]
        public String PatientId { get; set; }
        public virtual ApplicationUser Patient { get; set; }

        [Required]
        [Display(Name = "terapeut", ResourceType = typeof(Resources.Global))]
        public String TherapistId { get; set; }
        public virtual ApplicationUser Therapist { get; set; }

        [Required]
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }
    }
}