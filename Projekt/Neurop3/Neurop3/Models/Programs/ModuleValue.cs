﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Modules
{
    [Table(name: "ModuleValue")]
    public class ModuleValue
    {
        public int Id { get; set; }

        [Required]
        public int ModuleRecipeValueID { get; set; }

        public virtual ModuleRecipe ModuleRecipeValue { get; set; }
        
        [Required]
        public string Value { get; set; }
    }
}