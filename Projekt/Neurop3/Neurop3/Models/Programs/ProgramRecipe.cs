﻿using Neurop3.Models.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Programs
{
    [Table(name: "ProgramRecipe")]
    public class ProgramRecipe
    {
        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [Display(Name = "typ_programu", ResourceType = typeof(Resources.Global))]
        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }

        [Required]
        [Display(Name = "cislo_radky", ResourceType = typeof(Resources.Global))]
        public int RowNumber { get; set; }

        [Required]
        [Display(Name = "nazev_radky", ResourceType = typeof(Resources.Global))]
        public int RowNameId { get; set; }

        [Display(Name = "popis_radky", ResourceType = typeof(Resources.Global))]
        public int RowDescriptionId { get; set; }

        [Required]
        [Display(Name = "typ_dat", ResourceType = typeof(Resources.Global))]
        public int DataTypeId { get; set; }

        [StringLength(256)]
        [Display(Name = "hodnota", ResourceType = typeof(Resources.Global))]
        public string Value { get; set; }

        public virtual Translation TranslationName { get; set; }
        public virtual Translation TranslationDescription { get; set; }
    }
}