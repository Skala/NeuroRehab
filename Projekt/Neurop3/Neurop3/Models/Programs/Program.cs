﻿using Neurop3.Models.Exercises;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;



namespace Neurop3.Models.Programs
{
    [Table(name:"Program")]
    public class Program
    {
        public Program()
        {
            this.ProgramRecipes = new List<ProgramRecipe>();
            this.ProgramResults = new List<ProgramResult>();
            this.Exercises = new List<Exercise>();
        }


        [Display(Name = "id", ResourceType = typeof(Resources.Global))]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "nazev", ResourceType = typeof(Resources.Global))]
        public string Name { get; set; }

        [Required]
        [StringLength(1024)]
        [Display(Name = "popis", ResourceType = typeof(Resources.Global))]
        public string Description { get; set; }

        [Required]
        [Display(Name = "autor", ResourceType = typeof(Resources.Global))]
        public string AuthorId { get; set; }
        public virtual ApplicationUser Author { get; set; }

        [Required]
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public bool IsActive { get; set; }

        [Required]
        [Display(Name = "je_verejny", ResourceType = typeof(Resources.Global))]
        public bool IsPublic { get; set; }

        public virtual ICollection<Exercise> Exercises { get; set; }
        public virtual ICollection<ProgramRecipe> ProgramRecipes { get; set; }
        public virtual ICollection<ProgramResult> ProgramResults { get; set; }
    }
}