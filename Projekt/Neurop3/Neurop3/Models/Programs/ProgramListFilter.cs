﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Neurop3.Models.Programs
{
    [Table(name: "ProgramListFilter")]
    public class ProgramListFilter
    {
        
        public int Id { get; set; }

        [Display(Name = "seznam_id", ResourceType = typeof(Resources.Global))]
        public string ProgramIds { get; set; }

        [Display(Name = "text", ResourceType = typeof(Resources.Global))]
        public string Text { get; set; }

        [Required]
        [Display(Name = "aktivni", ResourceType = typeof(Resources.Global))]
        public int IsActive { get; set; }

        [Required]
        [Display(Name = "je_verejny", ResourceType = typeof(Resources.Global))]
        public int IsPublic { get; set; }

        [Required]
        public string UserId { get; set; }
        //public virtual ApplicationUser User { get; set; }

        [Required]
        [Display(Name = "pocet_zaznamu_na_stranku", ResourceType = typeof(Resources.Global))]
        public int RecordsPerPage { get; set; }

        [NotMapped]
        public int CurrentPage { get; set; }

        [NotMapped]
        public int TotalRecords { get; set; }
    }
}