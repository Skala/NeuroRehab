﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Neurop3.Models.Exercises;
using Neurop3.Models.Administration;
using Neurop3.Models.Programs;
using Neurop3.Models.User;
using System.ComponentModel.DataAnnotations.Schema;
using Neurop3.Models.Packages;
using System.Data.Entity.Validation;
using System.Text;

namespace Neurop3.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(100)]
        [Display(Name = "jmeno", ResourceType = typeof(Resources.Global))]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "prijmeni", ResourceType = typeof(Resources.Global))]
        public string LastName { get; set; }

        [Display(Name = "email")]
        public override string Email { get => base.Email; set => base.Email = value; }

        [StringLength(20)]
        [Display(Name = "telefonni_cislo", ResourceType = typeof(Resources.Global))]
        public override string PhoneNumber { get => base.PhoneNumber; set => base.PhoneNumber = value; }

        

        [Display(Name = "prirazene_ulohy", ResourceType = typeof(Resources.Global))]
        public virtual ICollection<ExerciseAssign> PatientExerciseAssigns { get; set; }
        [Display(Name = "prirazeni_k_terapautovi", ResourceType = typeof(Resources.Global))]
        public virtual ICollection<ExerciseAssign> TherapistExerciseAssigns { get; set; }
        [Display(Name = "prirazeni_pacietu", ResourceType = typeof(Resources.Global))]
        public virtual ICollection<PatientAssign> PatientAssigns { get; set; }
        public virtual ICollection<PatientAssign> TherapistAssigns { get; set; }
        public virtual ICollection<Exercise> ExerciseAuthors { get; set; }
        public virtual ICollection<Program> ProgramAuthors { get; set; }
        public virtual ICollection<LoginLog> LoginLogs { get; set; }

        public virtual PatientData PatientData { get; set; }
        public virtual TherapistData TherapistData { get; set; }
        public virtual AdministratorData AdministratorData { get; set; }
        


        //public virtual ICollection<ProgramListFilter> ProgramListFilters {get;set;}
        //public virtual ICollection<ExerciseListFilter> ExerciseListFilters { get; set; }
        //public virtual ICollection<TranslationListFilter> TranslationListFilters { get; set; }
        //public virtual ICollection<PackageListFilter> PackageListFilters { get; set; }


        [NotMapped]
        [Display(Name = "cele_jmeno", ResourceType = typeof(Resources.Global))]

        public string FullName {
            get {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch( DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                );
            }
        }


        #region exercise
        public DbSet<Exercise> Exercise { get; set; }
        public DbSet<ExerciseAssign> ExerciseAssign { get; set; }
        public DbSet<ExerciseAssignPackage> ExerciseAssignPackage { get; set; }
        public DbSet<ExerciseResult> ExerciseResult { get; set; }
        public DbSet<ExerciseData> ExerciseData { get; set; }
        public DbSet<ExerciseListFilter> ExerciseListFilter { get; set; }

        public DbSet<Package> Package { get; set; }
        public DbSet<PackageListFilter> PackageListFilter { get; set; }
        #endregion

        #region Program
        public DbSet<ProgramRecipe> ProgramRecipe { get; set; }
        public DbSet<ProgramResult> ProgramResult { get; set; }
        public DbSet<Program> Program { get; set; }
        public DbSet<ProgramListFilter> ProgramListFilter { get; set; }
        #endregion

        #region Administration
        public DbSet<PatientAssign> PatientAssign { get; set; }
        public DbSet<PatientAssignListFilter> PatientAssignListFilter { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<Translation> Translation { get; set; }
        public DbSet<TranslationListFilter> TranslationListFilter { get; set; }
        public DbSet<ErrorLog> ErrorLog { get; set; }
        public DbSet<Comment> Comment{ get; set; }
        public DbSet<ChangeLog> ChangeLog{ get; set; }
        public DbSet<LoginLog> LoginLog{ get; set; }
        #endregion

        #region User Account
        public DbSet<PatientData> PatientData { get; set; }
        public DbSet<TherapistData> TherapistData { get; set; }
        public DbSet<AdministratorData> AdministratorData { get; set; }
        public DbSet<PatientDiagnosis> PatientDiagnosis { get; set; }
        public DbSet<TherapistReport> TherapistReport { get; set; }
        public DbSet<PatientFeedback> PatientFeedback { get; set; }
        #endregion

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Rename Basic Entity
            modelBuilder.Entity<ApplicationUser>().ToTable("Users"); 
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            #endregion
            //-----
            //Prvni se uvadi tabulka, kde bude N vazba
            //-----  A A B A
            #region Exercise
            modelBuilder.Entity<Exercise>().HasRequired(x => x.Program).WithMany(x => x.Exercises).HasForeignKey(x => x.ProgramId);
            modelBuilder.Entity<Exercise>().HasRequired(x => x.Author).WithMany(x => x.ExerciseAuthors).HasForeignKey(x => x.AuthorId);

            modelBuilder.Entity<ExerciseResult>().HasRequired(x => x.ExerciseAssign).WithMany(x => x.ExerciseResults).HasForeignKey(x => x.ExerciseAssignId);

            modelBuilder.Entity<ExerciseAssign>().HasRequired(x => x.Patient).WithMany(x => x.PatientExerciseAssigns).HasForeignKey(x => x.PatientId).WillCascadeOnDelete(false);
            modelBuilder.Entity<ExerciseAssign>().HasRequired(x => x.Therapist).WithMany(x => x.TherapistExerciseAssigns).HasForeignKey(x => x.TherapistId).WillCascadeOnDelete(false);
            modelBuilder.Entity<ExerciseAssign>().HasRequired(x => x.Exercise).WithMany(x => x.ExerciseAssigns).HasForeignKey(x => x.ExerciseId).WillCascadeOnDelete(false);
            modelBuilder.Entity<ExerciseAssign>().HasRequired(x => x.Status).WithMany(x => x.ExerciseAssigns).HasForeignKey(x => x.StatusId).WillCascadeOnDelete(false);

            modelBuilder.Entity<ExerciseAssignPackage>().HasRequired(x => x.ExerciseAssign).WithMany(x => x.ExerciseAssignPackages).HasForeignKey(x => x.ExerciseAssignId).WillCascadeOnDelete(false);
            modelBuilder.Entity<ExerciseAssignPackage>().HasRequired(x => x.Package).WithMany(x => x.ExerciseAssignPackages).HasForeignKey(x => x.PackageId);

            modelBuilder.Entity<ExerciseData>().HasRequired(x => x.Exercise).WithMany(x => x.ExerciseDatas).HasForeignKey(x => x.ExerciseId);

            modelBuilder.Entity<Package>().HasRequired(x => x.Status).WithMany(x => x.ExercisesPackages).HasForeignKey(x => x.StatusId);
            #endregion

            #region Program
            modelBuilder.Entity<Program>().HasRequired(x => x.Author).WithMany(x => x.ProgramAuthors).HasForeignKey(x => x.AuthorId).WillCascadeOnDelete(false);

            modelBuilder.Entity<ProgramRecipe>().HasRequired(x => x.Program).WithMany(x => x.ProgramRecipes).HasForeignKey(x => x.ProgramId);
            modelBuilder.Entity<ProgramRecipe>().HasRequired(x => x.TranslationName).WithMany(x => x.ProgramRecipeNames).HasForeignKey(x => x.RowNameId);
            modelBuilder.Entity<ProgramRecipe>().HasRequired(x => x.TranslationDescription).WithMany(x => x.ProgramRecipeDescriptions).HasForeignKey(x => x.RowDescriptionId);

            modelBuilder.Entity<ProgramResult>().HasRequired(x => x.Program).WithMany(x => x.ProgramResults).HasForeignKey(x => x.ProgramId);
            modelBuilder.Entity<ProgramResult>().HasRequired(x => x.TranslationName).WithMany(x => x.ProgramResultNames).HasForeignKey(x => x.RowNameId);
            modelBuilder.Entity<ProgramResult>().HasRequired(x => x.TranslationDescription).WithMany(x => x.ProgramResultDescriptions).HasForeignKey(x => x.RowDescriptionId);
            #endregion

            #region User
            modelBuilder.Entity<PatientAssign>().HasRequired(x => x.Patient).WithMany(x => x.PatientAssigns).HasForeignKey(x => x.PatientId);
            modelBuilder.Entity<PatientAssign>().HasRequired(x => x.Therapist).WithMany(x => x.TherapistAssigns).HasForeignKey(x => x.TherapistId);

            modelBuilder.Entity<ApplicationUser>().HasOptional(u => u.PatientData).WithRequired(pa => pa.User);
            modelBuilder.Entity<ApplicationUser>().HasOptional(u => u.TherapistData).WithRequired(td => td.User);
            modelBuilder.Entity<ApplicationUser>().HasOptional(u => u.AdministratorData).WithRequired(ad => ad.User);

            modelBuilder.Entity<PatientDiagnosis>().HasRequired(pds => pds.PatientData).WithMany(pda => pda.PatientDiagnoses).HasForeignKey(pds => pds.PatientDataId);
            modelBuilder.Entity<TherapistReport>().HasRequired(mr => mr.PatientData).WithMany(pda => pda.MedicalReports).HasForeignKey(mr => mr.PatientDataId);
            modelBuilder.Entity<PatientFeedback>().HasRequired(pf => pf.Package).WithMany(pa => pa.PatientFeedbacks).HasForeignKey(pf => pf.PackageId);
            #endregion

            #region Admistration
            modelBuilder.Entity<LoginLog>().HasRequired(x => x.User).WithMany(x => x.LoginLogs).HasForeignKey(x => x.UserId);
            #endregion

        }
    }
}