﻿/* @author: Pavel Skala */
/*
param: pole s hodnotami radka po radce

Navratova hodnota:
Retezec, ktery odpovida nasledujicimi formatu:
Celkovy_cas##cislo1poziceX,cislo1poziceY,hodnotaCisla1,dobaHledaniCisla;cislo2poziceX, ... ;; Celkovy_cas, ...
*/

var mContext;
var mCanvas;
var mResults;
var mParsLine;
var startTime;
var tempStartTime; //startovni cas hledani dalsiho cisla
var mParam;
var actualNumber; //Aktualne hledane cislo
var LastNumber; //Posledni cislo, ktere ma pacient najit. Nasleduje konec ulohy
var tmpGelbRectangle; //Speichert das aktuelle gelbe Rectangle ein (um es später wieder grau zu färben)
var table2d; // 2 rozmerne pole urcene pro analyzu vysledku
var rectangle2d; //2 rozmerne pole pro ukladani obdelniku
var mResults; //Das Array mit den einzelnen Ergebnissen des Benutzers

var mRowPos; //Speichert die aktuelle Zeile der Tabelle ein 
var mRows;
var mColumns;


function runExercise(param) {

    setLang("lang");

    mCanvas = document.getElementById('mCanvas');
    mCanvas.width = mCanvas.clientWidth;
    mCanvas.height = Math.floor(mCanvas.width/2);

    if(mCanvas.getContext)
    {
        startTime = new Date();
        mContext = mCanvas.getContext('2d');
        //Hintergrund
        mContext.fillStyle = "rgb(35, 45,140)";
        mContext.fillRect(0,0,mCanvas.width,mCanvas.height);	
        //mRectangles = new Array();//Hier werden die Rectanglee abgespeichert
        mResults = new Array(); //Hier werden die Testergebnisse abgespeichert
        mParam = param.split("##")
        table2d = new Array();
        mParsLine = 0;
        mRowPos = 0;
        actualNumber = 1; //Die erste gesuchte Zahl ist: 1
        addListener();
        parse();
    }
    else
        alert(fatalError[lang]);
}

//PARSING
//"Konstanten" die benutzt werden um sich zu merken, was man gerade parst
var FREE = 0;  //In keinen Block
var INFO = 1;  //Info Block
var mode = FREE;
var example = false;

/* Parst ab der angegbenen Zeile und setzt alle globalen Variablen */
function parse() {

    var inputNo = 0;
    var error = 0;

    mColumns = parseInt(mParam[inputNo++]);
    mRows = parseInt(mParam[inputNo++]);

    tempRows = mParam[inputNo++].split(";");

    for (var row in tempRows){
        row = row.trim();
        table2d[mRowPos++] = tempRows[row].trim().split(",").map(item => item.trim());
    }

    if(table2d.length == mRows){
        for (row in table2d){
            if(table2d[row].length != mColumns){
                error++;
            }
        }
    }
    else{
        error++;
    }
    if(error != 0){
        alert(InputError[lang]);
    }
    createAll();

    // var infoString;
	// //Zeile für Zeile
    // for (; mParsLine < mParam.length; mParsLine++) {
        
    //     var row = mParam[mParsLine];
    //     if (rowStartsWith(row, ";") != -1) continue; //Komentare

    //     //Info
    //     if (mode == FREE && rowStartsWith(row, "I:") != -1) // Informacni radka
    //     {
    //         mode = INFO; //Infomodus setzen
    //         infoString = "" + row.substr(rowStartsWith(row, "I:") + 1); //Nastaveni informacniho textu
    //         continue;
    //     }
    //     if (mode == INFO) //Pri info rezimu
    //     {
    //         if (rowStartsWith(row, "END") == -1) //pokud to nekonci
    //             infoString += ("\n" + row);
    //         else
    //         {
    //             alert(infoString); //nakonec se dojde k vypisu
    //             mode = FREE;  //Modus zurückt
    //         }
    //         continue;
    //     }
    //     //Pokud nebylo nalezeno žádné klíčové slovo, jedná se o hlavní řádek
    //     newTableNublerRow(row.split(","));
    // }
    // //fertig geparst. Die Tabelle anzeigen und warten bis der Patient fertig gespielt hat
    //createAll();
}

//Überprüft ob eine Zeile mit einem Schlüsselwort beginnt oder nicht. Gibt den direkten Index zurück,
//an dem das Schlüsselwort aufhört oder -1.
// function rowStartsWith(row, value) {
//     var eStr = "";
//     var search = false; //Am Anfang leerzeichen überspringen
//     for (var i = 0; i < row.length; i++) {
//         if (!search && row.charAt(i) == " ") continue;
//         search = true;
//         eStr += row.charAt(i).toString();
//         if (value.toUpperCase() == eStr.toUpperCase()) {
//             return i;
//         }
//     }
//     return -1;
// }

//Vlozeni cisla s radky do pole poli
// function newTableNublerRow(numbers) {
//     table2d[mRowPos] = numbers;
//     mRowPos++;
// }

//Erstellt das Spielfeld mit allen Listenenern 
//Jedes Rectangle ist 50x50 groß, das gesamte Feld soll zentriert sein
function createAll() {
    LastNumber = 0; //Die gesamte Anzahl der gesuchten Rectanglee wird hier gleich mitgezählt
    rectangle2d = new Array(); //In dieses Array werden die Rectanglee gespeichert
    var font = "bold 26px Calibri"; ; //Die Font der Beschriftung
    var startX = mCanvas.width / 2 - table2d[0].length * 75 / 2; //So ausrechnen dass das Feld zentriert ist
    var startY = mCanvas.height / 2 - table2d.length * 75 / 2;
    for (var i = 0; i < table2d.length; ++i) {
        rectangle2d[i] = new Array();
        for (var j = 0; j < table2d[i].length; j++) {
            //Ist der Wert des Rectangles 00 so wird kein Text kein Rectangleobjekt gespeichert sondern NULL
            var text = "";
            if (table2d[i][j].toString() != "00")
            {
                text = table2d[i][j].toString();
                rectangle2d[i][j] = (new Rectangle(startX + j * 75, startY + i * 75, 75, 75, font, text,
                    "rgb(127,127,127)", parseInt(table2d[i][j]),j,i));
                LastNumber++; //Ein gesuchtes Rectangle wurde hinzugefügt -> LastNumber erhöhen
            }
            else
            {
                 rectangle2d[i][j] =null;
                 new Rectangle(startX + j * 75, startY + i * 75, 75, 75, font, text,
                    "rgb(127,127,127)",0,j,i)
            }
        }
     }
     tempStartTime = new Date(); //Die Zeit für die Suche nach der Zahl 1 beginnt jetzt zu laufen
}


//RectangleE, BUTTONS, GRAFIK; ONKLICK 
//Listener adden (Zur Übersichtlichkeit hier ausgelagert)
function addListener()
{
    mCanvas.addEventListener('mousedown', function (e) {
        //Hier wird zunächst x und y ermittelt. Wegen den verschiedenen Browsern etwas umfangreicher. 
        if (e.pageX || e.pageY) {
            x = e.pageX;
            y = e.pageY;
        }
        else {
            x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }
        x -= this.offsetLeft;
        y -= this.offsetTop;
        //Alle Rectanglee durchlaufen
        for (var i = 0; i < rectangle2d.length; i++)
            for (var j = 0; j < rectangle2d[i].length; j++) {
                var r = rectangle2d[i][j];
                //Wenn auf das richtige Rectangle geklickt wurde
                if (r != null && r.value == actualNumber && r.collide(x, y)) {
                    //Das alte Rectangle wieder grau machen
                    if (tmpGelbRectangle != null) {
                        tmpGelbRectangle.color = "rgb(127,127,127)";
                        tmpGelbRectangle.draw();
                    }
                    //Das neue Rectangle gelb machen und in tmpGelbRectangle einspeichern
                    r.color = "rgb(255,200,15)";
                    r.draw();
                    tmpGelbRectangle = r;
                    //Das Ergebnis einspeichern, die Zeit neu messen
                    var zeit = parseInt((new Date().getTime() - tempStartTime.getTime()));
                    mResults[actualNumber - 1] = new TestResult(r.posX, r.posY, r.value, zeit);
                    tempStartTime = new Date();

                    if (actualNumber == LastNumber) //Wenn das letzte Rectangle gefunden wurde
                    {
                        showModalAfter();
                    }

                    actualNumber++; //Die gesuchte Zahl erhöhen
                }
            }
    });
}

//Datenstruktur zum abspeichern eines Schrittes
function TestResult(posX, posY, zahl, zeit) {
    this.posX = posX;
    this.posY = posY;
    this.zahl = zahl;
    this.zeit = zeit;
}


/*Die Klasse Rectangle zum speichern der Koordinaten etc.
 *Die Rectangleklasse wird sowohl für die Buttons als auch für die Spalten verwendet
 *Parameter: x,y,w,h, font, text :Klar!
 *value: Der Wert des Rectangles, 0 wenn kein Wert
 *posX,posY: Die Positionen für das Rectangle (nicht Pixel) */
function Rectangle(x, y, w, h, font, text, color, value,posX,posY) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.text = text;
    this.value = value;
    this.font = font;
    this.color = color;
    this.draw();
    this.posX = posX;
    this.posY = posY; 
}
//Checkt ob der Punkt im Rectangle liegt
Rectangle.prototype.collide = function (x, y) {
    if (this.x < x && this.x + this.w > x && this.y < y && this.y + this.h > y)
        return true;
    else
        return false;
}

Rectangle.prototype.draw = function () {
    //Zeichnet sich selbst
    mContext.strokeStyle = "rgb(0,0,0)"; //Randfarbe
    mContext.fillStyle = this.color; //Die Farbe der "Buttons";
    mContext.fillRect(this.x, this.y, this.w, this.h);
    mContext.strokeRect(this.x, this.y, this.w, this.h); //Der Rand
    //Zeichnet den Text
    mContext.fillStyle = "rgb(255,255,255)";
    mContext.font = this.font;
    mContext.textAlign = "center";
    mContext.textBaseline = "middle";
    mContext.fillText(this.text, this.x + this.w / 2, this.y + this.h / 2);
}

function finish() {
    var resultString = "";
    var duration = parseInt((new Date().getTime() - startTime.getTime()) / 1000);
    resultString += duration + "##" + rectangle2d.length + "##" + rectangle2d[0].length + "##";


    var items = 0;
	
    for (var i = 0; i < mResults.length; i++) {
        resultString += mResults[i].posX + ", " ;
        resultString += mResults[i].posY + ", " ;
        resultString += mResults[i].zahl + ", " ;
        resultString += mResults[i].zeit + ", " ;
		items++;
    }
    var ddd = resultString.split(",");
    console.log(resultString);

	if(items != 0){
		document.getElementById("Finished").value = true;
    }	
	document.getElementById("OutputParameters").value = resultString;
    document.getElementById("ExerciseSubmit").submit();
}

function setLang(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0){
            switch(c.substring(nameEQ.length,c.length))
            {
                case "cs": lang = 0; break;
                case "en": lang = 1; break;
                case "de": lang = 2; break;
                default: lang = 0; break;
            }
        }
    }
}

var lang = 0;

var InputError = ["Špatný vstup", "Input Error", "Input Error"];
var startExercise = ["Začínáme", "Start", "Starten"];
var fatalError = ["Nastala chyba", "Fatal error", "Fatal error"];
var nextRound = ["Následuje kolo", "Next Round", "Následuje kolo"];