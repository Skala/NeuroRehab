const PUZZLE_HOVER_TINT = '#009900';

var mParam;
var mColumns;
var mRows;
var mImageName;
var mCanvas;

var mScale;

var _stage;


var _img;
var _pieces;
var _puzzleWidth;
var _puzzleHeight;
var _pieceWidth;
var _pieceHeight;
var _currentPiece;
var _currentDropPiece;  

var _mouse;

function runExercise(param){
	mParam = param.split("##"); 
	var inputNo = 0;
	mColumns = parseInt(mParam[inputNo++]);
	mRows = parseInt(mParam[inputNo++]);
	mImageName = mParam[inputNo++];
	
	_img = new Image();
	_img.addEventListener('load',onImage,false);
	_img.src = mImageName;
}
function onImage(e){
	
	mCanvas = document.getElementById('mCanvas');
	_stage = mCanvas.getContext('2d');

	mScale = _img.width / mCanvas.clientWidth;

	mCanvas.width = mCanvas.clientWidth;
	mCanvas.height = Math.floor(_img.height / mScale);
	
	
	//_stage.scale(0.66, 0.66);


	// _img.height = _img.height * mScale;
	// _img.width = _img.width * mScale;
	debugger;
	// _img.width = Math.floor(mCanvas.width);
	// _img.height = Math.floor(mCanvas.height);

	


	_pieceWidth = Math.floor(mCanvas.width / mColumns)
	_pieceHeight = Math.floor(mCanvas.height / mRows)
	_puzzleWidth = _pieceWidth * mColumns;
	_puzzleHeight = _pieceHeight * mRows;

	mCanvas.style.border = "2px solid black";
	//setCanvas();
	initPuzzle();
	
	
}

function initPuzzle(){
	_pieces = [];
	_mouse = {x:0,y:0};
	_currentPiece = null;
	_currentDropPiece = null;
	_stage.drawImage(_img, 0, 0, _puzzleWidth, _puzzleHeight, 0, 0, _puzzleWidth, _puzzleHeight);
	createTitle("Click to Start Puzzle");
	buildPieces();
}
function createTitle(msg){
	_stage.fillStyle = "#000000";
	_stage.globalAlpha = .4;
	_stage.fillRect(100,_puzzleHeight - 40,_puzzleWidth - 200,40);
	_stage.fillStyle = "#FFFFFF";
	_stage.globalAlpha = 1;
	_stage.textAlign = "center";
	_stage.textBaseline = "middle";
	_stage.font = "20px Arial";
	_stage.fillText(msg,_puzzleWidth / 2,_puzzleHeight - 20);
}
function buildPieces(){
	var i;
	var piece;
	var xPos = 0;
	var yPos = 0;
	for(i = 0;i < mColumns * mRows;i++){
		piece = {};
		piece.sx = xPos;
		piece.sy = yPos;
		_pieces.push(piece);
		xPos += _pieceWidth;
		if(xPos >= _puzzleWidth){
			xPos = 0;
			yPos += _pieceHeight;
		}
	}
	document.onmousedown = shufflePuzzle;
}
function shufflePuzzle(){
	_pieces = shuffleArray(_pieces);
	_stage.clearRect(0,0,_puzzleWidth,_puzzleHeight);
	var i;
	var piece;
	var xPos = 0;
	var yPos = 0;
	for(i = 0;i < _pieces.length;i++){
		piece = _pieces[i];
		piece.xPos = xPos;
		piece.yPos = yPos;
		_stage.drawImage(_img, piece.sx, piece.sy, _pieceWidth, _pieceHeight, xPos, yPos, _pieceWidth, _pieceHeight);
		_stage.strokeRect(xPos, yPos, _pieceWidth,_pieceHeight);
		xPos += _pieceWidth;
		if(xPos >= _puzzleWidth){
			xPos = 0;
			yPos += _pieceHeight;
		}
	}
	document.onmousedown = onPuzzleClick;
}
function onPuzzleClick(e){
	if(e.layerX || e.layerX == 0){
		_mouse.x = e.layerX;// - mCanvas.offsetLeft;
		_mouse.y = e.layerY;// - mCanvas.offsetTop;
	}
	else if(e.offsetX || e.offsetX == 0){
		_mouse.x = e.offsetX;// - mCanvas.offsetLeft;
		_mouse.y = e.offsetY;// - mCanvas.offsetTop;
	}
	_currentPiece = checkPieceClicked();
	if(_currentPiece != null){
		_stage.clearRect(_currentPiece.xPos,_currentPiece.yPos,_pieceWidth,_pieceHeight);
		_stage.save();
		_stage.globalAlpha = .9;
		_stage.drawImage(_img, _currentPiece.sx, _currentPiece.sy, _pieceWidth, _pieceHeight, _mouse.x - (_pieceWidth / 2), _mouse.y - (_pieceHeight / 2), _pieceWidth, _pieceHeight);
		_stage.restore();
		document.onmousemove = updatePuzzle;
		document.onmouseup = pieceDropped;
	}
}
function checkPieceClicked(){
	var i;
	var piece;
	for(i = 0;i < _pieces.length;i++){
		piece = _pieces[i];
		if(_mouse.x < piece.xPos || _mouse.x > (piece.xPos + _pieceWidth) || _mouse.y < piece.yPos || _mouse.y > (piece.yPos + _pieceHeight)){
			//PIECE NOT HIT
		}
		else{
			return piece;
		}
	}
	return null;
}
function updatePuzzle(e){
	_currentDropPiece = null;
	if(e.layerX || e.layerX == 0){
		_mouse.x = e.layerX;// - mCanvas.offsetLeft;
		_mouse.y = e.layerY;// - mCanvas.offsetTop;
	}
	else if(e.offsetX || e.offsetX == 0){
		_mouse.x = e.offsetX;// - mCanvas.offsetLeft;
		_mouse.y = e.offsetY;// - mCanvas.offsetTop;
	}
	_stage.clearRect(0,0,_puzzleWidth,_puzzleHeight);
	var i;
	var piece;
	for(i = 0;i < _pieces.length;i++){
		piece = _pieces[i];
		if(piece == _currentPiece){
			continue;
		}
		_stage.drawImage(_img, piece.sx, piece.sy, _pieceWidth, _pieceHeight, piece.xPos, piece.yPos, _pieceWidth, _pieceHeight);
		_stage.strokeRect(piece.xPos, piece.yPos, _pieceWidth,_pieceHeight);
		if(_currentDropPiece == null){
			if(_mouse.x < piece.xPos || _mouse.x > (piece.xPos + _pieceWidth) || _mouse.y < piece.yPos || _mouse.y > (piece.yPos + _pieceHeight)){
				//NOT OVER
			}
			else{
				_currentDropPiece = piece;
				_stage.save();
				_stage.globalAlpha = .4;
				_stage.fillStyle = PUZZLE_HOVER_TINT;
				_stage.fillRect(_currentDropPiece.xPos,_currentDropPiece.yPos,_pieceWidth, _pieceHeight);
				_stage.restore();
			}
		}
	}
	_stage.save();
	_stage.globalAlpha = .6;
	_stage.drawImage(_img, _currentPiece.sx, _currentPiece.sy, _pieceWidth, _pieceHeight, _mouse.x - (_pieceWidth / 2), _mouse.y - (_pieceHeight / 2), _pieceWidth, _pieceHeight);
	_stage.restore();
	_stage.strokeRect( _mouse.x - (_pieceWidth / 2), _mouse.y - (_pieceHeight / 2), _pieceWidth,_pieceHeight);
}
function pieceDropped(e){
	document.onmousemove = null;
	document.onmouseup = null;
	if(_currentDropPiece != null){
		var tmp = {xPos:_currentPiece.xPos,yPos:_currentPiece.yPos};
		_currentPiece.xPos = _currentDropPiece.xPos;
		_currentPiece.yPos = _currentDropPiece.yPos;
		_currentDropPiece.xPos = tmp.xPos;
		_currentDropPiece.yPos = tmp.yPos;
	}
	resetPuzzleAndCheckWin();
}
function resetPuzzleAndCheckWin(){
	_stage.clearRect(0,0,_puzzleWidth,_puzzleHeight);
	var gameWin = true;
	var i;
	var piece;
	for(i = 0;i < _pieces.length;i++){
		piece = _pieces[i];
		_stage.drawImage(_img, piece.sx, piece.sy, _pieceWidth, _pieceHeight, piece.xPos, piece.yPos, _pieceWidth, _pieceHeight);
		_stage.strokeRect(piece.xPos, piece.yPos, _pieceWidth,_pieceHeight);
		if(piece.xPos != piece.sx || piece.yPos != piece.sy){
			gameWin = false;
		}
	}
	if(gameWin){
		setTimeout(gameOver,500);
	}
}
function gameOver(){
	document.onmousedown = null;
	document.onmousemove = null;
	document.onmouseup = null;
	initPuzzle();
}
function shuffleArray(o){
	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}


// const PUZZLE_HOVER_TINT = '#009900';

// var mParam;
// var mColumns;
// var mRows;
// var mImageName;
// var mCanvas;

// var _context;
// var _img;

// var _pieces;


// var _sPieceWidth;
// var _sPieceHeight;
// var _dPieceWidth;
// var _dPieceHeight;

// var _sPuzzleWidth;
// var _sPuzzleHeight;
// var _dPuzzleWidth;
// var _dPuzzleHeight;

// var _scale = 1;

// var _currentPiece;
// var _currentDropPiece;  

// var _mouse;

// function runExercise(param){
// 	mParam = param.split("##"); 
// 	var inputNo = 0;
// 	mColumns = parseInt(mParam[inputNo++]);
// 	mRows = parseInt(mParam[inputNo++]);
// 	mImageName = mParam[inputNo++];

// 	_img = new Image();
// 	_img.addEventListener('load',onImage,false);
// 	_img.src = mImageName;
// }
// function onImage(e){
	
// 	mCanvas = document.getElementById('mCanvas');
// 	mCanvas.width = mCanvas.clientWidth;
// 	mCanvas.height = Math.floor(mCanvas.clientWidth/1.77);
	
// 	_context = mCanvas.getContext('2d');

// 	debugger;	
// 	_sPieceWidth = Math.floor(_context.width / mColumns)
// 	_sPieceHeight = Math.floor(_context.height / mRows)
// 	_sPuzzleWidth = _sPieceWidth * mColumns;
// 	_sPuzzleHeight = _sPieceHeight * mRows;

// 	// _scale = mCanvas.clientWidth/_img.width;
// debugger;
// 	mCanvas.clientWidth = _sPuzzleWidth;
//  	mCanvas.clientHeight = _sPuzzleHeight;
// 	mCanvas.style.border = "1px solid black";

// 	_context.drawImage(_img, 0, 0, _img.width, _img.height, 0, 0, _img.width, _img.height);

// 	//setCanvas();
// 	initPuzzle();
// }
// // function setCanvas(){
// // 	//mCanvas = document.getElementById('mCanvas');
// // 	//_context = mCanvas.getContext('2d');
// // 	//mCanvas.width = _sPuzzleWidth;
// // 	//mCanvas.height = _sPuzzleHeight;
// // 	//mCanvas.style.border = "2px solid black";
// // }


// function initPuzzle(){
// 	_pieces = [];
// 	_mouse = {x:0,y:0};
// 	_currentPiece = null;
// 	_currentDropPiece = null;
// 	_context.drawImage(_img, 0, 0, _sPuzzleWidth, _sPuzzleHeight, 0, 0, _sPuzzleWidth, _sPuzzleHeight);
// 	createTitle("Click to Start Puzzle");
// 	buildPieces();
// }
// function createTitle(msg){
// 	_context.fillStyle = "#000000";
// 	_context.globalAlpha = .4;
// 	_context.fillRect(100,_sPuzzleHeight - 40,_sPuzzleWidth - 200,40);
// 	_context.fillStyle = "#FFFFFF";
// 	_context.globalAlpha = 1;
// 	_context.textAlign = "center";
// 	_context.textBaseline = "middle";
// 	_context.font = "20px Arial";
// 	_context.fillText(msg,_sPuzzleWidth / 2,_sPuzzleHeight - 20);
// }
// function buildPieces(){
// 	var i;
// 	var piece;
// 	var xPos = 0;
// 	var yPos = 0;
// 	for(i = 0;i < mColumns * mRows;i++){
// 		piece = {};
// 		piece.sx = xPos;
// 		piece.sy = yPos;
// 		_pieces.push(piece);
// 		xPos += _sPieceWidth;
// 		if(xPos >= _sPuzzleWidth){
// 			xPos = 0;
// 			yPos += _sPieceHeight;
// 		}
// 	}
// 	document.onmousedown = shufflePuzzle;
// }
// function shufflePuzzle(){
// 	_pieces = shuffleArray(_pieces);
// 	_context.clearRect(0,0,_sPuzzleWidth,_sPuzzleHeight);
// 	var i;
// 	var piece;
// 	var xPos = 0;
// 	var yPos = 0;
// 	for(i = 0;i < _pieces.length;i++){
// 		piece = _pieces[i];
// 		piece.xPos = xPos;
// 		piece.yPos = yPos;
// 		_context.drawImage(_img, piece.sx, piece.sy, _sPieceWidth, _sPieceHeight, xPos, yPos, _sPieceWidth, _sPieceHeight);
// 		_context.strokeRect(xPos, yPos, _sPieceWidth,_sPieceHeight);
// 		xPos += _sPieceWidth;
// 		if(xPos >= _sPuzzleWidth){
// 			xPos = 0;
// 			yPos += _sPieceHeight;
// 		}
// 	}
// 	document.onmousedown = onPuzzleClick;
// }
// function onPuzzleClick(e){
// 	if(e.layerX || e.layerX == 0){
// 		_mouse.x = e.layerX; //- mCanvas.offsetLeft;
// 		_mouse.y = e.layerY; // - mCanvas.offsetTop;
// 		debugger;
// 	}
// 	else if(e.offsetX || e.offsetX == 0){
// 		_mouse.x = e.offsetX;// - mCanvas.offsetLeft;
// 		_mouse.y = e.offsetY;// - mCanvas.offsetTop;
// 		debugger;
// 	}
// 	_currentPiece = checkPieceClicked();
// 	if(_currentPiece != null){
// 		_context.clearRect(_currentPiece.xPos,_currentPiece.yPos,_sPieceWidth,_sPieceHeight);
// 		_context.save();
// 		_context.globalAlpha = .9;
// 		_context.drawImage(_img, _currentPiece.sx, _currentPiece.sy, _sPieceWidth, _sPieceHeight, _mouse.x - (_sPieceWidth / 2), _mouse.y - (_sPieceHeight / 2), _sPieceWidth, _sPieceHeight);
// 		_context.restore();
// 		document.onmousemove = updatePuzzle;
// 		document.onmouseup = pieceDropped;
// 	}
// }
// function checkPieceClicked(){
// 	var i;
// 	var piece;
// 	for(i = 0;i < _pieces.length;i++){
// 		piece = _pieces[i];
// 		if(_mouse.x < piece.xPos || _mouse.x > (piece.xPos + _sPieceWidth) || _mouse.y < piece.yPos || _mouse.y > (piece.yPos + _sPieceHeight)){
// 			//PIECE NOT HIT
// 		}
// 		else{
// 			return piece;
// 		}
// 	}
// 	return null;
// }
// function updatePuzzle(e){
// 	_currentDropPiece = null;
// 	if(e.layerX || e.layerX == 0){
// 		_mouse.x = e.layerX; //- mCanvas.offsetLeft;
// 		_mouse.y = e.layerY; // - mCanvas.offsetTop;
// 	}
// 	else if(e.offsetX || e.offsetX == 0){
// 		_mouse.x = e.offsetX; // - mCanvas.offsetLeft;
// 		_mouse.y = e.offsetY; // - mCanvas.offsetTop;
// 	}
// 	_context.clearRect(0,0,_sPuzzleWidth,_sPuzzleHeight);
// 	var i;
// 	var piece;
// 	for(i = 0;i < _pieces.length;i++){
// 		piece = _pieces[i];
// 		if(piece == _currentPiece){
// 			continue;
// 		}
// 		_context.drawImage(_img, piece.sx, piece.sy, _sPieceWidth, _sPieceHeight, piece.xPos, piece.yPos, _sPieceWidth, _sPieceHeight);
// 		_context.strokeRect(piece.xPos, piece.yPos, _sPieceWidth,_sPieceHeight);
// 		if(_currentDropPiece == null){
// 			if(_mouse.x < piece.xPos || _mouse.x > (piece.xPos + _sPieceWidth) || _mouse.y < piece.yPos || _mouse.y > (piece.yPos + _sPieceHeight)){
// 				//NOT OVER
// 			}
// 			else{
// 				_currentDropPiece = piece;
// 				_context.save();
// 				_context.globalAlpha = .4;
// 				_context.fillStyle = PUZZLE_HOVER_TINT;
// 				_context.fillRect(_currentDropPiece.xPos,_currentDropPiece.yPos,_sPieceWidth, _sPieceHeight);
// 				_context.restore();
// 			}
// 		}
// 	}
// 	_context.save();
// 	_context.globalAlpha = .6;
// 	_context.drawImage(_img, _currentPiece.sx, _currentPiece.sy, _sPieceWidth, _sPieceHeight, _mouse.x - (_sPieceWidth / 2), _mouse.y - (_sPieceHeight / 2), _sPieceWidth, _sPieceHeight);
// 	_context.restore();
// 	_context.strokeRect( _mouse.x - (_sPieceWidth / 2), _mouse.y - (_sPieceHeight / 2), _sPieceWidth,_sPieceHeight);
// }
// function pieceDropped(e){
// 	document.onmousemove = null;
// 	document.onmouseup = null;
// 	if(_currentDropPiece != null){
// 		var tmp = {xPos:_currentPiece.xPos,yPos:_currentPiece.yPos};
// 		_currentPiece.xPos = _currentDropPiece.xPos;
// 		_currentPiece.yPos = _currentDropPiece.yPos;
// 		_currentDropPiece.xPos = tmp.xPos;
// 		_currentDropPiece.yPos = tmp.yPos;
// 	}
// 	resetPuzzleAndCheckWin();
// }
// function resetPuzzleAndCheckWin(){
// 	_context.clearRect(0,0,_sPuzzleWidth,_sPuzzleHeight);
// 	var gameWin = true;
// 	var i;
// 	var piece;
// 	for(i = 0;i < _pieces.length;i++){
// 		piece = _pieces[i];
// 		_context.drawImage(_img, piece.sx, piece.sy, _sPieceWidth, _sPieceHeight, piece.xPos, piece.yPos, _sPieceWidth, _sPieceHeight);
// 		_context.strokeRect(piece.xPos, piece.yPos, _sPieceWidth,_sPieceHeight);
// 		if(piece.xPos != piece.sx || piece.yPos != piece.sy){
// 			gameWin = false;
// 		}
// 	}
// 	if(gameWin){
// 		setTimeout(gameOver,500);
// 	}
// }
// function gameOver(){
// 	document.onmousedown = null;
// 	document.onmousemove = null;
// 	document.onmouseup = null;
// 	initPuzzle();
// }
// function shuffleArray(o){
// 	for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
// 	return o;
// }