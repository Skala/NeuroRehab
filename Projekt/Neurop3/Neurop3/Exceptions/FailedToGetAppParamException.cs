﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neurop3.Exceptions
{
    /// <summary>
    /// oddedena trida od Exception. Pouziva se pri chybe nacteni parametru z webconfigu
    /// </summary>
    public class FailedToGetAppParamException: Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public FailedToGetAppParamException()
        {
        }

        public FailedToGetAppParamException(string message): base(message)
        {
        }
    }
}