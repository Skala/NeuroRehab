﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Neurop3.Services;
using Microsoft.AspNet.Identity;
using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Models.Exercises;
using Neurop3.Helpers;

namespace Neurop3.Controllers
{ 
    /// <summary>
    /// Kontroler pro spravu uloh
    /// </summary>
    [Authorize]
    public class ExerciseController : Controller
    {
        private readonly IExerciseService _es;

        public ExerciseController(IExerciseService ies)
        {
            this._es = ies;
        }

        #region CRUD
        /// <summary>
        /// Stranka pro zobrazeni formulare pro vytvoreni ulohy
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Create()
        {
            var model = new ExerciseCreateViewModel();
            ViewBag.ProgramList = _es.GetProgramList();
            return View(model);
        }

        /// <summary>
        /// Akce pro vytvoreni nove ulohy
        /// </summary>
        /// <param name="model">model vytvarene ulohy</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Create(ExerciseCreateViewModel model)
        {
            //seznam musi byt soucasti viewbagu pri nevalidnim vstupu
            ViewBag.ProgramList = _es.GetProgramList();
            if (ModelState.IsValid)
            {
                //nesmi byt vytovrena uloha se stejnym nazvem
                if (_es.ExistName(model.Name, 0))
                {
                    ModelState.AddModelError("", Resources.Global.uloha_s_timto_nazvem_jiz_existuje);
                    return View(model);
                }
                var exerciseId = _es.Create(model);
                return RedirectToAction("Manage", new { Id = exerciseId });
            }
            return View(model);
        }

        /// <summary>
        /// Akce pro duplikaci dane ulohy
        /// </summary>
        /// <param name="id">Identifikator duplikovane ulohy</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Duplicate(int id)
        {
            int duplicateId = _es.Duplicate(id);
            return RedirectToAction("Manage", new { @Id = duplicateId });
        }

        /// <summary>
        /// Akce pro smazani dane ulohy
        /// </summary>
        /// <param name="id">Identifikator mazane ulohy</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Delete(int id)
        {
            var deleted = _es.Remove(id);
            return deleted ? RedirectToAction("List") : null;
        }

        /// <summary>
        /// Akce pro obnoveni neaktivni ulohy.
        /// </summary>
        /// <param name="id">Identifikator ulohy, ktera ma byt obnovena.</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Activate(int id)
        {
            var activated = _es.Activate(id);
            return activated ? RedirectToAction("List") : null;
        }

        /// <summary>
        /// Akce pro deaktivaci dane ulohy
        /// </summary>
        /// <param name="id">Indentifikator pro deaktivovani ulohy</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Deactivate(int id)
        {
            var deactivated = _es.Deactivate(id);
            return deactivated ? RedirectToAction("List") : null;
        }

        /// <summary>
        /// Stranka pro editaci dane ulohy
        /// </summary>
        /// <param name="Id">Identifikator ulohy, ktera ma byt editovana.</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Manage(int Id)
        {
            var exercise = _es.GetExerciseVM(Id);
            if (exercise != null)
            {
                return View(exercise);
            }
            return null;
        }

        /// <summary>
        /// Akce pro ulozeni editovane ulohy. 
        /// </summary>
        /// <param name="model">Aktualizovany model editovane ulohy</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Manage(ExerciseManageViewModel model)
        {
            if (ModelState.IsValid || model.HasAssign)
            {
                //test na existenci nazvu, aby nebyly vytvoreny dve ulohy se stejnym nazvem.
                if (_es.ExistName(model.Name, model.Id))
                {
                    ModelState.AddModelError("", Resources.Global.nazev_teto_ulohy_jiz_existuje);
                    model = _es.GetExerciseVM(model.Id);
                    return View(model);
                }

                //TODO SMAZAT -- tohle je mozna zbytecne, protoze se soubory ukladaji prostrednictvim modalu hned!!!
                if (Request.Files.Count > 0)
                {
                    model.Files = Request.Files;
                }

                var updated = _es.Update(model);

                if (updated) {
                    FlashMessages.CreateFlashMessage("fm-info", Resources.Global.uloha_byla_ulozena, false, 3000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
                }
                return updated ? RedirectToAction("Manage", new { @Id = model.Id }) : null;
            }
            else
            {
                model = _es.GetExerciseVM(model.Id);
                return View(model);
            }
        }
        #endregion

        /// <summary>
        /// Stranka pro zobrazeni vsech uloh, ktere odpovidaji filtru v databazi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult List()
        {
            var model = _es.ListVM();
            return View(model);
        }

        #region ajaxove volani

        /// <summary>
        /// Ajaxove volani pro zobrazeni uloh, ktere odpovidaji prave aktualizovanemu filteru. Soucasti akce je prepsani filtru v databazi
        /// </summary>
        /// <param name="text">Nazev uloh oddelenych strednikem</param>
        /// <param name="isActive">Paremetr aktivity</param>
        /// <param name="isPublic">Parametr verejnosti</param>
        /// <param name="recordPerPage">Pocet zaznamu na stranku</param>
        /// <param name="currentPage">Cisla aktualni stranky</param>
        /// <param name="exerciseIds">Seznam identifikatoru uloh pro zobrazeni</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult ListFilter(string text, int isActive, int isPublic, int recordPerPage, int currentPage, string exerciseIds)
        {
            var currentUserId = User.Identity.GetUserId();
            //aktualizace filtru a ziskani dat
            var model = _es.SetListFilterAndGetData(new ExerciseListFilter() { IsActive = isActive, IsPublic = isPublic, Text = text, CurrentPage = currentPage, ExerciseIds = exerciseIds, RecordsPerPage = recordPerPage, UserId = currentUserId });

            int currentP = model.Filter.CurrentPage > 0 ? model.Filter.CurrentPage : 1;

            ViewBag.totalRecords = model.Filter.TotalRecords;
            ViewBag.rowStartsAt = model.Filter.RecordsPerPage * (currentP - 1) + 1;
            ViewBag.currentPage = currentP;

            return View("EditorTemplates/_ExerciseListTable", model.ExerciseVMs);
        }

        
        //TODO SMAZAT -- nejspíš se nepouziva
        /// <summary>
        /// Akce pro prirazeni ulohy pacientovi
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult AddExerciseAssign(string patientId, int exerciseId)
        {
            var model = _es.AddExerciseAssign(patientId, exerciseId);
            return View("EditorTemplates/_ExerciseAssignTableRow", model);
        }

        /// <summary>
        /// Metoda pro zjisteni, zda jiz existuje prirazeni konkretni ulohy s pacientem
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public JsonResult AssignExists(string patientId, int exerciseId)
        {
            bool result = _es.AssignExists(patientId, exerciseId);
            return Json(new { success = result });
        }


        //TODO Predelano -- na packagetopatient
        //[HttpPost]
        //[Authorize(Roles = "Superadmin, Patient")]
        //public ActionResult GetExerciseAssignToPatient()
        //{
        //    var userId = User.Identity.GetUserId();
        //    var models = _es.GetAssigns().Where(x => x.PatientId == userId).Where( x => x.IsActive == true).Where(x => x.IsUsersAssignActive == true).OrderBy(x => x.AssignDate).ToList();
        //    return models != null && models.Any() ? View(@"~\Views\Home\EditorTemplates\Patient\_ExerciseAssignToPatient.cshtml", models) : null;
        //}
        
        
        //TODO Predelano -- na packagebytherapist
        //[HttpPost]
        //[Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        //public ActionResult GetExerciseAssignByTherapist()
        //{
        //    var userId = User.Identity.GetUserId();
        //    var models = _es.GetAssigns().Where(x => x.TherapistId == userId).Where(x => x.IsActive == true).Where(x => x.IsUsersAssignActive == true).OrderBy(x => x.AssignDate).ToList();
        //    return models != null && models.Any() ? View(@"~\Views\Home\EditorTemplates\Therapist\_ExerciseAssignByTherapist.cshtml", models) : null;
        //}

        //TODO Predelano -- na moje baliky
        //[HttpGet]
        //[Authorize(Roles = "Superadmin, Patient")]
        //public ActionResult MyExercises()
        //{
        //    var currentUserId = User.Identity.GetUserId();
        //    //var assigns = _es.GetExerciseAssigns().Where(x => x.PatientId.CompareTo(currentUserId) == 0).ToList();
        //    var assigns = _es.GetAssigns().Where(x => x.PatientId.CompareTo(currentUserId) == 0 && x.IsUsersAssignActive == true).OrderBy(x => x.LastAccessDate).ToList();
        //    return View(assigns);
        //}
        #endregion


        /// <summary>
        /// Akce pro ulozeni souboru jako vstupniho parametru do ulohy
        /// </summary>
        /// <param name="selectedRow">Poradove cislo parametru ulohy</param>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <param name="programId">Identifikator programu</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UploadFile(string selectedRow, int exerciseId, int programId)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    _es.SaveFile(file, Int32.Parse(selectedRow), exerciseId, programId);
                }
            }
            return RedirectToAction("List");
        }
    }
}
