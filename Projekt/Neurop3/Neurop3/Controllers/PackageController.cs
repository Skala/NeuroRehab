﻿using Neurop3.Common;
using Neurop3.Models.Packages;
using Neurop3.Models.ViewModels.Package;
using Neurop3.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Neurop3.Helpers;

namespace Neurop3.Controllers
{
    /// <summary>
    /// Kontroler pro spravu baliku
    /// </summary>
    [Authorize]
    public class PackageController : Controller
    {
        private readonly IPackageService _ps;

        public PackageController(IPackageService ps)
        {
            this._ps = ps;
        }

        #region CRUD
        /// <summary>
        /// Stranka s formularem pro zalozeni noveho baliku
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult Create()
        {
            return View(new PackageCreateViewModel());
        }

        /// <summary>
        /// Akce pro zalozeni noveho baliku na zaklade dat z modelu
        /// </summary>
        /// <param name="model">Model zakladaneho baliku</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult Create(PackageCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_ps.ExistName(model.Name, 0))
                {
                    ModelState.AddModelError("", Resources.Global.balik_s_timto_nazvem_jiz_existuje);
                    return View(model);
                }
                var created = _ps.Create(model);
                return created > 0 ? RedirectToAction("Manage", new { id = created }) : null;
            }
            return View(model);
        }

        /// <summary>
        /// Akce pro zobrazeni editace konkretniho baliku
        /// </summary>
        /// <param name="id">Identifikator baliku, ktery ma byt editovat</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult Manage(int id, int? tab)
        {
            ViewBag.tab = tab ?? 1;
            var model = _ps.GetManageVM(id);
            return model != null ? View(model) : null;
        }

        /// <summary>
        /// Akce pro ulozeni editovaneho baliku
        /// </summary>
        /// <param name="model">Aktualizovany model baliku</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult Manage(PackageManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_ps.ExistName(model.Name, model.Id))
                {
                    ModelState.AddModelError("", Resources.Global.balik_s_timto_nazvem_jiz_existuje);
                    return View(model);
                }
                var updated = _ps.Update(model);
                if (updated == RequestResult.Success)
                {
                    FlashMessages.CreateFlashMessage("fm-info", Resources.Global.balik_byl_ulozen, false, 3000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
                }
                return updated ==RequestResult.Success ? RedirectToAction("Manage", new { @id = model.Id }) : null;
            }
            return View(model);
        }

        /// <summary>
        /// Akce pro smazani pozadovaneho baliku
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult Delete(int id)
        {
            var deleted = _ps.Delete(id);
            if(deleted == RequestResult.Success)
            {
                FlashMessages.CreateFlashMessage("fm-info", Resources.Global.balik_byl_smazan, false, 3000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
                return RedirectToAction("List");
            }
            return null;
        }


        #endregion

        #region Lists
        /// <summary>
        /// Akce pro zobrazeni seznamu baliku
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult List()
        {
            var model = _ps.ListVM();
            return View(model);
        }

        /// <summary>
        /// Akce pro nastaveni noveho filtru a ziskani pozadovanych baliku
        /// </summary>
        /// <param name="patientName">Jmeno pacienta</param>
        /// <param name="packageName">Nazev baliku</param>
        /// <param name="isActive">Aktivita baliku</param>
        /// <param name="recordPerPage">Pocet zaznamu na stranku</param>
        /// <param name="currentPage">Aktualni stranka</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult ListFilter(string patientName, string packageName, int isActive, int recordPerPage, int currentPage)
        {
            var currentUserId = User.Identity.GetUserId();
            var model = _ps.SetListFilterAndGetData(new PackageListFilter() { IsActive = isActive, PatientName = patientName, PackageName = packageName, CurrentPage = currentPage, RecordsPerPage = recordPerPage, UserId = currentUserId });

            int currentP = model.Filter.CurrentPage > 0 ? model.Filter.CurrentPage : 1;

            ViewBag.totalRecords = model.Filter.TotalRecords;
            ViewBag.rowStartsAt = model.Filter.RecordsPerPage * (currentP - 1) + 1;
            ViewBag.currentPage = currentP;

            return View("EditorTemplates/_PackageListTable", model.PackageViewModels);
        }
        #endregion

        #region Pomocne sluzby
        /// <summary>
        /// Akce pro odebrani konkretni ulohy z baliku
        /// </summary>
        /// <param name="packageId">Identifikator baliku</param>
        /// <param name="order">Poradove cislo ulohy v baliku</param>
        /// <returns></returns>
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult RemoveExerciseAssignFromPackage(int packageId, int order)
        {
            var removed = _ps.RemoveExerciseAssignFromPackage(packageId, order);
            return RedirectToAction("Manage", new { id = packageId, tab = 2});
        }

        /// <summary>
        /// Akrce pro ziskani seznamu vsech baliku, ktere si muze prihlaseny pacient zobrazit a zaroven spustit
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Patient")]
        public ActionResult PatientPackages()
        {
            var packages = _ps.GetPackageAssignToPatient();
            return View(packages);
        }

        #endregion


        #region Ajax
        /// <summary>
        /// Akce pro duplikaci konkretniho baliku pro nektereho z dalsich pacientu
        /// </summary>
        /// <param name="packageId">Identifikator baliku</param>
        /// <param name="packageName">Nazev duplikovaneho (noveho) baliku</param>
        /// <param name="patientId">Identifikator pacienta, kteremu je duplikovany balik prirazen</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public JsonResult Duplicate(int packageId, string packageName, string patientId)
        {
            var duplicated = _ps.Duplicate(packageId, packageName, patientId);
            ModelState.Clear();
            return Json(new { data = duplicated });
        }

        /// <summary>
        /// Akce pro ziskani vsech baliku, ktere ma pacient moznost spustit
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Patient")]
        public ActionResult GetPackageAssignToPatient()
        {
            var model = _ps.GetPackageAssignToPatient();
            return model != null && model.Any() ? View(@"~\Views\Home\EditorTemplates\Patient\_PackageAssignToPatient.cshtml", model) : null;

        }

        //TODO UPRAVIT -- protoze terapeut ma na homepage vsechny aktivni baliky
        /// <summary>
        /// Akce pro ziskani vsech baliku, ktere terapeut vytvoril a lze je analyzovat
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult GetPackageAssignByTherapist()
        {
            var model = _ps.GetPackageAssignByTherapist();
            var d = model.ToList();
            return model != null && model.Any() ? View(@"~\Views\Home\EditorTemplates\Therapist\_PackageAssignByTherapist.cshtml", model) : null;
        }


        /// <summary>
        /// Akce pro nalezeni konkretni ulohy, kterou bychom chteli pridat do baliku
        /// </summary>
        /// <param name="input">Podretezec hledane ulohy</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public JsonResult GetExerciseListToPackageAdd(string input)
        {
            var exercise = _ps.GetExerciseListToPackageAdd(input);
            var exerciseList = exercise.Select(x => new { id = x.Id, name = x.Name });
            return Json(exerciseList.ToArray());
        }

        /// <summary>
        /// Akce pro pridani konkretni ulohy do baliku
        /// </summary>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <param name="packageId">Identifikator baliku</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public JsonResult AddExerciseToPackage(int exerciseId, int packageId)
        {
            var assignCreated = _ps.AddExerciseToPackage(exerciseId, packageId);
            ViewBag.showTab = "packageAssign";
            return Json(new { success = assignCreated == RequestResult.Success ? true : false });
        }
        #endregion
    }
}