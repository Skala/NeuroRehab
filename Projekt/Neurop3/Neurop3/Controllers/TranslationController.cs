﻿using Neurop3.Common;
using Neurop3.Models.Administration;
using Neurop3.Models.ViewModels.Translation;
using Neurop3.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Neurop3.Controllers
{
    /// <summary>
    /// Kontroler pro spravu prekladu
    /// </summary>
    [Authorize]
    public class TranslationController : Controller
    {
        public ITranslationService _ts;


        public TranslationController(ITranslationService ts)
        {
            this._ts = ts;
        }

        #region CRUD
        /// <summary>
        /// Stranka pro zobrazeni formulare pro vytvoreni noveho prekladu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Create()
        {
            return View(new TranslationViewModel());
        }

        /// <summary>
        /// Akce pro vytvoreni noveho prekladu na zaklade vypleneneho formulare
        /// </summary>
        /// <param name="model">Model prekladu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Create(TranslationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_ts.ExistDbName(model.DbColumnName, 0))
                {
                    ModelState.AddModelError("", Resources.Global.preklad_s_timto_databazovym_nazvem_jiz_existuje);
                    return View(model);
                }
                var result = _ts.Create(model);
                if (result == RequestResult.Success)
                {
                    return RedirectToAction("List");
                }
                return null;
            }
            return View(model);
        }

        /// <summary>
        /// Akce pro duplikaci pozadovaneho prekladu
        /// </summary>
        /// <param name="id">Identifikator prekladu, ktery ma byt duplikovan</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Duplicate(int id)
        {
            var duplId = _ts.Duplicate(id);
            return RedirectToAction("Manage", new { id = duplId });
        }

        /// <summary>
        /// Akce pro smazani daneho prekladu
        /// </summary>
        /// <param name="id">Identifikator prekladu ke smazani</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Delete(int id)
        {
            var result = _ts.Delete(id);
            return result == RequestResult.Success ? RedirectToAction("List") : null;
        }
        #endregion


        #region akce
        /// <summary>
        /// V akci na hlavni stranku kontroleru dojde k presmerovani na seznam prekladu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        /// <summary>
        /// Stranka se seznamem vytvorenych prekladu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult List()
        {
            var translations = _ts.ListVM();
            return View(translations);
        }

        /// <summary>
        /// Akce pro zobrazeni pozadovaneho prekladu s moznosti jeho editace
        /// </summary>
        /// <param name="id">Identifikator prekladu</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Manage(int id)
        {
            var model = _ts.GetTranslation(id);
            return View(model);
        }

        /// <summary>
        /// Akce pro aktualizaci konkretniho prekladu
        /// </summary>
        /// <param name="model">Model prekladu, ktery byl modifikovan</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Manage(TranslationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_ts.ExistDbName(model.DbColumnName, model.Id))
                {
                    ModelState.AddModelError("", Resources.Global.preklad_s_timto_databazovym_nazvem_jiz_existuje);
                    return View(model);
                }
                var result = _ts.Update(model);

                return result == RequestResult.Success ? RedirectToAction("List") : null;
            }
            return View(model);
        }
        #endregion

        #region AJAX
        /// <summary>
        /// Metoda pro aktualizaci filtru uzivatele a ziskani prislusnych prekladu
        /// </summary>
        /// <param name="text">Text v prekladu</param>
        /// <param name="recordPerPage">Pocet zaznamu na strance</param>
        /// <param name="currentPage">Aktualni stranka</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult ListFilter(string text, string recordPerPage, string currentPage)
        {
            var model = _ts.ListFilter(new TranslationListFilter()
            {
                CurrentPage = !string.IsNullOrEmpty(currentPage) ? Int32.Parse(currentPage) : 1,
                RecordsPerPage = !string.IsNullOrEmpty(recordPerPage) ? Int32.Parse(recordPerPage) : 10,
                Text = text
            });

            int currentP = !string.IsNullOrEmpty(currentPage) ? Int32.Parse(currentPage) : 1;

            ViewBag.totalRecords = model.Filter.TotalRecords;
            ViewBag.rowStartsAt = model.Filter.RecordsPerPage * (currentP - 1) + 1;
            ViewBag.currentPage = !string.IsNullOrEmpty(currentPage) ? Int32.Parse(currentPage) : 1;

            return View("EditorTemplates/TranslationListTable", model.TranslationViewModels);
        }

        /// <summary>
        /// Akce pro vytvoreni noveho prekladu prostrednictvim ajaxoveho volani
        /// </summary>
        /// <param name="cz">preklad v cs</param>
        /// <param name="en">preklad v en</param>
        /// <param name="de">preklad v de</param>
        /// <param name="dbName">textovy identifikator prekladu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public JsonResult CreateFromModal(string cz, string en, string de, string dbName)
        {
            var result = _ts.Create(new TranslationViewModel() { Cz = cz, En = en, De = de, DbColumnName = dbName });
            return Json(new { success = result > 0 });
        }

        /// <summary>
        /// Akce pro zjisteni, zda pozadovany textovy identifikator v databazi jiz existuje
        /// </summary>
        /// <param name="dbName">Nazev identifikatoru</param>
        /// <param name="translationId">Identifikator prekladu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public JsonResult ExistsDbName(string dbName, int translationId)
        {
            var result = _ts.ExistDbName(dbName, translationId);
            return Json(new { success = result });
        }

        /// <summary>
        /// Akce, ktera vraci JSON strukturu s preklady v pouzivanem jazyce, ktere odpovidaji vstupnimu textu
        /// </summary>
        /// <param name="query">Text, ktery musi byt soucasti prekladu</param>
        /// <returns>Seznam prekladu v danem jazyce</returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public JsonResult GetTranslations(string query)
        {
            var langCookie = Request.Cookies["lang"];
            var locale = langCookie != null && !string.IsNullOrEmpty(langCookie.Value) ? langCookie.Value : "cs";

            var translations = _ts.GetFilteredTranslations(null);
            
            query = query.Trim().ToLower();

            switch (locale.Trim().ToLower())
            {
                case "cs":
                    translations = !string.IsNullOrEmpty(query) ? translations.Where(x => x.Cz.Trim().ToLower().Contains(query)) : translations;
                    translations = translations.OrderBy(x => x.Cz);
                    break;
                case "en":
                    translations = !string.IsNullOrEmpty(query) ? translations.Where(x => x.En.Trim().ToLower().Contains(query)) : translations;
                    translations = translations.OrderBy(x => x.En);
                    break;
                case "de":
                    translations = !string.IsNullOrEmpty(query) ? translations.Where(x => x.De.Trim().ToLower().Contains(query)) : translations;
                    translations = translations.OrderBy(x => x.De);
                    break;
                default:
                    translations = !string.IsNullOrEmpty(query) ? translations.Where(x => x.Cz.Trim().ToLower().Contains(query)) : translations;
                    translations = translations.OrderBy(x => x.Cz);
                    break;
            }

            if(translations != null)
            {
                var transList = translations.ToList().Select(x => new { id = x.Id, name = x.LocalizedTranslation });
                return Json(transList.ToArray());
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}