﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Neurop3.Models;
using Neurop3.Common;

using Neurop3.Services;
using Neurop3.Models.Exercises;
using Neurop3.Models.ViewModels.Test;

namespace Neurop3.Controllers
{
    /// <summary>
    /// Kontroler pro spousteni spousteni baliku a uloh
    /// </summary>
    [Authorize]
    public class TestController : Controller
    {

        private readonly IExerciseService _es;
        private readonly ITestService _ts;
        private readonly IPackageService _ps;

        public TestController(IExerciseService es, ITestService ts, IPackageService ps)
        {
            this._es = es;
            this._ts = ts;
            this._ps = ps;
        }

        #region Stare
        //[HttpGet]
        //[Authorize]
        //public ActionResult RunTest(int assignId)
        //{
        //    var model = _ts.RunTestVM(assignId);
        //    return View(model);
        //}
        //[HttpPost]
        //[Authorize]
        //public ActionResult RunTest(RunTestViewModel model)
        //{
        //    if (model != null && !string.IsNullOrEmpty(model.OutputParameters))
        //    {
        //        var saved = _ts.SaveExerciseResults(model);
        //    }
        //    return RedirectToAction("Index", "Home");
        //    //return saved ? RedirectToAction("Index", "Home") : null ;
        //}
        #endregion


        /// <summary>
        /// Akce pro spusteni daneho baliku s ulohami
        /// </summary>
        /// <param name="packageId">Identifikator baliku</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Patient")]
        public ActionResult RunPackage(int packageId)
        {
            var assignIds = _ts.GetAssignIdsInPackage(packageId);
            var model = _ts.RunTestVM(assignIds.FirstOrDefault());
            model.TotalExercises = assignIds.Count();
            model.MessageBefore = string.Concat(model.MessageBefore, "1/", model.TotalExercises);
            return View(model);
        }

        /// <summary>
        /// Akce pro ukladani vysledku z provedenych uloh a spousteni dalsich uloh, ktere jsou v poradi
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Patient")]
        public ActionResult RunPackage(RunTestViewModel model)
        {
            //pokud byla uloha dokoncena, pak ulozime vysledky
            if (model.Finished)
            {
                var saved = _ts.SaveExerciseResults(model);
            }

            //ziskani vsech identifikatoru prirazeni uloh v danem baliku
            var assignIds = _ts.GetAssignIdsInPackage(model.PackageId).ToArray();
            //pokud aktualne zpracovavana uloha neni posledni
            if (model.Order < assignIds.Count() -1)
            {
                var newOrder = (model.Order + 1);
                //ziskani dalsi ulohy z baliku
                var newTest = _ts.RunTestVM(assignIds[newOrder]);
                while (newTest == null)
                {
                    newOrder++;
                    //pokud jsme prosli pres vsechny ulohy, konec
                    if(newOrder == assignIds.Count())
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    newTest = _ts.RunTestVM(assignIds[newOrder]);
                }
                newTest.Order = newOrder;
                newTest.PackageId = model.PackageId;
                newTest.TotalExercises = model.TotalExercises;
                newTest.MessageBefore = string.Concat(newTest.MessageBefore, ", ", newTest.Order, "/", newTest.TotalExercises);
                
                ModelState.Clear();
                return View(newTest);
            }
            //Po splneni celeho baliku dojde k aktualizaci statutu a k presmerovani an domaci obrazovku a modalni okno pro hodnoceni ulohy
            var ps = DependencyResolver.Current.GetService<PackageService>();
            ps.SetPackageStatus(model.PackageId, ExerciseStatusEnum.Probihajici);

            return RedirectToAction("Index", "Home", new { @feedBack = 1, @packageId = model.PackageId});
        }

        /// <summary>
        /// Akce pro spusteni ukazky dane ulohy
        /// </summary>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Demo(int exerciseId)
        {
            var model = _ts.RunTestPrototypeVM(exerciseId);
            model.Demo = true;
            return View("Prototype", model );
        }

        #region test Prototype    
        /// <summary>
        /// Akce pro otestovani nove ulohy jejim autorem
        /// </summary>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Prototype(int exerciseId)
        {
            var model = _ts.RunTestPrototypeVM(exerciseId);
            return View(model);
        }

        /// <summary>
        /// Akce pro ukonceni testovani prototypu nebo ukazky ulohy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Prototype(RunTestViewModel model)
        {
            if (model != null)
            {
                if (!string.IsNullOrEmpty(model.OutputParameters))
                {
                    Console.Write(model.OutputParameters);
                }

                //pokud se jednalo o demo dojde k presmerovani na domovskou stranku
                if (model.Demo)
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            //pokud slo o testovani prototypu ulohy, dojde k presmerovani do editace ulohy
            return RedirectToAction("Manage", "Exercise", new { @id = model.ExerciseAssignId }); // jedna se o ExerciseId!!!
        }
        #endregion
    }
}
