﻿using Neurop3.Models.ViewModels.Program;
using Neurop3.Services;
using System.Web;
using System.Web.Mvc;
using Neurop3.Models.Programs;
using Neurop3.Models.ViewModels.Program.Views;
using Neurop3.Helpers;

namespace Neurop3.Controllers
{
    /// <summary>
    /// Kontroler pro spravu programu
    /// </summary>
    [Authorize]
    public class ProgramController : Controller
    {
        private readonly IProgramService _ps;

        public ProgramController(IProgramService ps)
        {
            this._ps = ps;
        }

        /// <summary>
        /// Vychozi stranka kontroleru, kde dochazi k presmerovani na stranku se seznamem programu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        #region CRUD
        /// <summary>
        /// Stranka s formularem pro vytvoreni noveho programu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Create()
        {
            return View(new ProgramCreateViewModel());
        }

        /// <summary>
        /// Akce pro zalozeni noveho programu z hodnot, ktere byly zadany ve formulari
        /// </summary>
        /// <param name="model">Model nove ulohy</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Create(ProgramCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_ps.ExistName(model.Name, 0))
                {
                    ModelState.AddModelError("", Resources.Global.nazev_tohoto_programu_jiz_existuje);
                    return View(model);
                }
                var id = _ps.CreateProgram(model);
                return RedirectToAction("Manage", new { @id = id });
            }
            else
            {
                return View(model);
            }
        }

        /// <summary>
        /// Akce pro duplikaci pozadovaneho programu
        /// </summary>
        /// <param name="id">Identifikator programu, ktery ma byt duplikovan</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Duplicate(int id)
        {
            var duplicateId = _ps.Duplicate(id);
            return RedirectToAction("Manage", new { id = duplicateId });
        }

        /// <summary>
        /// Akce pro odebrani programu
        /// </summary>
        /// <param name="id">Identifikator odebiraneho programu</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Delete(int id)
        {
            var deleted = _ps.Delete(id);
            return RedirectToAction("List");
        }
        #endregion

        #region Action
        /// <summary>
        /// Stranka pro zobrazeni seznamu programu
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult List()
        {
            return View(_ps.ListVM());
        }

        /// <summary>
        /// Akce pro aktivovani programu
        /// </summary>
        /// <param name="id">Identifikator programu, krery chceme aktivovat</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Activate(int id)
        {
            var deleted = _ps.Activate(id);
            return RedirectToAction("List");
        }

        /// <summary>
        /// Akce pro deaktivovani programu
        /// </summary>
        /// <param name="id">Identifikator programu, ktery ma byt deaktivovan</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Deactivate(int id)
        {
            var deleted = _ps.Deactivate(id);
            return RedirectToAction("List");
        }


        /// <summary>
        /// Stranka pro zobrazeni pozadovaneho programu
        /// </summary>
        /// <param name="id">Identifikator programu, ktery ma byt zobrazen</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult Manage(int id)
        {
            var program = _ps.GetProgram(id);
            return View(program);
        }

        /// <summary>
        /// Akce pro ulozeni modifikovaneho programu
        /// </summary>
        /// <param name="model">Model aktualizovaneho programu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult Manage(ProgramViewModel model)
        {
            //TODO IMPROVEMENT -- validaci
            //if (ModelState.IsValid)
            //{

            if (_ps.ExistName(model.Name, model.Id))
            {
                ModelState.AddModelError("", Resources.Global.nazev_tohoto_programu_jiz_existuje);
                return View(model);
            }

            //ulozeni souboru do modelu
            if (Request.Files.Count > 0)
            {
                model.File = Request.Files[0] as HttpPostedFileBase;
            }

            bool updated = _ps.Update(model);
            if (updated)
            {   
                FlashMessages.CreateFlashMessage("fm-info", Resources.Global.program_byl_aktualizovan, false, 2000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
                return RedirectToAction("Manage", new { @id = model.Id });
            }
            else
            {
                return null;
            }
            //}
            //return View(model);
        }
        #endregion

        #region AJAX

        /// <summary>
        /// Akce pro aktualizaci soucasneho filtru uzivatele a ziskani dat podle tohoto filtru
        /// </summary>
        /// <param name="text">Nazev programu</param>
        /// <param name="isActive">Aktivita programu</param>
        /// <param name="isPublic">Veřejnost programu</param>
        /// <param name="recordPerPage">Pocet zaznamu na stranku</param>
        /// <param name="currentPage">Aktualni stranka k zobrazeni</param>
        /// <param name="programIds">Seznam identifikatoru programu k zobrazeni oddelenych strednikem</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public ActionResult ListFilter(string text, int isActive, int isPublic, int recordPerPage, int currentPage, string programIds)
        {
            var model = _ps.SetListFilterAndGetData(new ProgramListFilter() { IsActive = isActive, IsPublic = isPublic, Text = text, CurrentPage = currentPage, ProgramIds = programIds, RecordsPerPage = recordPerPage });

            int currentP = model.Filter.CurrentPage > 0 ? model.Filter.CurrentPage : 1;

            ViewBag.totalRecords = model.Filter.TotalRecords;
            ViewBag.rowStartsAt = model.Filter.RecordsPerPage * (currentP - 1) + 1;
            ViewBag.currentPage = currentP;

            return View("EditorTemplates/ProgramListTable", model.ProgramVMs);
        }

        /// <summary>
        /// Akce pro ziskani fragmentu kodu pro pridani noveho vstupniho parametru
        /// </summary>
        /// <param name="id">Identifikator programu, do ktereho ma byt pridan vstupni parametr</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult AddNewRecipeRow(int id)
        {
            var model = _ps.AddNewRecipeRow(id);
            return View("EditorTemplates/AddNewRecipeRow", model);
        }

        /// <summary>
        /// Akce pro ziskani fragmentu kodu pro pridani noveho vystupniho parametru z programu
        /// </summary>
        /// <param name="id">Identifikator programu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult AddNewResultRow(int id)
        {
            var model = _ps.AddNewResultRow(id);
            return View("EditorTemplates/AddNewResultRow", model);
        }

        /// <summary>
        /// Akce pro ulozeni skriptu s ulohou 
        /// </summary>
        /// <param name="programName">Nazev programu</param>
        /// <param name="programId">Identifikator programu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult UploadScript(string programName, int programId)
        {
            //TODO UPRAVIT -- nazev posilam zbytecne, protoze se skript stejne jmenuje podle programu
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    _ps.SaveScripts(file, programName, programId);
                }
            }
            return RedirectToAction("List");
        }


        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist")]
        public ActionResult UploadFiles(int programId)
        {
            if (Request.Files.Count > 0)
            {
                if (Request.Files != null && Request.Files.Count != 0)
                {
                    var savedFiles = _ps.SaveFiles(Request.Files, programId);
                }
            }
            return RedirectToAction("List");
        }
        #endregion
    }
}