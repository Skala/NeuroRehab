﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Neurop3.Services;
using Microsoft.AspNet.Identity;
using Neurop3.Helpers;
using Neurop3.Models.ViewModels.User;
using Neurop3.Common;
using Neurop3.Models.User;
using System;
using Neurop3.Infrastructure;

namespace Neurop3.Controllers
{
    /// <summary>
    /// Kontroler pro spravu uzivatelu
    /// </summary>
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _us;
        private readonly IHomeService _hs;

        public UserController(IUserService us, IHomeService hs)
        {
            _us = us;
            _hs = hs;
        }

        #region User action

        /// <summary>
        /// Domovska stranka kontroleru se presmerovav na seznam uzivatelu v aplikaci
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        /// <summary>
        /// Stranka pro zobrazeni seznamu vsech uzivatelu v aplikaci
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult List()
        {
            var model = _us.GetListVM();
            return View(model);
        }

        /// <summary>
        /// Stranska s formularem, ve kterem lze modifikovat daneho uzivatele
        /// </summary>
        /// <param name="id">Identifikator uzivatele</param>
        /// <param name="edit">Pravdivostni hodnota, zda dochazi k nahledu nebo editaci</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult Manage(string id, bool edit)
        {
            var model = _us.GetManageVM(id);
            model.UserVM.IsEditable = edit;
            model.IsEditable = edit;
            return model != null ? View(model) : null;
        }

        /// <summary>
        /// Akce pro aktualizovani uzivatele, jehoz data jsou v modelu
        /// </summary>
        /// <param name="model">Model uzivatele</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult Manage(ManageViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(model.UserVM.Id))
                {
                    if (model.UserVM.IsEditable == true)
                    {
                        var result = _us.UpdateUser(model.UserVM);
                        return RedirectToAction("List");
                    }
                }
            }
            else
            {
                return View(model);
            }
            return null;
        }
        #endregion

        #region Assign Action

        /// <summary>
        /// Stranka se seznemem existujicich prirazeni pacienta k terapeutovi
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult PatientAssign()
        {
            return View(_us.PatientAssignListVM());
        }
        
        /// <summary>
        /// Stranka pro zobrazeni detailu vazby mezi konkrentim terapeutem a pacientem
        /// </summary>
        /// <param name="id">Identifikator vazby</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult AssignDetail(int id)
        {
            //TODO NEDEFINOVANO -- detail vazby mezi terapeutem a pacientem
            return View();
        }

        /// <summary>
        /// Stranka pro zobrazeni seznamu vsech pacientu, ktere aktualne prihlaseny terapeut ma
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult PatientList()
        {
            var currentUserId = User.Identity.GetUserId();
            if (!string.IsNullOrEmpty(currentUserId))
            {
                var model = _us.GetPatientsOfTherapist(currentUserId);
                return View(model);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Stranka urcena pro terapeuta kvuli zobrazeni informaci o svem pacientovi
        /// </summary>
        /// <param name="id">Identifikator pacienta</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult PatientDetail(string id)
        {
            var model = _us.GetPatientDetailVM(id);
            return View(model);
        }

        /// <summary>
        /// Akce pro odebrani vazby mezi terapeutem a pacientem
        /// </summary>
        /// <param name="Id">Identifikator vazby</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult RemoveAssign(int Id)
        {
            bool result = _us.RemoveAssign(Id);
            if (result)
            {
                FlashMessages.CreateFlashMessage("fm-success", Resources.Global.prirazeni_bylo_odebrano, true, 2000, true, "fm-fixed", "fm-bottom-right", "body-content", "", null);
            }
            return RedirectToAction("PatientAssign");
        }

        /// <summary>
        /// Akce pro obnoveni vazby mezi terapeutem a pacietem
        /// </summary>
        /// <param name="Id">Identifikator vazby</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult ActivateAssign(int Id)
        {
            bool result = _us.ActivateAssign(Id);
            if (result)
            {
                FlashMessages.CreateFlashMessage("fm-success", Resources.Global.prirazeni_bylo_obnoveno, true, 2000, true, "fm-fixed", "fm-bottom-right", "body-content", "", null);
            }
            return RedirectToAction("PatientAssign");
        }
        #endregion


        #region ajaxove sluzby
        /// <summary>
        /// Akce pro ziskani seznamu uzivatelu v aplikaci, jejich jmeno obsahuje zadany podretezec
        /// </summary>
        /// <param name="input">Hledany text ve jmenech uzivatelu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult ListFilter(string input)
        {
            var model = _us.GetUsersFiltered(input);
            return View("EditorTemplates/UserListTable", model);
        }

        /// <summary>
        /// Akce pro nastaveni filtru pro vyhledani vazeb mezi terapeuty a pacienty. Soucasti je ziskani vazeb na zaklade tohoto filtru.
        /// </summary>
        /// <param name="patientName">Hledany podretezec ve jmenu pacienta</param>
        /// <param name="therapistName">Hledany podretezec ve jmenu terapeuta</param>
        /// <param name="isActive">Jedna se o aktivni vazbu?</param>
        /// <param name="recordPerPage">Pocet zaznamu na stranku</param>
        /// <param name="currentPage">Aktualni stranka</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult PatientAssignListFilter(string patientName, string therapistName, int isActive, int recordPerPage, int currentPage)
        {
            var model = _us.SetListFilterAndGetData(new PatientAssignListFilter() { IsActive = isActive, PatientName = patientName, TherapistName = therapistName, CurrentPage = currentPage, RecordsPerPage = recordPerPage });

            int currentP = model.Filter.CurrentPage > 0 ? model.Filter.CurrentPage : 1;

            ViewBag.totalRecords = model.Filter.TotalRecords;
            ViewBag.rowStartsAt = model.Filter.RecordsPerPage * (currentP - 1) + 1;
            ViewBag.currentPage = currentP;

            return View("EditorTemplates/_PatientAssignListTable", model.PatientAssignViewModels);
        }

        /// <summary>
        /// Akce pro ziskani tabulky s pacienty aktualne prihlaseneho terapeuta. Pacienti jsou filtrovany na zaklade jmena, ktere musi odbsahovat podretezec z parametru.
        /// </summary>
        /// <param name="input">Hledany podretezec ve jmenech pacietnu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult PatientListFilter(string input)
        {
            var currentUserId = User.Identity.GetUserId();

            if (!string.IsNullOrEmpty(currentUserId))
            {
                var model = _us.GetPatientsOfTherapistFilter(currentUserId, input);
                return View("EditorTemplates/PatientListTable", model);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Metoda pro ziskani tabulky s reporty, ktere se tykaji aktualne prihlaseneho pacienta
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Patient")]
        public ActionResult ReportList()
        {
            var reports = _us.GetTherapistReportVMs(new Models.Filters.TherapistReportFilter()
            {
                PatientId = User.Identity.GetUserId()
            });
            return View(reports);
        }

        /// <summary>
        /// Akce, ktera vytvori vazbu mezi terapeutem a pacientem
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="therapistId">Identifikator terapeuta</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult AddPatientAssign(string patientId, string therapistId)
        {
            var added = _us.AddPatientAssign(patientId, therapistId);
            return Json(new { success = added });
        }

        /// <summary>
        /// Akce pro ziskani seznamu (JSON) roli v aplikaci, ktere odpovidaji zadanemu textu
        /// </summary>
        /// <param name="query">text, podle ktereho se filtruji role</param>
        /// <param name="selectedIds">dosud zvolene role</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin")]
        public JsonResult GetRolesSelect(string query, string selectedIds)
        {
            var roles = _us.GetRolesList();

            //Pouze superadmin ma pravo vytvaret dalsi superadminy... ostatnim tuto moznost odeberu
            if (!Infrastructure.Authorization.Authorize(User, RoleEnum.Superadmin))
            {
                var superAdmin = RoleEnum.Superadmin.GetHashCode().ToString();
                roles = roles.Where(x => x.Id != superAdmin);
            }

            //Toto je kvuli prvnotni inicializaci editoru, aby byly vyplneny aktualni role uzivatele
            //pri dalsim dotazovani je to prazdne
            if (!string.IsNullOrEmpty(selectedIds))
            {
                var selectedIdsList = selectedIds.Split(',').Select(x => x.Trim()).ToList();
                roles = roles.Where(x => selectedIdsList.Contains(x.Id));
            }

            //filtrace na zaklade zadaneho textu
            if (!string.IsNullOrEmpty(query))
            {
                roles = roles.Where(x => x.Name.ToLower().Trim().Contains(query));
            }

            var roleList = roles.Select(x => new { id = x.Id, name = x.Name }).ToArray();
            return Json(roleList);
        }

        /// <summary>
        /// Akce, ktera vraci seznam uzivatelu (JSON), kteri jsou nabizeny pri vytvareni vazeb mezi terapeuty a pacienty.
        /// Je provadena filtrace na zaklade podretezce je jmenu uzivatele a typu uzivatele
        /// </summary>
        /// <param name="input">Podretezec hledany ve jmenu uzivatele</param>
        /// <param name="type">Typ uzivatele (pacient/terapeut)</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin")]
        public JsonResult GetUsersToAssign(string input, int type)
        {
            var users = _us.GetUsersToAssign(input, type).Take(10).ToList();
            var resultList = users.Select(x => new { id = x.Id, name = x.FullName });

            return Json(resultList.ToArray());
        }

        /// <summary>
        /// Akce pro ziskani seznamu (JSON) pacientu, ktere dany terapeut ma. Seznam je filtrovany na zaklade zadaneho fragmentu jmena
        /// </summary>
        /// <param name="query">Fragment jmena hledaneho pacienta</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist")]
        public JsonResult GetTherapistPatients(string query)
        {
            var users = _us.GetPatientsOfTherapistFilter(User.Identity.GetUserId(), query).Take(10).ToList();
            var usersList = users.Select(x => new { id = x.Id, name = x.FullName });
            return Json(usersList.ToArray());
        }

        /// <summary>
        /// Test na existenci nove pridavaneho prirazeni
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="therapistId">Identifikator terapeuta</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin")]
        public JsonResult AssignExists(string patientId, string therapistId)
        {
            bool result = _us.AssignExists(patientId, therapistId);
            return Json(new { success = result });
        }
        #endregion

        /// <summary>
        /// Akce pro vytvoreni zpravy terapeutem o pokroku pacienta
        /// </summary>
        /// <param name="model">model zpravy vyplneny ve formulari</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public ActionResult CreateTherapistReport(TherapistReportCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var saved = _us.CreateTherapistReport(model);
                if (saved)
                {
                    FlashMessages.CreateFlashMessage("fm-info", "Report přidán!", false, 3000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
                }
                return RedirectToAction("PatientDetail", new { id = model.PatientDataId });
            }
            return View(model);
        }

        /// <summary>
        /// Akce, ktera vraci fragment HTML kodu, ktery obsahuje informace o pokroku pacienta
        /// </summary>
        /// <param name="reportId">Identifikator zpravy o pacientovo pokroku</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist, Patient")]
        public ActionResult ShowTherapistReport(Guid reportId)
        {
            var model = _us.GetTherapistReport(reportId);
            //pokud je to pacient, pak reportu nastavim priznak o jeho shlednuti
            if (!model.Shown && Authorization.Authorize(User, RoleEnum.Patient))
            {
                _us.PatientReportShown(model.Id);
            }
            return View("EditorTemplates/_TherapistReportDetail", model);
        }

        /// <summary>
        /// Akce pro smazanit reportu terapeuta o pokroku pacienta
        /// </summary>
        /// <param name="reportId">Identifikator reportu</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        public JsonResult DeleteTherapistReport(Guid reportId)
        {
            var deleted = _us.DeleteTherapistReport(reportId);
            if (deleted)
            {
                FlashMessages.CreateFlashMessage("fm-info", "Report smazán!", false, 3000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
            }
            return Json(new { success = deleted });
        }

        /// <summary>
        /// Akce pro vlozeni pacientovo hodnoceni baliku do databaze
        /// </summary>
        /// <param name="difficulty">Obtiznost baliku</param>
        /// <param name="exerciseAmount">Hodnoceni poctu uloh v baliku</param>
        /// <param name="note">Poznamka k baliku</param>
        /// <param name="packageId">Identifikator baliku</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Patient")]
        public JsonResult AddPatientFeedback(int difficulty, int exerciseAmount, string note, int packageId)
        {
            var saved = _us.AddPatientFeedback(difficulty, exerciseAmount, note, packageId);
            return Json(new { success = true });
        }

        /// <summary>
        /// Akce pro vraceni fragmentu HTML kodu pro zobrazeni pacientovo hodnoceni baliku
        /// </summary>
        /// <param name="feedBackId">Identifikator hodnoceni</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist, Patient")]
        public ActionResult ShowPatietFeedBack(Guid feedBackId)
        {
            var model = _us.GetPatientFeedBack(feedBackId);
            return View("EditorTemplates/_PatientFeedback", model);
        }
    }
}
