﻿using Neurop3.Models.ViewModels.Analysis;
using Neurop3.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Neurop3.Controllers
{
    /// <summary>
    /// Kontroler pro provedeni analyzi vysledků
    /// </summary>
    [Authorize]
    public class AnalysisController : Controller
    {
        private readonly IAnalysisService _as;

        public AnalysisController(IAnalysisService ias)
        {
            this._as = ias;
        }

        #region Akce
        /// <summary>
        /// Stranka pro nalezeni konkretni ulohy, nad kterou bude chtit terapeut provest analyzu
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Stranka pro zobrazeni vysledku dane ulohy
        /// </summary>
        /// <param name="id">Identifikator ulohy, kterou chce terapeut analyzovat</param>
        /// <returns></returns>
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        [HttpGet]
        public ActionResult Analyze(int id)
        {
            var model = _as.AnalyzeVM(id);
            return View(model);
        }
        #endregion

        #region AJAX

        /// <summary>
        /// Akce pro ziskani vsech uloh, ktere splnuji zadana kriteria. 
        /// </summary>
        /// <param name="programIds">Identifikatory programu pro analyzu</param>
        /// <param name="exerciseIds">Identifikatory uloh pro analyzu</param>
        /// <param name="patientIds">Identifikatory pacientu pro analyzu</param>
        /// <returns>Fragment HTML kodu s tabulkou ulohu, ktere odpovidaji danym krieteriim</returns>
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        [HttpPost]
        public ActionResult GetExerciseAssignsToAnalysisView(string programIds, string exerciseIds, string patientIds)
        {
            var exResList = _as.GetExerciseAssignsToAnalysisView(programIds, exerciseIds, patientIds);
            return View("EditorTemplates/_ExerciseAssignList", exResList);
        }

        /// <summary>
        /// Akce pro ziskani vsech uloh, ktere odpovidaji zadanym kriteriim, kterymi jsou uzivatelsky vstup pro nazev ulohy a seznam identifikatoru programu.
        /// </summary>
        /// <param name="input">Kriterium nazvu ulohy</param>
        /// <param name="programIds">Kriterium typu ulohy</param>
        /// <returns>Ulohy, ktere koresponduji se zadanymi kriterii ve formatu JSON</returns>
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        [HttpPost]
        public JsonResult GetExerciseSelect(string input, string programIds)
        {
            var models = _as.GetExerciseSelect().Where(x => x.IsActive == true);
            //Pokud hledame ulohy z daneho modulu, tak je vyfiltrujeme
            if (!string.IsNullOrEmpty(programIds))
            {
                var programList = Array.ConvertAll(programIds.Split(','), s => Int32.Parse(s));
                models = models.Where(x => programList.Contains(x.ProgramId));
            }

            input = input.Trim().ToLower();
            models = models.Where(x => x.Name.ToLower().Trim().Contains(input));
            var modelList = models.Select(x => new { id = x.Id, name = x.Name }).ToArray();
            return Json(modelList);
        }

        /// <summary>
        /// Akce pro ziskani vsech uloh uzivatele, ktere podlehaji filtraci pomoci vstupnich parameteru.
        /// </summary>
        /// <param name="input">Pokud je vyplneno, budou vraceny ulohy, ktere nalezi danemu uzivateli</param>
        /// <param name="programIds">Pokud je vyplneno, budou nabidnuty ulohy, ktere byly vytvoreny z programu, jejich idenfikator je obsazen v tomto retezci (jsou oddeleny strednikem).</param>
        /// <param name="exerciseIds">Pokud je vyplneno, budou nabidnuty ulohy, jejichz identifikatory jsou obsazeny v tomto retezci (jsou oddeleny strednikem).</param>
        /// <returns></returns>
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        [HttpPost]
        public JsonResult GetPatientSelect(string input, string programIds, string exerciseIds)
        {
            input = input.ToLower().Trim();
            var models = _as.GetPatientSelect(programIds, exerciseIds).Where(x => x.FirstName.ToLower().Trim().Contains(input) || x.LastName.ToLower().Trim().Contains(input)).Select(x => new { Id = x.Id, FirstName = x.FirstName, LastName = x.LastName }).AsEnumerable();
            var modelList = models.Select(x => new { id = x.Id, name = string.Format("{0} {1}", x.FirstName, x.LastName)}).ToArray();
            return Json(modelList);
        }

        /// <summary>
        /// Akce pro ziskani programu, jejichz nazev odpovida zadanemu vstupu
        /// </summary>
        /// <param name="input">Nazev hledaneho programu</param>
        /// <returns></returns>
        [Authorize(Roles = "Superadmin, Super_therapist, Therapist")]
        [HttpPost]
        public JsonResult GetProgramSelect(string input)
        {
            input = input.Trim().ToLower();
            var models = _as.GetProgramsSelect().Where(x => x.IsActive == true).Where(x => x.Name.ToLower().Trim().Contains(input));
            var modelList = models.Select(x => new { id = x.Id, name = x.Name }).ToArray();
            return Json(modelList);
        }
        #endregion
    }
}