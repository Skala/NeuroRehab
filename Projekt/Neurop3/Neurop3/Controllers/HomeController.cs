﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using Neurop3.Services;
using Microsoft.AspNet.Identity;
using Neurop3.Helpers;
using Neurop3.Common;
using Neurop3.Infrastructure;
using Neurop3.Models.Administration;
using System.Data.Entity;

namespace Neurop3.Controllers
{
    /// <summary>
    /// Kontroler urceny pro akce vyvolavane v domovske strance. Dale jsou zde uvedeny akce, jejichz funkcnost se nehodi do ostatnich kontroleru
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IExerciseService _es;
        private readonly IHomeService _hs;

        public HomeController(IExerciseService es, IHomeService hs)
        {
            this._es = es;
            this._hs = hs;
        }
        
        /// <summary>
        /// Domovska stranka aplikace. Pokud doslo k presmerovani na tuto stranku po dokoncenem cviceni, pak se zobrazi formular pro zaslani zpetne vazby pacienta na balik
        /// </summary>
        /// <param name="feedBack">Pravdivostni hodnota, zda se jedna o zpetnou vazbu</param>
        /// <param name="packageId">Identifikator baliku s ulohami</param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Index(int? feedBack, int? packageId)
        {
            if (feedBack.HasValue) ViewBag.feedBack = (int)feedBack;
            if (packageId.HasValue) ViewBag.packageId = (int) packageId;

            //FlashMessages.CreateFlashMessage("fm-info", "Protokol o opravených chybách můžete naleznout pod odkazem v patičce této stránky kliknutím na číslo verze", false, 5000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
            //if (Authorization.Authorize(User, RoleEnum.Superadmin))
            //{
            //    FlashMessages.CreateFlashMessage("fm-info", "Jste superadministrátor a máte povoleny veškeré operace!", false, 3000, true, "fm-fixed", "fm-bottom-right", "body", "", null);
            //}
            return View();
        }

        #region Info
        /// <summary>
        /// Stranka s informacemi o aplikace
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Info()
        {
            return View();
        }

        /// <summary>
        /// Stranka s infromacemi 2
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// Stranska s konktaktnimi udaji
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Contact()
        {
            return View();
        }
        #endregion
        
        #region Administrace

        /// <summary>
        /// Stranka pro zobrazeni vsech vyjimek, ktere v aplikaci nastali
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist, Patient")]
        public ActionResult Errors()
        {
            var errors = _hs.GetErrors().Take(50).OrderByDescending(x => x.TimeUtc); //.Where(x => x.TimeUtc > DbFunctions.AddDays(DateTime.UtcNow, -7))
            return View(errors);
        }

        /// <summary>
        /// Akce pro ziskani konkretni chyby, ktera se zobrazi v modalnim oknu
        /// </summary>
        /// <param name="errorId">Identifikator chyby</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ShowError(Guid errorId)
        {
            var error = _hs.GetErrors().Where(x => x.ErrorId == errorId).FirstOrDefault();
            return View(error);
        }

        /// <summary>
        /// Stranska pro zobrazeni seznamu s provedenymi zmenami v aplikaci
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist, Patient")]
        public ActionResult ChangeLogs()
        {
            var changeLogs = _hs.GetChangeLogs().OrderByDescending(x => x.TimeUtc);
            return View(changeLogs);
        }

        /// <summary>
        /// Formular pro vytvoreni noveho zmenoveho logu v aplikaci
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin")]
        public ActionResult CreateChangeLog()
        {
            var model = new ChangeLog();
            return View(model);
        }

        /// <summary>
        /// Vytvoreni noveho zmenoveho logu podle vyplneneho formulare
        /// </summary>
        /// <param name="log">Log od uzivatele</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin")]
        public ActionResult CreateChangeLog(ChangeLog log)
        {
            var saved = _hs.CreateChangeLog(log);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Stranka pro zobrazeni vsech pripominek v aplikaci, ktere nebyly vyrizene
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist, Patient")]
        public ActionResult CommentList()
        {
            var comments = _hs.GetComments().Where(x => x.IsDone == false);
            return View(comments);
        }

        /// <summary>
        /// Formular pro vytvoreni nove pripominky na aplikaci
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist, Patient")]
        public ActionResult CommentCreate()
        {
            return View(new Comment() {
                UserId = User.Identity.GetUserId(),
                IsDone = false,
                IsShown = false,
            });
        }

        /// <summary>
        /// Akce pro vytvoreni nove pripominky, ktera byla vytvorena prostrednictvim formulare
        /// </summary>
        /// <param name="model">Model pridavane pripominky</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist, Patient")]
        public ActionResult CommentCreate(Comment model)
        {
            if (ModelState.IsValid)
            {
                var created = _hs.CreateComment(model);
                if (created)
                {
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            return View(model);
        }

        /// <summary>
        /// Akce pro zobrazeni konkretni pripominky pro jeji editaci
        /// </summary>
        /// <param name="id">Identifikator pripominky</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist, Patient")]
        public ActionResult CommentManage(int id)
        {
            var comment = _hs.GetComment(id);
            return View(comment);
        }

        /// <summary>
        /// Akce pro aktualizaci existujici pripominky prostrednictvim upraveneho modelu
        /// </summary>
        /// <param name="model">Model s upravenou pripominkou</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Superadmin, Admin, Super_therapist, Therapist, Patient")]
        public ActionResult CommentManage(Comment model)
        {
            if(model != null)
            {
                var updated = _hs.UpdateComment(model);
                if (updated) {
                    return RedirectToAction("CommentList");
                }
                return View(model);
            }
            return View(model);
        }

        /// <summary>
        /// Stranka pro zobrazeni prihlasovacich logu prostrednictvim tabulky
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LoginLogs()
        {
            var logs = _hs.GetLoginLogs().Take(100).OrderByDescending(x => x.TimeUtc).ToList();
            return View(logs);
        }

        #endregion

        /// <summary>
        /// Stranka pro dosud nenaimplementovane akce
        /// </summary>
        /// <param name="message">Zprava, ktera se zobrazi na strance</param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult NotImplemented(string message)
        {
            ViewBag.message = message;
            return View();
        }
    }
}