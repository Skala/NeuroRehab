﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Neurop3.Common;
using Neurop3.Models;
using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Models.Exercises;
using Neurop3.Helpers;
using System.Web.Hosting;
using System.Configuration;
using System.IO;
using Neurop3.Models.ViewModels.Translation;

namespace Neurop3.Services
{

    public interface IExerciseService
    {
        /// <summary>
        /// Metoda pro vytvoreni nove ulohy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        int Create(ExerciseCreateViewModel model);

        /// <summary>
        /// Metoda pro aktualizaci editovane ulohy
        /// </summary>
        /// <param name="model">Model ulohy, ktera ma byt aktualizovana.</param>
        /// <returns>Byla uloha aktualizovana?</returns>
        bool Update(ExerciseManageViewModel model);

        /// <summary>
        /// Metoda pro duplikaci ulohu s danym identifikatorem
        /// </summary>
        /// <param name="id">Identifikator ulohy, kterou chceme duplikovat</param>
        /// <returns>Identifikator nove ulohy</returns>
        int Duplicate(int id);

        /// <summary>
        /// Zaktivovni pozadovane ulohy
        /// </summary>
        /// <param name="id">Identifikator ulohy</param>
        /// <returns>Aktivovano?</returns>
        bool Activate(int id);

        /// <summary>
        /// Deaktivace pozadovane ulohy
        /// </summary>
        /// <param name="id">Identifikator ulohy</param>
        /// <returns>Deaktivovano?</returns>
        bool Deactivate(int id);

        /// <summary>
        /// Odstraneni ulohy ze systemu
        /// </summary>
        /// <param name="id">Identifikator ulohy, ktera ma byt smazana.</param>
        /// <returns>Informace o smazani ulohy</returns>
        bool Remove(int id);

        /// <summary>
        /// Metoda pro zjisteni existence ulohy se stejnym nazvem
        /// </summary>
        /// <param name="name">Nazev aktualizovane ulohy</param>
        /// <param name="exerciseId">Identifikator aktualizovane ulohy</param>
        /// <returns></returns>
        bool ExistName(string name, int exerciseId);

        /// <summary>
        /// Ziskani seznamu uloh, ktere odpovidaji filtru v databazi.
        /// </summary>
        /// <returns>seznam uloh</returns>
        Models.ViewModels.Exercise.ListViewModel ListVM();

        /// <summary>
        /// Metoda pro aktualizaci filtru a ziskani pozadovanych dat
        /// </summary>
        /// <param name="filter">Novy filter, ktery ma byt upraven v databazi</param>
        /// <returns></returns>
        Models.ViewModels.Exercise.ListViewModel SetListFilterAndGetData(ExerciseListFilter filter);

        /// <summary>
        /// Metoda pro ziskani uloh, ktere odpovidaji definovanemu filtru
        /// </summary>
        /// <param name="filter">Filtr uloh</param>
        /// <returns>Seznam uloh odpovidajicich filtru</returns>
        IQueryable<ExerciseViewModel> GetFilteredExercises(ExerciseListFilter filter);


        /// <summary>
        /// Metoda pro ziskani pozadovane ulohy
        /// </summary>
        /// <param name="id">Identifikator pozadovane uloh</param>
        /// <returns>VM ulohy</returns>
        ExerciseManageViewModel GetExerciseVM(int id);


        /// <summary>
        /// Metoda pro overeni existence vazby mezi pacientem a ulohou
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <returns></returns>
        bool AssignExists(string patientId, int exerciseId);

        /// <summary>
        /// Metoda pro vytvoreni noveho prirazeni ulohy k pacientovi
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <returns></returns>
        ExerciseAssignViewModel AddExerciseAssign(string patientId, int exerciseId);

        /// <summary>
        /// Metoda pro ulozeni souboru, ktery predstavuje vstupni parametr do ulohy
        /// </summary>
        /// <param name="file">Soubor, ktery ma byt ulozen</param>
        /// <param name="row">Poradove cislo parametru</param>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <param name="programId">Identifikator programu</param>
        /// <returns></returns>
        bool SaveFile(HttpPostedFileBase file, int row, int exerciseId, int programId);

        /// <summary>
        /// Metoda pro ziskani seznamu programu, ze kterych muze prihlaseny terapeut vytvorit ulohu
        /// </summary>
        /// <returns></returns>
        SelectList GetProgramList();

        /// <summary>
        /// Metoda pro ziskani vsech prirazeni uloh na zaklade nastaveneho filtru
        /// </summary>
        /// <param name="filter">Filtr pro prirazeni uloh</param>
        /// <returns></returns>
        IQueryable<ExerciseAssignViewModel> GetFilteredExerciseAssigns(ExerciseAssignListFilter filter);

        //TODO SMAZAT -- pouzit getFilteredExercise
        IQueryable<ExerciseViewModel> GetExercises();


        //TODO SMAZAT -- nevola se 
        //IQueryable<ExerciseViewModel> GetExercises(int programId);
        //ExerciseAssignViewModel GetAssign(int id);
        //IQueryable<ExerciseAssignViewModel> GetAssigns();
        //IQueryable<ExerciseAssignViewModel> GetAssigns(int exerciseId);

        //TODO SMAZAT -- nevola se
        //IQueryable<ExerciseAssignViewModel> GetAssignsFiltered(string input);
        //IQueryable<ExerciseAssignViewModel> GetAssignsFiltered(string input, int exerciseId);
        //IQueryable<ExerciseAssign> GetExerciseAssigns();
    }


    public class ExerciseService : IExerciseService
    {
        private readonly ApplicationDbContext _db;

        public ExerciseService(ApplicationDbContext db)
        {
            _db = db;
        }

        #region CRUD
        public int Create(ExerciseCreateViewModel model)
        {
            try
            {
                using (var transaction = _db.Database.BeginTransaction())
                {
                    var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                    
                    var exercise = _db.Exercise.Add(new Exercise()
                    {
                        AuthorId = currentUserId,
                        ProgramId = model.ProgramId,
                        Name = model.Name,
                        Description = model.Description,
                        CreatedDate = DateTime.Now,
                        IsActive = true,
                        IsPublic = false,
                    });
                    _db.SaveChanges();

                    //nestaci pouze zalozit program, musime mu zalozit i vychozi vstupni parametry
                    var tmpModel = _db.ProgramRecipe.Where(x => x.ProgramId == exercise.ProgramId).Select(x => new ExerciseDataViewModel()
                    {
                        ExerciseId = exercise.Id,
                        Row = x.RowNumber,
                        DataType = x.DataTypeId
                    }).ToList();

                    foreach (var item in tmpModel)
                    {
                        exercise.ExerciseDatas.Add(new ExerciseData()
                        {
                            ExerciseId = item.Id,
                            Row = item.Row,
                            Value = item.DataType == DataTypeRecipeEnum.Text.GetHashCode() ? "Text" : "0"
                        });
                    }
                    _db.SaveChanges();
                    transaction.Commit();
                    return exercise.Id;
                }
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                throw e;
            }
        }

        public bool Update(ExerciseManageViewModel model)
        {
            try
            {
                if (model != null)
                {
                    var currentTherapistId = HttpContext.Current.User.Identity.GetUserId();

                    var oldModel = _db.Exercise.Find(model.Id);

                    //TODO UPRAVIT -- pri modifikaci HTML lze menit nazev... zakazat kdyz je pouzivana
                    if (currentTherapistId.CompareTo(model.AuthorId) == 0)
                    {
                        oldModel.Name = model.Name;
                        oldModel.IsPublic = model.IsPublic;
                        _db.SaveChanges();
                    }
                   
                    if (!(model.HasActiveAssign || model.HasResult))
                    {
                        oldModel.IsActive = model.IsActive;
                        //Aktualizace vstupnich parametru do ulohy
                        for (int i = 0; i < model.ExerciseDataViewModels.Count(); i++)
                        {
                            var newDataRow = model.ExerciseDataViewModels.Where(x => x.Row == i).FirstOrDefault();
                            var dataType = (DataTypeRecipeEnum)_db.ProgramRecipe.Where(x => x.ProgramId == model.ProgramId).Where(x => x.RowNumber == newDataRow.Row).FirstOrDefault().DataTypeId;
                            if (!(dataType == DataTypeRecipeEnum.Obrazek || dataType == DataTypeRecipeEnum.Zvuk))
                            {
                                oldModel.ExerciseDatas.Where(x => x.Row == i).FirstOrDefault().Value =
                                    model.ExerciseDataViewModels.Where(x => x.Row == i).FirstOrDefault().Value;
                            }

                        }
                        _db.SaveChanges();
                    }
                    //TODO SMAZAT -- aktualizaci prirazeni, uz se to dela jen pomoci baliku
                    #region aktualizace prirazeni 
                    //var allAssigns = _db.ExerciseAssign.Where(x => x.ExerciseId == model.Id).ToList();

                    ////pokud v modelu neni nic, tak vsechno z db zneplatnim
                    //if (model.ExerciseAssignViewModels == null)
                    //{
                    //    foreach (var aa in allAssigns)
                    //    {
                    //        //jeste potrebujeme overit, zda se to neumazalo rucne u cizich terapeutu
                    //        if (aa.TherapistId.CompareTo(currentTherapistId) == 0)
                    //        {
                    //            aa.IsActive = false;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    var assignsFromView = model.ExerciseAssignViewModels.Select(x => x.Id).ToList();
                    //    //smazu vsechny, ktere zmizeli z view
                    //    foreach (var aa in allAssigns)
                    //    {
                    //        if (!assignsFromView.Contains(aa.Id))
                    //        {
                    //            if (aa.TherapistId.CompareTo(currentTherapistId) == 0)
                    //            {
                    //                aa.IsActive = false;
                    //            }
                    //        }
                    //    }
                    //    _db.SaveChanges();

                    //    //proces pridani prirazeni
                    //    foreach (var assign in model.ExerciseAssignViewModels)
                    //    {
                    //        if (assign.TherapistId.CompareTo(currentTherapistId) == 0)
                    //        {
                    //            var exercise = _db.Exercise.Find(assign.ExerciseId);
                    //            var therapist = _db.Users.Find(assign.TherapistId);

                    //            if (assign.Id == 0)
                    //            {
                    //                //pokud se jedna o uplne nove prirazeni
                    //                _db.ExerciseAssign.Add(new ExerciseAssign()
                    //                {
                    //                    AssignDate = DateTime.Now,
                    //                    ExerciseId = assign.ExerciseId,
                    //                    IsActive = assign.IsActive,
                    //                    LastAccessDate = DateTime.Now,
                    //                    StatusId = assign.StatusId,
                    //                    PatientId = assign.PatientId,
                    //                    TherapistId = HttpContext.Current.User.Identity.GetUserId()
                    //                });
                    //                var saved = _db.SaveChanges();
                    //                if (saved > 0)
                    //                {
                    //                    Utils.SendMail(new MailMessage()
                    //                    {
                    //                        UserId = assign.PatientId,
                    //                        Subject = Resources.Global.byla_vam_prirazena_nova_uloha,
                    //                        Body = string.Format("<p>" + Resources.Global.vas_terapeut_0_vam_priradil_novou_ulohu + ": {1}<p>", therapist.FullName, exercise.Name),
                    //                    });
                    //                }
                    //            }
                    //            else if (!assign.IsActive)
                    //            {
                    //                var oldAssign = _db.ExerciseAssign.Find(assign.Id);
                    //                oldAssign.IsActive = true;
                    //                var saved = _db.SaveChanges();
                    //                if (saved > 0)
                    //                {
                    //                    Utils.SendMail(new MailMessage()
                    //                    {
                    //                        UserId = assign.PatientId,
                    //                        Subject = Resources.Global.byla_vam_prirazena_nova_uloha,
                    //                        Body = string.Format("<p>" + Resources.Global.vas_terapeut_0_vam_priradil_novou_ulohu + ": {1}<p>", therapist.FullName, exercise.Name),
                    //                    });
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //_db.SaveChanges();
                    #endregion
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public bool Remove(int id)
        {
            try
            {
                var exToDelete = _db.Exercise.Find(id);

                if (exToDelete != null)
                {
                    //overeni, zda ji maze jeji autor
                    var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                    if (currentUserId.CompareTo(exToDelete.AuthorId) == 0)
                    {
                        var exToDeleteVM = GetExercises().Where(x => x.Id == id).FirstOrDefault();
                        //TODO IMPROVEMENT -- pouzit toto
                        //var exToDeleteVM1 = GetFilteredExercises(new ExerciseListFilter()
                        //{
                        //    ExerciseIds = id.ToString()
                        //}).FirstOrDefault();

                        //pokud mazeme ulohu bez prirazeni bez vysledku, smazeme ji z databace. Jinak ji zneaktivnime
                        if (!exToDeleteVM.HasResult)
                        {
                            var assigns = exToDelete.ExerciseAssigns;
                            _db.ExerciseAssign.RemoveRange(assigns);
                            _db.SaveChanges();
                            _db.Exercise.Remove(exToDelete);
                        }
                        else
                        {
                            exToDelete.IsActive = false;
                        }
                        _db.SaveChanges();
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public bool Activate(int id)
        {
            try
            {
                var exercise = _db.Exercise.Find(id);

                if (exercise != null)
                {
                    //overeni, zda ji aktivuje jeji autor
                    var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                    if (currentUserId.CompareTo(exercise.AuthorId) == 0)
                    {
                        exercise.IsActive = true;
                        var success = _db.SaveChanges();
                        return success > 0 ? true : false;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public bool Deactivate(int id)
        {
            try
            {
                var exercise = _db.Exercise.Find(id);

                if (exercise != null)
                {
                    //overeni, zda ji deaktivuje jeji autor
                    var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                    if (currentUserId.CompareTo(exercise.AuthorId) == 0)
                    {
                        exercise.IsActive = false;
                        var success = _db.SaveChanges();
                        return success > 0 ? true : false;
                    }
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public int Duplicate(int id)
        {
            try
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                var oldModel = _db.Exercise.Where(x => x.Id == id).Where(x => x.AuthorId.CompareTo(currentUserId) == 0 || x.IsPublic == true).FirstOrDefault();
                if (oldModel != null)
                {
                    var extent = Guid.NewGuid().ToString().Substring(0, 2);
                    var dupEx = new Exercise()
                    {
                        AuthorId = currentUserId,
                        CreatedDate = DateTime.Now,
                        IsActive = false,
                        IsPublic = false,
                        Name = string.Format("{0}_{1}", oldModel.Name, extent),
                        ProgramId = oldModel.ProgramId,
                        Description = oldModel.Description,
                    };
                    _db.Exercise.Add(dupEx);
                    _db.SaveChanges();

                    //duplikace vstupnich parametru ulohy
                    foreach (var item in oldModel.ExerciseDatas)
                    {
                        _db.ExerciseData.Add(new ExerciseData()
                        {
                            ExerciseId = dupEx.Id,
                            Row = item.Row,
                            Value = item.Value
                        });
                        _db.SaveChanges();
                    }
                    return dupEx.Id;
                }
                else return 0;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return 0;
            }
        }
        #endregion

        #region Exercises

        public Models.ViewModels.Exercise.ListViewModel ListVM()
        {
            try
            {
                var CurrentUserId = HttpContext.Current.User.Identity.GetUserId();
                //ziskani filtru aktualniho uzivatele
                var filter = _db.ExerciseListFilter.Where(x => x.UserId.CompareTo(CurrentUserId) == 0).FirstOrDefault();
                if (filter == null)
                {
                    //pokud filtr dosud neexistuje, pak jej vytvorime
                    filter = CreateFilter(CurrentUserId);
                }

                return new Models.ViewModels.Exercise.ListViewModel()
                {
                    Filter = filter,
                    ExerciseVMs = GetFilteredExercises(filter).ToList()
                };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public Models.ViewModels.Exercise.ListViewModel SetListFilterAndGetData(ExerciseListFilter filter)
        {
            if (filter != null)
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                var oldFilter = _db.ExerciseListFilter.Where(x => x.UserId.CompareTo(currentUserId) == 0).FirstOrDefault();

                oldFilter.ExerciseIds = filter.ExerciseIds;
                oldFilter.Text = filter.Text;
                oldFilter.IsActive = filter.IsActive;
                oldFilter.IsPublic = filter.IsPublic;
                oldFilter.RecordsPerPage = filter.RecordsPerPage;
                _db.SaveChanges();

                return new Models.ViewModels.Exercise.ListViewModel()
                {
                    Filter = filter,
                    ExerciseVMs = GetFilteredExercises(filter).ToList()
                };
            }
            return null;
        }

        public IQueryable<ExerciseViewModel> GetFilteredExercises(ExerciseListFilter filter)
        {
            try
            {

                if (filter != null)
                {
                    var exercises = _db.Exercise.AsQueryable();
                    var currentUserId = HttpContext.Current.User.Identity.GetUserId();

                    //Filtrace
                    if (filter.IsPublic == 1)
                    {
                        exercises = exercises.Where(x => x.IsPublic == true);
                    }
                    else if (filter.IsPublic == 0)
                    {
                        //kdyz chci privatni ulohy, tak musi byt moje
                        exercises = exercises.Where(x => x.IsPublic == false && x.AuthorId.CompareTo(currentUserId) == 0);
                    }
                    else
                    {
                        //pokud je to jedno, tak vratim vsechny verejne nebo ty moje
                        exercises = exercises.Where(x => x.IsPublic == true || x.AuthorId.CompareTo(currentUserId) == 0);
                    }

                    if (filter.IsActive == 1)
                    {
                        exercises = exercises.Where(x => x.IsActive == true);
                    }
                    else if (filter.IsActive == 0)
                    {
                        exercises = exercises.Where(x => x.IsActive == false);
                    }
                    else
                    {
                        exercises = exercises.Where(x => x.IsActive == true || x.AuthorId.CompareTo(currentUserId) == 0);
                    }


                    if (!string.IsNullOrEmpty(filter.ExerciseIds))
                    {
                        try
                        {
                            List<int> ids = filter.ExerciseIds.Split(';').Select(Int32.Parse).ToList();
                            exercises = exercises.Where(x => ids.Contains(x.Id));
                        }
                        catch (FormatException fe)
                        {

                        }
                    }

                    if (!string.IsNullOrEmpty(filter.Text))
                    {
                        var listNames = filter.Text.Split(';').Select(x => x.Trim().ToLower()).ToList();
                        exercises = exercises.Where(x => listNames.Any(a => x.Name.Trim().ToLower().Contains(a)));
                    }


                    filter.TotalRecords = exercises.Count();

                    if (filter.CurrentPage < 1)
                    {
                        filter.CurrentPage = 1;
                    }

                    var start = (filter.CurrentPage - 1) * filter.RecordsPerPage;
                    if (start >= filter.TotalRecords)
                    {
                        start = start - filter.TotalRecords;
                    }

                    //ziskani uloh na zaklade nastaveneho strankovani              
                    exercises = exercises.OrderByDescending(x => x.CreatedDate).Skip(start).Take(filter.RecordsPerPage).AsQueryable();

                    return exercises.Select(x => new ExerciseViewModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        AuthorId = x.AuthorId,
                        Program = x.Program,
                        Author = x.Author,
                        Description = x.Description,
                        IsActive = x.IsActive,
                        IsPublic = x.IsPublic,
                        CreatedDate = x.CreatedDate,
                        ProgramId = x.ProgramId,
                        HasAssign = _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id).Any(),
                        HasActiveAssign = _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id && y.IsActive == true).Any(),
                        HasResult = _db.ExerciseResult.Where(z => _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id).Select(y => y.Id).ToList().Contains(z.ExerciseAssignId)).Any(),
                        IsAuthor = currentUserId.CompareTo(x.AuthorId) == 0,
                    }).AsQueryable();
                }
                return null;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public IQueryable<ExerciseViewModel> GetExercises()
        {
            try
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();

                //Ziskame vsechny ulohy prihlaseneho uzivatele + vsechny verejne ulohy
                return _db.Exercise.Where(x => x.AuthorId.CompareTo(currentUserId) == 0 || x.IsPublic == true).Select(x => new ExerciseViewModel
                {
                    Id = x.Id,
                    Program = x.Program,
                    Name = x.Name,
                    Author = x.Author,
                    AuthorId = x.AuthorId,
                    CreatedDate = x.CreatedDate,
                    ProgramId = x.ProgramId,
                    IsActive = x.IsActive,
                    IsPublic = x.IsPublic,
                    Description = x.Description,

                    HasAssign = _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id).Any(),
                    HasActiveAssign = _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id && y.IsActive == true).Any(),
                    HasResult = _db.ExerciseResult.Where(z => _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id).Select(y => y.Id).ToList().Contains(z.ExerciseAssignId)).Any(),
                    IsAuthor = currentUserId.CompareTo(x.AuthorId) == 0,

                    ExerciseAssignViewModels = x.ExerciseAssigns.Select(y => new ExerciseAssignViewModel()
                    {
                        AssignDate = y.AssignDate,
                        ExerciseId = y.ExerciseId,
                        Id = y.Id,
                        IsActive = y.IsActive,
                        StatusId = y.StatusId,
                        TherapistId = y.PatientId,
                        PatientId = y.PatientId,

                    }).ToList(),
                    ExerciseDataViewModels = x.ExerciseDatas.Select(y => new ExerciseDataViewModel()
                    {
                        ExerciseId = y.ExerciseId,
                        Id = y.Id,
                        Row = y.Row,
                        Value = y.Value
                    }).ToList()
                });
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public ExerciseManageViewModel GetExerciseVM(int id)
        {
            try
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();

                return _db.Exercise.Where(x => x.IsPublic == true || x.AuthorId.CompareTo(currentUserId) == 0).Where(x => x.Id == id).Select(x => new ExerciseManageViewModel()
                {
                    Id = x.Id,
                    Program = x.Program,
                    ProgramId = x.ProgramId,
                    Name = x.Name,
                    Author = x.Author,
                    AuthorId = x.AuthorId,
                    CreatedDate = x.CreatedDate,
                    IsActive = x.IsActive,
                    IsPublic = x.IsPublic,
                    Description = x.Description,
                    HasAssign = _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id).Any(),
                    HasActiveAssign = _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id && y.IsActive == true).Any(),
                    HasResult = _db.ExerciseResult.Where(z => _db.ExerciseAssign.Where(y => y.ExerciseId == x.Id).Select(y => y.Id).ToList().Contains(z.ExerciseAssignId)).Any(),
                    IsAuthor = currentUserId.CompareTo(x.AuthorId) == 0,

                    ExerciseAssignViewModels = x.ExerciseAssigns.Select(y => new ExerciseAssignViewModel()
                    {
                        AssignDate = y.AssignDate,
                        ExerciseId = y.ExerciseId,
                        Id = y.Id,
                        IsActive = y.IsActive,
                        StatusId = y.StatusId,
                        Status = y.Status,
                        PatientId = y.PatientId,
                        Patient = y.Patient,
                        Therapist = y.Therapist,
                        TherapistId = y.TherapistId
                    }).Where(y => y.IsActive == true).ToList(),
                    ExerciseDataViewModels = x.ExerciseDatas.Select(y => new ExerciseDataViewModel()
                    {
                        ExerciseId = y.ExerciseId,
                        Id = y.Id,
                        Row = y.Row,
                        DataType = _db.ProgramRecipe.Where(z => z.ProgramId == _db.Exercise.Where(e => e.Id == y.ExerciseId).FirstOrDefault().ProgramId).Where(z => z.RowNumber == y.Row).FirstOrDefault().DataTypeId,
                        Value = y.Value,
                        TranslationName = _db.Translation.Where(t => t.Id == _db.ProgramRecipe.Where(r => r.RowNumber == y.Row).Where(r => r.ProgramId == x.ProgramId).FirstOrDefault().RowNameId).Select(t => new TranslationViewModel()
                        {
                            Id = t.Id,
                            Cz = t.Cz,
                            De = t.De,
                            En = t.En,
                            DbColumnName = t.DbColumnName
                        }).FirstOrDefault(),
                        TranslationDescription = _db.Translation.Where(t => t.Id == _db.ProgramRecipe.Where(r => r.RowNumber == y.Row).Where(r => r.ProgramId == x.ProgramId).FirstOrDefault().RowDescriptionId).Select(t => new TranslationViewModel()
                        {
                            Id = t.Id,
                            Cz = t.Cz,
                            De = t.De,
                            En = t.En,
                            DbColumnName = t.DbColumnName
                        }).FirstOrDefault(),
                    }).ToList()
                }).FirstOrDefault();
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        #endregion


        #region Assign

        public IQueryable<ExerciseAssignViewModel> GetFilteredExerciseAssigns(ExerciseAssignListFilter filter)
        {
            try
            {
                if (filter != null)
                {
                    var assigns = _db.ExerciseAssign.AsQueryable();

                    //filtrace nad vsemi parametry filtru
                    if (!string.IsNullOrEmpty(filter.PatientIds))
                    {
                        try
                        {
                            List<string> ids = filter.PatientIds.Split(';').ToList();
                            assigns = assigns.Where(x => ids.Contains(x.PatientId));
                        }
                        catch (FormatException fe) { }
                    }

                    if (!string.IsNullOrEmpty(filter.TherapistIds))
                    {
                        try
                        {
                            List<string> ids = filter.TherapistIds.Split(';').ToList();
                            assigns = assigns.Where(x => ids.Contains(x.TherapistId));
                        }
                        catch (FormatException fe) { }
                    }

                    if (!string.IsNullOrEmpty(filter.ExerciseIds))
                    {
                        try
                        {
                            List<int> ids = filter.ExerciseIds.Split(';').Select(Int32.Parse).ToList();
                            assigns = assigns.Where(x => ids.Contains(x.ExerciseId));
                        }
                        catch (FormatException fe) { }
                    }

                    if (!string.IsNullOrEmpty(filter.StatusIds))
                    {
                        try
                        {
                            List<int> ids = filter.StatusIds.Split(';').Select(Int32.Parse).ToList();
                            assigns = assigns.Where(x => ids.Contains(x.StatusId));
                        }
                        catch (FormatException fe) { }
                    }

                    if (filter.IsActive == ActiveEnum.True.GetHashCode())
                    {
                        assigns = assigns.Where(x => x.IsActive == true);
                    }
                    else if (filter.IsActive == ActiveEnum.False.GetHashCode())
                    {
                        assigns = assigns.Where(x => x.IsActive == false);
                    }

                    #region strankovani
                    //filter.TotalRecords = assigns.Count();

                    //if (filter.CurrentPage < 1)
                    //{
                    //    filter.CurrentPage = 1;
                    //}

                    //var start = (filter.CurrentPage - 1) * filter.RecordsPerPage;
                    //if (start >= filter.TotalRecords)
                    //{
                    //    start = start - filter.TotalRecords;
                    //}


                    //todoo seradit podle aktualne nastaveneho jazyka
                    //assigns = assigns.OrderBy(x => x.Id).Skip(start).Take(filter.RecordsPerPage).AsQueryable();
                    #endregion

                    return assigns.Select(x => new ExerciseAssignViewModel()
                    {
                        Id = x.Id,
                        ExerciseId = x.ExerciseId,
                        Exercise = x.Exercise,
                        AssignDate = x.AssignDate,
                        IsActive = x.IsActive,
                        StatusId = x.StatusId,
                        Status = x.Status,
                        LastAccessDate = x.LastAccessDate,
                        Therapist = x.Therapist,
                        TherapistId = x.TherapistId,
                        Patient = x.Patient,
                        PatientId = x.PatientId,
                        IsUsersAssignActive = _db.PatientAssign.Where(y => y.PatientId.CompareTo(x.PatientId) == 0 && y.TherapistId.CompareTo(x.TherapistId) == 0 && y.IsActive == true).Any(),
                        ExerciseResultsVMs = _db.ExerciseResult.Where(y => y.ExerciseAssignId == x.Id).Select(y => new ExerciseResultViewModel()
                        {
                            ExerciseAssignId = y.ExerciseAssignId,
                            ExerciseAssign = y.ExerciseAssign,
                            Id = y.Id,
                            Row = y.Row,
                            Sequence = y.Sequence,
                            TimeUtc = y.TimeUtc,
                            Value = y.Value
                        }).ToList()
                    });
                }
                return null;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public bool AssignExists(string patientId, int exerciseId)
        {
            try
            {
                //autorizace prihlaseneho uzivatele
                var auth = Infrastructure.Authorization.AuthorizeAny(HttpContext.Current.User, new RoleEnum[] { RoleEnum.Super_therapist, RoleEnum.Therapist });
                if (auth)
                {
                    var therapistId = HttpContext.Current.User.Identity.GetUserId();
                    var assign = _db.ExerciseAssign.Where(x => x.TherapistId == therapistId && x.PatientId == patientId && x.ExerciseId == exerciseId).FirstOrDefault();

                    //pokud prirazeni existuje a bylo neaktivni, pak jej aktivujeme.
                    if (assign != null)
                    {
                        if (!assign.IsActive)
                        {
                            assign.IsActive = true;
                            _db.SaveChanges();
                            return false;
                        }
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public ExerciseAssignViewModel AddExerciseAssign(string patientId, int exerciseId)
        {
            try
            {
                var statId = ExerciseStatusEnum.Novy.GetHashCode();
                var therapistId = HttpContext.Current.User.Identity.GetUserId();

                var assign = _db.ExerciseAssign.Where(x => x.TherapistId == therapistId && x.PatientId == patientId && x.ExerciseId == exerciseId).FirstOrDefault();
                //pokud prirazeni jiz excistovalo, pak vytvorime jeho viewmodel a vratime jej
                if (assign != null)
                {
                    return new ExerciseAssignViewModel()
                    {
                        Id = assign.Id,
                        AssignDate = assign.AssignDate,
                        ExerciseId = assign.ExerciseId,
                        IsActive = assign.IsActive,
                        PatientId = assign.PatientId,
                        Patient = assign.Patient,
                        TherapistId = therapistId,
                        Therapist = assign.Therapist,
                        Status = assign.Status,
                        StatusId = assign.StatusId
                    };
                }
                //jinak jej vytvorime
                else
                {
                    return new ExerciseAssignViewModel()
                    {
                        Id = 0,
                        Exercise = _db.Exercise.Find(exerciseId),// GetExercise(exerciseId),
                        ExerciseId = exerciseId,
                        Patient = _db.Users.Find(patientId),
                        PatientId = patientId,
                        Therapist = _db.Users.Find(therapistId),
                        TherapistId = therapistId,
                        Status = _db.Status.Find(statId),
                        StatusId = statId,
                        LastAccessDate = DateTime.Now,
                        AssignDate = DateTime.Now,
                        IsActive = true,
                    };
                }
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        //TODO SMAZAT -- nejspis se nevola
        //public IQueryable<ExerciseAssignViewModel> GetAssigns()
        //{
        //    try
        //    {
        //        return _db.ExerciseAssign.Select(x => new ExerciseAssignViewModel()
        //        {
        //            Id = x.Id,
        //            ExerciseId = x.ExerciseId,
        //            Exercise = x.Exercise,
        //            AssignDate = x.AssignDate,
        //            IsActive = x.IsActive,
        //            StatusId = x.StatusId,
        //            Status = x.Status,
        //            LastAccessDate = x.LastAccessDate,
        //            Therapist = x.Therapist,
        //            TherapistId = x.TherapistId,
        //            Patient = x.Patient,
        //            PatientId = x.PatientId,
        //            IsUsersAssignActive = _db.PatientAssign.Where(y => y.PatientId.CompareTo(x.PatientId) == 0 && y.TherapistId.CompareTo(x.TherapistId) == 0 && y.IsActive == true).Any(),
        //            ExerciseResultsVMs = _db.ExerciseResult.Where(y => y.ExerciseAssignId == x.Id).Select(y => new ExerciseResultViewModel()
        //            {
        //                ExerciseAssignId = y.ExerciseAssignId,
        //                ExerciseAssign = y.ExerciseAssign,
        //                Id = y.Id,
        //                Row = y.Row,
        //                Sequence = y.Sequence,
        //                TimeUtc = y.TimeUtc,
        //                Value = y.Value
        //            }).ToList()
        //        });
        //    }
        //    catch (Exception e)
        //    {
        //        LogHelper.Log(LogTarget.Database, e);
        //        return null;
        //    }
        //}

        //TODO SMAZAT -- nevola se
        //public IQueryable<ExerciseAssignViewModel> GetAssigns(int exerciseId)
        //{
        //    try
        //    {
        //        return GetFilteredExerciseAssigns(new ExerciseAssignListFilter() { ExerciseIds = exerciseId.ToString() });
        //        //return GetAssigns().Where(x => x.ExerciseId == exerciseId);
        //    }
        //    catch (Exception e)
        //    {
        //        LogHelper.Log(LogTarget.Database, e);
        //        return null;
        //    }
        //}

        //TODO SMAZAT -- nevola se
        //public IQueryable<ExerciseAssignViewModel> GetAssignsFiltered(string input)
        //{
        //    try
        //    {
        //        return GetAssigns().Where(x =>
        //        x.Therapist.FirstName.Contains(input) || x.Therapist.LastName.Contains(input) ||
        //        x.Exercise.Author.FirstName.Contains(input) || x.Exercise.Author.LastName.Contains(input));
        //    }
        //    catch (Exception e)
        //    {
        //        LogHelper.Log(LogTarget.Database, e);
        //        return null;
        //    }
        //}
        //public IQueryable<ExerciseAssignViewModel> GetAssignsFiltered(string input, int exerciseId)
        //{
        //    try
        //    {
        //        var ret = GetAssignsFiltered(input);
        //        var t1 = ret.ToList();
        //        ret = ret.Where(x => x.ExerciseId == exerciseId);
        //        var t2 = ret.ToList();
        //        return ret;
        //    }
        //    catch (Exception e)
        //    {
        //        LogHelper.Log(LogTarget.Database, e);
        //        return null;
        //    }
        //}


        //TODO SMAZAT -- nevola se
        //public ExerciseAssignViewModel GetAssign(int id)
        //{
        //    try
        //    {
        //        return _db.ExerciseAssign.Where(x => x.Id == id).Select(x => new ExerciseAssignViewModel()
        //        {
        //            Id = x.Id,
        //            AssignDate = x.AssignDate,
        //            LastAccessDate = x.LastAccessDate,
        //            ExerciseId = x.ExerciseId,
        //            IsActive = x.IsActive,
        //            StatusId = x.StatusId,
        //            TherapistId = x.PatientId,
        //        }).FirstOrDefault();
        //    }
        //    catch (Exception e)
        //    {
        //        LogHelper.Log(LogTarget.Database, e);
        //        return null;
        //    }
        //}




        //TODO SMAZAT -- nevola se nejspis
        //public IQueryable<ExerciseAssign> GetExerciseAssigns()
        //{
        //    return _db.ExerciseAssign;
        //}

        #endregion

        public SelectList GetProgramList()
        {
            try
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                //Muzeme vytvaret ulohy pouze z aktivnich a pristupnych programu
                return new SelectList(_db.Program.Where(x => x.IsActive == true).Where(x => x.AuthorId.CompareTo(currentUserId) == 0 || x.IsPublic == true), "Id", "Name");
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }


        /// <summary>
        /// Vytvoreni filteru pro seznam uloh
        /// </summary>
        /// <param name="currentUserId">Identifikator aktualne prihlaseneho uzivatele</param>
        /// <returns>vychozi filtr</returns>
        private ExerciseListFilter CreateFilter(string currentUserId)
        {
            try
            {
                var filter = _db.ExerciseListFilter.Add(new ExerciseListFilter()
                {
                    Text = "",
                    UserId = currentUserId,
                    IsActive = 1,
                    IsPublic = 1,
                    ExerciseIds = "",
                    RecordsPerPage = 5
                });
                _db.SaveChanges();
                return filter.Id > 0 ? filter : null;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public bool ExistName(string name, int exerciseId)
        {
            var exercise = _db.Exercise.Where(x => x.Name.CompareTo(name) == 0).FirstOrDefault();
            if (exercise != null)
            {
                //pokud je nalezena uloha totozna s upravovanou, pak existenci ignorujeme
                if (exercise.Id == exerciseId)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        public bool SaveFile(HttpPostedFileBase file, int row, int exerciseId, int programId)
        {
            if (file != null && file.ContentLength > 0)
            {
                //ziskani nazvu atributu v programu -- bude slouzit jako nazev souboru
                var rowDbName = _db.Translation.Where(x => x.Id == _db.ProgramRecipe.Where(y => y.ProgramId == programId && y.RowNumber == row).FirstOrDefault().RowNameId).FirstOrDefault().DbColumnName;

                string filePath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["FilesPath"]);
                filePath = string.Format("{0}/{1}/{2}", filePath, programId, exerciseId);

                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }

                var dirInfo = new DirectoryInfo(filePath);
                var oldFile = dirInfo.GetFiles(row + "_*").FirstOrDefault();

                if (oldFile != null)
                {
                    oldFile.Delete();
                }

                FileInfo fi = new FileInfo(file.FileName);

                //Ukladat se to bude pod stejnym nazvem jako je db nazev radky + identifikator radky
                string saveFileName = string.Format("{0}/{1}_{2}{3}", filePath, row, rowDbName, fi.Extension);
                file.SaveAs(saveFileName);
                var exData = _db.ExerciseData.Where(x => x.ExerciseId == exerciseId && x.Row == row).FirstOrDefault();
                //do databaze se ulozi hodnota 1 (soubor ulozen)
                exData.Value = "1";
                
                _db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}