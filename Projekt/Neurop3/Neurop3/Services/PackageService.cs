﻿using Neurop3.Common;
using Neurop3.Helpers;
using Neurop3.Models;
using Neurop3.Models.Administration;
using Neurop3.Models.Exercises;
using Neurop3.Models.Packages;
using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Models.ViewModels.Package;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Neurop3.Models.ViewModels.User;

namespace Neurop3.Services
{
    public interface IPackageService
    {
        /// <summary>
        /// Metoda pro vytvoreni noveho baliku a ulozeni do databaze
        /// </summary>
        /// <param name="model">Model baliu</param>
        /// <returns>Identifikator vytvoreneho baliku</returns>
        int Create(PackageCreateViewModel model);

        /// <summary>
        /// Metoda pro aktualizaci upraveneho baliku v databazi
        /// </summary>
        /// <param name="model">Model baliku</param>
        /// <returns>Informace o navratove hodnote</returns>
        RequestResult Update(PackageManageViewModel model);

        /// <summary>
        /// Metoda pro zduplikovani konkretniho baliku
        /// </summary>
        /// <param name="packageId">Identifikator baliku</param>
        /// <param name="packageName">Novy nazev pro zduplikovany balik</param>
        /// <param name="patientId">Identifikator pacienta, kteremu je duplikovany balik urceny</param>
        /// <returns>Identifikator noveho baliku</returns>
        int Duplicate(int packageId, string packageName, string patientId);

        /// <summary>
        /// Metoda pro smazani daneho baliku
        /// </summary>
        /// <param name="packageId">Identifikator baliku ke smazani</param>
        /// <returns></returns>
        RequestResult Delete(int packageId);

        /// <summary>
        /// Metoda pro ziskani konkretniho baliku z databaze
        /// </summary>
        /// <param name="id">Identifikator baliku</param>
        /// <returns></returns>
        PackageManageViewModel GetManageVM(int id);

        /// <summary>
        /// Metoda pro zobrazeni seznamu baliku uloh
        /// </summary>
        /// <returns></returns>
        Models.ViewModels.Package.ListViewModel ListVM();

        /// <summary>
        /// Metoda pro aktualizaci filtru v databazi a ziskani baliku, ktere danemu filtru odpovidaji
        /// </summary>
        /// <param name="filter">Novy filtr</param>
        /// <returns></returns>
        Models.ViewModels.Package.ListViewModel SetListFilterAndGetData(PackageListFilter filter);

        /// <summary>
        /// Metoda pro ziskani vsech baliku, ktere pacient muze spustit (novy a probihajici)
        /// </summary>
        /// <returns>Seznam baliku ke spusteni</returns>
        IQueryable<PackagePatientHomeViewModel> GetPackageAssignToPatient();

        /// <summary>
        /// Metoda pro ziskani vsech baliku, ktere terapeut bude mit na homepage
        /// </summary>
        /// <returns>Seznam baliku</returns>
        IQueryable<PackageTherapistHomeViewModel> GetPackageAssignByTherapist();

        /// <summary>
        /// Metoda pro nalezeni ulohy, kterou bychom chteli pridat do baliku
        /// </summary>
        /// <param name="input">Castecny nazev hledane ulohy</param>
        /// <returns>Seznam odpovidajicich uloh</returns>
        IQueryable<ExerciseJSONViewModel> GetExerciseListToPackageAdd(string input);

        /// <summary>
        /// Metoda pro pridani konkretni ulohy do baliku
        /// </summary>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <param name="packageId">Identifikator baliku</param>
        /// <returns>Vysledek operace</returns>
        RequestResult AddExerciseToPackage(int exerciseId, int packageId);

        /// <summary>
        /// Metoda pro odebrani pozadovane ulohy z baliku
        /// </summary>
        /// <param name="packageId">Identifikator baliku</param>
        /// <param name="order">Poradove cislo ulohy v baliku</param>
        /// <returns></returns>
        RequestResult RemoveExerciseAssignFromPackage(int packageId, int order);

        /// <summary>
        /// Metoda pro zjisteni existence jmena baliku. Nesmi dojit k vytvoreni baliku se stejnym nazvem
        /// </summary>
        /// <param name="name">Název baliku</param>
        /// <param name="packageId">Identifikator baliku</param>
        /// <returns>Existence nazvu</returns>
        bool ExistName(string name, int packageId);

        /// <summary>
        /// Metoda pro nastaveni stavu aktualniho baliku vcetne stavu jeho poduloh
        /// </summary>
        /// <param name="packageId">Identifikator baliku</param>
        /// <param name="status">Status</param>
        /// <returns></returns>
        bool SetPackageStatus(int packageId, ExerciseStatusEnum status);
    }

    public class PackageService : IPackageService
    {
        private readonly ApplicationDbContext _db;

        public PackageService(ApplicationDbContext db)
        {
            _db = db;
        }

        #region CRUD
        public int Create(PackageCreateViewModel model)
        {
            try
            {
                var newPackage = new Package()
                {
                    Name = model.Name,
                    CreateTimeUtc = DateTime.UtcNow,
                    LastAccessTimeUtc = DateTime.UtcNow,
                    IsActive = false,
                    StatusId = (int)ExerciseStatusEnum.Novy,
                    PatientId = model.PatientId,
                    TherapistId = HttpContext.Current.User.Identity.GetUserId(),
                };
                _db.Package.Add(newPackage);
                _db.SaveChanges();
                return newPackage.Id > 0 ? newPackage.Id : 0;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return 0;
            }

        }

        public RequestResult Update(PackageManageViewModel model)
        {
            try
            {
                if (model != null)
                {
                    var oldModel = _db.Package.Find(model.Id);
                    var oldActive = oldModel.IsActive;

                    oldModel.IsActive = model.IsActive;
                    oldModel.Name = model.Name;
                    oldModel.StatusId = model.StatusId;
                    SetPackageStatus(model.Id, (ExerciseStatusEnum)model.StatusId);
                    oldModel.LastAccessTimeUtc = DateTime.UtcNow;
                    var saved = _db.SaveChanges();

                    if(saved > 0)
                    {
                        //pokud doslo k aktivaci baliku, poslu mail
                        if(!oldActive && model.IsActive)
                        {
                            var mailSended = Utils.SendMail(new MailMessage()
                            {
                                UserId = model.PatientId,
                                Subject = Resources.Global.bylo_vam_zadano_nove_cviceni_s_ulohami,
                                Body = string.Concat(Resources.Global.bylo_vam_zadano_nove_cviceni_s_ulohami, ": " + model.Name)
                            });
                        }
                    }
                    return RequestResult.Success;
                }
                return RequestResult.Failure;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return RequestResult.Exception;
            }
        }

        public int Duplicate(int packageId, string packageName, string patientId)
        {
            try
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();

                var oldPackage = _db.Package.Find(packageId);

                var extent = Guid.NewGuid().ToString().Substring(0, 2);
                //definice noveho baliku na zaklade stareho
                var newPackage = new Package()
                {
                    CreateTimeUtc = DateTime.UtcNow,
                    LastAccessTimeUtc = DateTime.UtcNow,
                    IsActive = false,
                    Name = !string.IsNullOrEmpty(packageName) ? packageName : string.Concat(oldPackage.Name, "_", extent),
                    StatusId = (int)ExerciseStatusEnum.Novy,
                    TherapistId = oldPackage.TherapistId,
                    PatientId = patientId,
                };

                var exerciseIds = _db.ExerciseAssignPackage.Where(x => x.PackageId == packageId).OrderBy(x => x.Order).Select(y => y.ExerciseAssign.ExerciseId).ToList();

                //do baliku musi byt pridany vsechny prirazeni ulohy, ktere byly v predeslem baliku
                var order = 0;
                foreach (var exerciseId in exerciseIds)
                {
                    var exerciseAssign = _db.ExerciseAssign.Where(x => x.ExerciseId == exerciseId && x.PatientId == patientId && x.TherapistId == currentUserId).FirstOrDefault();

                    //pokud uzivatel doposud nemel tuto ulohu prirazenou (od daneho terapeuta), pak prirazeni vytvorime
                    if (exerciseAssign == null)
                    {
                        exerciseAssign = _db.ExerciseAssign.Add(new ExerciseAssign()
                        {
                            PatientId = patientId,
                            TherapistId = currentUserId,
                            ExerciseId = exerciseId,
                            StatusId = (int)ExerciseStatusEnum.Novy,
                            AssignDate = DateTime.UtcNow,
                            LastAccessDate = DateTime.UtcNow,
                            IsActive = true,
                        });
                        _db.SaveChanges();
                    }

                    //pokud se to vytvorilo nebo aspon naslo
                    if (exerciseAssign.Id > 0)
                    {
                        newPackage.ExerciseAssignPackages.Add(new ExerciseAssignPackage()
                        {
                            ExerciseAssignId = exerciseAssign.Id,
                            PackageId = packageId,
                            Order = order++
                        });
                    }
                    else
                    {
                        return -1;
                    }
                }
                //ulozeni baliku do databaze
                _db.Package.Add(newPackage);
                _db.SaveChanges();
                return newPackage.Id > 0 ? newPackage.Id : -1;
            }
            catch(Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return -1;
            }
        }

        public RequestResult Delete(int packageId)
        {
            try {
                //TODO UPRAVIT -- nejsou zadne dodatecne kontroly (nejspis zadna pravidla byt nemusi, je to jen zalezitost terapeuta)
                var package = _db.Package.Find(packageId);
                _db.Package.Remove(package);
                var deleted = _db.SaveChanges();
                return deleted > 0 ? RequestResult.Success : RequestResult.Error;
            }
            catch(Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return RequestResult.Exception;
            }
        }

        #endregion

        #region VMs

        public Models.ViewModels.Package.ListViewModel ListVM()
        {
            try
            {
                var CurrentUserId = HttpContext.Current.User.Identity.GetUserId();
                //ziskam uzivateluv filtr, pokud neexistuje, pak jej vytvorim
                var filter = _db.PackageListFilter.Where(x => x.UserId.CompareTo(CurrentUserId) == 0).FirstOrDefault();
                if (filter == null)
                {
                    filter = CreateFilter(CurrentUserId);
                }

                //ziskani baliku na zaklade filtru
                var model = GetFilteredPackage(filter).ToList();

                return new Models.ViewModels.Package.ListViewModel()
                {
                    Filter = filter,
                    PackageViewModels = model
                };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        /// <summary>
        /// Metoda pro ziskani baliku na zaklade filtru
        /// </summary>
        /// <param name="filter">Filtr baliku</param>
        /// <returns></returns>
        private IQueryable<PackageViewModel> GetFilteredPackage(PackageListFilter filter)
        {
            if (filter != null)
            {
                var currentTherapist = HttpContext.Current.User.Identity.GetUserId();
                //potrebuji package daneho terapeuta, ovsem s aktivni vazbou na pacienta
                var myPatientdIds = _db.PatientAssign.Where(x => x.TherapistId.CompareTo(currentTherapist) == 0 && x.IsActive == true).Select(x => x.PatientId).ToList();
                //ziskani vsech baliku co terapeut zadal pacientum s aktivni vazbou
                var packages = _db.Package.Where(x => myPatientdIds.Contains(x.PatientId) && x.TherapistId.CompareTo(filter.UserId) == 0).AsQueryable();

                //Filtrace
                if (filter.IsActive == 1)
                {
                    packages = packages.Where(x => x.IsActive == true);
                }
                else if (filter.IsActive == 0)
                {
                    packages = packages.Where(x => x.IsActive == false);
                }

                if (!string.IsNullOrEmpty(filter.PackageName))
                {
                    var listNames = filter.PackageName.Split(';').Select(x => x.Trim().ToLower()).ToList();
                    packages = packages.Where(x => listNames.Any(a => x.Name.Trim().ToLower().Contains(a)));
                }

                if (!string.IsNullOrEmpty(filter.PatientName))
                {
                    var listNames = filter.PatientName.Split(';').Select(x => x.Trim().ToLower()).ToList();
                    var patientIdsList = _db.Users.Where(x => listNames.Any(a =>
                        x.FirstName.Trim().ToLower().Contains(a.Trim().ToLower()) ||
                        x.LastName.Trim().ToLower().Contains(a.Trim().ToLower()) ||
                        string.Concat(x.FirstName, " ", x.LastName).ToLower().Trim().Contains(a.Trim().ToLower())
                    )).Select(x => x.Id).ToList();
                    packages = packages.Where(x => patientIdsList.Contains(x.PatientId));
                }

                //Nastaveni strankovani
                filter.TotalRecords = packages.Count();

                if (filter.CurrentPage < 1)
                {
                    filter.CurrentPage = 1;
                }

                var start = (filter.CurrentPage - 1) * filter.RecordsPerPage;
                if (start >= filter.TotalRecords)
                {
                    start = start - filter.TotalRecords;
                }

                packages = packages.OrderByDescending(x => x.CreateTimeUtc).Skip(start).Take(filter.RecordsPerPage).AsQueryable();
                
                return packages.Select(x => new PackageViewModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsActive = x.IsActive,
                    CreateTimeUtc = x.CreateTimeUtc,
                    LastAccessTimeUtc = x.LastAccessTimeUtc,
                    Status = x.Status,
                    StatusId = x.StatusId,
                    Patient = _db.Users.Where(y => y.Id == x.PatientId).Select(y => new UserViewModel()
                    {
                        Id = y.Id,
                        FirstName = y.FirstName,
                        LastName = y.LastName
                    }).FirstOrDefault(),
                    AssignCount = x.ExerciseAssignPackages.Count()
                }).AsQueryable();
            }
            return null;
        }

        public PackageManageViewModel GetManageVM(int id)
        {
            try
            {
                var model = _db.Package.Where(x => x.Id == id).Select(x => new PackageManageViewModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    CreateTimeUtc = x.CreateTimeUtc,
                    LastAccessTimeUtc = x.LastAccessTimeUtc,
                    IsActive = x.IsActive,
                    StatusId = x.StatusId,
                    Status = x.Status,
                    TherapistId = x.TherapistId,
                    PatientId = x.PatientId,
                    Patient = _db.Users.Where(y => y.Id == x.PatientId).Select(y => new UserViewModel()
                    {
                        FirstName = y.FirstName,
                        LastName = y.LastName,
                        Email = y.Email,
                    }).FirstOrDefault(),
                    ExerciseAssignPackageViewModels = _db.ExerciseAssignPackage.Where(y => y.PackageId == x.Id).Select(y => new ExerciseAssignPackageViewModel()
                    {
                        Id = y.Id,
                        ExerciseAssignId = y.ExerciseAssignId,
                        Order = y.Order,
                        ExercisePackageId = y.PackageId,
                        ExerciseAssign = _db.ExerciseAssign.Where(z => z.Id == y.ExerciseAssignId).Select(z => new ExerciseAssignViewModel()
                        {
                            Id = z.Id,
                            Patient = z.Patient,
                            PatientId = z.PatientId,
                            Therapist = z.Therapist,
                            TherapistId = z.TherapistId,
                            AssignDate = z.AssignDate,
                            ExerciseId = z.ExerciseId,
                            Exercise = z.Exercise,
                            IsActive = z.IsActive,
                            LastAccessDate = z.LastAccessDate,
                            Status = z.Status,
                            StatusId = z.StatusId,
                            ResultsCount = z.ExerciseResults.Where(r => r.Row == 0).Count()  //_db.ExerciseResult.Where(r => r.ExerciseAssignId == z.Id && r.Row == 0).Count(),
                        }).FirstOrDefault()
                    }).ToList(),
                    patientFeedbackVMs = _db.PatientFeedback.Where(y => y.PackageId == x.Id).Select(y => new PatientFeedbackViewModel() {
                        Id = y.Id,
                        Difficulty = y.Difficulty,
                        PackageId = y.PackageId,
                        Editable = false,
                        ExerciseAmount = y.ExerciseAmount,
                        Note = y.Note,
                        TimeUtc = y.TimeUtc
                    }).ToList()
                }).FirstOrDefault();
                return model;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public Models.ViewModels.Package.ListViewModel SetListFilterAndGetData(PackageListFilter filter)
        {
            if (filter != null)
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                var oldFilter = _db.PackageListFilter.Where(x => x.UserId.CompareTo(currentUserId) == 0).FirstOrDefault();
                //aktualizace filtru
                oldFilter.PatientName = filter.PatientName;
                oldFilter.PackageName = filter.PackageName;
                oldFilter.IsActive = filter.IsActive;
                oldFilter.RecordsPerPage = filter.RecordsPerPage;
                _db.SaveChanges();

                //Ziskani baliku na zaklade noveho filtru
                return new Models.ViewModels.Package.ListViewModel()
                {
                    Filter = filter,
                    PackageViewModels = GetFilteredPackage(filter).ToList()
                };
            }
            return null;
        }
        #endregion

        #region Other
        public IQueryable<PackagePatientHomeViewModel> GetPackageAssignToPatient()
        {
            var currentPatientId = HttpContext.Current.User.Identity.GetUserId();
            //ziskani vsech aktivnich terapeutu pacienta
            var myTherapistIds = _db.PatientAssign.Where(x => x.PatientId == currentPatientId && x.IsActive == true).Select(x => x.TherapistId).ToList();

            //ziskani vsech baliku od vsech terapeutu, ktere jsou aktivni
            var packages = _db.Package.Where(x => myTherapistIds.Contains(x.TherapistId) && x.PatientId.CompareTo(currentPatientId) == 0 && x.IsActive == true);
            //baliky musi byt ve stavu nove nebo zpracovavane
            var statusIds = new List<int> { ExerciseStatusEnum.Novy.GetHashCode(), ExerciseStatusEnum.Probihajici.GetHashCode() };
            packages = packages.Where(x => statusIds.Contains(x.StatusId));

            return packages.Select(x => new PackagePatientHomeViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                CreateTimeUtc = x.CreateTimeUtc,
                LastAccessTimeUtc = x.LastAccessTimeUtc,
                IsActive = x.IsActive,
                Status = x.Status,
                StatusId = x.StatusId,
                Therapist = _db.Users.Where(y => y.Id == x.TherapistId).Select(y => new UserViewModel() {
                    FirstName = y.FirstName,
                    LastName = y.LastName,
                    Id = y.Id,
                }).FirstOrDefault(),
                ExercisesCount = x.ExerciseAssignPackages.Where(y => y.ExerciseAssign.Exercise.IsActive == true).Count(),
                //RunCount = _db.ExerciseResult.Where(y => y.ExerciseAssignId == x.ExerciseAssignPackages.Where(z => z.PackageId == ))
            });
        }

        public IQueryable<PackageTherapistHomeViewModel> GetPackageAssignByTherapist()
        {
            var currentTherapist = HttpContext.Current.User.Identity.GetUserId();

            //aktivni pacienti daneho terapeuta
            var myPatientIds = _db.PatientAssign.Where(x => x.TherapistId.CompareTo(currentTherapist) == 0 && x.IsActive == true).Select(x => x.PatientId).ToList();



            //ziskani aktivnich baliku od prihlaseneho terapeuta vsech svych pacientu
            var packages = _db.Package.Where(x => myPatientIds.Contains(x.PatientId) && x.TherapistId.CompareTo(currentTherapist) == 0 && x.IsActive == true);
            //baliky musi byt nove nebo zpracovavane
            var statusIds = new List<int> {ExerciseStatusEnum.Novy.GetHashCode(), ExerciseStatusEnum.Probihajici.GetHashCode() };
            packages = packages.Where(x => statusIds.Contains(x.StatusId));

            return packages.Select(x => new PackageTherapistHomeViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                CreateTimeUtc = x.CreateTimeUtc,
                LastAccessTimeUtc = x.LastAccessTimeUtc,
                IsActive = x.IsActive,
                Status = x.Status,
                StatusId = x.StatusId,
                ExercisesCount = x.ExerciseAssignPackages.Where(y => y.ExerciseAssign.Exercise.IsActive == true).Count()
            });
        }

        public RequestResult AddExerciseToPackage(int exerciseId, int packageId)
        {
            var package = _db.Package.Find(packageId);

            var assignToAdd = _db.ExerciseAssign.Where(x => x.PatientId == package.PatientId && x.TherapistId == package.TherapistId && x.ExerciseId == exerciseId).FirstOrDefault(); ;

            //Pokud dany pacient nemel doposud ulohu prirazenou, pak vytvorime prirazeni
            if (assignToAdd == null)
            {
                assignToAdd = new ExerciseAssign()
                {
                    AssignDate = DateTime.UtcNow,
                    LastAccessDate = DateTime.UtcNow,
                    ExerciseId = exerciseId,
                    IsActive = true,
                    PatientId = package.PatientId,
                    TherapistId = package.TherapistId,
                    StatusId = ExerciseStatusEnum.Novy.GetHashCode()
                };
                _db.ExerciseAssign.Add(assignToAdd);
                _db.SaveChanges();
            }

            //pokud se to ulozilo nebo prirazeni existovalo
            if (assignToAdd.Id > 0)
            {
                //Vytvorime novou vazbu mezi balikem a prirazenim ulohy
                var newExerciseAssignPackage = new ExerciseAssignPackage()
                {
                    ExerciseAssignId = assignToAdd.Id,
                    Order = package.ExerciseAssignPackages.Count(),
                    PackageId = package.Id,
                };
                _db.ExerciseAssignPackage.Add(newExerciseAssignPackage);
                _db.SaveChanges();
                if (newExerciseAssignPackage.Id > 0)
                {
                    return RequestResult.Success;
                }
            }
            return RequestResult.Failure;
        }

        public RequestResult RemoveExerciseAssignFromPackage(int packageId, int order)
        {
            try
            {
                //ziskani vsech uloh v baliku
                var assignsInPackage = _db.ExerciseAssignPackage.Where(x => x.PackageId == packageId).OrderBy(x => x.Order).ToList();

                int newOrder = 0;
                foreach (var assing in assignsInPackage)
                {
                    //nez narazim na pozadovanou ulohu, nic se nedeje
                    if (assing.Order < order)
                    {
                        continue;
                    }
                    //po nalezeni pozadovane ulohy ji odstranim
                    else if (assing.Order == order)
                    {
                        newOrder = order;
                        //deaktivace vazby, aby sla smazat uloha
                        var exerciseAssign = _db.ExerciseAssign.Find(assing.ExerciseAssignId);
                        exerciseAssign.IsActive = false;

                        _db.ExerciseAssignPackage.Remove(assing);
                        _db.SaveChanges();
                    }
                    // Jinak nastavuji nove poradove cislo
                    else
                    {
                        assing.Order = newOrder++;
                        _db.SaveChanges();
                    }
                }
                return RequestResult.Success;
            }
            catch( Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return RequestResult.Exception;
            }
        }

        public IQueryable<ExerciseJSONViewModel> GetExerciseListToPackageAdd(string input)
        {
            var currentUserId = HttpContext.Current.User.Identity.GetUserId();

            var exercises = _db.Exercise.Where(x => x.IsActive == true).Where(x => x.AuthorId.CompareTo(currentUserId) == 0 || x.IsPublic == true).AsQueryable();

            if (!string.IsNullOrEmpty(input))
            {
                exercises = exercises.Where(x => x.Name.ToLower().Trim().Contains(input.ToLower().Trim()));
            }
            return exercises.Select(x => new ExerciseJSONViewModel()
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        public bool ExistName(string name, int packageId)
        {
            var packages = _db.Package.Where(x => x.Name.CompareTo(name) == 0);
            if (packages.Any())
            {
                if (packages.FirstOrDefault().Id == packageId)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        /// <summary>
        /// Vytvoreni noveho filtru pro aktualne prihlaseneho uzivatele
        /// </summary>
        /// <param name="currentUserId">Identifikator uzivatle</param>
        /// <returns>Filtr pro baliky</returns>
        private PackageListFilter CreateFilter(string currentUserId)
        {
            try
            {
                //nastaveni na vychozi hodnoty
                var filter = _db.PackageListFilter.Add(new PackageListFilter()
                {
                    PatientName = "",
                    UserId = currentUserId,
                    IsActive = 1,
                    RecordsPerPage = 5,
                });
                _db.SaveChanges();
                return filter.Id > 0 ? filter : null;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public bool SetPackageStatus(int packageId, ExerciseStatusEnum status)
        {
            try
            {
                int statusId = status.GetHashCode();

                var package = _db.Package.Find(packageId);
                package.StatusId = statusId;

                foreach (var exAssing in package.ExerciseAssignPackages.Select(x => x.ExerciseAssign))
                {
                    exAssing.StatusId = statusId;
                    exAssing.LastAccessDate = DateTime.Now;
                }
                _db.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }
    }
}