﻿using Neurop3.Common;
using Neurop3.Helpers;
using Neurop3.Models;
using Neurop3.Models.Exercises;
using Neurop3.Models.Programs;
using Neurop3.Models.ViewModels.Exercise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Neurop3.Models.ViewModels.User;
using Neurop3.Models.ViewModels.Analysis;

namespace Neurop3.Services
{
    public interface IAnalysisService
    {
        /// <summary>
        /// Metoda pro ziskani vsech uloh, ktere odpovidaji kriteriim v parametrech. 
        /// Nejprve je filtrace provedena na zaklade identifikatoru programu, pote nad Id ulohy a nasledne id pacienta.
        /// Vsechny parametry nohou obsahovat vice identifikatoru, ktere jsou oddeleny strednikem
        /// </summary>
        /// <param name="programIds"></param>
        /// <param name="exerciseIds"></param>
        /// <param name="patientIds"></param>
        /// <returns></returns>
        IQueryable<ExerciseAssignViewModel> GetExerciseAssignsToAnalysisView(string programIds, string exerciseIds, string patientIds);

        /// <summary>
        /// Metoda pro vraceni konretniho prirazni ulohy pro daneho uzivatele.
        /// Slouzi pro analyzu hledaneho prirazeni ulohy
        /// </summary>
        /// <param name="assignId">Identifikator prirazeni ulohy, ktere chceme analyzovat</param>
        /// <returns></returns>
        AnalyzeViewModel AnalyzeVM(int assignId);

        /// <summary>
        /// Ziskani vsech pacientu, kteri maji prirazene ulohy se zadanymi mnozinami identifikatoru.
        /// </summary>
        /// <param name="programIds">Identifikatory programu oddelene strednikem</param>
        /// <param name="exerciseIds">Identifikatory uloh oddelene strednikem</param>
        /// <returns></returns>
        IQueryable<ApplicationUser> GetPatientSelect(string programIds, string exerciseIds);
        
        /// <summary>
        /// Ziskani vsech vytvorenych uloh
        /// </summary>
        /// <returns></returns>
        IQueryable<Exercise> GetExerciseSelect();

        /// <summary>
        /// Ziskani vsech vytvoreny programu.
        /// </summary>
        /// <returns></returns>
        IQueryable<Program> GetProgramsSelect();
    }

    public class AnalysisService : IAnalysisService
    {
        private readonly ApplicationDbContext _db;

        public AnalysisService(ApplicationDbContext db)
        {
            _db = db;
        }


        public AnalyzeViewModel AnalyzeVM(int assignId)
        {
            var patientId = _db.ExerciseAssign.Where(x => x.Id == assignId).FirstOrDefault().PatientId;
            var programId = _db.ExerciseAssign.Where(x => x.Id == assignId).FirstOrDefault().Exercise.ProgramId;

            //Chci ziskat vsechny vysledky dane ulohy. Vysledky seskupuji podle casu jejich spusteni. Kazde spusteni obsahuje nekolik parametru
            var groupedResults = _db.ExerciseResult.Where(x => x.ExerciseAssignId == assignId).
                    GroupBy(x => x.Sequence).OrderBy(g => g.Key).Select(s => s.ToList()).ToList();

            var resultRowList = new List<ExerciseResultsRowViewModel>();

            foreach (var item in groupedResults)
            {
                //pridavani vysledku z jednotlivych spusteni do kolekce
                resultRowList.Add(new ExerciseResultsRowViewModel()
                {
                    Id = item.FirstOrDefault().Id,
                    ExerciseAssignId = item.FirstOrDefault().ExerciseAssignId,
                    Sequence = item.FirstOrDefault().Sequence,
                    TimeUtc = item.FirstOrDefault().TimeUtc,
                    Values = item.Select(x => new Values() {
                        RowIndex = x.Row,
                        Value = x.Value,
                        RowName = _db.Translation.Where(z => z.Id == (_db.ProgramResult.Where(y => y.ProgramId == programId && y.RowNumber == x.Row).FirstOrDefault().RowNameId)).FirstOrDefault().LocalizedTranslation,
                    }).ToList()
                });
            }

            return new AnalyzeViewModel()
            {
                ExerciseResultsViewModels = _db.ExerciseResult.Where(x => x.ExerciseAssignId == assignId).Select(x => new ExerciseResultViewModel()
                {
                    Id = x.Id,
                    ExerciseAssignId = x.ExerciseAssignId,
                    ExerciseAssign = x.ExerciseAssign,
                    Row = x.Row,
                    Value = x.Value,
                    TimeUtc = x.TimeUtc,
                    Sequence = x.Sequence
                }).ToList(),
                patient = _db.Users.Where(x => x.Id.CompareTo(patientId) == 0).Select(x => new UserViewModel()
                {
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).FirstOrDefault(),
                ExerciseResultsRowVMs = resultRowList
            };
        }

        public IQueryable<ExerciseAssignViewModel> GetExerciseAssignsToAnalysisView(string programIds, string exerciseIds, string patientIds)
        {
            var currentUserId = HttpContext.Current.User.Identity.GetUserId();
            //ziskani vsech uloh, ktere uzivatel ma
            var assigns = _db.ExerciseAssign.Where(x => x.TherapistId == currentUserId).AsQueryable();

            //filtrace zaznamu
            if (!string.IsNullOrEmpty(programIds))
            {
                var programList = Array.ConvertAll(programIds.Split(','), s => Int32.Parse(s));
                assigns = assigns.Where(x => programList.Contains(x.Exercise.ProgramId));
            }

            if (!string.IsNullOrEmpty(exerciseIds))
            {
                var exerciseList = Array.ConvertAll(exerciseIds.Split(','), s => Int32.Parse(s));
                assigns = assigns.Where(x => exerciseList.Contains(x.ExerciseId));
            }

            if (!string.IsNullOrEmpty(patientIds))
            {
                var patientList = patientIds.Split(',').ToList();
                assigns = assigns.Where(x => patientList.Contains(x.PatientId));
            }

            //vraceni vsech prirazeni, ktere prosli filtrem
            return assigns.Select(x => new ExerciseAssignViewModel()
            {
                Id = x.Id,
                AssignDate = x.AssignDate,
                Exercise = _db.Exercise.Where(y => y.Id == x.ExerciseId).FirstOrDefault(),
                IsActive = x.IsActive,
                PatientId = x.PatientId,
                Patient = x.Patient,
                TherapistId = x.TherapistId,
                Therapist = x.Therapist,
                StatusId = x.StatusId,
                Status = x.Status,
                LastAccessDate = x.LastAccessDate,
                ExerciseId = x.ExerciseId,
                //vsechny vysledky dane ulohy
                ExerciseResultsVMs = _db.ExerciseResult.Where(y => y.ExerciseAssignId == x.Id).Select(y => new ExerciseResultViewModel()
                {
                    ExerciseAssignId = y.ExerciseAssignId,
                    Id = y.Id,
                    Row = y.Row,
                    Value = y.Value,
                    TimeUtc = y.TimeUtc,
                }).ToList()
            });
        }


        #region Data for selects
        public IQueryable<Exercise> GetExerciseSelect()
        {
            try
            {
                return _db.Exercise;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public IQueryable<ApplicationUser> GetPatientSelect(string programIds, string exerciseIds)
        {
            try
            {
                //potrebuji ziskat pacienty, jejichz terapeut je aktualne prihlaseny 
                var therapistId = HttpContext.Current.User.Identity.GetUserId();
                var patientIds = _db.PatientAssign.Where(x => x.IsActive == true).Where(x => x.TherapistId.CompareTo(therapistId) == 0).Select(x => x.PatientId);

                if (!string.IsNullOrEmpty(programIds))
                {
                    var programList = Array.ConvertAll(programIds.Split(','), s => Int32.Parse(s));
                    var patIds = patientIds.AsEnumerable();
                    patientIds = _db.ExerciseAssign.Where(x => x.IsActive == true).Where(x => patIds.Contains(x.PatientId)).Where(y => programList.Contains(y.Exercise.ProgramId)).Select(x => x.PatientId);
                }

                if (!string.IsNullOrEmpty(exerciseIds))
                {
                    var exerciseList = Array.ConvertAll(exerciseIds.Split(','), s => Int32.Parse(s));
                    var patIds = patientIds.AsEnumerable();
                    patientIds = _db.ExerciseAssign.Where(x => x.IsActive == true).Where(x => patIds.Contains(x.PatientId)).Where(y => exerciseList.Contains(y.ExerciseId)).Select(x => x.PatientId);
                }

                patientIds = patientIds.Distinct();

                return _db.PatientAssign.Where(x => x.IsActive == true).Where(x => patientIds.Contains(x.PatientId)).Select(x => x.Patient);

            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public IQueryable<Program> GetProgramsSelect()
        {
            try
            {
                return _db.Program;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }
        #endregion
    }
}