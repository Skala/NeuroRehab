﻿using Neurop3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Neurop3.Models.ViewModels.Test;
using System.Configuration;
using Neurop3.Models.Exercises;
using Neurop3.Common;
using System.Text;
using System.IO;
using System.Web.Hosting;
using Neurop3.Helpers;

namespace Neurop3.Services
{

    public interface ITestService
    {
        /// <summary>
        /// Metoda pro ziskani modelu pro spusteni dane ulohy
        /// </summary>
        /// <param name="assignId">Identifikator prirazeni ulohy k pacientovi</param>
        /// <returns>Model ulohy ke spusteni</returns>
        RunTestViewModel RunTestVM(int assignId);

        /// <summary>
        /// Metoda pro ziskani modelu pro spusteni ukazky ulohy
        /// </summary>
        /// <param name="exerciseId">Identifikator ulohy</param>
        /// <returns></returns>
        RunTestViewModel RunTestPrototypeVM(int exerciseId);

        /// <summary>
        /// Metoda pro ziskani identifikatoru vsech uloh v danem baliku 
        /// </summary>
        /// <param name="packageId">Identifikator baliku</param>
        /// <returns>Seznam s identifikatory prirazeni uloh</returns>
        List<int> GetAssignIdsInPackage(int packageId);

        /// <summary>
        /// Ulozeni vysledku z ulohy
        /// </summary>
        /// <param name="model">Model s dokoncenou ulohou</param>
        /// <returns></returns>
        bool SaveExerciseResults(RunTestViewModel model);
    }


    public class TestService : ITestService
    {
        private readonly ApplicationDbContext _db;

        public TestService(ApplicationDbContext db)
        {
            _db = db;
        }


        public RunTestViewModel RunTestPrototypeVM(int exerciseId)
        {
            try
            {
                var exercise = _db.Exercise.Find(exerciseId);

                var model = new RunTestViewModel()
                {
                    IsValid = true,
                    ExerciseAssignId = exerciseId,
                    Name = exercise.Name,
                    PathToScript = string.Format("{0}/{1}/{2}.js", ConfigurationManager.AppSettings["FilesPath"], exercise.ProgramId, exercise.Program.Name),
                    //InputParameters = string.Join("##", exercise.ExerciseDatas.OrderBy(x => x.Row).Select(x => x.Value)),

                };

                model.PathToScript = model.PathToScript.Replace("\\", "\\");

                var newLineTag = ConfigurationManager.AppSettings["NewLineTag"];
                var dir = string.Format("{0}{1}/{2}", HostingEnvironment.MapPath(ConfigurationManager.AppSettings["FilesPath"]), exercise.ProgramId, exercise.Id);

                var dirInfo = new DirectoryInfo(dir);

                var recipes = _db.ProgramRecipe.Where(x => x.ProgramId == exercise.ProgramId).ToList();
                var sb = new StringBuilder();

                foreach (var recipe in recipes)
                {
                    var recipeValue = "";

                    if (recipe.DataTypeId == DataTypeRecipeEnum.Obrazek.GetHashCode() || recipe.DataTypeId == DataTypeRecipeEnum.Zvuk.GetHashCode())
                    {
                        //TODO UPRAVIT -- dodefinovat co udelat kdyz by to bylo null
                        if (dirInfo != null)
                        {
                            var filePath = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["FilesPath"], exercise.ProgramId, exercise.Id);
                            var file = dirInfo.GetFiles(recipe.RowNumber + "_*").FirstOrDefault();
                            var toAdd = string.Format("{0}/{1}", filePath, file.Name);
                            toAdd = toAdd.Replace("\\", "\\");
                            recipeValue = toAdd;
                        }

                    }
                    else
                    {
                        var value = exercise.ExerciseDatas.Where(x => x.Row == recipe.RowNumber).FirstOrDefault().Value;
                        recipeValue = value;
                    }

                    if (!string.IsNullOrEmpty(recipeValue))
                    {
                        sb.Append(string.Concat(recipeValue, newLineTag));
                    }
                    else
                    {
                        throw new Exception();
                    }

                }

                model.InputParameters = sb.ToString();

                return model;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return new RunTestViewModel()
                {
                    IsValid = false,
                    IsValidMessage = "Nelze otestovat úlohu, protože její vstupní parametry nejsou validní",
                    ExerciseAssignId = exerciseId,
                };
            }
        }

        public RunTestViewModel RunTestVM(int assignId)
        {
            try
            {
                //na zaklade prirazeni zjistim o kterou ulohu se jedn
                var exercise = _db.Exercise.Find(_db.ExerciseAssign.Find(assignId).ExerciseId);

                if (exercise.IsActive)
                {
                    //nastaveni modelu pro spusteni ulohy
                    var model = new RunTestViewModel()
                    {
                        ExerciseAssignId = assignId,
                        Name = exercise.Name,
                        PathToScript = string.Format("{0}/{1}/{2}.js", ConfigurationManager.AppSettings["FilesPath"], exercise.ProgramId, exercise.Program.Name),
                        //InputParameters = string.Join("##", exercise.ExerciseDatas.OrderBy(x => x.Row).Select(x => x.Value)),
                        Demo = false
                    };

                    model.PathToScript = model.PathToScript.Replace("\\", "\\");

                    var newLineTag = ConfigurationManager.AppSettings["NewLineTag"];
                    var dir = string.Format("{0}{1}/{2}", HostingEnvironment.MapPath(ConfigurationManager.AppSettings["FilesPath"]), exercise.ProgramId, exercise.Id);

                    var dirInfo = new DirectoryInfo(dir);

                    var recipes = _db.ProgramRecipe.Where(x => x.ProgramId == exercise.ProgramId).ToList();
                    var sb = new StringBuilder();

                    //ziskani vstupnich parametru z ulohy do textove podoby, ktera bude predana skriptu
                    foreach (var recipe in recipes)
                    {
                        if (recipe.DataTypeId == DataTypeRecipeEnum.Obrazek.GetHashCode() || recipe.DataTypeId == DataTypeRecipeEnum.Zvuk.GetHashCode())
                        {
                            //TODO UPRAVIT --  dodefinovat co udelat kdyz by to bylo null
                            if (dirInfo != null)
                            {
                                var filePath = string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["FilesPath"], exercise.ProgramId, exercise.Id);
                                var file = dirInfo.GetFiles(recipe.RowNumber + "_*").FirstOrDefault();
                                var toAdd = string.Format("{0}/{1}", filePath, file.Name);
                                toAdd = toAdd.Replace("\\", "\\");
                                sb.Append(toAdd);
                            }

                        }
                        else
                        {
                            var value = exercise.ExerciseDatas.Where(x => x.Row == recipe.RowNumber).FirstOrDefault().Value;
                            sb.Append(value);
                        }
                        sb.Append(newLineTag);
                    }

                    model.InputParameters = sb.ToString();

                    model.MessageBefore = string.Format("{0} - {1}", Resources.Global.nasledujici_uloha_bude_spustena, model.Name);
                    model.MessageAfter = Resources.Global.bude_spustena_dalsi_uloha;
                    model.Message = exercise.Description;
                    return model;
                }
                else
                {
                    return new RunTestViewModel()
                    {
                        IsValid = false,
                        IsValidMessage = Resources.Global.ulohu_nelze_spustit_protoze_je_oznacena_jako_neaktivni,
                    };
                }
            }
            catch(Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return new RunTestViewModel()
                {
                    IsValid = false,
                    IsValidMessage = Resources.Global.ulohu_nelze_spustit_protoze_pri_nacitani_vstupnich_parametru_doslo_k_chybe,
                };
            }
        }

        public bool SaveExerciseResults(RunTestViewModel model)
        {
            var itemToSaveNo = 0; // aby nebyly casy ulozeni uplne stejne pri ukladani vice dat.
            
            //poradove cislo nove pridavaneho zaznamu
            var records = _db.ExerciseResult.Where(x => x.ExerciseAssignId == model.ExerciseAssignId);
            var sequence = records != null && records.Any() ? records.OrderByDescending(x => x.TimeUtc).FirstOrDefault().Sequence : 0;
            sequence++;

            foreach (var result in model.OutputParameters.Split(";;".ToCharArray()))
            {
                if (string.IsNullOrEmpty(result))
                {
                    continue;
                }

                var rowCount = 0;

                var saveTime = DateTime.UtcNow.AddSeconds(itemToSaveNo);
                foreach (var item in result.Split("##".ToCharArray()))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        _db.ExerciseResult.Add(new ExerciseResult()
                        {
                            Row = rowCount++,
                            Value = item.Trim(),
                            TimeUtc = saveTime,
                            ExerciseAssignId = model.ExerciseAssignId,
                            Sequence = sequence
                        });
                    }
                }
                itemToSaveNo++;
                sequence++;
                _db.SaveChanges();
            }
            //aktualizace prirazeni ulohy
            var currentAssign = _db.ExerciseAssign.Find(model.ExerciseAssignId);
            currentAssign.LastAccessDate = DateTime.UtcNow;
            //nastaveni noveho stavu, v pripade prvniho procviceni
            if (((ExerciseStatusEnum)currentAssign.StatusId) == ExerciseStatusEnum.Novy)
            {
                currentAssign.StatusId = ExerciseStatusEnum.Probihajici.GetHashCode();
            }

            _db.SaveChanges();
            return true;
        }

        public List<int> GetAssignIdsInPackage(int packageId)
        {
            return _db.ExerciseAssignPackage.Where(x => x.PackageId == packageId).Where(x => x.ExerciseAssign.Exercise.IsActive == true).OrderBy(x => x.Order).Select(x => x.ExerciseAssignId).ToList();
        }
    }
}