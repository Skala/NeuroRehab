﻿using Neurop3.Common;
using Neurop3.Helpers;
using Neurop3.Models;
using Neurop3.Models.Programs;
using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Models.ViewModels.Program;
using Neurop3.Models.ViewModels.Translation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using Microsoft.AspNet.Identity;
using Neurop3.Models.ViewModels.Program.Views;
using System.Net;
using System.Text;

namespace Neurop3.Services
{
    public interface IProgramService
    {
        /// <summary>
        /// Metoda pro vytvoreni noveho programu na zaklade modelu
        /// </summary>
        /// <param name="model">Model vytvareneho programu</param>
        /// <returns>Identifikator vytvoreneho programu</returns>
        int CreateProgram(ProgramCreateViewModel model);

        /// <summary>
        /// Aktualizace daneho programu podle jeho upraveneho modelu
        /// </summary>
        /// <param name="model">Model programu, ktery ma byt prepsan v databazi</param>
        /// <returns></returns>
        bool Update(ProgramViewModel model);

        /// <summary>
        /// Metoda pro zduplikovani programu
        /// </summary>
        /// <param name="id">Identifikator programu</param>
        /// <returns></returns>
        int Duplicate(int id);

        /// <summary>
        /// Metoda pro odebrani daneho programu ze system
        /// </summary>
        /// <param name="id">Identifikator odebiraneho programu</param>
        /// <returns></returns>
        bool Delete(int id);

        /// <summary>
        /// Metoda pro aktivovani neaktivniho programu
        /// </summary>
        /// <param name="id">Identifikator programu, ktery chceme aktivovat</param>
        /// <returns></returns>
        bool Activate(int id);

        /// <summary>
        /// Metoda pro Deaktivovatmi programu
        /// </summary>
        /// <param name="id">Identifikatr deaktivovaneho programu</param>
        /// <returns></returns>
        bool Deactivate(int id);

        /// <summary>
        /// Metoda pro zobrazeni seznamu programu, ktere odpovidaji filtru z databaze
        /// </summary>
        /// <returns></returns>
        Models.ViewModels.Program.ListViewModel ListVM();

        /// <summary>
        /// Metoda pro aktualizaci filtru uzivatele a ziskani patricnych dat
        /// </summary>
        /// <param name="filter">Novy filtr, ktery ma byt ulozen do databaze</param>
        /// <returns></returns>
        Models.ViewModels.Program.ListViewModel SetListFilterAndGetData(ProgramListFilter filter);

        /// <summary>
        /// Metoda pro ziskani programu, ktere odpovidaji zadanemu filtru
        /// </summary>
        /// <param name="filter">Filtr programu</param>
        /// <returns>Seznam programu odpovidajici danemu filtru</returns>
        IQueryable<ProgramViewModel> GetFilteredPrograms(ProgramListFilter filter);

        /// <summary>
        /// Metoda pro ziskani pozadovaneho programu z databaze
        /// </summary>
        /// <param name="programType">Identifikator programu</param>
        /// <returns>Pozadovany programu</returns>
        ProgramViewModel GetProgram(int programType);

        /// <summary>
        /// Metoda pro ziskani modelu nove radky, resp. vstupniho parametru do ulohy
        /// </summary>
        /// <param name="programType">Identifikator programu</param>
        /// <returns></returns>
        ProgramRecipeViewModel AddNewRecipeRow(int programType);

        /// <summary>
        /// Metoda pro ziskani modelu nove radky, resp. vystupniho parametru z programu
        /// </summary>
        /// <param name="programType">Identifikator programu</param>
        /// <returns></returns>
        ProgramResultViewModel AddNewResultRow(int programType);

        /// <summary>
        /// Metoda pro zjisteni existence zadaneho nazvu programu. Nesmi dojit k vytvoreni programu, ktery ma duplicitni nazev.
        /// </summary>
        /// <param name="name">Nazev programu</param>
        /// <param name="programId">Identifikator editovaneho programu</param>
        /// <returns></returns>
        bool ExistName(string name, int programId);

        /// <summary>
        /// Metoda pro ulozeni skriptu k programu
        /// </summary>
        /// <param name="file">Soubor se skriptem</param>
        /// <param name="name">Nazev programu</param>
        /// <param name="programId">Identifikator programu</param>
        /// <returns></returns>
        bool SaveScripts(HttpPostedFileBase file, string name, int programId);

        /// <summary>
        /// Metoda pro ulozeni dodatecnych souboru k programu
        /// </summary>
        /// <param name="files">Kolekce souboru k ulozeni</param>
        /// <param name="programId">Identifikator programu</param>
        /// <returns></returns>
        int SaveFiles(HttpFileCollectionBase files, int programId);
    }


    public class ProgramService : IProgramService
    {
        private readonly ApplicationDbContext _db;

        public ProgramService(ApplicationDbContext db)
        {
            _db = db;
        }

        #region CRUD
        public int CreateProgram(ProgramCreateViewModel model)
        {
            try
            {
                var newProgram = new Program()
                {
                    Name = model.Name,
                    Description = model.Description,
                    AuthorId = HttpContext.Current.User.Identity.GetUserId(),
                    IsActive = false,
                    IsPublic = false,
                };

                _db.Program.Add(newProgram);
                var saved = _db.SaveChanges();
                return saved > 0 ? newProgram.Id : 0;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return -1;
            }
        }

        public int Duplicate(int id)
        {
            try
            {
                var model = _db.Program.Find(id);

                var extent = Guid.NewGuid().ToString().Substring(0, 2);
                var newProgram = new Program()
                {
                    Name = string.Concat(model.Name, "_", extent),
                    IsActive = true,
                    AuthorId = HttpContext.Current.User.Identity.GetUserId(),
                    Description = model.Description,
                    IsPublic = false,
                };

                //pokud program obsahuje definici vstupu, pak jej musim vytvorit pro novy program
                if (model.ProgramRecipes != null && model.ProgramRecipes.Any())
                {
                    foreach (var recipe in model.ProgramRecipes.OrderBy(x => x.RowNumber))
                    {
                        newProgram.ProgramRecipes.Add(new ProgramRecipe()
                        {
                            DataTypeId = recipe.DataTypeId,
                            ProgramId = recipe.ProgramId,
                            RowDescriptionId = recipe.RowDescriptionId,
                            RowNameId = recipe.RowNameId,
                            RowNumber = recipe.RowNumber,
                            Value = recipe.Value
                        });
                    }
                }

                //Pokud program obsahuje definici vystupu, pak je musim take zduplikovat
                if (model.ProgramResults != null && model.ProgramResults.Any())
                {
                    foreach (var result in model.ProgramResults.OrderBy(x => x.RowNumber))
                    {
                        newProgram.ProgramResults.Add(new ProgramResult()
                        {
                            DataTypeId = result.DataTypeId,
                            ProgramId = result.ProgramId,
                            RowDescriptionId = result.RowDescriptionId,
                            RowNameId = result.RowNameId,
                            RowNumber = result.RowNumber
                        });
                    }
                }
                _db.Program.Add(newProgram);
                _db.SaveChanges();
                return newProgram.Id;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return 0;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                //Nejprve si ziskam VM, ze ktereho zjistim, zda je jiz pouzit nebo ne
                var toDelete = GetProgram(id);
                if (toDelete != null && !toDelete.IsUsed)
                {
                    _db.Program.Remove(_db.Program.Find(id));
                    var saved = _db.SaveChanges();
                    return saved > 0;
                }
                else { return false; };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public bool Activate(int id)
        {
            try
            {
                //nalezeni programu a nastavevni aktivity
                var activateProgram = _db.Program.Find(id);
                if (activateProgram != null)
                {
                    activateProgram.IsActive = true;
                    var saved = _db.SaveChanges();
                    return saved > 0;
                }
                else { return false; };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public bool Deactivate(int id)
        {
            try
            {
                var deletedProgram = _db.Program.Find(id);
                if (deletedProgram != null)
                {
                    deletedProgram.IsActive = false;

                    var saved = _db.SaveChanges();
                    return saved > 0;
                }
                else { return false; };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public bool Update(ProgramViewModel model)
        {
            try
            {
                var isAuthor = HttpContext.Current.User.Identity.GetUserId().CompareTo(!string.IsNullOrEmpty(model.AuthorId) ? model.AuthorId : "") == 0;
                if (model != null && isAuthor)
                {
                    if (model.IsUsed)
                    {
                        var oldModel = _db.Program.Find(model.Id);
                        oldModel.IsActive = model.IsActive;
                        oldModel.IsPublic = model.IsPublic;
                        _db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        //nahrani skriptu
                        if (model.File != null && model.File.ContentLength > 0)
                        {
                            this.SaveScripts(model.File, model.Name, model.Id);
                        }

                        #region aktualizace modulu
                        var oldModel = _db.Program.Find(model.Id);
                        oldModel.Name = model.Name;
                        oldModel.Description = model.Description;
                        oldModel.IsActive = model.IsActive;
                        oldModel.IsPublic = model.IsPublic;
                        _db.SaveChanges();
                        #endregion

                        #region aktualizace programu recipe
                        //aktualni predpis, ktery byl v databazi
                        var allRecipes = _db.ProgramRecipe.Where(x => x.ProgramId == model.Id).ToList();
                        //predpisy, ktere maji byt zapsany do databaze
                        var recipesFromView = model.ProgramRecipeViewModels.Select(x => x.Id).ToList();

                        //odebrani predpisu, ktere jsou v databazi, ale nejsou v modelu
                        foreach (var ar in allRecipes)
                        {
                            if (!recipesFromView.Contains(ar.Id))
                            {
                                _db.ProgramRecipe.Remove(ar);
                            }
                        }
                        _db.SaveChanges();

                        var rowCount = 0;

                        //aktualizace predpisu podle hodnot ve VM
                        foreach (var recipe in model.ProgramRecipeViewModels)
                        {
                            //pokud se jedna o novy predpis, pak jej ulozime do databaze
                            if (recipe.Id == 0)
                            {
                                _db.ProgramRecipe.Add(new ProgramRecipe()
                                {
                                    DataTypeId = recipe.DataTypeId,
                                    ProgramId = recipe.ProgramId,
                                    RowDescriptionId = recipe.TranslationDescriptionViewModel.Id,
                                    RowNameId = recipe.TranslationNameViewModel.Id,
                                    RowNumber = rowCount++,
                                    Value = recipe.Value,
                                });
                            }
                            //pokud jiz existoval, tak mu nastavim hodnoty z VM a nastavim nove poradove cislo
                            else
                            {
                                var oldRecipe = _db.ProgramRecipe.Find(recipe.Id);
                                oldRecipe.RowDescriptionId = recipe.TranslationDescriptionViewModel.Id;
                                oldRecipe.RowNameId = recipe.TranslationNameViewModel.Id;
                                oldRecipe.RowNumber = rowCount++;
                                oldRecipe.Value = recipe.Value;
                                oldRecipe.DataTypeId = recipe.DataTypeId;
                            }
                        }
                        _db.SaveChanges();
                        #endregion

                        //Stejnym zpusobem provedeme aktualizaci predpisu pro vysledky
                        #region aktualizace program result
                        var allResults = _db.ProgramResult.Where(x => x.ProgramId == model.Id).ToList();
                        var resultsFromView = model.ProgramResultViewModels.Select(x => x.Id).ToList();

                        foreach (var ar in allResults)
                        {
                            if (!resultsFromView.Contains(ar.Id))
                            {
                                _db.ProgramResult.Remove(ar);
                            }
                        }
                        _db.SaveChanges();

                        rowCount = 0;
                        foreach (var result in model.ProgramResultViewModels)
                        {
                            if (result.Id == 0)
                            {
                                _db.ProgramResult.Add(new ProgramResult()
                                {
                                    DataTypeId = result.DataTypeId,
                                    ProgramId = result.ProgramId,
                                    RowDescriptionId = result.TranslationDescriptionViewModel.Id,
                                    RowNameId = result.TranslationNameViewModel.Id,
                                    RowNumber = rowCount++,
                                });
                            }
                            else
                            {
                                var oldResult = _db.ProgramResult.Find(result.Id);
                                oldResult.RowDescriptionId = result.TranslationDescriptionViewModel.Id;
                                oldResult.RowNameId = result.TranslationNameViewModel.Id;
                                oldResult.RowNumber = rowCount++;
                                oldResult.DataTypeId = result.DataTypeId;
                            }
                        }
                        _db.SaveChanges();
                        #endregion
                        return true;
                    }
                }
                return false;

            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }
        #endregion

        #region VMs
        public Models.ViewModels.Program.ListViewModel ListVM()
        {
            try
            {
                var CurrentUserId = HttpContext.Current.User.Identity.GetUserId();
                //ziskani uzivatelova filtru
                var filter = _db.ProgramListFilter.Where(x => x.UserId.CompareTo(CurrentUserId) == 0).FirstOrDefault();
                if (filter == null)
                {
                    filter = CreateFilter(CurrentUserId);
                }

                return new Models.ViewModels.Program.ListViewModel()
                {
                    Filter = filter,
                    ProgramVMs = GetFilteredPrograms(filter).ToList()
                };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        /// <summary>
        /// Metoda pro nastaveni filtru a ziskani dat
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public Models.ViewModels.Program.ListViewModel SetListFilterAndGetData(ProgramListFilter filter)
        {
            if (filter != null)
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                var oldFilter = _db.ProgramListFilter.Where(x => x.UserId.CompareTo(currentUserId) == 0).FirstOrDefault();

                oldFilter.ProgramIds = filter.ProgramIds;
                oldFilter.Text = filter.Text;
                oldFilter.IsActive = filter.IsActive;
                oldFilter.IsPublic = filter.IsPublic;
                oldFilter.RecordsPerPage = filter.RecordsPerPage;
                _db.SaveChanges();

                return new Models.ViewModels.Program.ListViewModel()
                {
                    Filter = filter,
                    ProgramVMs = GetFilteredPrograms(filter).ToList()
                };
            }
            return null;
        }

        #endregion

        public IQueryable<ProgramViewModel> GetFilteredPrograms(ProgramListFilter filter)
        {
            if (filter != null)
            {
                var programs = _db.Program.AsQueryable();

                var currentUserId = HttpContext.Current.User.Identity.GetUserId();

                if (filter.IsPublic == 1)
                {
                    programs = programs.Where(x => x.IsPublic == true);
                }
                else if (filter.IsPublic == 0)
                {
                    //pokud chci privatni, tak musi byt zaroven moje
                    programs = programs.Where(x => x.IsPublic == false && x.AuthorId.CompareTo(currentUserId) == 0);
                }
                else
                {
                    //pokud je to jedno, tak vratim vsechny verejne nebo ty moje
                    programs = programs.Where(x => x.IsPublic == true || x.AuthorId == currentUserId);
                }

                if (filter.IsActive == 1)
                {
                    programs = programs.Where(x => x.IsActive == true);
                }
                else if (filter.IsActive == 0)
                {
                    //pokud chci zobrazit neaktivni, tak musi byt moje
                    programs = programs.Where(x => x.IsActive == false && x.AuthorId.CompareTo(currentUserId) == 0);
                }
                else
                {
                    //pokud je to jedno, tak vratim vsechny verejne nebo ty moje
                    programs = programs.Where(x => x.IsActive == true || x.AuthorId == currentUserId);
                }

                if (!string.IsNullOrEmpty(filter.ProgramIds))
                {
                    try
                    {
                        List<int> ids = filter.ProgramIds.Split(';').Select(Int32.Parse).ToList();
                        programs = programs.Where(x => ids.Contains(x.Id));
                    }
                    catch (FormatException fe)
                    {

                    }
                }

                //if (!string.IsNullOrEmpty(filter.UserId))
                //{
                //    programs = programs.Where(x => x.AuthorId.CompareTo(filter.UserId) == 0);
                //}

                if (!string.IsNullOrEmpty(filter.Text))
                {
                    var listNames = filter.Text.Split(';').Select(x => x.Trim().ToLower()).ToList();
                    programs = programs.Where(x => listNames.Any(a => x.Name.Trim().ToLower().Contains(a)));
                }

                //if (!string.IsNullOrEmpty(filter.Text))
                //{
                //    programs = programs.Where(x => x.Name.Trim().ToLower().Contains(filter.Text.Trim().ToLower()));
                //}

                //nastaveni strankovani
                filter.TotalRecords = programs.Count();

                if (filter.CurrentPage < 1)
                {
                    filter.CurrentPage = 1;
                }

                var start = (filter.CurrentPage - 1) * filter.RecordsPerPage;
                if (start >= filter.TotalRecords)
                {
                    start = start - filter.TotalRecords;
                }

                programs = programs.OrderBy(x => x.Name).Skip(start).Take(filter.RecordsPerPage).AsQueryable();

                return programs.Select(x => new ProgramViewModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    AuthorId = x.AuthorId,
                    Author = x.Author,
                    Description = x.Description,
                    IsActive = x.IsActive,
                    IsPublic = x.IsPublic,
                    IsUsed = _db.Exercise.Where(y => y.ProgramId == x.Id).Any(),
                }).AsQueryable();
            }
            return null;
        }

        public ProgramViewModel GetProgram(int programId)
        {
            try
            {
                return _db.Program.Where(x => x.Id == programId).Select(x => new ProgramViewModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    IsUsed = _db.Exercise.Where(y => y.ProgramId == x.Id).Any(),
                    IsPublic = x.IsPublic,
                    Description = x.Description,
                    AuthorId = x.AuthorId,
                    IsActive = x.IsActive,
                    Author = x.Author,
                    ExerciseViewModels = _db.Exercise.Where(y => y.ProgramId == x.Id).Select(y => new ExerciseViewModel()
                    {
                        Author = y.Author,
                        CreatedDate = y.CreatedDate,
                        Id = y.Id,
                        IsActive = y.IsActive,
                        Program = y.Program,
                        Name = y.Name
                    }).ToList(),
                    ProgramRecipeViewModels = _db.ProgramRecipe.Where(y => y.ProgramId == x.Id).Select(y => new ProgramRecipeViewModel()
                    {
                        DataTypeId = y.DataTypeId,
                        Id = y.Id,
                        ProgramId = y.ProgramId,
                        TranslationNameViewModel = _db.Translation.Where(z => z.Id == y.RowNameId).Select(z => new TranslationViewModel()
                        {
                            Id = z.Id,
                            Cz = z.Cz,
                            De = z.De,
                            En = z.En
                        }).FirstOrDefault(),
                        TranslationDescriptionViewModel = _db.Translation.Where(z => z.Id == y.RowDescriptionId).Select(z => new TranslationViewModel()
                        {
                            Id = z.Id,
                            Cz = z.Cz,
                            De = z.De,
                            En = z.En
                        }).FirstOrDefault(),
                        RowNumber = y.RowNumber,
                        Value = y.Value
                    }).ToList(),
                    ProgramResultViewModels = _db.ProgramResult.Where(y => y.ProgramId == x.Id).Select(y => new ProgramResultViewModel()
                    {
                        DataTypeId = y.DataTypeId,
                        RowNumber = y.RowNumber,
                        TranslationNameViewModel = _db.Translation.Where(z => z.Id == y.RowNameId).Select(z => new TranslationViewModel()
                        {
                            Id = z.Id,
                            Cz = z.Cz,
                            De = z.De,
                            En = z.En
                        }).FirstOrDefault(),
                        TranslationDescriptionViewModel = _db.Translation.Where(z => z.Id == y.RowDescriptionId).Select(z => new TranslationViewModel()
                        {
                            Id = z.Id,
                            Cz = z.Cz,
                            De = z.De,
                            En = z.En
                        }).FirstOrDefault(),
                        Id = y.Id,
                        ProgramId = y.ProgramId
                    }).ToList(),
                }).FirstOrDefault();
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        

        public ProgramRecipeViewModel AddNewRecipeRow(int programId)
        {
            try
            {
                var rows = _db.ProgramRecipe.Where(x => x.ProgramId == programId).Count();

                var newRecipeRow = new ProgramRecipeViewModel()
                {
                    ProgramId = programId,
                    RowNumber = rows,
                    TranslationNameViewModel = _db.Translation.Where(z => z.Id == 1).Select(z => new TranslationViewModel()
                    {
                        Id = z.Id,
                        Cz = z.Cz,
                        De = z.De,
                        En = z.En
                    }).FirstOrDefault(),
                    TranslationDescriptionViewModel = _db.Translation.Where(z => z.Id == 1).Select(z => new TranslationViewModel()
                    {
                        Id = z.Id,
                        Cz = z.Cz,
                        De = z.De,
                        En = z.En
                    }).FirstOrDefault(),
                    DataTypeId = DataTypeRecipeEnum.Text.GetHashCode(),
                };
                return newRecipeRow;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public ProgramResultViewModel AddNewResultRow(int programId)
        {
            try
            {
                var rows = _db.ProgramResult.Where(x => x.ProgramId == programId).Count();

                var newResultRow = new ProgramResultViewModel()
                {
                    ProgramId = programId,
                    RowNumber = rows,
                    TranslationNameViewModel = _db.Translation.Where(z => z.Id == 1).Select(z => new TranslationViewModel()
                    {
                        Id = z.Id,
                        Cz = z.Cz,
                        De = z.De,
                        En = z.En
                    }).FirstOrDefault(),
                    TranslationDescriptionViewModel = _db.Translation.Where(z => z.Id == 1).Select(z => new TranslationViewModel()
                    {
                        Id = z.Id,
                        Cz = z.Cz,
                        De = z.De,
                        En = z.En
                    }).FirstOrDefault(),
                    DataTypeId = DataTypeResultEnum.Text.GetHashCode(),
                };
                return newResultRow;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        /// <summary>
        /// Vytvoreni noveho filtru pro uzivatele
        /// </summary>
        /// <param name="currentUserId">Identifikator uzivatele, kteremu se filtr vytvari</param>
        /// <returns></returns>
        private ProgramListFilter CreateFilter(string currentUserId)
        {
            try
            {
                var filter = _db.ProgramListFilter.Add(new ProgramListFilter()
                {
                    Text = "",
                    UserId = currentUserId,
                    IsActive = ActiveEnum.True.GetHashCode(),
                    IsPublic = ActiveEnum.True.GetHashCode(),
                    ProgramIds = "",
                });
                _db.SaveChanges();
                return filter.Id > 0 ? filter : null;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public bool ExistName(string name, int programId)
        {
            var program = _db.Program.Where(x => x.Name.CompareTo(name) == 0);
            if (program.Any())
            {
                if (program.FirstOrDefault().Id == programId)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        public bool SaveScripts(HttpPostedFileBase file, string name, int programId)
        {
            if (file != null && file.ContentLength > 0)
            {
                string scriptPath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["FilesPath"]);
                scriptPath = string.Format("{0}/{1}", scriptPath, programId);

                if (!Directory.Exists(scriptPath))
                {
                    Directory.CreateDirectory(scriptPath);
                }

                //Ukladat se to bude pod stejnym nazvem jako je nazev programu
                string saveFileName = string.Format("{0}/{1}.js", scriptPath, name);
                file.SaveAs(saveFileName);
                return true;
            }
            return false;
        }

        public int SaveFiles(HttpFileCollectionBase files, int programId)
        {
            var savedFiles = 0;
            if (files != null && files.Count != 0)
            {
                for(int i = 0; i < files.Count; i++)
                {
                    var file = files[i];
                    if (file != null && file.ContentLength > 0)
                    {
                        string scriptPath = HostingEnvironment.MapPath(ConfigurationManager.AppSettings["FilesPath"]);
                        scriptPath = string.Format("{0}/{1}", scriptPath, programId);

                        if (!Directory.Exists(scriptPath))
                        {
                            Directory.CreateDirectory(scriptPath);
                        }
                        //Ukladat se to bude pod stejnym nazvem jako je nazev programu
                        string saveFileName = string.Format("{0}/{1}", scriptPath, file.FileName);
                        file.SaveAs(saveFileName);
                        savedFiles++;
                    }

                }
            }
            return savedFiles;
        }
    }
}