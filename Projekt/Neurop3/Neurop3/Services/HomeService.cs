﻿using Microsoft.AspNet.Identity;
using Neurop3.Common;
using Neurop3.Helpers;
using Neurop3.Models;
using Neurop3.Models.Administration;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using Neurop3.GlobalListValues.Enums;


namespace Neurop3.Services
{

    public interface IHomeService
    {
        /// <summary>
        /// Metoda pro ziskani seznamu vyjimek, ktere v aplikaci nastaly
        /// </summary>
        /// <returns></returns>
        IQueryable<ErrorLog> GetErrors();

        /// <summary>
        /// Metoda pro vytvoreni nove pripominky k aplikaci.
        /// </summary>
        /// <param name="model">Pridavana pripominka</param>
        /// <returns></returns>
        bool CreateComment(Comment model);

        /// <summary>
        /// Metoda pro ziskani vsech pripominky, ktere byly vytvoreny
        /// </summary>
        /// <returns></returns>
        IQueryable<Comment> GetComments();

        /// <summary>
        /// Metoda pro ziskani konkretni pripominky
        /// </summary>
        /// <param name="id">Identifikator pripominky</param>
        /// <returns></returns>
        Comment GetComment(int id);

        /// <summary>
        /// Metoda pro upraveni pripominky
        /// </summary>
        /// <param name="model">Model pripominky, ktera ma byt upravena</param>
        /// <returns></returns>
        bool UpdateComment(Comment model);

        /// <summary>
        /// Metoda pro ziskani vsech logu o zmenach v aplikaci
        /// </summary>
        /// <returns></returns>
        IQueryable<ChangeLog> GetChangeLogs();

        /// <summary>
        /// Metoda pro vytvoreniho noveho logu v aplikaci
        /// </summary>
        /// <param name="log">Pridavany log o zmenach v aplikaci</param>
        /// <returns></returns>
        bool CreateChangeLog(ChangeLog log);

        /// <summary>
        /// Metoda pro ziskani seznamu se vsemi prihlasenimi, ktere byly v ramci aplikace provedeny
        /// </summary>
        /// <returns></returns>
        IQueryable<LoginLog> GetLoginLogs();

        /// <summary>
        /// Metoda pro vytvoreni noveho zaznamu o prihlaseni uzivatele
        /// </summary>
        /// <param name="userMail">E-mail prihlasovaneho uzivatele</param>
        /// <returns></returns>
        bool CreateLoginLog(string userMail);
    }

    public class HomeService: IHomeService
    {
        private readonly ApplicationDbContext _db;

        public HomeService(ApplicationDbContext db)
        {
            _db = db;
        }

        #region Comment
        public bool CreateComment(Comment model)
        {
            try
            {
                if(model != null)
                {
                    model.TimeUtc = DateTime.UtcNow;
                    _db.Comment.Add(model);
                    var saved = _db.SaveChanges();
                    if (saved > 0)
                    {
                        var user = _db.Users.Find(model.UserId);
                        var SAMail = AppSetting.GetSetting<string>("smtpMail");
                        string SAId = _db.Users.Where(x => x.Email.CompareTo(SAMail) == 0).FirstOrDefault().Id;

                        Utils.SendMail(new MailMessage() {
                            UserId = SAId,
                            Subject = Resources.Global.byla_pridana_pripominka,
                            Body = string.Format("<h2>" + Resources.Global.uzivatel_0_pridal_novou_pripominku + "</h2>" + 
                                "<p>" + Resources.Global.nazev + ": "+ model.Name +"</p>" +
                                "<p>" + Resources.Global.zprava + ": "+ model.Message +"</p>" +
                                "<p>" + Resources.Global.typ_pripominky + ": "+ ((CommentType) model.Type).GetDescriptionString() +"</p>", user.FullName)
                        });
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public Comment GetComment(int id)
        {
            try
            {
                return _db.Comment.Find(id);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public IQueryable<Comment> GetComments()
        {
            try
            {
                return _db.Comment.OrderByDescending(x => x.TimeUtc);
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public bool UpdateComment(Comment model)
        {
            try
            {
                if(model != null)
                {
                    var oldModel = _db.Comment.Find(model.Id);
                    oldModel.Message = model.Message;

                    var currentUser = HttpContext.Current.User.Identity.GetUserId();
                    var SAMail = AppSetting.GetSetting<string>("smtpMail");
                    var SAId = _db.Users.Where(x => x.Email.CompareTo(SAMail) == 0).FirstOrDefault().Id;

                    if (currentUser.CompareTo(SAId) == 0)
                    {
                        oldModel.IsDone = model.IsDone;
                        oldModel.IsShown = model.IsShown;
                    }
                    _db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }
        #endregion

        #region Error
        public IQueryable<ErrorLog> GetErrors()
        {
            try
            {
                return _db.ErrorLog;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }
        #endregion

        #region ChangeLog
        public IQueryable<ChangeLog> GetChangeLogs()
        {
            try
            {
                return _db.ChangeLog;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public bool CreateChangeLog(ChangeLog log)
        {
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

                log.Version = fvi.FileVersion;
                log.Id = Guid.NewGuid();
                log.TimeUtc = DateTime.UtcNow;
                
                
                var aa = _db.ChangeLog.Add(log);
                var saved = _db.SaveChanges();
                return saved > 0 ? true : false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }
        #endregion

        #region Login log
        public IQueryable<LoginLog> GetLoginLogs()
        {
            try
            {
                return _db.LoginLog;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public bool CreateLoginLog(string userMail)
        {
            try
            {
                var userId = _db.Users.Where(x => x.Email.CompareTo(userMail) == 0).FirstOrDefault().Id;
                var log = _db.LoginLog.Add(new LoginLog()
                {
                    Id = Guid.NewGuid(),
                    TimeUtc = DateTime.Now,
                    UserId = userId
                });
                var saved = _db.SaveChanges();

                return saved > 0 ? true : false;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }
        #endregion
        
    }
}