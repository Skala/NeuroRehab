﻿using Neurop3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Neurop3.Models.ViewModels.Translation;
using Neurop3.Models.Administration;
using Neurop3.Helpers;
using Microsoft.AspNet.Identity;
using Neurop3.Common;

namespace Neurop3.Services
{

    public interface ITranslationService
    {
        /// <summary>
        /// Metoda pro vytvoreni noveho prekladu z modelu
        /// </summary>
        /// <param name="model">Model prekladu</param>
        /// <returns></returns>
        RequestResult Create(TranslationViewModel model);

        /// <summary>
        /// Metoda pro ulozeni upraveneho prekladu do databaze
        /// </summary>
        /// <param name="model">Model upraveneho prekladu</param>
        /// <returns></returns>
        RequestResult Update(TranslationViewModel model);

        /// <summary>
        /// Metoda pro smazani daneho prekladu
        /// </summary>
        /// <param name="id">Identifikator prekladu ke smazani</param>
        /// <returns></returns>
        RequestResult Delete(int id);

        /// <summary>
        /// Metoda, ktera vytvori kopii daneho prekladu
        /// </summary>
        /// <param name="id">Identifikator prekladu, ktery ma byt duplikovan</param>
        /// <returns>Identifikator noveho prekladu</returns>
        int Duplicate(int id);

        /// <summary>
        /// Metoda pro ziskani konkretniho prekladu
        /// </summary>
        /// <param name="id">Identifikator prekladu</param>
        /// <returns>Pozadovany preklad</returns>
        TranslationViewModel GetTranslation(int id);

        /// <summary>
        /// Metoda pro ziskani seznamu prekladu, ktere odpovidaji filtru z databaze
        /// </summary>
        /// <returns>strukturu se seznamem prekladu a filtrem</returns>
        ListViewModel ListVM();

        /// <summary>
        /// Metoda pro aktualizaci uzivatelskeho filtru a ziskani prislusnych prekladu
        /// </summary>
        /// <param name="filter">Filtr, ktery bude ulozen do databaze</param>
        /// <returns>Filtr s pozadovanymi preklady</returns>
        ListViewModel ListFilter(TranslationListFilter filter);

        /// <summary>
        /// Metoda pro ziskani vsech prekladu, ktere odpovidaji zadanemu filtru
        /// </summary>
        /// <param name="filter">Filtr</param>
        /// <returns>Seznam prekladu</returns>
        IQueryable<TranslationViewModel> GetFilteredTranslations(TranslationListFilter filter);

        /// <summary>
        /// Metoda pro zjisteni, zda existuje preklad s timto nazvem
        /// </summary>
        /// <param name="name">Nazev prekladu</param>
        /// <param name="translationId">Identifikator prekladu</param>
        /// <returns></returns>
        bool ExistDbName(string name, int translationId);
    }


    public class TranslationService : ITranslationService
    {
        private readonly ApplicationDbContext _db;

        public TranslationService(ApplicationDbContext db)
        {
            this._db = db;
        }


        #region CRUD
        public RequestResult Create(TranslationViewModel model)
        {
            try
            {
                var translation = new Translation()
                {
                    Cz = model.Cz,
                    En = model.En,
                    De = model.De,
                    DbColumnName = model.DbColumnName,
                };
                _db.Translation.Add(translation);
                _db.SaveChanges();
                return translation.Id > 0 ? RequestResult.Success : RequestResult.Error;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return RequestResult.Exception;
            }

        }

        public RequestResult Update(TranslationViewModel model)
        {
            if (model != null)
            {
                try
                {
                    var oldTranslation = _db.Translation.Find(model.Id);
                    oldTranslation.En = model.En;
                    oldTranslation.Cz = model.Cz;
                    oldTranslation.De = model.De;
                    oldTranslation.DbColumnName = model.DbColumnName;
                    var saved = _db.SaveChanges();
                    return RequestResult.Success;
                    //return saved > 0 ? RequestResult.Success : RequestResult.Error;
                }
                catch (Exception e)
                {
                    LogHelper.Log(LogTarget.Database, e);
                    return RequestResult.Exception;
                }
            }
            return RequestResult.Error;
        }

        public RequestResult Delete(int id)
        {
            try
            {
                var isUsed = _db.Translation.Where(x => x.Id == id).Where(x => x.ProgramRecipeNames.Any() || x.ProgramRecipeDescriptions.Any() || x.ProgramResultNames.Any() || x.ProgramRecipeDescriptions.Any()).Count();

                //pokud preklad neni nikde pouzit, pak jej smazeme
                if (isUsed == 0)
                {
                    _db.Translation.Remove(_db.Translation.Find(id));
                    var result = _db.SaveChanges();
                }
                return RequestResult.Success;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return RequestResult.Exception;
            }
        }

        public int Duplicate(int id)
        {
            try
            {
                var oldModel = _db.Translation.Find(id);
                //koncovka duplikovaneho prekladu
                var extent = Guid.NewGuid().ToString().Substring(0, 2);
                var newTranslation = new Translation()
                {
                    Cz = string.Concat(oldModel.Cz, extent),
                    De = string.Concat(oldModel.De, extent),
                    En = string.Concat(oldModel.En, extent),
                    DbColumnName = string.Concat(oldModel.DbColumnName, extent),
                };
                _db.Translation.Add(newTranslation);
                _db.SaveChanges();
                return newTranslation.Id;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return 0;
            }
        }
        #endregion

        #region Pristupy
        public TranslationViewModel GetTranslation(int id)
        {
            try
            {
                return _db.Translation.Where(x => x.Id == id).Select(x => new TranslationViewModel()
                {
                    Id = x.Id,
                    Cz = x.Cz,
                    En = x.En,
                    De = x.De,
                    DbColumnName = x.DbColumnName,
                    IsUsed = x.ProgramRecipeDescriptions.Any() || x.ProgramRecipeNames.Any() || x.ProgramResultDescriptions.Any() || x.ProgramResultNames.Any()
                }).FirstOrDefault();
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public IQueryable<TranslationViewModel> GetFilteredTranslations(TranslationListFilter filter)
        {
            var translations = _db.Translation.AsQueryable();
            var start = 0;
            var count = Int32.MaxValue;

            if(filter != null)
            {
                if (!string.IsNullOrEmpty(filter.Text))
                {
                    var listNames = filter.Text.Split(';').Select(x => x.Trim().ToLower()).ToList();
                    translations = translations.Where(x => listNames.Any(a => x.Cz.Trim().ToLower().Contains(a) || x.De.Trim().ToLower().Contains(a) || x.En.Trim().ToLower().Contains(a)));
                }

                filter.TotalRecords = translations.Count();
                if (filter.CurrentPage < 1)
                {
                    filter.CurrentPage = 1;
                }

                start = (filter.CurrentPage - 1) * filter.RecordsPerPage;
                count = filter.RecordsPerPage;

                if (start >= filter.TotalRecords)
                {
                    start = start - filter.TotalRecords;
                }
            }
            

            var langCookie = HttpContext.Current.Request.Cookies["lang"];
            var locale = langCookie != null && !string.IsNullOrEmpty(langCookie.Value) ? langCookie.Value : "cs";
            switch (locale.Trim().ToLower())
            {
                case "cs":
                    translations = translations.OrderBy(x => x.Cz);
                    break;
                case "en":
                    translations = translations.OrderBy(x => x.En);
                    break;
                case "de":
                    translations = translations.OrderBy(x => x.De);
                    break;
                default:
                    translations = translations.OrderBy(x => x.Cz);
                    break;
            }

            translations = translations.Skip(start).Take(count);

            return translations.Select(x => new TranslationViewModel()
            {
                Id = x.Id,
                Cz = x.Cz,
                En = x.En,
                De = x.De,
                DbColumnName = x.DbColumnName,
                IsUsed = x.ProgramRecipeDescriptions.Any() || x.ProgramRecipeNames.Any() || x.ProgramResultDescriptions.Any() || x.ProgramResultNames.Any()
            });
        }
        #endregion

        #region Akce

        public ListViewModel ListVM()
        {
            try
            {
                var CurrentUserId = HttpContext.Current.User.Identity.GetUserId();
                var filter = _db.TranslationListFilter.Where(x => x.UserId.CompareTo(CurrentUserId) == 0).FirstOrDefault();
                //pokud aktualni uzivatel dosud nema svuj filtr, pak jej vytvorime
                if (filter == null)
                {
                    filter = CreateFilter(CurrentUserId);
                }

                return new ListViewModel()
                {
                    Filter = filter,
                    TranslationViewModels = GetFilteredTranslations(filter).ToList(),
                };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public ListViewModel ListFilter(TranslationListFilter filter)
        {
            try
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                var oldFilter = _db.TranslationListFilter.Where(x => x.UserId.CompareTo(currentUserId) == 0).FirstOrDefault();

                oldFilter.Text = filter.Text;
                oldFilter.RecordsPerPage = filter.RecordsPerPage;
                _db.SaveChanges();

                return new ListViewModel()
                {
                    Filter = filter,
                    TranslationViewModels = GetFilteredTranslations(filter).ToList()
                };

            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }
        #endregion

        #region private method

        /// <summary>
        /// Metoda pro vytvoreni vychoziho filtru pro aktualne prihlaseneho uzivatele
        /// </summary>
        /// <param name="currentUserId">Identifikator prihlaseneho uzivalele</param>
        /// <returns>Vychozi filtr</returns>
        private TranslationListFilter CreateFilter(string currentUserId)
        {
            try
            {
                var filter = _db.TranslationListFilter.Add(new TranslationListFilter()
                {
                    Text = "",
                    UserId = currentUserId,
                    RecordsPerPage = 10
                });
                _db.SaveChanges();

                return filter.Id > 0 ? filter : null;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }
        #endregion

        #region AJAX
        public bool ExistDbName(string name, int translationId)
        {
            var translations = _db.Translation.Where(x => x.DbColumnName.CompareTo(name) == 0);
            if (translations.Any())
            {
                if (translations.FirstOrDefault().Id == translationId)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
}