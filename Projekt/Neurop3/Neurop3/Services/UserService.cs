﻿using System;
using System.Linq;
using System.Web;

using Neurop3.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Neurop3.Helpers;
using Microsoft.AspNet.Identity.EntityFramework;
using Neurop3.Models.ViewModels.User;
using Neurop3.Common;
using Neurop3.Models.Administration;
using Neurop3.Models.User;
using Neurop3.Models.ViewModels.Exercise;
using Neurop3.Infrastructure;
using Neurop3.Models.ViewModels.Account;
using Neurop3.Models.Filters;

namespace Neurop3.Services
{

    public interface IUserService
    {
        #region Users
        /// <summary>
        /// Metoda pro ziskani seznamu vsech uzivatelu v aplikaci
        /// </summary>
        /// <returns>Struktura se seznamem vsech uzivatelu v aplikaci</returns>
        Models.ViewModels.User.ListViewModel GetListVM();

        /// <summary>
        /// Metoda pro ziskani infromaci o pacientovi aktualne prihlaseneho terapeuta
        /// </summary>
        /// <param name="userId">Identifikator pacienta</param>
        /// <returns></returns>
        PatientDetailViewModel GetPatientDetailVM(string userId);

        /// <summary>
        /// Metoda pro ziskani viewmodelu, ktery obsahuje informace o uzivateli
        /// </summary>
        /// <param name="userId">Identifikator uzivatele</param>
        /// <returns></returns>
        ManageViewModel GetManageVM(string userId);

        /// <summary>
        /// Metoda pro ziskani uzivatele se zadanym identifikatorem
        /// </summary>
        /// <param name="id">Identifikator uzivatele</param>
        /// <returns></returns>
        UserViewModel GetUser(string id);

        //IQueryable<UserViewModel> GetUsers();

        /// <summary>
        /// Metoda pro vraceni uzivatelu, jejich jmeno obsahuje zadany podrezetec 
        /// </summary>
        /// <param name="query">Hledany podretezec ve jmenech uzivatelu</param>
        /// <returns>Seznam odpovidajicich uzivatelu</returns>
        IQueryable<UserViewModel> GetUsersFiltered(string query);

        /// <summary>
        /// Metoda pro aktualizaci konkretniho uzivatele
        /// </summary>
        /// <param name="model">Data uzivatele</param>
        /// <returns></returns>
        bool UpdateAppUser(AccountManageViewModel model);
        //UserViewModel GetUserFromMail(string mail);

        /// <summary>
        /// Metoda pro aktualizaci stavu uzivatele
        /// </summary>
        /// <param name="user">Model uzivatele</param>
        /// <returns></returns>
        bool UpdateUser(UserViewModel user);

        /// <summary>
        /// Metoda pro odebrani uzivatele ze systemu
        /// </summary>
        /// <param name="userId">Identifikator uzivatele</param>
        void DeleteUser(int userId);


        #endregion

        #region Assign
        /// <summary>
        /// Metoda pro ziskani vsech prirazeni pacientu k terapeutum podle filtru ulozeneho v databazi
        /// </summary>
        /// <returns></returns>
        PatientAssignListViewModel PatientAssignListVM();

        /// <summary>
        /// Metoda pro aktualizaci uzivatelova filtru a ziskani prirazeni, ktere filtru odpovidaji
        /// </summary>
        /// <param name="filter">Novy filtr, ktery ma byt ulozen</param>
        /// <returns>Struktura s filtrem a seznamem prirazeni</returns>
        PatientAssignListViewModel SetListFilterAndGetData(PatientAssignListFilter filter);
        
        /// <summary>
        /// Metoda pro ziskani vsech pacietnu daneho terapeuta
        /// </summary>
        /// <param name="therapistId">Identifikator terapeuta</param>
        /// <returns>Seznam terapeutovo pacietnu</returns>
        IQueryable<UserViewModel> GetPatientsOfTherapist(string therapistId);

        /// <summary>
        /// Metoda pro ziskani vsech pacientu daneho terapeuta, jejichz jmeno musi obsahovat text z parametru
        /// </summary>
        /// <param name="therapistId">Identifikator terapeuta</param>
        /// <param name="query">Text hledany ve jmenu pacienta</param>
        /// <returns>Seznam pacientu vyhovujuci podminkam</returns>
        IQueryable<UserViewModel> GetPatientsOfTherapistFilter(string therapistId, string query);

        /// <summary>
        /// Metoda, ktera otestuje, zda zadana dvojice obsahuje aktivni vazbu
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="therapistId">Identifikator terapeuta</param>
        /// <returns></returns>
        bool AssignExists(string patientId, string therapistId);

        /// <summary>
        /// Metoda, ktera ma za ukol vytvorit aktivni vazbu mezi terapeutem a pacientem.
        /// </summary>
        /// <param name="patientId">Identifikator pacienta</param>
        /// <param name="therapistId">Identifikator terapeuta</param>
        /// <returns></returns>
        bool AddPatientAssign(string patientId, string therapistId);

        /// <summary>
        /// Metoda, ktera vraci seznam uzivatelu, kteri jsou nabizeny pri vytvareni vazeb mezi terapeuty a pacienty.
        /// Je provadena filtrace na zaklade podretezce je jmenu uzivatele a typu uzivatele 
        /// </summary>
        /// <param name="input">Podretezec hledany ve jmenu uzivatele</param>
        /// <param name="type">Typ uzivatele (pacient/terapeut)</param>
        /// <returns>Seznam uzivatelu</returns>
        IQueryable<UserViewModel> GetUsersToAssign(string input, int type);

        /// <summary>
        /// Metoda pro odebrani vazby mezi terapeutem a pacientem
        /// </summary>
        /// <param name="Id">Identifikator vazby</param>
        /// <returns></returns>
        bool RemoveAssign(int Id);

        /// <summary>
        /// Metoda pro obnoveni vazby mezi terapeutem a pacietem.
        /// </summary>
        /// <param name="Id">Identifikator vazby</param>
        /// <returns></returns>
        bool ActivateAssign(int Id);

        /// <summary>
        /// Metoda pro ziskani seznamu vsech roli v aplikaci
        /// </summary>
        /// <returns>seznam roli v aplikaci</returns>
        IQueryable<IdentityRole> GetRolesList();
        #endregion

        #region UserData
        /// <summary>
        /// Metoda pro ziskani dat o aktualne prihlasenem pacientovi
        /// </summary>
        /// <returns>Pacientovo data</returns>
        PatientDataViewModel GetPatientDataVM();

        /// <summary>
        /// Metoda pro ziskani dat aktualne prihlaseneho terapeuta
        /// </summary>
        /// <returns>Data terapeuta</returns>
        TherapistDataViewModel GetPTherapistDataVM();

        /// <summary>
        /// Metoda pro ziskani dat aktualne prihlaseneho administratora
        /// </summary>
        /// <returns>Data administratora</returns>
        AdministratorDataViewModel GetAdministratorDataVM();

        /// <summary>
        /// Metoda pro ulozeni zpravy o pokroku pacienta. Zpravu pise jeho terapeut.
        /// </summary>
        /// <param name="model">Model zpravy s daty</param>
        /// <returns></returns>
        bool CreateTherapistReport(TherapistReportCreateViewModel model);

        /// <summary>
        /// Metoda, ktera vraci report od terapeuta
        /// </summary>
        /// <param name="reportId">Identifikator reportu</param>
        /// <returns></returns>
        TherapistReportViewModel GetTherapistReport(Guid reportId);

        /// <summary>
        /// Metoda pro ziskani reportu o pacientech, ktere odpovidaji zadanemu filtru
        /// </summary>
        /// <param name="filter">Filtr reportu</param>
        /// <returns>seznam reportu odpovidajici filtru</returns>
        IQueryable<TherapistReportViewModel> GetTherapistReportVMs(TherapistReportFilter filter);

        /// <summary>
        /// Metoda pro smazani terapeutovo reportu o stavu pacienta
        /// </summary>
        /// <param name="reportId">Identifikator reportu</param>
        /// <returns></returns>
        bool DeleteTherapistReport(Guid reportId);

        /// <summary>
        /// Metoda pro nastaveni priznaku "zobrazeno" u konkretniho reportu od terapeuta pacientem
        /// </summary>
        /// <param name="reportId">Identifikator reportu</param>
        /// <returns></returns>
        bool PatientReportShown(Guid reportId);

        /// <summary>
        /// Metoda pro vlozeni hodnoceni pacienta daneho baliku
        /// </summary>
        /// <param name="difficulty">Obtiznost baliku</param>
        /// <param name="exerciseAmount">Pocet uloh v baliku</param>
        /// <param name="note">Poznamka k baliku</param>
        /// <param name="packageId">Identifikator baliku</param>
        /// <returns></returns>
        bool AddPatientFeedback(int difficulty, int exerciseAmount, string note, int packageId);

        /// <summary>
        /// Metoda pro ziskani hodnoceni 
        /// </summary>
        /// <param name="feedBackId"></param>
        /// <returns></returns>
        PatientFeedbackViewModel GetPatientFeedBack(Guid feedBackId);
        #endregion

    }

    public class UserService : IUserService
    {

        private ApplicationDbContext _db;


        public UserService(ApplicationDbContext db)
        {
            this._db = db;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        #region Users


        public Models.ViewModels.User.ListViewModel GetListVM()
        {
            try
            {
                return new Models.ViewModels.User.ListViewModel()
                {
                    UserVMs = _db.Users.Select(x => new UserViewModel()
                    {
                        Id = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Email = x.Email,
                        Roles = x.Roles.ToList(),
                    }).ToList()
                };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        

        public ManageViewModel GetManageVM(string userId)
        {
            try
            {
                var ret = new ManageViewModel()
                {
                    UserVM = _db.Users.Where(x => x.Id == userId).Select(x => new UserViewModel()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        PhoneNumber = x.PhoneNumber,
                        Roles = x.Roles.ToList()
                    }).FirstOrDefault()
                };

                if (ret != null && ret.UserVM != null)
                {
                    ret.UserVM.RoleIds = ret.UserVM.RolesStringify;
                }
                return ret;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }



        public IQueryable<UserViewModel> GetUsersFiltered(string query)
        {
            try
            {
                return _db.Users.Where(x => x.FirstName.Contains(query) || x.LastName.Contains(query)).Select(x => new UserViewModel()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    Roles = x.Roles.ToList(),
                });
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public UserViewModel GetUser(string userId)
        {
            try
            {
                var user = _db.Users.Where(x => x.Id == userId).Select(x => new UserViewModel()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    PhoneNumber = x.PhoneNumber,
                    Roles = x.Roles.ToList(),
                    EmailConfirmed = x.EmailConfirmed,
                }).FirstOrDefault();

                if (user != null)
                {
                    user.RoleIds = user.RolesStringify;
                }
                return user;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }

        }

        public IQueryable<IdentityRole> GetRolesList()
        {
            try
            {
                return _db.Roles;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }


        public bool UpdateUser(UserViewModel model)
        {
            try
            {

                

                //overeni, zda dochazi k provedeni akce administratorem
                var auth = Infrastructure.Authorization.Authorize(HttpContext.Current.User, RoleEnum.Admin);
                if (!auth)
                {
                    return false;
                }

                var admin = _db.Users.Find(HttpContext.Current.User.Identity.GetUserId());

                #region sprava roli
                //Slouzi pouze pro nastaveni roli
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));


                //ziskani roli, ktere ma aktualne nastaveny
                var currentRoles = UserManager.GetRoles(model.Id);

                //ziskani roli z modelu
                var roleListIds = model.RoleIds.Split(',').Select(y => y.Trim()).ToList();
                var newRoles = _db.Roles.Where(x => roleListIds.Contains(x.Id)).Select(x => x.Name).ToList();


                var mail = new MailMessage() { UserId = model.Id, Subject = String.Format("{0} ({1})", Resources.Global.zmena_role, admin.FullName) };
                var roleString = "";

                //role, ktere jsou obsazeny v modelu bez tech aktualne nastavenych
                var toAdd = newRoles.Except(currentRoles);
                if (toAdd != null && toAdd.Any())
                {
                    roleString = string.Format("<p>{0}:</p><ul>", Resources.Global.byly_vam_pridany_tyto_role);
                    foreach (var roleToAdd in toAdd)
                    {
                        roleString += string.Format("<li>{0}</li>", roleToAdd);
                    }
                    roleString += "</ul>";
                    mail.Body = roleString;
                }

                //role, ktere maji byt odebrany
                var toRemove = currentRoles.Except(newRoles);
                roleString = "";
                foreach (var rem in toRemove.ToList())
                {
                    //Vzdy musi zustat alespon jeden superadmin
                    if (rem.CompareTo(RoleEnum.Superadmin.ToString()) == 0)
                    {
                        var superAdmin = RoleEnum.Superadmin.GetHashCode().ToString();
                        var count = _db.Users.Where(x => x.Roles.Any(y => y.RoleId == superAdmin)).Count();

                        if (count > 1)
                        {
                            UserManager.RemoveFromRole(model.Id, rem);
                            roleString += string.Format("<li>{0}</li>", rem);
                        }
                    }
                    else
                    {
                        UserManager.RemoveFromRole(model.Id, rem);
                        roleString += string.Format("<li>{0}</li>", rem);
                    }
                }

                if (!string.IsNullOrEmpty(roleString))
                {
                    
                    mail.Body = string.Format("{0}<p>{1}:</p><ul>{2}</ul>", mail.Body, Resources.Global.byly_vam_odebrany_tyto_role, roleString);
                }

                // pridani roli
                UserManager.AddToRoles(model.Id, toAdd.ToArray());

                if (!string.IsNullOrEmpty(mail.Body))
                {
                    Utils.SendMail(mail);
                }
                #endregion

                return true;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }

        }

        public void DeleteUser(int userId)
        {
            try
            {
                if (Infrastructure.Authorization.Authorize(HttpContext.Current.User, RoleEnum.Admin))
                {
                    _db.Users.Remove(_db.Users.Find(userId));
                    _db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
            }
        }

        #endregion

        #region Assign

        public IQueryable<PatientAssignViewModel> GetPatientAssigns()
        {
            try
            {
                var ret = _db.PatientAssign.Select(x => new PatientAssignViewModel()
                {
                    Id = x.Id,
                    Patient = _db.Users.Where(y => y.Id == x.PatientId).Select(y => new UserViewModel()
                    {
                        FirstName = y.FirstName,
                        LastName = y.LastName,
                        Id = y.Id,
                        Email = y.Email,
                        Roles = y.Roles.ToList(),
                    }).FirstOrDefault(),
                    Therapist = _db.Users.Where(y => y.Id == x.TherapistId).Select(y => new UserViewModel()
                    {
                        FirstName = y.FirstName,
                        LastName = y.LastName,
                        Id = y.Id,
                        Email = y.Email,
                        Roles = y.Roles.ToList(),
                    }).FirstOrDefault(),
                    IsUsed = _db.ExerciseAssign.Where(y => y.PatientId == x.PatientId).Where(y => y.TherapistId == x.TherapistId).Any(),
                    IsActive = x.IsActive
                });
                return ret;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }

        }
        
        public IQueryable<UserViewModel> GetPatientsOfTherapist(string therapistId)
        {
            try
            {
                var patientsId = GetPatientAssigns().Where(x => x.Therapist.Id == therapistId).Where(x => x.IsActive == true).Select(x => x.Patient.Id).ToList();
                return _db.Users.Where(x => patientsId.Contains(x.Id)).Select(x => new UserViewModel()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,
                    Roles = x.Roles.ToList(),
                });
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }

        }

        public IQueryable<UserViewModel> GetPatientsOfTherapistFilter(string therapistId, string query)
        {
            try
            {
                query = query.Trim().ToLower();
                return GetPatientsOfTherapist(therapistId).Where(x =>
                    x.FirstName.Trim().ToLower().Contains(query) ||
                    x.LastName.Trim().ToLower().Contains(query));
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public IQueryable<UserViewModel> GetUsersToAssign(string input, int type)
        {
            try
            {
                var users = GetUsersFiltered(input).AsEnumerable();

                if (type == RoleEnum.Patient.GetHashCode())
                {
                    var superadmin = RoleEnum.Superadmin.GetHashCode().ToString();
                    users = users.Where(x => x.RolesStringify.Contains(superadmin) || x.RolesStringify.Contains(type.ToString()));
                }
                else
                {
                    var therapistId = RoleEnum.Therapist.GetHashCode().ToString();
                    var super_therapistId = RoleEnum.Super_therapist.GetHashCode().ToString();
                    var superadmin = RoleEnum.Superadmin.GetHashCode().ToString();

                    users = users.Where(x =>
                        x.RolesStringify.Contains(superadmin) ||
                        x.RolesStringify.Contains(therapistId) ||
                        x.RolesStringify.Contains(super_therapistId)
                    );
                }

                //if (type == RoleEnum.Patient.GetHashCode())
                //{
                //    var therapistId = HttpContext.Current.User.Identity.GetUserId();
                //    var patientIds = _db.PatientAssign.Where(x => x.TherapistId == therapistId).Select(x => x.PatientId).ToList();
                //    users = users.Where(y => patientIds.Contains(y.Id));

                //    //var superAdmin = RoleEnum.Superadmin.GetHashCode().ToString();
                //    //users = users.Where(x => x.RolesStringify.Contains(type.ToString()) || x.RolesStringify.Contains(superAdmin) );
                //}
                //else
                //{
                //    var therapistId = RoleEnum.Therapist.GetHashCode().ToString();
                //    var super_therapistId = RoleEnum.Super_therapist.GetHashCode().ToString();
                //    var superadmin = RoleEnum.Superadmin.GetHashCode().ToString();

                //    users = users.Where(x =>
                //        x.RolesStringify.Contains(superadmin) ||
                //        x.RolesStringify.Contains(therapistId) ||
                //        x.RolesStringify.Contains(super_therapistId)
                //    );
                //}
                return users.AsQueryable();
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }

        }

        public bool AssignExists(string patientId, string therapistId)
        {
            try
            {
                return _db.PatientAssign.Where(x => x.IsActive == true).Where(x => x.PatientId == patientId).Where(x => x.TherapistId == therapistId).Any();
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }

        }

        public bool AddPatientAssign(string patientId, string therapistId)
        {
            try
            {
                if (!Infrastructure.Authorization.Authorize(HttpContext.Current.User, RoleEnum.Admin))
                {
                    return false;
                }

                //zkusime najit vazbu mezi temito uzivateli
                var assigns = GetPatientAssigns().Where(x => x.Patient.Id == patientId && x.Therapist.Id == therapistId);

                //test zda prirazeni jiz neexistovalo... pote se pouze aktivuje
                if (assigns.Any())
                {
                    var item = assigns.FirstOrDefault();
                    var assign = _db.PatientAssign.Find(item.Id);
                    assign.IsActive = true;
                    _db.SaveChanges();
                }
                else
                {
                    _db.PatientAssign.Add(new PatientAssign()
                    {
                        PatientId = patientId,
                        TherapistId = therapistId,
                        IsActive = true,
                    });
                    _db.SaveChanges();
                }

                //odeslani informace o prirazeni terapeuta pacientovi
                var therapist = _db.Users.Find(therapistId);
                Utils.SendMail(new MailMessage()
                {
                    UserId = patientId,
                    Subject = Resources.Global.nove_prirazeni_k_terapeutovi,
                    Body = string.Format("<p>{0}: {1}<p>", Resources.Global.vasim_novym_terapeutem_je, therapist.FullName)
                });

                return true;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }

        }

        public bool RemoveAssign(int Id)
        {
            try
            {
                if (!Infrastructure.Authorization.Authorize(HttpContext.Current.User, RoleEnum.Admin))
                {
                    return false;
                }
                //dojde pouze k jeji zneplatneni, aby mohlo v budoucnu dojit k jeji obnove a tim i vsech ulozenych dat.
                var assign = _db.PatientAssign.Find(Id);
                assign.IsActive = false;
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }

        }

        public bool ActivateAssign(int Id)
        {
            try
            {
                if (!Infrastructure.Authorization.Authorize(HttpContext.Current.User, RoleEnum.Admin))
                {
                    return false;
                }
                var assign = _db.PatientAssign.Find(Id);
                assign.IsActive = true;
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }

        }

        public PatientDetailViewModel GetPatientDetailVM(string userId)
        {
            var currentUserId = HttpContext.Current.User.Identity.GetUserId();
            return new PatientDetailViewModel()
            {
                Patient = _db.Users.Where(x => x.Id.CompareTo(userId) == 0).Select(x => new UserViewModel() {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Roles = x.Roles.ToList(),
                    PhoneNumber = x.PhoneNumber,
                }).FirstOrDefault(),
                PatientDataVM = _db.PatientData.Where(x => x.UserId == userId).Select(x => new PatientDataViewModel() {
                    DateOfBirth = x.DateOfBirth,
                    Gender = x.Gender,
                    UserId = x.UserId,
                    Profession = x.Profession,
                    Representative = x.Representative,
                    SocialStatus = x.SocialStatus
                }).FirstOrDefault(),
                ExerciseAssignVMs = _db.ExerciseAssign.Where(x => x.TherapistId.CompareTo(currentUserId) == 0 && x.PatientId.CompareTo(userId) == 0).Select(x => new ExerciseAssignViewModel() {
                    AssignDate = x.AssignDate,
                    Exercise = x.Exercise,
                    ExerciseId = x.ExerciseId,
                    IsActive = x.IsActive,
                    Id = x.Id,
                    LastAccessDate = x.LastAccessDate,
                    Status = x.Status,
                    StatusId = x.StatusId,
                    ExerciseResultsVMs = _db.ExerciseResult.Where(y => y.ExerciseAssignId == x.Id).Select(y => new ExerciseResultViewModel() {
                        ExerciseAssign = y.ExerciseAssign,
                        ExerciseAssignId = y.ExerciseAssignId,
                        Id = y.Id,
                        Row = y.Row,
                        Sequence = y.Sequence,
                        TimeUtc = y.TimeUtc,
                        Value = y.Value,
                    }).ToList()
                }).ToList(),
                TherapistReportVMs = _db.TherapistReport.Where(x => x.PatientDataId.CompareTo(userId) == 0 && x.TherapistId.CompareTo(currentUserId) == 0).OrderByDescending(x => x.TimeUtc).OrderByDescending(x => x.Shown).Select(x => new TherapistReportViewModel() {
                    Id = x.Id,
                    Name = x.Name,
                    Report = x.Report,
                    Shown = x.Shown,
                    TimeUtc = x.TimeUtc,
                    PatientDataId = x.PatientDataId,
                    TherapistId = x.TherapistId,
                    Therapist = _db.Users.Where(y => y.Id == x.TherapistId).Select(y => new UserViewModel() {
                        FirstName = y.FirstName,
                        LastName = y.LastName,
                        Id = y.Id,
                    }).FirstOrDefault(),
                }).ToList()
            };
        }

        public bool UpdateAppUser(AccountManageViewModel model)
        {
            try
            {
                //TODO NEDODEFINOVANO -- nevim jaka data budou potreba uchovavat
                if(model.AdministratorDataVM != null)
                {
                    var oldAdmin = _db.AdministratorData.Find(model.AdministratorDataVM.UserId);
                    //_db.SaveChanges();
                }
                if(model.TherapistDataVM != null)
                {
                    var oldTherapist = _db.TherapistData.Find(model.TherapistDataVM.UserId);
                    oldTherapist.DateOfBirth = model.TherapistDataVM.DateOfBirth;
                    oldTherapist.Education = model.TherapistDataVM.Education;
                    oldTherapist.Gender = model.TherapistDataVM.Gender;
                    _db.SaveChanges();
                }
                if(model.PatientDataVM != null)
                {
                    var oldPatient = _db.PatientData.Find(model.PatientDataVM.UserId);
                    oldPatient.DateOfBirth = model.PatientDataVM.DateOfBirth;
                    oldPatient.Gender = model.PatientDataVM.Gender;
                    oldPatient.Profession = model.PatientDataVM.Profession;
                    oldPatient.Representative = model.PatientDataVM.Representative;
                    oldPatient.SocialStatus = model.PatientDataVM.SocialStatus;
                    _db.SaveChanges();
                }

                if(model.UserVM != null)
                {
                    var oldUser = _db.Users.Find(model.UserVM.Id);
                    oldUser.FirstName = model.UserVM.FirstName;
                    oldUser.LastName = model.UserVM.LastName;
                    oldUser.PhoneNumber = model.UserVM.PhoneNumber;
                    _db.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return false;
            }
        }

        public PatientAssignListViewModel PatientAssignListVM()
        {
            try
            {
                //ziskani filtru z databaze a jeho pouziti
                var CurrentUserId = HttpContext.Current.User.Identity.GetUserId();
                var filter = _db.PatientAssignListFilter.Where(x => x.UserId.CompareTo(CurrentUserId) == 0).FirstOrDefault();
                if (filter == null)
                {
                    filter = CreateFilter(CurrentUserId);
                }

                return new PatientAssignListViewModel()
                {
                    Filter = filter,
                    PatientAssignViewModels = GetFilteredPatientAssigns(filter).ToList()
                };
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        public PatientAssignListViewModel SetListFilterAndGetData(PatientAssignListFilter filter)
        {
            if (filter != null)
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                var oldFilter = _db.PatientAssignListFilter.Where(x => x.UserId.CompareTo(currentUserId) == 0).FirstOrDefault();
                
                //aktualizace filtru
                oldFilter.PatientName = filter.PatientName;
                oldFilter.TherapistName = filter.TherapistName;
                oldFilter.IsActive = filter.IsActive;
                oldFilter.RecordsPerPage = filter.RecordsPerPage;
                _db.SaveChanges();

                return new PatientAssignListViewModel()
                {
                    Filter = filter,
                    PatientAssignViewModels = GetFilteredPatientAssigns(filter).ToList()
                };
            }
            return null;
        }

        /// <summary>
        /// Metoda pro ziskani prirazeni mezi pacienty a terapeuty na zaklade filtru 
        /// </summary>
        /// <param name="filter">Filtr</param>
        /// <returns>Seznam prirazeni</returns>
        private IQueryable<PatientAssignViewModel> GetFilteredPatientAssigns(PatientAssignListFilter filter)
        {
            if (filter != null)
            {
                var patientAssigns = _db.PatientAssign.AsQueryable();


                if (filter.IsActive == 1)
                {
                    patientAssigns = patientAssigns.Where(x => x.IsActive == true);
                }
                else if (filter.IsActive == 0)
                {
                    patientAssigns = patientAssigns.Where(x => x.IsActive == false);
                }

                if (!string.IsNullOrEmpty(filter.PatientName))
                {
                    var listNames = filter.PatientName.Split(';').Select(x => x.Trim().ToLower()).ToList();
                    patientAssigns = patientAssigns.Where(x => listNames.Any(a => x.Patient.FirstName.Trim().ToLower().Contains(a) || x.Patient.LastName.Trim().ToLower().Contains(a)));
                }

                if (!string.IsNullOrEmpty(filter.TherapistName))
                {
                    var listNames = filter.TherapistName.Split(';').Select(x => x.Trim().ToLower()).ToList();
                    patientAssigns = patientAssigns.Where(x => listNames.Any(a => x.Therapist.FirstName.Trim().ToLower().Contains(a) || x.Therapist.LastName.Trim().ToLower().Contains(a)));
                }

                //nastaveni strankovani
                filter.TotalRecords = patientAssigns.Count();

                if (filter.CurrentPage < 1)
                {
                    filter.CurrentPage = 1;
                }

                var start = (filter.CurrentPage - 1) * filter.RecordsPerPage;
                if (start >= filter.TotalRecords)
                {
                    start = start - filter.TotalRecords;
                }

                patientAssigns = patientAssigns.OrderBy(x => x.Id).Skip(start).Take(filter.RecordsPerPage).AsQueryable();



                return patientAssigns.Select(x => new PatientAssignViewModel()
                {
                    Id = x.Id,
                    IsActive = x.IsActive,
                    Patient = _db.Users.Where(y => y.Id == x.PatientId).Select(y => new UserViewModel()
                    {
                        Id = y.Id,
                        FirstName = y.FirstName,
                        LastName = y.LastName
                    }).FirstOrDefault(),
                    Therapist = _db.Users.Where(y => y.Id == x.TherapistId).Select(y => new UserViewModel()
                    {
                        Id = y.Id,
                        FirstName = y.FirstName,
                        LastName = y.LastName
                    }).FirstOrDefault(),
                }).AsQueryable();
            }
            return null;
        }

        /// <summary>
        /// Metoda pro vytvoreni filtru aktualnimu uzivateli a jeho ulozeni do databaze
        /// </summary>
        /// <param name="currentUserId">Identifikator uzivatele</param>
        /// <returns></returns>
        private PatientAssignListFilter CreateFilter(string currentUserId)
        {
            try
            {
                var filter = _db.PatientAssignListFilter.Add(new PatientAssignListFilter()
                {
                    UserId = currentUserId,
                    IsActive = 1,
                    PatientName = "",
                    TherapistName = "",
                    RecordsPerPage = 5,
                });
                _db.SaveChanges();
                return filter.Id > 0 ? filter : null;
            }
            catch (Exception e)
            {
                LogHelper.Log(LogTarget.Database, e);
                return null;
            }
        }

        #endregion


        public PatientDataViewModel GetPatientDataVM()
        {
            var currentUser = HttpContext.Current.User;
            var currentUserId = currentUser.Identity.GetUserId();

            if (Authorization.Authorize(currentUser, RoleEnum.Patient))
            {
                //test na existenci dat v db... pokud tam zatim nejsou, tak je zalozime
                var exists = _db.PatientData.Where(x => x.UserId.CompareTo(currentUserId) == 0).Any();
                if (!exists)
                {
                    var pd = _db.PatientData.Add(new PatientData()
                    {
                        DateOfBirth = DateTime.UtcNow,
                        Gender = (int)Gender.Neuvedeno,
                        Profession = Resources.Global.neuvedeno,
                        Representative = Resources.Global.neuvedeno,
                        SocialStatus = (int)SocialStatus.Neuvedeno,
                        UserId = currentUserId
                    });
                    _db.SaveChanges();
                    if (!string.IsNullOrEmpty(pd.UserId))
                    {
                        exists = true;
                    }
                }

                if (exists)
                {
                    return _db.PatientData.Where(x => x.UserId.CompareTo(currentUserId) == 0).Select(x => new PatientDataViewModel()
                    {
                        UserId = x.UserId,
                        DateOfBirth = x.DateOfBirth,
                        Gender = x.Gender,
                        Profession = x.Profession,
                        Representative = x.Representative,
                        SocialStatus = x.SocialStatus,
                        MedicalReports = _db.TherapistReport.Where(y => y.PatientDataId.CompareTo(x.UserId) == 0).Select(y => new TherapistReportViewModel()
                        {
                            Id = y.Id,
                            Report = y.Report,
                            TimeUtc = y.TimeUtc
                        }).ToList(),
                        PatientDiagnoses = _db.PatientDiagnosis.Where(y => y.PatientDataId.CompareTo(x.UserId) == 0).Select(y => new PatientDiagnosisViewModel()
                        {
                            Id = y.Id,
                            Name = y.Name,
                            Description = y.Description,
                            PatientDataId = y.PatientDataId,
                            TimeUtc = y.TimeUtc
                        }).ToList()

                    }).FirstOrDefault();
                }
                return null;
            }
            return null;
        }
        public TherapistDataViewModel GetPTherapistDataVM()
        {
            var currentUser = HttpContext.Current.User;
            var currentUserId = currentUser.Identity.GetUserId();

            if (Authorization.AuthorizeAny(currentUser, new RoleEnum[] { RoleEnum.Therapist, RoleEnum.Super_therapist }))
            {
                //test na existenci dat v db... pokud tam zatim nejsou, tak je zalozime
                var exists = _db.TherapistData.Where(x => x.UserId.CompareTo(currentUserId) == 0).Any();
                if (!exists)
                {
                    var td = _db.TherapistData.Add(new TherapistData()
                    {
                        DateOfBirth = DateTime.UtcNow,
                        Gender = (int)Gender.Neuvedeno,
                        Education = Resources.Global.neuvedeno,
                        UserId = currentUserId
                    });
                    _db.SaveChanges();
                    if (!string.IsNullOrEmpty(td.UserId))
                    {
                        exists = true;
                    }
                }

                if (exists)
                {
                    return _db.TherapistData.Where(x => x.UserId.CompareTo(currentUserId) == 0).Select(x => new TherapistDataViewModel()
                    {
                        UserId = x.UserId,
                        Gender = x.Gender,
                        Education = x.Education,
                        DateOfBirth = x.DateOfBirth,
                    }).FirstOrDefault();
                }
                return null;
            }
            return null;
        }
        public AdministratorDataViewModel GetAdministratorDataVM()
        {
            var currentUser = HttpContext.Current.User;
            var currentUserId = currentUser.Identity.GetUserId();
            if (Authorization.AuthorizeAny(currentUser, new RoleEnum[] { RoleEnum.Admin }))
            {
                //test na existenci dat v db... pokud tam zatim nejsou, tak je zalozime
                var exists = _db.AdministratorData.Where(x => x.UserId.CompareTo(currentUserId) == 0).Any();
                if (!exists)
                {
                    var ad = _db.AdministratorData.Add(new AdministratorData()
                    {
                        UserId = currentUserId
                    });
                    _db.SaveChanges();
                    if (!string.IsNullOrEmpty(ad.UserId))
                    {
                        exists = true;
                    }
                }

                if (exists)
                {
                    return _db.AdministratorData.Where(x => x.UserId.CompareTo(currentUserId) == 0).Select(x => new AdministratorDataViewModel()
                    {
                        UserId = x.UserId
                    }).FirstOrDefault();
                }
                return null;
            }
            return null;
        }

        public bool CreateTherapistReport(TherapistReportCreateViewModel model)
        {
            var report = _db.TherapistReport.Add(new TherapistReport()
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                PatientDataId = model.PatientDataId,
                Report = model.Report,
                TherapistId = model.TherapistId,
                TimeUtc = DateTime.UtcNow
            });
            var saved =_db.SaveChanges();
            if(saved > 0)
            {
                var user = _db.Users.Find(model.TherapistId);
                Utils.SendMail(new MailMessage()
                {
                    UserId = model.PatientDataId,
                    Subject = Resources.Global.terapeut_vytvoril_novy_report,
                    Body = string.Format("<p>{0} {1}</p><ul><li>{2}: {3}</li><li>{4}: {5}</li></ul>",
                        user.FullName,
                        Resources.Global.vytvoril_novy_report_o_vasem_pokroku_v_rehabilitaci,
                        Resources.Global.nazev,
                        model.Name,
                        Resources.Global.text,
                        model.Report
                        )
                });
            }



            return saved > 0 ? true : false;
        }

        public IQueryable<TherapistReportViewModel> GetTherapistReportVMs(TherapistReportFilter filter)
        {
            var reports = _db.TherapistReport.AsQueryable();

            if(filter != null)
            {
                if (!string.IsNullOrEmpty(filter.PatientId))
                {
                    reports = reports.Where(x => x.PatientDataId.CompareTo(filter.PatientId) == 0);
                }
                if (!string.IsNullOrEmpty(filter.TherapistId))
                {
                    reports = reports.Where(x => x.TherapistId.CompareTo(filter.TherapistId) == 0);
                }
                if (!string.IsNullOrEmpty(filter.ReportIds))
                {
                    reports = reports.Where(x => x.Id.CompareTo(filter.ReportIds) == 0);
                }
            }

            return reports.OrderBy(x => x.Shown).ThenByDescending(x => x.TimeUtc).Select(x => new TherapistReportViewModel() {
                Id = x.Id,
                Name = x.Name,
                Report = x.Report,
                PatientDataId = x.PatientDataId,
                TherapistId = x.TherapistId,
                Therapist = _db.Users.Where(y => y.Id == x.TherapistId).Select(y => new UserViewModel() {
                    FirstName = y.FirstName,
                    LastName = y.LastName,
                    Id = y.Id
                }).FirstOrDefault(),
                Shown = x.Shown,
                TimeUtc = x.TimeUtc 
            });
        }

        public TherapistReportViewModel GetTherapistReport(Guid reportId)
        {
            return _db.TherapistReport.Where(x => x.Id == reportId).Select(x => new TherapistReportViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Report = x.Report,
                Shown = x.Shown,
                TimeUtc = x.TimeUtc,
                PatientDataId = x.PatientDataId,
                TherapistId = x.TherapistId,
                Patient = _db.Users.Where(y => y.Id == x.PatientDataId).Select(y => new UserViewModel() {
                    FirstName = y.FirstName,
                    LastName = y.LastName,
                    Id = y.Id
                }).FirstOrDefault(),
                Therapist = _db.Users.Where(y => y.Id == x.TherapistId).Select(y => new UserViewModel() {
                    FirstName = y.FirstName,
                    LastName = y.LastName,
                    Id = y.Id
                }).FirstOrDefault(),
            }).FirstOrDefault();
        }
 
        public bool DeleteTherapistReport(Guid reportId)
        {
            _db.TherapistReport.Remove(_db.TherapistReport.Find(reportId));
            var removed = _db.SaveChanges();
            return removed > 0 ? true : false;
        }

        public bool PatientReportShown(Guid id)
        {
            var report = _db.TherapistReport.Where(x => x.Id == id).FirstOrDefault();
            if(report != null)
            {
                var currentUserId = HttpContext.Current.User.Identity.GetUserId();
                if(report.PatientDataId.CompareTo(currentUserId) == 0)
                {
                    report.Shown = true;
                    _db.SaveChanges();
                    return true;
                }
                return false;
            }
            return false;
            
        }

        public bool AddPatientFeedback(int difficulty, int exerciseAmount, string note, int packageId)
        {
            var currentUserId = HttpContext.Current.User.Identity.GetUserId();
            var package = _db.Package.Find(packageId);
            
            //overeni pacienta
            if(currentUserId.CompareTo(package.PatientId) == 0)
            {
                var item = _db.PatientFeedback.Add(new PatientFeedback()
                {
                    Id = Guid.NewGuid(),
                    Difficulty = difficulty,
                    ExerciseAmount = exerciseAmount,
                    Note = note,
                    TimeUtc = DateTime.UtcNow,
                    PackageId = packageId
                });
                var saved = _db.SaveChanges();

                //odeslani maila terapeutovi o ohodnoceni baliku
                if (saved > 0)
                {
                    var patient = _db.Users.Find(package.PatientId);
                    var body = string.Format(string.Concat("<p>", Resources.Global.pacient_0_ohodnotil_balik_1, "</p>"), patient.FullName, package.Name);
                    body = string.Concat(body, string.Format("<ul><li>{0}: {1}</li><li>{2}: {3}</li><li>{4}: {5}</li></ul>",
                            Resources.Global.obtiznost,
                            difficulty,
                            Resources.Global.mnozstvi_uloh,
                            exerciseAmount,
                            Resources.Global.text,
                            note
                        )
                    );
                    Utils.SendMail(new MailMessage()
                    {
                        UserId = package.TherapistId,
                        Subject = Resources.Global.ohodnoceni_baliku,
                        Body = body
                    });
                }


                return saved > 0 ? true : false;
            }
            return false;
        }

        public PatientFeedbackViewModel GetPatientFeedBack(Guid feedBackId)
        {
            return _db.PatientFeedback.Where(x => x.Id == feedBackId).Select(x => new PatientFeedbackViewModel() {
                Id = x.Id,
                Difficulty = x.Difficulty,
                ExerciseAmount = x.ExerciseAmount,
                Note = x.Note,
                Package = x.Package,
                PackageId = x.PackageId,
                TimeUtc = x.TimeUtc,
                Editable = false
            }).FirstOrDefault();

        }
    }
}