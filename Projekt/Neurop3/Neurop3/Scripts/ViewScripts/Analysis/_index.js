﻿function AnalysisIndex() {
    var instance = this;

    var programPH = "";
    var patientPH = "";
    var exercisePH = "";



    instance.Init = function () {

        $("#searchExerciseToAnalysis").click(function () {
            $.ajax({
                cache: false,
                type: "POST",
                async: false,
                url: Router.action("Analysis", "GetExerciseAssignsToAnalysisView"),
                data: {
                    programIds: $('#ProgramSelect').val(),
                    exerciseIds: $('#ExerciseSelect').val(),
                    patientIds: $('#PatientSelect').val()
                },
                success: function (data) {
                    $("#analysisTableWrapper").html("");
                    $("#analysisTableWrapper").append(data);
                },
            });
        });

        $("#PatientSelect").select2({
            width: '100%',
            minimumInputLength: -1,
            placeholder: instance.patientPH,
            allowClear: false,
            editable: false,
            multiple: true,
            ajax: {
                dataType: 'json',
                type: "POST",
                url: Router.action("Analysis", "GetPatientSelect"),
                data: function (searchTerm) {
                    return {
                        input: searchTerm,
                        programIds: $('#ProgramSelect').val(),
                        exerciseIds: $('#ExerciseSelect').val(),
                    };

                },
                results: function (data) {
                    return { results: data };
                }
            },
            formatResult: function (item) {
                return item.name;
            },
            formatSelection: function (item) {
                return item.name;
            }
        })/*.select2('data', getRolesDefaultValues())*/;


        $("#ExerciseSelect").select2({
            width: '100%',
            minimumInputLength: -1,
            placeholder: instance.exercisePH,
            allowClear: false,
            editable: false,
            multiple: true,
            ajax: {
                dataType: 'json',
                type: "POST",
                url: Router.action("Analysis", "GetExerciseSelect"),
                data: function (searchTerm) {
                    return {
                        input: searchTerm,
                        programIds: $('#ProgramSelect').val(),
                    };

                },
                results: function (data) {
                    return { results: data };
                }
            },
            formatResult: function (item) {
                return item.name;
            },
            formatSelection: function (item) {
                return item.name;
            }
        });

        $("#ProgramSelect").select2({
            width: '100%',
            minimumInputLength: -1,
            placeholder: instance.programPH,
            allowClear: false,
            editable: false,
            multiple: true,
            ajax: {
                dataType: 'json',
                type: "POST",
                url: Router.action("Analysis", "GetProgramSelect"),
                data: function (searchTerm) {
                    return {
                        input: searchTerm
                    };

                },
                results: function (data) {
                    return { results: data };
                }
            },
            formatResult: function (item) {
                return item.name;
            },
            formatSelection: function (item) {
                return item.name;
            }
        });


    }
}