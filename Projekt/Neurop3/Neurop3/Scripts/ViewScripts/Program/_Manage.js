﻿function ProgramManage() {
    var instance = this;
    instance.ProgramTypeId = "";   

    instance.Init = function () {

        $("#AddNewRecipeRow").click(function () {

            $.ajax({
                cache: false,
                type: "POST",
                //dataType: "json",
                //async: false,
                url: Router.action("Program", "AddNewRecipeRow"),
                data: {
                    id: instance.ProgramTypeId
                },
                success: function (data) {
                    $("#RecipeRowsDivWrapper").append(data);

                }
            });

        });

        $(".removeRecipeRowBtn").click(function () {

            if (confirm("Opravdu chcete smazat tuto radku?")) {
                $(this).closest("div.recipeRowDiv").remove();
            }

        });

        $("#AddNewResultRow").click(function () {

            $.ajax({
                cache: false,
                type: "POST",
                //dataType: "json",
                //async: false,
                url: Router.action("Program", "AddNewResultRow"),
                data: {
                    id: instance.ProgramTypeId
                },
                success: function (data) {
                    $("#ResultRowsDivWrapper").append(data);
                }
            });
        });

        $(".removeResultRowBtn").click(function () {
            if (confirm("Opravdu chcete smazat tuto radku?")) {
                $(this).closest("div.resultRowDiv").remove();
            }
        });

        $("#addNewTranslationConfirm").click(function () {

            $("addTranslationErrorMessage").text("");

            var czech = $("#CzTransId").val();
            var dbName = $("#DbNameId").val();

            var errorMessage = "";
            if (!czech || 0 == czech.length) {
                errorMessage += "Czech title is missing, ";
            }
            if (!dbName || 0 == dbName.length) {
                errorMessage += "DB name is missing";
            }

            if (!(errorMessage.length == 0)) {
                $("#addTranslationErrorMessage").text(errorMessage);
            }
            else {
                $.ajax({
                    cache: false,
                    type: "POST",
                    dataType: "json",
                    //async: false,
                    url: Router.action("Translation", "ExistsDbName"),
                    data: {
                        dbName: dbName,
                        translationId: 0
                    },
                    success: function (data) {
                        if (data.success == false) {
                            $.ajax({
                                cache: false,
                                type: "POST",
                                dataType: "json",
                                //async: false,
                                url: Router.action("Translation", "CreateFromModal"),
                                data: {
                                    cz: $("#CzTransId").val(),
                                    en: $("#EnTransId").val(),
                                    de: $("#DeTransId").val(),
                                    dbName: $("#DbNameId").val()
                                },
                                success: function (data) {
                                    if (data.success == true) {
                                        $("#addNewTranslation").modal('hide');
                                    }
                                }
                            });
                        }
                        else {
                            $("#addTranslationErrorMessage").text("DbName has already exists");
                        }
                    }
                });
            }
        });

        //$.validator.addMethod("BIC", function (value, element) {
        //    var guid = $(element).closest("div.OrganizationDiv").children('input.guid').val();
        //    var ibannumber = $("#IBAN-" + guid);

        //    if ($(element).val() == "" && ibannumber.val() != "") {
        //        return false;
        //    }
        //    return true;
        //}, resources.bic_je_nutne_vyplnit);

        //$.validator.addClassRules({
        //    BICValidate: {
        //        BIC: true
        //    }
        //});
    };
    //instance.chechInvoicingAndSetVisibility = function () {
    //    if ($("#InvoicingAddressIsDifferent").is(':checked')) {
    //        $("#invoicingAddress").show();
    //    }
    //    else {
    //        $("#invoicingAddress").hide();
    //    }
    //}
}