﻿function UserManage() {

    var instance = this;
    //instance.userId = "";
    //instance.roleId = "";
    //instance.editable = "";
    //instance.rolePlaceholder = "";

    instance.Init = function () {

        var getRolesDefaultValues = function () {
            var result = [];
            $.ajax({
                cache: false,
                dataType: "json",
                type: "POST",
                async: false,
                url: Router.action("User", "GetRolesSelect"),
                data: { selectedIds: $('#UserVM_RoleIds').val() },
                success: function (data) {
                    result = data;
                }
            });
            return result;
        };

        $("#UserVM_RoleIds").select2({
            width: '100%',
            minimumInputLength: -1,
            placeholder: instance.rolePlaceholder,
            allowClear: false,
            editable: false,
            multiple: true,
            ajax: {
                dataType: 'json',
                type: "POST",
                url: Router.action("User", "GetRolesSelect"),
                data: function (searchTerm) {
                    return {
                        query: searchTerm
                    };

                },
                results: function (data) {
                    return { results: data };
                }
            },
            formatResult: function (item) {
                return item.name;
            },
            formatSelection: function (item) {
                return item.name;
            }
        }).select2('data', getRolesDefaultValues());


        if (!instance.editable) {
            $("#UserVM_RoleIds").prop("disabled", true);
        }
    };
}