﻿$.validator.addMethod('date',
    function (value, element, params) {
        if (this.optional(element)) {
            return true;
        }

        var matchesLongFormat = value.match(/^(\d{1,2})\.(\d{1,2})\.(\d{4}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/);
        var matchesLongFormatEn = value.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4}) (\d{1,2}):(\d{1,2}):(\d{1,2}) (\b[A-Z]{2})$/);
        var matchesShortFormat = value.match(/^(\d{2})\.(\d{2})\.(\d{4})$/);


        if (matchesLongFormat || matchesShortFormat || matchesLongFormatEn) {
            return true;
        }
        else {
            return false;
        }
    }
);