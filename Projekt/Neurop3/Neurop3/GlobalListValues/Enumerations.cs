﻿using System.Reflection;
using System.Security.Policy;
using System.Web;
using System.Web.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Neurop3.Models;
using Neurop3.Models.ViewModels;
using Neurop3.Models.ViewModels.Account;
using Neurop3.Infrastructure;

namespace Neurop3.GlobalListValues
{
    namespace Enums
    {
        public static class Enums
        {
            public static string PascalCaseToPrettyString(this string s)
            {
                return Regex.Replace(s, @"(\B[A-Z]|[0-9]+)", " $1");
            }

            public static string GetDescriptionString(this Enum val)
            {
                try
                {
                    var attributes = (DescriptionAttribute[]) val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
                    return attributes.Length > 0 ? attributes[0].Description : val.ToString().PascalCaseToPrettyString();
                }
                catch (Exception ex)
                {
                    //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    return val.ToString().PascalCaseToPrettyString();
                }
            }


            public static int GetID(this Enum val)
            {
                return (int)(val.GetType().GetField(val.ToString()).GetRawConstantValue());

            }

            public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
            {
                var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                              select new { ID = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() }).OrderBy(e => e.Name).ToList();

                return new SelectList(values, "Id", "Name", enumObj);
            }


            public static SelectList ToSelectListOrderId<TEnum>(this TEnum enumObj)
            {
                var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                              select new { ID = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() }).OrderBy(e => e.ID).ToList();

                return new SelectList(values, "Id", "Name", enumObj);
            }

            public static SelectList ToSelectList<TEnum>(this TEnum enumObj, int? selectedValue, bool ordered = true)
            {
                var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                              select new { ID = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() });
                if (ordered) values = values.OrderBy(e => e.Name);
                if (selectedValue != null)
                {
                    return new SelectList(values.ToList(), "Id", "Name", selectedValue.Value);
                }
                else
                {
                    return new SelectList(values.ToList(), "Id", "Name");
                }
            }

            public static MultiSelectList ToMultiSelectList<TEnum>(this TEnum enumObj)
            {
                var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                              select new { ID = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() }).OrderBy(e => e.Name).ToList();

                return new MultiSelectList(values, "Id", "Name", new List<TEnum>() { enumObj });
            }

            public static IEnumerable<EnumViewModel> ToEnumViewModel<TEnum>(this TEnum enumObj)
            {
                return (from TEnum e in Enum.GetValues(typeof(TEnum))
                        select new EnumViewModel { Id = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() }).OrderBy(e => e.Name).ToList();
            }

            public static IEnumerable<EnumViewModel> ToIdOrederedEnumViewModel<TEnum>(this TEnum enumObj)
            {
                return (from TEnum e in Enum.GetValues(typeof(TEnum))
                        select new EnumViewModel { Id = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() }).OrderBy(e => e.Id).ToList();
            }

            public static IEnumerable<EnumViewModel> ToIdOrederedEnumViewModel<TEnum>(this TEnum enumObj, List<int> selectedValues)
            {
                return (from TEnum e in Enum.GetValues(typeof(TEnum))
                        where selectedValues.Contains((e as Enum).GetID())
                        select new EnumViewModel { Id = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() }).OrderBy(e => e.Id).ToList();
            }

            public static IEnumerable<EnumViewModel> ToEnumViewModel<TEnum>(this TEnum enumObj, List<int> selectedValues)
            {
                return (from TEnum e in Enum.GetValues(typeof(TEnum))
                        where selectedValues.Contains((e as Enum).GetID())
                        select new EnumViewModel { Id = (e as Enum).GetID(), Name = (e as Enum).GetDescriptionString() }
                         ).OrderBy(e => e.Name).ToList();
            }

            public static string GetEnumJson<T>()
            {
                var type = typeof(T);
                var values = Enum.GetValues(type).Cast<T>();
                var dict = values.ToDictionary(e => e.ToString(), e => Convert.ToInt32(e));
                return new JavaScriptSerializer().Serialize(dict);
            }

            public static List<string> GetWorkflowStates(int WorkItemStateId)
            {
                List<string> result = new List<string>();
                //switch (WorkItemStateId)
                //{
                //    //case (int)WorkItemState.Probihajici:
                //    //    result.Add("Úkol zadán");
                //    //    result.Add("Práce probíhá");
                //    //    result.Add("Práce přerušena");
                //    //    break;
                //    //case (int)WorkItemState.KeSchvaleni:
                //    //    result.Add("Úkol připraven ke kontrole");
                //    //    result.Add("Odsouhlasení");
                //    //    break;
                //    //case (int)WorkItemState.Nezadany:
                //    //    result.Add("Workflow Spuštěno");
                //    //    result.Add("Zadání úkolu");
                //    //    result.Add("Požadavek nepřijat");
                //    //    break;
                //}
                return result;
            }

            public static string GetWorkItemState(string WorkflowState)
            {
                switch (WorkflowState)
                {
                    case "Úkol zadán":
                        return "Probíhající";
                    case "Práce probíhá":
                        return "Probíhající";
                    case "Práce přerušena":
                        return "Probíhající";
                    case "Úkol připraven ke kontrole":
                        return "Ke schválení";
                    case "Odsouhlasení":
                        return "Ke schválení";
                    case "Workflow Spuštěno":
                        return "Nezadaný";
                    case "Zadání úkolu":
                        return "Nezadaný";
                    case "Požadavek nepřijat":
                        return "Nezadaný";
                    default:
                        return "";

                }
            }

        }

        public enum SystemSettingsDataType
        {
            Bool = 1,
            String = 2,
            Int = 3,
            Enum = 4,
        }

        public enum SystemSettingsEnum
        {
            SystemovyEmail = 1,
            LokalizaceSystemu = 2,
            PasswordMinLength = 3,
            PasswordMaxLength = 4,
            PasswordUpperCaseLetterRequired = 5,
            PasswordLowerCaseLetterRequired = 6,
            PasswordDecimalDigitRequired = 7,
            PasswordSpecialCharRequired = 8,
            MailAlias = 9,
            SyncFuncPositionsToGroups = 10,
            UserFullNameMask = 11,
            DefaultModule = 12
        }


        //public enum UserStatusCode
        //{
        //    [LocalizedDescription("aktivni", typeof(Resources))]
        //    Active = 1,
        //    [LocalizedDescription("neaktivni", typeof(Resources))]
        //    Disabled = 2
        //    //[LocalizedDescription("vytvoren_administratorem", typeof(Resources))]
        //    //AdminCreated = 3
        //}

        //public enum SemaphoreStatus
        //{

        //    [Description("Neutrální")]
        //    Transparent = 0,
        //    [Description("Zelená")]
        //    Green = 1,
        //    [Description("Žlutá")]
        //    Yellow = 2,
        //    [Description("Červená")]
        //    Red = 3,

        //}
    }
}