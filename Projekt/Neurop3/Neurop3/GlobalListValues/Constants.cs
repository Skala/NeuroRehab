﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Web;

namespace Sofokles.GlobalListValues
{
    public static class Constants
    {
        public static string dateFormat {
            get 
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                { 
                    case "cs-CZ":
                        format = "d. M. yyyy";
                        break;
                    case "en-US":
                        format = "M/d/yyyy";
                        break;
                    default:
                        format = "d. M. yyyy";
                        break;
                }
                return format;
            }
        }

        public static string dateFormatYearMonth
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "MMMM yyyy";
                        break;
                    case "en-US":
                        format = "MMMM yyyy";
                        break;
                    default:
                        format = "MMMM yyyy";
                        break;
                }
                return format;
            }
        }

        public static string underScoreDateFormat
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "d_M_yyyy";
                        break;
                    case "en-US":
                        format = "M_d_yyyy";
                        break;
                    default:
                        format = "d_M_yyyy";
                        break;
                }
                return format;
            }
        }

        /// <summary>
        /// Pro formát datepickeru, pokud máte v planů využívat inputmask plugin
        /// </summary>
        public static string dateFormatWithMaskForDatepicker
        {
            //dd.mm.yy
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "dd.mm.yy";
                        break;
                    case "en-US":
                        format = "mm/dd/yy";
                        break;
                    default:
                        format = "dd.mm.yy";
                        break;
                }
                return format;
            }
        }

        /// <summary>
        /// Pro formát InputMask pluginu
        /// </summary>
        public static string dateFormatWithMaskForDatepickerMask
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "dd.mm.yyyy";
                        break;
                    case "en-US":
                        format = "mm/dd/yyyy";
                        break;
                    default:
                        format = "dd.mm.yyyy";
                        break;
                }
                return format;
            }
        }

        public static string dateFormatJavascript
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "d. m. yy";
                        break;
                    case "en-US":
                        format = "m/d/yy";
                        break;
                    default:
                        format = "d. m. yy";
                        break;
                }
                return format;
            }
        }

        public static string dateTimeFormatJavascript
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "d. m. yy HH:mm";
                        break;
                    case "en-US":
                        format = "m/d/yy HH:mm";
                        break;
                    default:
                        format = "d. m. yy HH:mm";
                        break;
                }
                return format;
            }
        }

        public static string DateFormatMoment
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "D. M. YYYY";
                        break;
                    case "en-US":
                        format = "M/D/YYYY";
                        break;
                    default:
                        format = "D. M. YYYY";
                        break;
                }
                return format;
            }
        }

        public static string dateTimeFormat
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "d. M. yyyy HH:mm";
                        break;
                    case "en-US":
                        format = "M/d/yy HH:mm";
                        break;
                    default:
                        format = "d. M. yyyy HH:mm";
                        break;
                }
                return format;
            }
        }

        public static string DateTimeFormatMoment
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "D. M. YYYY HH:mm";
                        break;
                    case "en-US":
                        format = "M/D/YYYY HH:mm";
                        break;
                    default:
                        format = "D. M. YYYY HH:mm";
                        break;
                }
                return format;
            }
        }




        public static string TimeFormat
        {
            get
            {
                string format;
                switch (CultureInfo.CurrentCulture.Name)
                {
                    case "cs-CZ":
                        format = "HH:mm";
                        break;
                    case "en-US":
                        format = "HH:mm";
                        break;
                    default:
                        format = "HH:mm";
                        break;
                }
                return format;
            }
        }



        public static string Serialize<T>(T value)
        {

            //var ps = new ProgramString {ProgramId = this.Id};

            try
            {
                var xmlserializer = new XmlSerializer(typeof(T));
                var stringWriter = new StringWriter();
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    xmlserializer.Serialize(writer, value);
                    return stringWriter.ToString();
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("An error occurred", ex);
                return string.Empty;
            }
        }


        public const bool dataTablesSearchBar = false;
        
        /// <summary>
        /// Udava hodnotu, ktera se pricita k id jednoho ciselniku, aby je bylo mozne spojit s jinym ciselnikem a nebyly duplicitni id.
        /// </summary>
        public static int ExpenseInvoiceRangeAmount
        {
            get { return 10000; }
        }

        public const string OryxExtension = ".orxx";

        //Modules 
        public const string ModuleNameDMS = "DMS";
        public const string ModuleNameProjects = "Projects";
        public const string ModuleNameHelpdesk = "Helpdesk";
        public const string ModuleNameRoot = "Root";
        public const string ModuleNameReklamace = "Reklamace";
        public const string ModuleNameTIS= "TIS";
        public const string ModuleNameMeridla = "Meridla";
        public const string ModuleNameLidskeZdroje = "LidskeZdroje";

        public const string ModuleNameAudits = "Audits";
        public const string ModuleNameORYX = "ORYX"; 

        public const int WorkflowDirName = 26;

        public const string LOGIN_TEXT = "Succesfull login.";
        public const string LOGIN_DENIED_TEXT = "Login denied.";

        public const string DMSAuditsFolderAudity = "Audity";
        public const string DMSAuditsFolderKartaNeshody = "Karty neshody";
        public const string DMSAuditsFolderOdkazNaNesplnenyPredpis = "Odkaz na nesplněný předpis";
        public const string DMSAuditsFolderDukazNeshody = "Důkaz neshody";
        public const string DMSAuditsFolderQuestionnaireTemplateAttachment = "Dotazníky";
        public const string DMSAuditsFolderQuestionnaireTemplateAttachmentQuestion = "Otázka";
        public const string DMSAuditsFolderNaprava = "Náprava";
        public const string DMSAuditsFolderNapravneOpatreni = "Nápravné opatření";
        public const string DMSAuditsFolderPreventivniOpatreni = "Preventivní opatření";

        public const string DMSGaugesFolderRoot = "Měřidla";
        public const string DMSGaugesFolderMeridla = "Měřidla";
        public const string DMSGaugesFolderHistorie = "Historie";

        public const string DMSHelpdeskFolderRoot = "Helpdesk";
        public const string DMSHelpdeskFolderRequests = "Požadavky";
        public const string DMSHelpdeskFolderRequestsChat = "Z diskuze";

        public const int EncryptedFileUploadChunkSize = 2;
        public const int FileUploadChunkSize = 50;

        public const string ForgottenPasswordPurpose = "ForgottenPasswordPurposeHash";
        public const int ForgottenPasswordLinkValidityInMinutes = 30;

        public const string defaultMaskFullName = "<{JM} ><{P}>";
    }
}