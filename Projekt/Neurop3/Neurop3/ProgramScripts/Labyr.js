﻿/* @author: Pavel Skala */

var startTime;
var mContext;
var mCanvas;

var mParam;
var mTrace = 0;
var mPresentTime = 0;
var mStarPointVis = 0;
var mEndPointVis = 0;
var mRows = 0; //počet řádků
var mColumn = 0;
var mGapHor = 0;
var mGapVer = 0;
var mRadius = 0;
var mCriterion = 0;
var mMaxVers = 0;
var mMaxtime = 0;
var mStartMessage;

var mWayPoints; //cesta uvedená v konfiguraci
var mCircle; //Pole kruhů (očíslováno jako v konfiguračním souboru, ale začíná číslem 0)
var mLastCircleIndex;
var mStartCircleIndex;
var mRoundCount;
var mResults;

var listener_active;

function runExercise(param) {
    mCanvas = document.getElementById('mCanvas');

    if (mCanvas.getContext) {
        startTime = new Date();
        mContext = mCanvas.getContext('2d');
        mContext.strokeStyle = "rgb(255,0,0)"; //Bílé pozadí, pouze rám kolem úlohy
        mContext.strokeRect(0, 0, mCanvas.width, mCanvas.height);
        mResults = new Array(); //Výsledky testů
        mCircle = new Array();
        mParam = param.split("##"); //oddelovač jednotlivých vstupů
        mRoundCount = 0; //Počet opakování dané úlohy
        addListener();
        listener_active = false;
        parse();
        //TODO zavolat zadání
    }
    else
        alert("Fatal Error");
}


/* Analyzuji jednotlivé vstupy a plnim proměnné */
function parse() {
    var infoString;
    inputNo = 0;

    //Jednotlivé vstupy
    mTrace = parseInt(mParam[inputNo++]);
    mPresentTime = parseInt(mParam[inputNo++]);
    mStarPointVis = parseInt(mParam[inputNo++]);
    mEndPointVis = parseInt(mParam[inputNo++]);
    mRows = parseInt(mParam[inputNo++]);
    mColumn = parseInt(mParam[inputNo++]);
    mGapHor = parseInt(mParam[inputNo++]);
    mGapVer = parseInt(mParam[inputNo++]);
    mCriterion = parseInt(mParam[inputNo++]);
    mMaxVers = parseInt(mParam[inputNo++]);
    mMaxtime = parseInt(mParam[inputNo++]);
    mWayPoints = mParam[inputNo++].split(",");
	mStartMessage = mParam[inputNo++].split(",");
    mRadius = parseInt(mParam[inputNo++]);
    
    create();
}

//Vytvoreni hraci plochy
function create() {

    var stx = mRadius + 10; //startX (pozice prvniho kruhu)
    var sty = mRadius + 10; //startY (pozice prvniho kruhu)

    if (mGapHor == 0) //pri nulovem parametru se roztahne na cele hraci pole
    {
        var width = mCanvas.width - 20; //Okraj hraciho pole
        mGapHor = (width - mRadius * 2 * mColumn) / (mColumn - 1); //dopocteni mezer mezi kruhy
    }
    if (mGapVer == 0) {
        var height = mCanvas.height - 20; 
        mGapVer = (height - mRadius * 2 * mRows) / (mRows - 1);
    }
    //vytvoreni a ulozeni kruhu do pole
    for (var i = 0; i < mRows; i++) {
        for (var j = 0; j < mColumn; j++) {
            mCircle[i * mColumn + j] = new Circle(stx + j * (mRadius * 2) + mGapHor * j, sty + i * (mGapVer + mRadius * 2),
                mRadius, i * mColumn + j + 1, "rgb(0, 160,230)");
        }
    }

	confirm("Hra založena, Files");
	
    if (mPresentTime == 0) {
        //pri nule hra zacina bez zobrazeni hledane cesty
        startRound();
    }
    else {
        //ukazeme cestu
        var previewCircle; 
        for (var i = 0; i < mWayPoints.length; i++) {
            //Der Index im Array Kreise entspricht den Wegpunktwert - 1
            var k = mCircle[mWayPoints[i] - 1];
            k.color = "rgb(160,200,20)";
            k.draw();
            if (i > 0) //Ab dem zweiten Durchlauf: Diesen Kreis mit vorherigen verbinden
            {
                mContext.moveTo(previewCircle.x, previewCircle.y);
                mContext.lineTo(k.x, k.y);
                mContext.stroke();
            }
            previewCircle = k;
        }
        setTimeout(startRound, mPresentTime * 1000);
    }
    mWithouMistakeInRow = 0;
}

var mFault;
var mRoundStartTime;

function startRound() {

    //Malovat nad všemi, překreslovat kruhy
    //Bílý pozadí, jen rám kolem hracího pole
    mContext.fillStyle = "rgb(255, 255,255)";
    mContext.fillRect(0, 0, mCanvas.width, mCanvas.height);
    mContext.strokeStyle = "rgb(0,0,0)"; //Barva rámečku
    mContext.strokeRect(0, 0, mCanvas.width, mCanvas.height);
    for (var i = 0; i < mCircle.length; ++i) {
        mCircle[i].color = "rgb(0, 160,230)"; //nastaveni na modrou barvu
        mCircle[i].draw();
    }
    if (mStarPointVis == 1) {
        var k = mCircle[mWayPoints[0] - 1];
        k.color = "rgb(160, 200,20)";
        k.draw();
        mStartCircleIndex = 1;
    }
    else {
        mStartCircleIndex = 0;
    }
    if (mEndPointVis == 1) {
        var k = mCircle[mWayPoints[mWayPoints.length - 1] - 1];
        k.color = "rgb(160, 200,20)";
        k.draw();
        mLastCircleIndex = (mWayPoints.length) - 2;
    }
    else {
        mLastCircleIndex = (mWayPoints.length) - 1;
    }
    //Pokud byl pro hru zadán čas, spustí se nyní
    if (mMaxtime != 0)
        setTimeout(finish, mMaxtime * 1000);

    mWayPointArrIndex = mStartCircleIndex;
    mNextCircle = mWayPoints[mWayPointArrIndex];  - 1;

    //Nyní hra čeká na KlickEvents
    listener_active = true;
    mRoundCount++;
    mFault = 0;
    mfailed = 0;
    mRoundStartTime = new Date();
}

var mNextCircle;
var mWayPointArrIndex;
var mWithouMistakeInRow;
var mfailed;

//Pridani posluchace
function addListener() {
    mCanvas.addEventListener('mousedown', function (e) {
        if (!listener_active) return; //nedelat nic (pokud je cesta stále zobrazena)
        //Zde jsou určeny první x a y. Vzhledem k různým prohlížečům trochu rozsáhlejší.
        if (e.pageX || e.pageY) {
            x = e.pageX;
            y = e.pageY;
        }
        else {
            x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }
        x -= this.offsetLeft;
        y -= this.offsetTop;

        for (var i = 0; i < mCircle.length; i++) {
            if (mCircle[i].collide(x, y)) {
                if (i == mNextCircle) { //Po kliknutí na spranvy kruh
                    if (mWayPointArrIndex > 0) { //Pokud ne první kruh
                        //Malovat přes předchozí kružnici s
                        var lk = mCircle[mWayPoints[mWayPointArrIndex - 1] - 1];
                        if (mTrace == 1) //Při přehrávání stopy
                            lk.color = "rgb(20, 100,40)" //tmavozelený
                        else //jinak
                            lk.color = "rgb(0, 160,230)"; //modry
                        lk.draw();
                    }
                    mCircle[i].color = "rgb(160, 200,20)";
                    mCircle[i].draw();

                    if (i == mWayPoints[mLastCircleIndex] - 1) { //kdyz cíl byl dosažen


                        mResults[mRoundCount - 1] = new RoundResult(mFault, //Zadejte nový výsledek
                            parseInt((new Date().getTime() - mRoundStartTime.getTime()) / 1000));
                        if (mCriterion != 0) {  //Zkontrolujte parametr Kritérium
                            if (mFault == 0)
                                mWithouMistakeInRow++;
                            else
                                mWithouMistakeInRow = 0;
                            if (mWithouMistakeInRow >= mCriterion) { finish(); return; }
                        }
                        if (mMaxVers != 0 && mFault > 0) { //Zkontrolujte parametr MaxSearch
                            mfailed++;
                            if (mfailed >= mMaxVers)
                            { finish(); return; }
                        }
                        alert("Nasleduje kolo: " + (parseInt(mRoundCount) + 1));
                        startRound();
                        return;
                    }

                    mWayPointArrIndex++;  //Dalším bodem je nový cíl
                    mNextCircle = mWayPoints[mWayPointArrIndex] - 1;
                }
                else { //Pokud klepnete na nesprávný kruh
                    wrongCircle = mCircle[i];
                    previousColor = mCircle[i].color;
                    mCircle[i].color = "rgb(255, 0,0)";
                    mCircle[i].draw();
                    mFault++;
                    listener_active = false;
                    setTimeout(endMouseBlock, 500);
                }
                return;
            }
        }

    }, false);
}

//Struktura dat, kterou chceme uložit
function RoundResult(fault, time) {
    this.fault = fault;
    this.time = time;
}

//Červené Blikání a opětovné zapnutí posluchače
var previousColor;
var wrongCircle;
function endMouseBlock() {
    wrongCircle.color = previousColor;
    wrongCircle.draw();
    listener_active = true;
}



/** Třída kruhu je speciálně přizpůsobena tomuto testu
     a představuje Labyrinthwegstück */
function Circle(x, y, r, number, color) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.number = number;
    this.color = color;
    this.draw();
}

//Kvůli jednoduchosti se kružnice zde zpracovává jako čtverec
Circle.prototype.collide = function (x, y) {
    var qx = this.x - this.r; //X des Quadrates
    var qy = this.y - this.r;//Y des Quadrates
    var d = 2 * this.r; //Der Durchmesser
    if (qx < x && qx + d > x && qy < y && qy + d > y)
        return true;
    else
        return false;
}

Circle.prototype.draw = function () {
    mContext.fillStyle = this.color;
    mContext.beginPath();
    mContext.arc(this.x, this.y, this.r, 0, Math.PI * 2, true);
    mContext.closePath();
    mContext.fill();
    mContext.stroke();
}


function finish() {
    var resultString = "";
    //var duration = parseInt((new Date().getTime() - startTime.getTime()) / 1000);
    //resultString += duration;
    for (var i = 0; i < mResults.length; i++) {
        resultString += mResults[i].fault;
        resultString += "##" + mResults[i].time;
        resultString += ";;";
    }
    document.getElementById("OutputParameters").value = resultString;
    document.getElementById("ExerciseSubmit").submit();
}