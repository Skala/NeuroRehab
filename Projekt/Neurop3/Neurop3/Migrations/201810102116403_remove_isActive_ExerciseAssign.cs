namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_isActive_ExerciseAssign : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExerciseAssign", "IsActive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExerciseAssign", "IsActive", c => c.Boolean(nullable: false));
        }
    }
}
