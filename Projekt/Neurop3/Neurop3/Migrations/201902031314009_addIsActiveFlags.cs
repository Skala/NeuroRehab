namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsActiveFlags : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseAssign", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.PatientAssign", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PatientAssign", "IsActive");
            DropColumn("dbo.ExerciseAssign", "IsActive");
        }
    }
}
