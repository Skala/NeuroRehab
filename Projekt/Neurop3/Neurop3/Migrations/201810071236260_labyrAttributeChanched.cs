namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class labyrAttributeChanched : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Labyr", "StartPoint", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Labyr", "EndPoint", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Labyr", "EndPoint", c => c.Int(nullable: false));
            AlterColumn("dbo.Labyr", "StartPoint", c => c.Int(nullable: false));
        }
    }
}
