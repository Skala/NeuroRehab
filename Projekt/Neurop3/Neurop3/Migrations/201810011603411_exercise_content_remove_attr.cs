namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exercise_content_remove_attr : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Exercise", "Content");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Exercise", "Content", c => c.String(nullable: false));
        }
    }
}
