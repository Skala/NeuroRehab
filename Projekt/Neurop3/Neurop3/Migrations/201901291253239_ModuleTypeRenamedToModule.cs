namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModuleTypeRenamedToModule : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Exercise", name: "ModuleTypeId", newName: "ModuleId");
            RenameColumn(table: "dbo.ModuleRecipe", name: "ModuleTypeId", newName: "ModuleId");
            RenameColumn(table: "dbo.ModuleResult", name: "ModuleTypeId", newName: "ModuleId");
            RenameIndex(table: "dbo.Exercise", name: "IX_ModuleTypeId", newName: "IX_ModuleId");
            RenameIndex(table: "dbo.ModuleRecipe", name: "IX_ModuleTypeId", newName: "IX_ModuleId");
            RenameIndex(table: "dbo.ModuleResult", name: "IX_ModuleTypeId", newName: "IX_ModuleId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ModuleResult", name: "IX_ModuleId", newName: "IX_ModuleTypeId");
            RenameIndex(table: "dbo.ModuleRecipe", name: "IX_ModuleId", newName: "IX_ModuleTypeId");
            RenameIndex(table: "dbo.Exercise", name: "IX_ModuleId", newName: "IX_ModuleTypeId");
            RenameColumn(table: "dbo.ModuleResult", name: "ModuleId", newName: "ModuleTypeId");
            RenameColumn(table: "dbo.ModuleRecipe", name: "ModuleId", newName: "ModuleTypeId");
            RenameColumn(table: "dbo.Exercise", name: "ModuleId", newName: "ModuleTypeId");
        }
    }
}
