namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exerciseDescriptionToRequired : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE [dbo].[Exercise] SET[Description] = 'Neuvedeno' WHERE[Description] is null");
            AlterColumn("dbo.Exercise", "Description", c => c.String(nullable: false, maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Exercise", "Description", c => c.String());
        }
    }
}
