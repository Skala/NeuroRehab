namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class commnetBoolAttrAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comment", "IsShown", c => c.Boolean(nullable: false));
            AddColumn("dbo.Comment", "IsDone", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comment", "IsDone");
            DropColumn("dbo.Comment", "IsShown");
        }
    }
}
