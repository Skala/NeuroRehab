namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExerciseToExAssignCascadeDeleteSet : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExerciseAssign", "ExerciseId", "dbo.Exercise");
            DropForeignKey("dbo.ExerciseAssign", "PatientId", "dbo.Users");
            DropForeignKey("dbo.ExerciseAssign", "StatusId", "dbo.Status");
            AddForeignKey("dbo.ExerciseAssign", "ExerciseId", "dbo.Exercise", "Id");
            AddForeignKey("dbo.ExerciseAssign", "PatientId", "dbo.Users", "Id");
            AddForeignKey("dbo.ExerciseAssign", "StatusId", "dbo.Status", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExerciseAssign", "StatusId", "dbo.Status");
            DropForeignKey("dbo.ExerciseAssign", "PatientId", "dbo.Users");
            DropForeignKey("dbo.ExerciseAssign", "ExerciseId", "dbo.Exercise");
            AddForeignKey("dbo.ExerciseAssign", "StatusId", "dbo.Status", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ExerciseAssign", "PatientId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ExerciseAssign", "ExerciseId", "dbo.Exercise", "Id", cascadeDelete: true);
        }
    }
}
