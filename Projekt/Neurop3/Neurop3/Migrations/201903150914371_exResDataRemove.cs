namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exResDataRemove : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExerciseResultData", "ExerciseResultId", "dbo.ExerciseResult");
            DropIndex("dbo.ExerciseResultData", new[] { "ExerciseResultId" });
            AddColumn("dbo.ExerciseResult", "Row", c => c.Int(nullable: false));
            AddColumn("dbo.ExerciseResult", "Value", c => c.String(nullable: false));
            AddColumn("dbo.ExerciseResult", "TimeUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.ExerciseResult", "LastResultTimeUtc");
            DropTable("dbo.ExerciseResultData");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ExerciseResultData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExerciseResultId = c.Int(nullable: false),
                        Row = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                        TimeUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ExerciseResult", "LastResultTimeUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.ExerciseResult", "TimeUtc");
            DropColumn("dbo.ExerciseResult", "Value");
            DropColumn("dbo.ExerciseResult", "Row");
            CreateIndex("dbo.ExerciseResultData", "ExerciseResultId");
            AddForeignKey("dbo.ExerciseResultData", "ExerciseResultId", "dbo.ExerciseResult", "Id", cascadeDelete: true);
        }
    }
}
