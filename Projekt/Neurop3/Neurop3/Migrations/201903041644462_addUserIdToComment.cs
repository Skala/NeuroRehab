namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addUserIdToComment : DbMigration
    {
        public override void Up()
        {

            AddColumn("dbo.Comment", "UserId", c => c.String(nullable: false));
            Sql("UPDATE [dbo].[Comment] SET UserId = 'Neuvedeno'");

        }

        public override void Down()
        {
            DropColumn("dbo.Comment", "UserId");
        }
    }
}
