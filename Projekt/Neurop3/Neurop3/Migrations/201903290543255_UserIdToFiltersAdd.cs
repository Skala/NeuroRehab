namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserIdToFiltersAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseListFilter", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.ProgramListFilter", "UserId", c => c.String(nullable: false));
            AddColumn("dbo.TranslationListFilter", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TranslationListFilter", "UserId");
            DropColumn("dbo.ProgramListFilter", "UserId");
            DropColumn("dbo.ExerciseListFilter", "UserId");
        }
    }
}
