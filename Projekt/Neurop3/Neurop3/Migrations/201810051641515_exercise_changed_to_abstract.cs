namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exercise_changed_to_abstract : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Labyr");
            AlterColumn("dbo.Labyr", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Labyr", "Id");
            CreateIndex("dbo.Labyr", "Id");
            AddForeignKey("dbo.Labyr", "Id", "dbo.Exercise", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Labyr", "Id", "dbo.Exercise");
            DropIndex("dbo.Labyr", new[] { "Id" });
            DropPrimaryKey("dbo.Labyr");
            AlterColumn("dbo.Labyr", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Labyr", "Id");
        }
    }
}
