namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class recordsPerPageToFiltersAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseListFilter", "RecordsPerPage", c => c.Int(nullable: false));
            AddColumn("dbo.ProgramListFilter", "RecordsPerPage", c => c.Int(nullable: false));
            AddColumn("dbo.TranslationListFilter", "RecordsPerPage", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TranslationListFilter", "RecordsPerPage");
            DropColumn("dbo.ProgramListFilter", "RecordsPerPage");
            DropColumn("dbo.ExerciseListFilter", "RecordsPerPage");
        }
    }
}
