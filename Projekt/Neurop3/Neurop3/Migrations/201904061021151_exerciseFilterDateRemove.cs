namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exerciseFilterDateRemove : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExerciseListFilter", "ProgramIds", c => c.String());
            DropColumn("dbo.ExerciseListFilter", "CreatedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExerciseListFilter", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.ExerciseListFilter", "ProgramIds", c => c.Int(nullable: true));
        }
    }
}
