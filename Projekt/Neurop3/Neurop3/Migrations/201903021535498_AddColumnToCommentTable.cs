namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnToCommentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comment", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comment", "Name");
        }
    }
}
