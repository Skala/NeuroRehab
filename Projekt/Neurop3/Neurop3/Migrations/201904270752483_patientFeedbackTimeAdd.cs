namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patientFeedbackTimeAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PatientFeedback", "TimeUtc", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PatientFeedback", "TimeUtc");
        }
    }
}
