namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExerciseIsPublicAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exercise", "IsPublic", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Exercise", "IsPublic");
        }
    }
}
