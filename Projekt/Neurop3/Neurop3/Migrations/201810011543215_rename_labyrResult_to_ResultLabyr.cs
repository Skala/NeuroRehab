namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rename_labyrResult_to_ResultLabyr : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.LabyrResult", newName: "ResultLabyr");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ResultLabyr", newName: "LabyrResult");
        }
    }
}
