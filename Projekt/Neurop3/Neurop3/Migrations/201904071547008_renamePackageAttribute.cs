namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renamePackageAttribute : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Exercise_AssignPackage", name: "ExercisePackageId", newName: "PackageId");
            RenameIndex(table: "dbo.Exercise_AssignPackage", name: "IX_ExercisePackageId", newName: "IX_PackageId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Exercise_AssignPackage", name: "IX_PackageId", newName: "IX_ExercisePackageId");
            RenameColumn(table: "dbo.Exercise_AssignPackage", name: "PackageId", newName: "ExercisePackageId");
        }
    }
}
