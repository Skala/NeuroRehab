namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_labyr3_removeRelation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Labyr", "Id", "dbo.Exercise");
            DropIndex("dbo.Labyr", new[] { "Id" });
            DropPrimaryKey("dbo.Labyr");
            AlterColumn("dbo.Labyr", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Labyr", "Id");
            DropColumn("dbo.Labyr", "ExerciseId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Labyr", "ExerciseId", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.Labyr");
            AlterColumn("dbo.Labyr", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Labyr", "Id");
            CreateIndex("dbo.Labyr", "Id");
            AddForeignKey("dbo.Labyr", "Id", "dbo.Exercise", "Id");
        }
    }
}
