namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class diagnosisAndReportTableUpdate : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.MedicalReport", newName: "TherapistReport");
            AddColumn("dbo.PatientDiagnosis", "TherapistId", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.TherapistReport", "Name", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.TherapistReport", "TherapistId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.TherapistReport", "TherapistId");
            CreateIndex("dbo.PatientDiagnosis", "TherapistId");
            AddForeignKey("dbo.TherapistReport", "TherapistId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PatientDiagnosis", "TherapistId", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatientDiagnosis", "TherapistId", "dbo.Users");
            DropForeignKey("dbo.TherapistReport", "TherapistId", "dbo.Users");
            DropIndex("dbo.PatientDiagnosis", new[] { "TherapistId" });
            DropIndex("dbo.TherapistReport", new[] { "TherapistId" });
            DropColumn("dbo.TherapistReport", "TherapistId");
            DropColumn("dbo.TherapistReport", "Name");
            DropColumn("dbo.PatientDiagnosis", "TherapistId");
            RenameTable(name: "dbo.TherapistReport", newName: "MedicalReport");
        }
    }
}
