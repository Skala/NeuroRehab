namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class resultTableUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseResult", "LastResultTimeUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExerciseResultData", "TimeUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.ExerciseResult", "ResultDate");
            DropColumn("dbo.ExerciseResult", "ResultRawData");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExerciseResult", "ResultRawData", c => c.String(nullable: false));
            AddColumn("dbo.ExerciseResult", "ResultDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.ExerciseResultData", "TimeUtc");
            DropColumn("dbo.ExerciseResult", "LastResultTimeUtc");
        }
    }
}
