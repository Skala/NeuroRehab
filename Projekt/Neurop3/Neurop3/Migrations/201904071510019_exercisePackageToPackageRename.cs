namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exercisePackageToPackageRename : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ExercisesPackage", newName: "Package");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Package", newName: "ExercisesPackage");
        }
    }
}
