namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class therapistReportUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TherapistReport", "Shown", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TherapistReport", "Shown");
        }
    }
}
