namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsActiveToModuleAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Module", "IsActive", c => c.Boolean(nullable: false));
            Sql("UPDATE [dbo].[Module] SET IsActive = 1");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Module", "IsActive");
        }
    }
}
