namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patientAssignListFilterAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientAssignListFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PatientName = c.String(),
                        TherapistName = c.String(),
                        IsActive = c.Int(nullable: false),
                        UserId = c.String(nullable: false),
                        RecordsPerPage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PatientAssignListFilter");
        }
    }
}
