namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_isActive_to_exercise : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exercise", "IsActive", c => c.Boolean(nullable: false));
            Sql("update dbo.Exercise set IsActive = 1");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Exercise", "IsActive");
        }
    }
}
