namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeLogAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChangeLog",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TimeUtc = c.DateTime(nullable: false),
                        Name = c.String(nullable: false),
                        Message = c.String(nullable: false),
                        Version = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ChangeLog");
        }
    }
}
