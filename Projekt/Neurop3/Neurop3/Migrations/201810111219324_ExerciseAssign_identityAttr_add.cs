namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExerciseAssign_identityAttr_add : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult");
            DropIndex("dbo.ExerciseResult", new[] { "ExerciseAssignId" });
            DropPrimaryKey("dbo.ExerciseResult");
            AlterColumn("dbo.ExerciseResult", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.ExerciseResult", "ExerciseAssignId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ExerciseResult", "Id");
            CreateIndex("dbo.ExerciseResult", "ExerciseAssignId");
            AddForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult");
            DropIndex("dbo.ExerciseResult", new[] { "ExerciseAssignId" });
            DropPrimaryKey("dbo.ExerciseResult");
            AlterColumn("dbo.ExerciseResult", "ExerciseAssignId", c => c.Int(nullable: false));
            AlterColumn("dbo.ExerciseResult", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ExerciseResult", "Id");
            CreateIndex("dbo.ExerciseResult", "ExerciseAssignId");
            AddForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult", "Id");
        }
    }
}
