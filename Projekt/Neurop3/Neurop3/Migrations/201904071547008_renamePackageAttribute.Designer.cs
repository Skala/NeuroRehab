// <auto-generated />
namespace Neurop3.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class renamePackageAttribute : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(renamePackageAttribute));
        
        string IMigrationMetadata.Id
        {
            get { return "201904071547008_renamePackageAttribute"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
