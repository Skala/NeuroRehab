namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class programExerciseTranslationListFiltersAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExerciseListFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        ProgramIds = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsPublic = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProgramListFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProgramIds = c.String(),
                        Text = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsPublic = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TranslationListFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Program", "IsPublic", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Program", "IsPublic");
            DropTable("dbo.TranslationListFilter");
            DropTable("dbo.ProgramListFilter");
            DropTable("dbo.ExerciseListFilter");
        }
    }
}
