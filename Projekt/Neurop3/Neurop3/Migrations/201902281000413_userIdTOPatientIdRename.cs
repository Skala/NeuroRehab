namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userIdTOPatientIdRename : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ExerciseAssign", name: "UserId", newName: "PatientId");
            RenameIndex(table: "dbo.ExerciseAssign", name: "IX_UserId", newName: "IX_PatientId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ExerciseAssign", name: "IX_PatientId", newName: "IX_UserId");
            RenameColumn(table: "dbo.ExerciseAssign", name: "PatientId", newName: "UserId");
        }
    }
}
