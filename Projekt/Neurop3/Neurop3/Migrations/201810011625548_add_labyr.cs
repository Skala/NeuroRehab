namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_labyr : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Labyr",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Trace = c.Int(nullable: false),
                        PresentationTime = c.Int(nullable: false),
                        StartPoint = c.Int(nullable: false),
                        EndPoint = c.Int(nullable: false),
                        Columns = c.Int(nullable: false),
                        Rows = c.Int(nullable: false),
                        GapHorizontal = c.Int(nullable: false),
                        GapVertical = c.Int(nullable: false),
                        Radius = c.Int(nullable: false),
                        Criterion = c.Int(nullable: false),
                        MaxTrials = c.Int(nullable: false),
                        MaxTime = c.Int(nullable: false),
                        Series = c.String(),
                        Instruction = c.String(),
                        ExerciseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exercise", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Labyr", "Id", "dbo.Exercise");
            DropIndex("dbo.Labyr", new[] { "Id" });
            DropTable("dbo.Labyr");
        }
    }
}
