namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class synchronizeMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Translation", "DbColumnName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.Translation", "DbName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Translation", "DbName", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.Translation", "DbColumnName");
        }
    }
}
