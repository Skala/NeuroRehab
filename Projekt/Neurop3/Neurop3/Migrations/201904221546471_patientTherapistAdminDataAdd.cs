namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patientTherapistAdminDataAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdministratorData",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.PatientData",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        DateOfBirth = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        Representative = c.String(nullable: false),
                        SocialStatus = c.Int(nullable: false),
                        Profession = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.TherapistData",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        DateOfBirth = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        Education = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TherapistData", "UserId", "dbo.Users");
            DropForeignKey("dbo.PatientData", "UserId", "dbo.Users");
            DropForeignKey("dbo.AdministratorData", "UserId", "dbo.Users");
            DropIndex("dbo.TherapistData", new[] { "UserId" });
            DropIndex("dbo.PatientData", new[] { "UserId" });
            DropIndex("dbo.AdministratorData", new[] { "UserId" });
            DropTable("dbo.TherapistData");
            DropTable("dbo.PatientData");
            DropTable("dbo.AdministratorData");
        }
    }
}
