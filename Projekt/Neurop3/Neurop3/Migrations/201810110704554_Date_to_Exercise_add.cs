namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Date_to_Exercise_add : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseResult", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExerciseResult", "Date");
        }
    }
}
