namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class translation_update : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Translation", "Cz", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Translation", "En", c => c.String(maxLength: 1000));
            AlterColumn("dbo.Translation", "De", c => c.String(maxLength: 1000));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Translation", "De", c => c.String());
            AlterColumn("dbo.Translation", "En", c => c.String());
            AlterColumn("dbo.Translation", "Cz", c => c.String());
        }
    }
}
