namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserDataUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MedicalReport", "TimeUtc", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.MedicalReport", "TimeUtc");
        }
    }
}
