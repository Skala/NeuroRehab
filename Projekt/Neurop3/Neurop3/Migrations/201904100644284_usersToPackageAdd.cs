namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usersToPackageAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Package", "TherapistId", c => c.String(nullable: false, defaultValue: "1c6c2f14-72ee-4862-9f20-e07a6405164d"));
            AddColumn("dbo.Package", "PatientId", c => c.String(nullable: false, defaultValue: "1c6c2f14-72ee-4862-9f20-e07a6405164d"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Package", "PatientId");
            DropColumn("dbo.Package", "TherapistId");
        }
    }
}
