namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class therapistIdExAssignToRequired : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ExerciseAssign", new[] { "TherapistId" });
            AlterColumn("dbo.ExerciseAssign", "TherapistId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.ExerciseAssign", "TherapistId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ExerciseAssign", new[] { "TherapistId" });
            AlterColumn("dbo.ExerciseAssign", "TherapistId", c => c.String(maxLength: 128));
            CreateIndex("dbo.ExerciseAssign", "TherapistId");
        }
    }
}
