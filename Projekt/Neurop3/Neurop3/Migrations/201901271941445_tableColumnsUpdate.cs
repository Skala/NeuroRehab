namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tableColumnsUpdate : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ExerciseData", name: "ExerciseDataId", newName: "ExerciseId");
            RenameIndex(table: "dbo.ExerciseData", name: "IX_ExerciseDataId", newName: "IX_ExerciseId");
            AddColumn("dbo.ExerciseAssign", "AssignDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExerciseAssign", "LastAccessDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExerciseResult", "ResultDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExerciseResultData", "Value", c => c.String(nullable: false));
            AddColumn("dbo.ExerciseData", "Value", c => c.String(nullable: false));
            DropColumn("dbo.Exercise", "EditDate");
            DropColumn("dbo.ExerciseResult", "Date");
            DropColumn("dbo.ExerciseResultData", "Text");
            DropColumn("dbo.ExerciseResultData", "Number");
            DropColumn("dbo.ExerciseResultData", "LogicalValue");
            DropColumn("dbo.ExerciseResultData", "Single");
            DropColumn("dbo.ExerciseData", "Number");
            DropColumn("dbo.ExerciseData", "LogicalValue");
            DropColumn("dbo.ExerciseData", "Text");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExerciseData", "Text", c => c.String());
            AddColumn("dbo.ExerciseData", "LogicalValue", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExerciseData", "Number", c => c.Int(nullable: false));
            AddColumn("dbo.ExerciseResultData", "Single", c => c.String());
            AddColumn("dbo.ExerciseResultData", "LogicalValue", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExerciseResultData", "Number", c => c.Int(nullable: false));
            AddColumn("dbo.ExerciseResultData", "Text", c => c.String());
            AddColumn("dbo.ExerciseResult", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Exercise", "EditDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.ExerciseData", "Value");
            DropColumn("dbo.ExerciseResultData", "Value");
            DropColumn("dbo.ExerciseResult", "ResultDate");
            DropColumn("dbo.ExerciseAssign", "LastAccessDate");
            DropColumn("dbo.ExerciseAssign", "AssignDate");
            RenameIndex(table: "dbo.ExerciseData", name: "IX_ExerciseId", newName: "IX_ExerciseDataId");
            RenameColumn(table: "dbo.ExerciseData", name: "ExerciseId", newName: "ExerciseDataId");
        }
    }
}
