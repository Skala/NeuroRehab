namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class packageFilterAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PackageListFilter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        IsActive = c.Int(nullable: false),
                        UserId = c.String(nullable: false),
                        RecordsPerPage = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ExercisesPackage", "StatusId");
            AddForeignKey("dbo.ExercisesPackage", "StatusId", "dbo.Status", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExercisesPackage", "StatusId", "dbo.Status");
            DropIndex("dbo.ExercisesPackage", new[] { "StatusId" });
            DropTable("dbo.PackageListFilter");
        }
    }
}
