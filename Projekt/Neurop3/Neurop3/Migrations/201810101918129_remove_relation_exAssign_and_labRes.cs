namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_relation_exAssign_and_labRes : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ResultLabyr", "ExerciseAssignId", "dbo.ExerciseAssign");
            DropIndex("dbo.ResultLabyr", new[] { "ExerciseAssignId" });
            DropColumn("dbo.ResultLabyr", "ExerciseAssignId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ResultLabyr", "ExerciseAssignId", c => c.Int(nullable: false));
            CreateIndex("dbo.ResultLabyr", "ExerciseAssignId");
            AddForeignKey("dbo.ResultLabyr", "ExerciseAssignId", "dbo.ExerciseAssign", "Id", cascadeDelete: true);
        }
    }
}
