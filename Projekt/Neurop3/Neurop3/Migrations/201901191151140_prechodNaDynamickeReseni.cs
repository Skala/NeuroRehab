namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class prechodNaDynamickeReseni : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Abt", "Id", "dbo.Exercise");
            DropForeignKey("dbo.Labyr", "Id", "dbo.Exercise");
            DropForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult");
            DropIndex("dbo.Abt", new[] { "Id" });
            DropIndex("dbo.Labyr", new[] { "Id" });
            DropIndex("dbo.ResultLabyr", new[] { "Id" });
            DropTable("dbo.Abt");
            DropTable("dbo.Labyr");
            DropTable("dbo.ResultLabyr");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ResultLabyr",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Time = c.Int(nullable: false),
                        Fault = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Labyr",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Trace = c.Boolean(nullable: false),
                        PresentationTime = c.Int(nullable: false),
                        StartPoint = c.Boolean(nullable: false),
                        EndPoint = c.Boolean(nullable: false),
                        Columns = c.Int(nullable: false),
                        Rows = c.Int(nullable: false),
                        GapHorizontal = c.Int(nullable: false),
                        GapVertical = c.Int(nullable: false),
                        Radius = c.Int(nullable: false),
                        Criterion = c.Int(nullable: false),
                        MaxTrials = c.Int(nullable: false),
                        MaxTime = c.Int(nullable: false),
                        Series = c.String(),
                        Instruction = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Abt",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        FontSize = c.Int(nullable: false),
                        PanelWidth = c.Int(nullable: false),
                        PanelHeight = c.Int(nullable: false),
                        VerticalGap = c.Int(nullable: false),
                        TimeToExercise = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ResultLabyr", "Id");
            CreateIndex("dbo.Labyr", "Id");
            CreateIndex("dbo.Abt", "Id");
            AddForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult", "Id");
            AddForeignKey("dbo.Labyr", "Id", "dbo.Exercise", "Id");
            AddForeignKey("dbo.Abt", "Id", "dbo.Exercise", "Id");
        }
    }
}
