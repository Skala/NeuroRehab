namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rename_moduletype_to_exerciseType : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ModuleType", newName: "ExerciseType");
            RenameColumn(table: "dbo.Exercise", name: "ModuleTypeId", newName: "ExerciseTypeId");
            RenameIndex(table: "dbo.Exercise", name: "IX_ModuleTypeId", newName: "IX_ExerciseTypeId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Exercise", name: "IX_ExerciseTypeId", newName: "IX_ModuleTypeId");
            RenameColumn(table: "dbo.Exercise", name: "ExerciseTypeId", newName: "ModuleTypeId");
            RenameTable(name: "dbo.ExerciseType", newName: "ModuleType");
        }
    }
}
