namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moduleTableRename : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ModuleType", newName: "Module");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Module", newName: "ModuleType");
        }
    }
}
