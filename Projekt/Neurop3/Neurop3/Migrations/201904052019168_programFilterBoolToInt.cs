namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class programFilterBoolToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProgramListFilter", "IsActive", c => c.Int(nullable: false));
            AlterColumn("dbo.ProgramListFilter", "IsPublic", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProgramListFilter", "IsPublic", c => c.Boolean(nullable: false));
            AlterColumn("dbo.ProgramListFilter", "IsActive", c => c.Boolean(nullable: false));
        }
    }
}
