namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patientDataDiagAndReportAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MedicalReport",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Report = c.String(nullable: false, maxLength: 2048),
                        PatientDataId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PatientData", t => t.PatientDataId, cascadeDelete: true)
                .Index(t => t.PatientDataId);
            
            CreateTable(
                "dbo.PatientDiagnosis",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(nullable: false, maxLength: 1024),
                        TimeUtc = c.DateTime(nullable: false),
                        PatientDataId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PatientData", t => t.PatientDataId, cascadeDelete: true)
                .Index(t => t.PatientDataId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatientDiagnosis", "PatientDataId", "dbo.PatientData");
            DropForeignKey("dbo.MedicalReport", "PatientDataId", "dbo.PatientData");
            DropIndex("dbo.PatientDiagnosis", new[] { "PatientDataId" });
            DropIndex("dbo.MedicalReport", new[] { "PatientDataId" });
            DropTable("dbo.PatientDiagnosis");
            DropTable("dbo.MedicalReport");
        }
    }
}
