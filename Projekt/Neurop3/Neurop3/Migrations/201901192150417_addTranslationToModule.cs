namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTranslationToModule : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.ModuleRecipe", "RowNameId");
            CreateIndex("dbo.ModuleRecipe", "RowDescriptionId");
            CreateIndex("dbo.ModuleResult", "RowNameId");
            CreateIndex("dbo.ModuleResult", "RowDescriptionId");
            AddForeignKey("dbo.ModuleResult", "RowDescriptionId", "dbo.Translation", "Id", cascadeDelete: false);
            AddForeignKey("dbo.ModuleResult", "RowNameId", "dbo.Translation", "Id", cascadeDelete: false);
            AddForeignKey("dbo.ModuleRecipe", "RowDescriptionId", "dbo.Translation", "Id", cascadeDelete: false);
            AddForeignKey("dbo.ModuleRecipe", "RowNameId", "dbo.Translation", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ModuleRecipe", "RowNameId", "dbo.Translation");
            DropForeignKey("dbo.ModuleRecipe", "RowDescriptionId", "dbo.Translation");
            DropForeignKey("dbo.ModuleResult", "RowNameId", "dbo.Translation");
            DropForeignKey("dbo.ModuleResult", "RowDescriptionId", "dbo.Translation");
            DropIndex("dbo.ModuleResult", new[] { "RowDescriptionId" });
            DropIndex("dbo.ModuleResult", new[] { "RowNameId" });
            DropIndex("dbo.ModuleRecipe", new[] { "RowDescriptionId" });
            DropIndex("dbo.ModuleRecipe", new[] { "RowNameId" });
        }
    }
}
