namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTranslation : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ExerciseType", newName: "ModuleType");
            CreateTable(
                "dbo.Translation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cz = c.String(),
                        En = c.String(),
                        De = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Translation");
            RenameTable(name: "dbo.ModuleType", newName: "ExerciseType");
        }
    }
}
