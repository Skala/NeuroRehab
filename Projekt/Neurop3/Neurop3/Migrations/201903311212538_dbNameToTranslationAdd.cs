namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbNameToTranslationAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("[dbo].[Translation]", "[DbColumnName]", c => c.String(nullable: false, maxLength: 100, defaultValue: "neuvedeno"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Translation", "DbColumnName");
        }
    }
}
