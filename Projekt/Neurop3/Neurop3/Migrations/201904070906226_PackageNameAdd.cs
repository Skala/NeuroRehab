namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PackageNameAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExercisesPackage", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExercisesPackage", "Name");
        }
    }
}
