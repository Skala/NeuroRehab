namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exercisePackageAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExercisesPackage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StatusId = c.Int(nullable: false),
                        CreateTimeUtc = c.DateTime(nullable: false),
                        LastAccessTimeUtc = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Exercise_AssignPackage",
                c => new
                    {
                        ExerciseAssignId = c.Int(nullable: false),
                        ExercisePackageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ExerciseAssignId, t.ExercisePackageId })
                .ForeignKey("dbo.ExerciseAssign", t => t.ExerciseAssignId, cascadeDelete: true)
                .ForeignKey("dbo.ExercisesPackage", t => t.ExercisePackageId, cascadeDelete: true)
                .Index(t => t.ExerciseAssignId)
                .Index(t => t.ExercisePackageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Exercise_AssignPackage", "ExercisePackageId", "dbo.ExercisesPackage");
            DropForeignKey("dbo.Exercise_AssignPackage", "ExerciseAssignId", "dbo.ExerciseAssign");
            DropIndex("dbo.Exercise_AssignPackage", new[] { "ExercisePackageId" });
            DropIndex("dbo.Exercise_AssignPackage", new[] { "ExerciseAssignId" });
            DropTable("dbo.Exercise_AssignPackage");
            DropTable("dbo.ExercisesPackage");
        }
    }
}
