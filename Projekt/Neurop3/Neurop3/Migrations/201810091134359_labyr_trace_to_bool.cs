namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class labyr_trace_to_bool : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Labyr", "Trace", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Labyr", "Trace", c => c.Int(nullable: false));
        }
    }
}
