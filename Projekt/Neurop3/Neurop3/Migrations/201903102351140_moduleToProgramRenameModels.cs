namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moduleToProgramRenameModels : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Exercise", name: "ModuleId", newName: "ProgramId");
            RenameColumn(table: "dbo.ProgramRecipe", name: "ModuleId", newName: "ProgramId");
            RenameColumn(table: "dbo.ProgramResult", name: "ModuleId", newName: "ProgramId");
            RenameIndex(table: "dbo.Exercise", name: "IX_ModuleId", newName: "IX_ProgramId");
            RenameIndex(table: "dbo.ProgramRecipe", name: "IX_ModuleId", newName: "IX_ProgramId");
            RenameIndex(table: "dbo.ProgramResult", name: "IX_ModuleId", newName: "IX_ProgramId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ProgramResult", name: "IX_ProgramId", newName: "IX_ModuleId");
            RenameIndex(table: "dbo.ProgramRecipe", name: "IX_ProgramId", newName: "IX_ModuleId");
            RenameIndex(table: "dbo.Exercise", name: "IX_ProgramId", newName: "IX_ModuleId");
            RenameColumn(table: "dbo.ProgramResult", name: "ProgramId", newName: "ModuleId");
            RenameColumn(table: "dbo.ProgramRecipe", name: "ProgramId", newName: "ModuleId");
            RenameColumn(table: "dbo.Exercise", name: "ProgramId", newName: "ModuleId");
        }
    }
}
