namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeModuleValue : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ModuleValue", "ModuleRecipeValueID", "dbo.ModuleRecipe");
            DropIndex("dbo.ModuleValue", new[] { "ModuleRecipeValueID" });
            AddColumn("dbo.ModuleRecipe", "Value", c => c.String(maxLength: 256));
            DropColumn("dbo.ModuleRecipe", "ValueId");
            DropTable("dbo.ModuleValue");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ModuleValue",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModuleRecipeValueID = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ModuleRecipe", "ValueId", c => c.Int(nullable: false));
            DropColumn("dbo.ModuleRecipe", "Value");
            CreateIndex("dbo.ModuleValue", "ModuleRecipeValueID");
            AddForeignKey("dbo.ModuleValue", "ModuleRecipeValueID", "dbo.ModuleRecipe", "Id", cascadeDelete: true);
        }
    }
}
