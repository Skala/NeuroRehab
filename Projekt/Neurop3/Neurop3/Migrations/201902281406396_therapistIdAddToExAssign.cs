namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class therapistIdAddToExAssign : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseAssign", "TherapistId", c => c.String(maxLength: 128));
            CreateIndex("dbo.ExerciseAssign", "TherapistId");
            AddForeignKey("dbo.ExerciseAssign", "TherapistId", "dbo.Users", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExerciseAssign", "TherapistId", "dbo.Users");
            DropIndex("dbo.ExerciseAssign", new[] { "TherapistId" });
            DropColumn("dbo.ExerciseAssign", "TherapistId");
        }
    }
}
