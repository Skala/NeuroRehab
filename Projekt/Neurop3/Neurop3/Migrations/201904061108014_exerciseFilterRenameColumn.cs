namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exerciseFilterRenameColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseListFilter", "ExerciseIds", c => c.String());
            DropColumn("dbo.ExerciseListFilter", "ProgramIds");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExerciseListFilter", "ProgramIds", c => c.String());
            DropColumn("dbo.ExerciseListFilter", "ExerciseIds");
        }
    }
}
