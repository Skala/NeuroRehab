namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class loginLogAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoginLog",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.String(nullable: false),
                        TimeUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LoginLog");
        }
    }
}
