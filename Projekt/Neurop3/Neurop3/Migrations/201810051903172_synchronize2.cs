namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class synchronize2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ExerciseType", "DefaultContent");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExerciseType", "DefaultContent", c => c.String());
        }
    }
}
