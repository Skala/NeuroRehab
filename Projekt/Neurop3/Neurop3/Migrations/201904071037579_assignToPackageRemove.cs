namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class assignToPackageRemove : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Exercise_AssignPackage", "ExerciseAssignId", "dbo.ExerciseAssign");
            DropForeignKey("dbo.Exercise_AssignPackage", "ExercisePackageId", "dbo.ExercisesPackage");
            DropIndex("dbo.Exercise_AssignPackage", new[] { "ExerciseAssignId" });
            DropIndex("dbo.Exercise_AssignPackage", new[] { "ExercisePackageId" });
            DropTable("dbo.Exercise_AssignPackage");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Exercise_AssignPackage",
                c => new
                    {
                        ExerciseAssignId = c.Int(nullable: false),
                        ExercisePackageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ExerciseAssignId, t.ExercisePackageId });
            
            CreateIndex("dbo.Exercise_AssignPackage", "ExercisePackageId");
            CreateIndex("dbo.Exercise_AssignPackage", "ExerciseAssignId");
            AddForeignKey("dbo.Exercise_AssignPackage", "ExercisePackageId", "dbo.ExercisesPackage", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Exercise_AssignPackage", "ExerciseAssignId", "dbo.ExerciseAssign", "Id", cascadeDelete: true);
        }
    }
}
