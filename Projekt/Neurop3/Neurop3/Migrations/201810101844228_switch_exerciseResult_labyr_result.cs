namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class switch_exerciseResult_labyr_result : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ResultLabyr", "ExerciseAssignId", "dbo.ExerciseAssign");
            AddForeignKey("dbo.ResultLabyr", "ExerciseAssignId", "dbo.ExerciseAssign", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResultLabyr", "ExerciseAssignId", "dbo.ExerciseAssign");
            AddForeignKey("dbo.ResultLabyr", "ExerciseAssignId", "dbo.ExerciseAssign", "Id", cascadeDelete: true);
        }
    }
}
