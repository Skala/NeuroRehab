namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAbt : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Abt",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        FontSize = c.Int(nullable: false),
                        PanelWidth = c.Int(nullable: false),
                        PanelHeight = c.Int(nullable: false),
                        VerticalGap = c.Int(nullable: false),
                        TimeToExercise = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exercise", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Abt", "Id", "dbo.Exercise");
            DropIndex("dbo.Abt", new[] { "Id" });
            DropTable("dbo.Abt");
        }
    }
}
