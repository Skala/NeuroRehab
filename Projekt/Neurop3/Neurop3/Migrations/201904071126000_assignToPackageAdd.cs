namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class assignToPackageAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Exercise_AssignPackage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExerciseAssignId = c.Int(nullable: false),
                        ExercisePackageId = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExerciseAssign", t => t.ExerciseAssignId)
                .ForeignKey("dbo.ExercisesPackage", t => t.ExercisePackageId, cascadeDelete: true)
                .Index(t => t.ExerciseAssignId)
                .Index(t => t.ExercisePackageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Exercise_AssignPackage", "ExercisePackageId", "dbo.ExercisesPackage");
            DropForeignKey("dbo.Exercise_AssignPackage", "ExerciseAssignId", "dbo.ExerciseAssign");
            DropIndex("dbo.Exercise_AssignPackage", new[] { "ExercisePackageId" });
            DropIndex("dbo.Exercise_AssignPackage", new[] { "ExerciseAssignId" });
            DropTable("dbo.Exercise_AssignPackage");
        }
    }
}
