namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class packageFilterUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PackageListFilter", "PatientName", c => c.String());
            AddColumn("dbo.PackageListFilter", "PackageName", c => c.String());
            DropColumn("dbo.PackageListFilter", "Text");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PackageListFilter", "Text", c => c.String());
            DropColumn("dbo.PackageListFilter", "PackageName");
            DropColumn("dbo.PackageListFilter", "PatientName");
        }
    }
}
