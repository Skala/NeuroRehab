namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ErrorLogAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ErrorLog",
                c => new
                    {
                        ErrorId = c.Guid(nullable: false),
                        Source = c.String(nullable: false, maxLength: 60),
                        Type = c.String(nullable: false, maxLength: 100),
                        Message = c.String(nullable: false, maxLength: 500),
                        User = c.String(nullable: false, maxLength: 50),
                        HResult = c.Int(nullable: false),
                        TimeUtc = c.DateTime(nullable: false),
                        StackTrace = c.String(nullable: false),
                        Stringify = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ErrorId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ErrorLog");
        }
    }
}
