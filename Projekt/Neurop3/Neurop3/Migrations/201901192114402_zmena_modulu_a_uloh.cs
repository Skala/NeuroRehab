namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zmena_modulu_a_uloh : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ExerciseResult", new[] { "ExerciseAssignId" });
            RenameColumn(table: "dbo.Exercise", name: "ExerciseTypeId", newName: "ModuleTypeId");
            RenameIndex(table: "dbo.Exercise", name: "IX_ExerciseTypeId", newName: "IX_ModuleTypeId");
            DropPrimaryKey("dbo.ExerciseResult");
            CreateTable(
                "dbo.ExerciseResultData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExerciseResultId = c.Int(nullable: false),
                        Row = c.Int(nullable: false),
                        Text = c.String(),
                        Number = c.Int(nullable: false),
                        LogicalValue = c.Boolean(nullable: false),
                        Single = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExerciseResult", t => t.ExerciseResultId, cascadeDelete: true)
                .Index(t => t.ExerciseResultId);
            
            CreateTable(
                "dbo.ExerciseData",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExerciseDataId = c.Int(nullable: false),
                        Row = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        LogicalValue = c.Boolean(nullable: false),
                        Text = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Exercise", t => t.ExerciseDataId, cascadeDelete: true)
                .Index(t => t.ExerciseDataId);
            
            CreateTable(
                "dbo.ModuleRecipe",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModuleTypeId = c.Int(nullable: false),
                        RowNumber = c.Int(nullable: false),
                        RowNameId = c.Int(nullable: false),
                        RowDescriptionId = c.Int(nullable: false),
                        DataTypeId = c.Int(nullable: false),
                        ValueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ModuleType", t => t.ModuleTypeId, cascadeDelete: true)
                .Index(t => t.ModuleTypeId);
            
            CreateTable(
                "dbo.ModuleValue",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModuleRecipeValueID = c.Int(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ModuleRecipe", t => t.ModuleRecipeValueID, cascadeDelete: true)
                .Index(t => t.ModuleRecipeValueID);
            
            CreateTable(
                "dbo.ModuleResult",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModuleTypeId = c.Int(nullable: false),
                        RowNumber = c.Int(nullable: false),
                        RowNameId = c.Int(nullable: false),
                        RowDescriptionId = c.Int(nullable: false),
                        DataTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ModuleType", t => t.ModuleTypeId, cascadeDelete: true)
                .Index(t => t.ModuleTypeId);
            
            AddColumn("dbo.Exercise", "EditDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExerciseResult", "ResultRawData", c => c.String(nullable: false));
            AlterColumn("dbo.ExerciseResult", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.ExerciseResult", "ExerciseAssignId", c => c.Int(nullable: false));
            AlterColumn("dbo.ModuleType", "Name", c => c.String(nullable: false, maxLength: 100));
            AddPrimaryKey("dbo.ExerciseResult", "Id");
            CreateIndex("dbo.ExerciseResult", "ExerciseAssignId");
            DropColumn("dbo.Exercise", "CheckDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Exercise", "CheckDate", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.ModuleResult", "ModuleTypeId", "dbo.ModuleType");
            DropForeignKey("dbo.ModuleValue", "ModuleRecipeValueID", "dbo.ModuleRecipe");
            DropForeignKey("dbo.ModuleRecipe", "ModuleTypeId", "dbo.ModuleType");
            DropForeignKey("dbo.ExerciseData", "ExerciseDataId", "dbo.Exercise");
            DropForeignKey("dbo.ExerciseResultData", "ExerciseResultId", "dbo.ExerciseResult");
            DropIndex("dbo.ModuleResult", new[] { "ModuleTypeId" });
            DropIndex("dbo.ModuleValue", new[] { "ModuleRecipeValueID" });
            DropIndex("dbo.ModuleRecipe", new[] { "ModuleTypeId" });
            DropIndex("dbo.ExerciseData", new[] { "ExerciseDataId" });
            DropIndex("dbo.ExerciseResultData", new[] { "ExerciseResultId" });
            DropIndex("dbo.ExerciseResult", new[] { "ExerciseAssignId" });
            DropPrimaryKey("dbo.ExerciseResult");
            AlterColumn("dbo.ModuleType", "Name", c => c.String(maxLength: 100));
            AlterColumn("dbo.ExerciseResult", "ExerciseAssignId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.ExerciseResult", "Id", c => c.Int(nullable: false));
            DropColumn("dbo.ExerciseResult", "ResultRawData");
            DropColumn("dbo.Exercise", "EditDate");
            DropTable("dbo.ModuleResult");
            DropTable("dbo.ModuleValue");
            DropTable("dbo.ModuleRecipe");
            DropTable("dbo.ExerciseData");
            DropTable("dbo.ExerciseResultData");
            AddPrimaryKey("dbo.ExerciseResult", "Id");
            RenameIndex(table: "dbo.Exercise", name: "IX_ModuleTypeId", newName: "IX_ExerciseTypeId");
            RenameColumn(table: "dbo.Exercise", name: "ModuleTypeId", newName: "ExerciseTypeId");
            CreateIndex("dbo.ExerciseResult", "ExerciseAssignId");
        }
    }
}
