namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exerciseFilterBoolToInts : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ExerciseListFilter", "IsActive", c => c.Int(nullable: false));
            AlterColumn("dbo.ExerciseListFilter", "IsPublic", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ExerciseListFilter", "IsPublic", c => c.Boolean(nullable: false));
            AlterColumn("dbo.ExerciseListFilter", "IsActive", c => c.Boolean(nullable: false));
        }
    }
}
