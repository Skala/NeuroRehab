namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_exerciseResult : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ResultLabyr");
            CreateTable(
                "dbo.ExerciseResult",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExerciseAssignId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExerciseAssign", t => t.ExerciseAssignId, cascadeDelete: true)
                .Index(t => t.ExerciseAssignId);
            
            AlterColumn("dbo.ResultLabyr", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.ResultLabyr", "Id");
            CreateIndex("dbo.ResultLabyr", "Id");
            AddForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResultLabyr", "Id", "dbo.ExerciseResult");
            DropForeignKey("dbo.ExerciseResult", "ExerciseAssignId", "dbo.ExerciseAssign");
            DropIndex("dbo.ResultLabyr", new[] { "Id" });
            DropIndex("dbo.ExerciseResult", new[] { "ExerciseAssignId" });
            DropPrimaryKey("dbo.ResultLabyr");
            AlterColumn("dbo.ResultLabyr", "Id", c => c.Int(nullable: false, identity: true));
            DropTable("dbo.ExerciseResult");
            AddPrimaryKey("dbo.ResultLabyr", "Id");
        }
    }
}
