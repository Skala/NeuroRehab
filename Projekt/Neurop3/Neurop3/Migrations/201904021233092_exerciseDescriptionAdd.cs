namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exerciseDescriptionAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Exercise", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Exercise", "Description");
        }
    }
}
