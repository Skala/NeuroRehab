namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sequenceToExResultAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExerciseResult", "Sequence", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExerciseResult", "Sequence");
        }
    }
}
