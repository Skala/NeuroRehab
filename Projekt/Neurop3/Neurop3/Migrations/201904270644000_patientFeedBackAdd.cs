namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patientFeedBackAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientFeedback",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Difficulty = c.Int(nullable: false),
                        ExerciseAmount = c.Int(nullable: false),
                        Note = c.String(nullable: false, maxLength: 1024),
                        PackageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Package", t => t.PackageId, cascadeDelete: true)
                .Index(t => t.PackageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatientFeedback", "PackageId", "dbo.Package");
            DropIndex("dbo.PatientFeedback", new[] { "PackageId" });
            DropTable("dbo.PatientFeedback");
        }
    }
}
