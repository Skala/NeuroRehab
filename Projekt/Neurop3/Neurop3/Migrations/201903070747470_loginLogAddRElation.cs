namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class loginLogAddRElation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LoginLog", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.LoginLog", "UserId");
            AddForeignKey("dbo.LoginLog", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoginLog", "UserId", "dbo.Users");
            DropIndex("dbo.LoginLog", new[] { "UserId" });
            AlterColumn("dbo.LoginLog", "UserId", c => c.String(nullable: false));
        }
    }
}
