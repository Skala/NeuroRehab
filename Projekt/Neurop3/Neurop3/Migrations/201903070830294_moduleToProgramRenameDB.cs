namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class moduleToProgramRenameDB : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Module", newName: "Program");
            RenameTable(name: "dbo.ModuleRecipe", newName: "ProgramRecipe");
            RenameTable(name: "dbo.ModuleResult", newName: "ProgramResult");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ProgramResult", newName: "ModuleResult");
            RenameTable(name: "dbo.ProgramRecipe", newName: "ModuleRecipe");
            RenameTable(name: "dbo.Program", newName: "Module");
        }
    }
}
