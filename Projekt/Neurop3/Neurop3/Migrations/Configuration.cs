namespace Neurop3.Migrations
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using Neurop3.Models;
    using Neurop3.Common;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Neurop3.Models.Administration;
    using Neurop3.Models.Programs;

    internal sealed class Configuration : DbMigrationsConfiguration<Neurop3.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Neurop3.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Roles.AddOrUpdate(
                new IdentityRole() { Id = ((int)RoleEnum.Superadmin).ToString(), Name = RoleEnum.Superadmin.ToString()},
                new IdentityRole() { Id = ((int)RoleEnum.Admin).ToString() , Name = RoleEnum.Admin.ToString() },
                new IdentityRole() { Id = ((int)RoleEnum.Super_therapist).ToString(), Name = RoleEnum.Super_therapist.ToString()},
                new IdentityRole() { Id = ((int)RoleEnum.Therapist).ToString(), Name = RoleEnum.Therapist.ToString() },
                new IdentityRole() { Id = ((int)RoleEnum.Patient).ToString(), Name = RoleEnum.Patient.ToString() },
                new IdentityRole() { Id = ((int)RoleEnum.Unassigned).ToString(), Name = RoleEnum.Unassigned.ToString(),
            });

            context.Status.AddOrUpdate(
                new Status() { Id = (int)ExerciseStatusEnum.Novy, Name = ExerciseStatusEnum.Novy.ToString()  },
                new Status() { Id = (int)ExerciseStatusEnum.Probihajici, Name = ExerciseStatusEnum.Probihajici.ToString() },
                new Status() { Id = (int)ExerciseStatusEnum.Dokoncena, Name = ExerciseStatusEnum.Dokoncena.ToString() },
                new Status() { Id = (int)ExerciseStatusEnum.Zkontrolovana, Name = ExerciseStatusEnum.Zkontrolovana.ToString() },
                new Status() { Id = (int)ExerciseStatusEnum.Uzavrena, Name = ExerciseStatusEnum.Uzavrena.ToString() }
            );

            context.Translation.AddOrUpdate(
                new Translation() { Id = 1, Cz = "Neuvedeno", En = "Unknown", De = "unbekannt", DbColumnName = "neuvedeno" }
            );
        }
    }
}
