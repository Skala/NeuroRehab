namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class exerciseAssignPackageRename : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Exercise_AssignPackage", newName: "ExerciseAssignPackage");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ExerciseAssignPackage", newName: "Exercise_AssignPackage");
        }
    }
}
