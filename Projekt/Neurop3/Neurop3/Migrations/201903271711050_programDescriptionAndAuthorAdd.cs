namespace Neurop3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class programDescriptionAndAuthorAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Program", "Description", c => c.String(nullable: false, maxLength: 1024));
            Sql("UPDATE [dbo].[Program] SET Description = 'Neuvedeno'");
            AddColumn("dbo.Program", "AuthorId", c => c.String(nullable: false, maxLength: 128));
            Sql("UPDATE [dbo].[Program] SET AuthorId = '1c6c2f14-72ee-4862-9f20-e07a6405164d'");
            CreateIndex("dbo.Program", "AuthorId");
            AddForeignKey("dbo.Program", "AuthorId", "dbo.Users", "Id");

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Program", "AuthorId", "dbo.Users");
            DropIndex("dbo.Program", new[] { "AuthorId" });
            DropColumn("dbo.Program", "AuthorId");
            DropColumn("dbo.Program", "Description");
        }
    }
}
