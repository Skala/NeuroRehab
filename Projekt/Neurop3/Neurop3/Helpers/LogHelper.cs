﻿using Microsoft.AspNet.Identity;
using Neurop3.Models.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;


namespace Neurop3.Helpers
{
    public enum LogTarget
    {
        File, Database, EventLog
    }

    public abstract class LogBase
    {
        public abstract void Log(Exception e);
    }

    //public class FileLogger : LogBase
    //{
    //    public string filePath = @”D:\IDGLog.txt”;
    //    public override void Log(string message)
    //    {
    //        using (StreamWriter streamWriter = new StreamWriter(filePath))
    //        {
    //            streamWriter.WriteLine(message);
    //            streamWriter.Close();
    //        }
    //    }
    //}

    //public class EventLogger : LogBase
    //{
    //    public override void Log(string message)
    //    {
    //        EventLog eventLog = new EventLog(“”);
    //        eventLog.Source = "IDGEventLog";
    //        eventLog.WriteEntry(message);
    //    }
    //}

    public class DBLogger : LogBase
    {
        public override void Log(Exception e)
        {
            string user;
            try
            {
                user = HttpContext.Current.User.Identity.GetUserName();
            }
            catch(Exception npe)
            {
                user = "Unsigned";
            }

            var log = new ErrorLog()
            {
                ErrorId = Guid.NewGuid(),
                Source = e.Source.Replace("\'", "\""),
                Message = e.Message.Replace("\'", "\""),
                StackTrace = e.StackTrace.Replace("\'", "\""),
                HResult = e.HResult,
                Stringify = e.ToString().Replace("\'", "\""),
                Type = e.GetType().ToString().Replace("\'", "\""),
                //TimeUtc = DateTime.Now, //pouziji funkci databaze
                User = user
            };

            log.Source = log.Source.Length > 60 ? log.Source.Substring(0, 60) : log.Source;
            log.Type = log.Type.Length > 100 ? log.Type.Substring(0, 100) : log.Type;
            log.Message = log.Message.Length > 500 ? log.Message.Substring(0, 500) : log.Message;
            log.User = log.User.Length > 50 ? log.User.Substring(0, 50) : log.User;
            log.Source = log.Source.Length > 60 ? log.Source.Substring(0, 60) : log.Source;
            
            var query = "INSERT INTO [dbo].[ErrorLog] " +
                "([ErrorId], [Source], [Type], [Message], [User], [HResult], [TimeUtc], [StackTrace], [Stringify])" +
                " VALUES" +
                string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', {5}, {6}, '{7}', '{8}')",
                    log.ErrorId, 
                    @log.Source, 
                    @log.Type, 
                    @log.Message, 
                    @log.User, 
                    log.HResult, 
                    "SYSUTCDATETIME()", 
                    @log.StackTrace, 
                    @log.Stringify
                );
            Neurop3.Common.Utils.RunSqlQuery(query);
        }
    }


    public static class LogHelper
    {
        private static LogBase logger = null;
        public static void Log(LogTarget target, Exception e)
        {
            switch (target)
            {
                case LogTarget.File:
                    //logger = new FileLogger();
                    //logger.Log(message);
                    break;
                case LogTarget.Database:
                    logger = new DBLogger();
                    logger.Log(e);
                    break;
                case LogTarget.EventLog:
                    //logger = new EventLogger();
                    //logger.Log(message);
                    break;
                default:
                    return;
            }
        }
    }

}