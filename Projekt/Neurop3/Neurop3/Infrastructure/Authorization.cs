﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Neurop3.Common;

namespace Neurop3.Infrastructure
{
    public static class Authorization
    {
        public static ApplicationUserManager UserManager
        {
            get
            {
               return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }


        public static bool AuthorizeAll(IPrincipal user, RoleEnum [] roles)
        {
            if(user != null & roles.Any())
            {
                foreach (var role in roles)
                {
                    if (!Authorize(user, role))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }


        }
        
        public static bool AuthorizeWithOnly(IPrincipal user, RoleEnum role)
        {
            if(user != null)
            {
                var list = UserManager.GetRoles(user.Identity.GetUserId());
                if(list.ToArray().Length == 1)
                {
                    return list.First().CompareTo(role.ToString()) == 0 ? true : false;
                }
                return false;
            }
            else
            {
                return false;
            }

        }

        public static bool AuthorizeAny(IPrincipal user, RoleEnum[] roles)
        {
            if(user != null)
            {
                foreach (var role in roles)
                {
                    if (Authorize(user, role))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        public static bool Authorize(IPrincipal user, RoleEnum role)
        {
            if(user != null)
            {
                return user.IsInRole(RoleEnum.Superadmin.ToString()) ? true : user.IsInRole(role.ToString());
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Ostatni autorizace prosli pro superadministratora vzdy, tato funkce autorizuje pouze roli v paramteru
        /// </summary>
        public static bool AuthorizeOnly(IPrincipal user, RoleEnum role)
        {
            if (user != null)
            {
                return user.IsInRole(role.ToString());
            }
            else
            {
                return false;
            }
        }
    }
}