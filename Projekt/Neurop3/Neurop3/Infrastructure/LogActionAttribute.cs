﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace Neurop3.Infrastructure
{
    public class LogActionAttribute : ActionFilterAttribute
    {

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //TODO IMPROVEMENT -- vytvorit DB tabulku a nastavit atribut u vsech kontroleru... pote se budou logovat vsechny akce do databaze
            var controller = filterContext.RequestContext.RouteData.Values.ContainsKey("Controller") ? filterContext.RequestContext.RouteData.Values["Controller"].ToString() : null;
            var action = filterContext.RequestContext.RouteData.Values.ContainsKey("Action") ? filterContext.RequestContext.RouteData.Values["Action"].ToString() : null;
            var area = filterContext.RequestContext.RouteData.DataTokens.ContainsKey("Area") ? filterContext.RequestContext.RouteData.DataTokens["Area"].ToString() : null;
            var userId = filterContext.RequestContext.HttpContext.User.Identity.GetUserId();
            var userName = filterContext.RequestContext.HttpContext.User.Identity.GetUserName();

            var query = "INSERT INTO [dbo].[ErrorLog] " +
                "([TimeUtc], [UserName], [UserId], [Controller], [Action], [Area])" +
                " VALUES" +
                string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')",
                    "SYSUTCDATETIME()",
                    userName,
                    userId,
                    controller,
                    action,
                    area
                );
            Common.Utils.RunSqlQuery(query);

            base.OnActionExecuting(filterContext);
        }
    }
}