﻿using Neurop3.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Neurop3.Infrastructure
{
    public class HandleAndLogErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            LogHelper.Log(LogTarget.Database, filterContext.Exception);
            base.OnException(filterContext);
        }
    }
}