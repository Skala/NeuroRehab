﻿using System.Web;
using System.Web.Optimization;

namespace Neurop3
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"
                        ,"~/Scripts/myScripts/DateTimeValidator.js"
                        ));
            
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                "~/Scripts/bootstrap-datepicker.min.js",
                "~/Scripts/locales/bootstrap-datepicker.de.min.js",
                "~/Scripts/locales/bootstrap-datepicker.sk.min.js",
                "~/Scripts/locales/bootstrap-datepicker.en-GB.min.js",
                "~/Scripts/locales/bootstrap-datepicker.cs.min.js"

                ));


            bundles.Add(new ScriptBundle("~/bundles/select2").Include(
                        "~/Scripts/select2.min.js",
                        "~/Scripts/Select2-locales/select2_locale_de.js",
                        "~/Scripts/Select2-locales/select2_locale_sk.js",
                        "~/Scripts/Select2-locales/select2_locale_cs.js"
                        ));

            //bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
            //            "~/Scripts/DataTable/datatables.min.js"));



            //bundles.Add(new StyleBundle("~/Content/DataTable").Include(
            //        "~/Content/DataTable/datatables.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/flashmessages/jquery.flashmessages.css",
                      "~/Content/css/select2.css",
                      //"~/Content/bootstrap-datepicker3.min.css",
                      "~/Content/bootstrap-datepicker.min.css",
                      "~/Content/site.css"
                      ));
        }
    }
}
