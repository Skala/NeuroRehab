﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Neurop3;
using Neurop3.Controllers;
using Neurop3.Services;
using Microsoft.AspNet.Identity;

namespace Neurop3.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        public static HomeController GetController()
        {
            var _es = DependencyResolver.Current.GetService<IExerciseService>();
            var _hs = DependencyResolver.Current.GetService<IHomeService>();

            //var contextMock = new Mock<ControllerContext>();
            //var httpContextMock = new Mock<HttpContextBase>();
            //var lWindowsIdentity = new WindowsIdentity("Administrator");
            //httpContextMock.Setup(x => x.User).Returns(new WindowsPrincipal(lWindowsIdentity));
            //contextMock.Setup(ctx => ctx.HttpContext).Returns(httpContextMock.Object);
            //myController.ControllerContext = contextMock.Object;

            var context = new Mock<HttpContextBase>();
            var mockIdentity = new Mock<IIdentity>();
            mockIdentity.Setup(x => x.Name).Returns("skala.neurop.sa@gmail.com");
            mockIdentity.Setup(x => x.GetUserId()).Returns("1c6c2f14-72ee-4862-9f20-e07a6405164d");
            context.SetupGet(x => x.User.Identity).Returns(mockIdentity.Object);

            HomeController ctrl = new HomeController(_es, _hs) { };
            return ctrl;
        }


        [TestMethod]
        public void Errors()
        {
            // Arrange
            var ctrl = GetController();

            // Act
            ViewResult result = ctrl.Errors() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
