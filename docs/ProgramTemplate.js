﻿//Funkce pro spusteni ulohy
function runExercise(param) {
    //Kod pro ziskani vstupnich parametru z textoveho retezce (param)
    //Kod pro inicializaci a spusteni ulohy
    showModalAfter() //Funkce, ktera se vola po dokonceni ulohy pro prechod na dalsi ulohu. Tato funkce vola funkci finish().
}

//Funkce, ktera provede odeslani vysledku na server nastavenim konkretnich elementu (Finished, OutpuParameters) potvrzenim formulare s daty.
function finish() {
    var resultString = ""; //Vystupni retezec, ktery bude na serveru zpracovan a ulozen
    var items = 0; //Pocet spusteni dane ulohy (pocita se s jednim spustenim)
    
    for (var i = 0; i < mResults.length; i++) {
        resultString += mResults[i].fault;
        resultString += "##"; //oddelovac parametru v ramci ulohy
        resultString +=  mResults[i].time; 
        resultString += ";;"; //oddelovac vysledku v ramci vicero spusteni ulohy (pocita se s jednim spustenim)
		item ++;
    }
    if(item != 0){
        document.getElementById("Finished").value = true; //Pokud existuji vystupni parametry, pak je na serveru ulozime.
    }
    document.getElementById("OutputParameters").value = resultString; //Vystup z programu	
    document.getElementById("ExerciseSubmit").submit(); //Odeslani vysledku na server
}